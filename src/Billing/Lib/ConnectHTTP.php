<?php
namespace Billing\Lib;

class ConnectHTTP
{
    public static function connect($uri, $arr, $method = 'POST')
    {
        $ci = curl_init($uri);
        curl_setopt($ci, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ci, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ci, CURLOPT_POSTFIELDS, json_encode($arr));
        curl_setopt(
            $ci,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen(json_encode($arr)),
            )
        );
        curl_setopt($ci, CURLOPT_RETURNTRANSFER, 1);
        return json_decode(curl_exec($ci), true);
    }
}
