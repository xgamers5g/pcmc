<?php
namespace Billing\Lib;

use Billing\Model\CategoryModel;
use Billing\Model\ListModel;
use Billing\Model\Package;

class LCCUD
{
    private static $pkg = null;

    protected function __construct(Package $pkg)
    {
        self::$pkg = $pkg;
    }

    public static function load(Package $pkg)
    {
        return new self($pkg);
    }

    public function createList()
    {
        $type = CheckInput::checkId(self::$pkg->getData('Category'));
        $type = $type && CategoryModel::has(self::$pkg->getData('Category'));

        if ($type === false) {
            return self::$pkg;
        }

        $type = CheckInput::checkStr(self::$pkg->getData('ListName')) && $type;
        $type = CheckInput::checkId(self::$pkg->getData('Amount')) && $type;

        if (self::$pkg->getData('ListDate') === null || self::$pkg->getData('ListDate') === false) {
            $date = new \DateTime('now');
            self::$pkg->setData('ListDate', $date->format('Y-m-d H:i:s'));
        } else {
            $type = CheckInput::checkDate(self::$pkg->getData('ListDate')) && $type;
        }

        if ($type === true && self::$pkg->getAct() === 'List_C') {
            ListModel::create(self::$pkg);
        } else {
            self::$pkg->setAct('List_C_L_Err');
        }
        return self::$pkg;
    }

    public function updateList()
    {
        $type = CheckInput::checkId(self::$pkg->getData('ListID')) && ListModel::has(self::$pkg->getData('ListID'));
        $type = CategoryModel::has(self::$pkg->getData('Category')) && $type;

        if ($type === false) {
            return self::$pkg;
        }

        $type = CheckInput::checkStr(self::$pkg->getData('ListName')) && $type;
        $type = CheckInput::checkId(self::$pkg->getData('Amount')) && $type;
        $type = CheckInput::checkDate(self::$pkg->getData('ListDate')) && $type;

        if ($type === true && self::$pkg->getAct() === 'List_U') {
            ListModel::load(self::$pkg)->update();
        } else {
            self::$pkg->setAct('List_U_L_Err');
        }
        return self::$pkg;
    }

    public function deleteList()
    {
        if (CheckInput::checkId(self::$pkg->getData('ListID')) && ListModel::has(self::$pkg->getData('ListID'))) {
            ListModel::load(self::$pkg)->delete();
        } else {
            self::$pkg->setAct('List_D_L_Err');
        }
        return self::$pkg;
    }

    public function createCategory()
    {
        $type = CheckInput::checkStr(self::$pkg->getData('CategoryName'));

        if (self::$pkg->has('Payment')) {
            $type = CheckInput::checkBool(self::$pkg->getData('Payment')) && $type;
        } else {
            self::$pkg->setAct('Category_C_L_Err');
            return self::$pkg;
        }

        if ($type === true && self::$pkg->getAct() === 'Category_C') {
            CategoryModel::create(self::$pkg);
        } else {
            self::$pkg->setAct('Category_C_L_Err');
        }
        return self::$pkg;
    }

    public function updateCategory()
    {
        $type = CheckInput::checkId(self::$pkg->getData('Category'));
        $type = $type && CategoryModel::has(self::$pkg->getData('Category'));

        if ($type === false) {
            return self::$pkg;
        }

        $type = CheckInput::checkStr(self::$pkg->getData('CategoryName'));

        if (self::$pkg->has('Payment')) {
            $type = CheckInput::checkBool(self::$pkg->getData('Payment')) && $type;
        } else {
            self::$pkg->setAct('Category_U_L_Err');
            return self::$pkg;
        }

        if ($type === true && self::$pkg->getAct() === 'Category_U') {
            CategoryModel::load(self::$pkg)->update();
        } else {
            self::$pkg->setAct('Category_U_L_Err');
        }
        return self::$pkg;
    }

    public function deleteCategory()
    {
        if (CheckInput::checkId(self::$pkg->getData('Category'))) {
            if (CategoryModel::has(self::$pkg->getData('Category'))) {
                if (self::$pkg->has('otherCategory') === true) {
                    $category = self::$pkg->getData('Category');
                    $other = self::$pkg->getData('otherCategory');
                    if (CheckInput::checkId($other) && CategoryModel::has($other)) {
                        $this->updateAllList($category, $other);
                    } else {
                        self::$pkg->setAct('Category_D_L_Err');
                    }
                } else {
                    $this->deleteAllList(self::$pkg->getData('Category'));
                }
                $act = self::$pkg->getAct();
                if ($act === 'List_D_Suc' || $act === 'List_U_Suc' || $act === 'Category_D') {
                    self::$pkg->setAct('Category_D');
                    CategoryModel::load(self::$pkg)->delete();
                } else {
                    self::$pkg->setAct('Category_D_L_Err');
                }
            } else {
                self::$pkg->setAct('Category_D_L_Err');
            }
        } else {
            self::$pkg->setAct('Category_D_L_Err');
        }
        return self::$pkg;
    }

    private function deleteAllList($category)
    {
        $larr = Report::getList(self::$pkg)->getData('List');
        foreach ($larr as $list) {
            if ((int)$list['Category'] == $category) {
                self::$pkg->setData('ListID', $list['ListID']);
                self::$pkg->setAct('List_D');
                ListModel::load(self::$pkg)->delete();
            }
        }
        self::$pkg->delData('ListID');
        self::$pkg->delData('List');
        return self::$pkg;
    }

    private function updateAllList($category, $other)
    {
        $larr = Report::getList(self::$pkg)->getData('List');
        foreach ($larr as $list) {
            if ((int)$list['Category'] == $category) {
                self::$pkg->setData('ListID', $list['ListID']);
                self::$pkg->setData('ListName', $list['ListName']);
                self::$pkg->setData('ListDate', $list['ListDate']);
                self::$pkg->setData('Category', $other);
                self::$pkg->setData('Amount', $list['Amount']);
                self::$pkg->setAct('List_U');
                ListModel::load(self::$pkg)->update();
            }
        }
        self::$pkg->delAllData();
        self::$pkg->setData('Category', $category);
        return self::$pkg;
    }

    public static function hasList(Package $pkg)
    {
        if ($pkg->has('ListID') && CheckInput::checkId($pkg->getData('ListID'))) {
            if (ListModel::has($pkg->getData('ListID')) === true) {
                $pkg->setAct('List_H_Suc');
            } else {
                $pkg->setAct('List_H_Err');
            }
        } else {
            $pkg->setAct('List_H_L_Err');
        }
        return $pkg;
    }

    public static function hasCategory(Package $pkg)
    {
        if ($pkg->has('Category') && CheckInput::checkId($pkg->getData('Category'))) {
            if (CategoryModel::has($pkg->getData('Category')) === true) {
                $pkg->setAct('Category_H_Suc');
            } else {
                $pkg->setAct('Category_H_Err');
            }
        } else {
            $pkg->setAct('Category_H_L_Err');
        }
        return $pkg;
    }

    public static function createAtNewAccount(Package $pkg)
    {
        $payCategory = array(
            '早餐', '晚餐', '零食',
            '日常用品', '社交', '午餐',
            '飲料', '交通', '娛樂',
            '衣物', '購物', '禮物',
            '醫療', '投資', '轉帳',
            '房租', '禮金', '電話費',
            '信用卡', '其他'
        );

        $incomeCategory = array(
            '薪水', '獎金', '補助費', '投資', '其他'
        );

        foreach ($payCategory as $value) {
            $pkg->setData('CategoryName', $value);
            $pkg->setData('Payment', false);
            CategoryModel::create($pkg);
            $pkg->setAct('Category_C');
        }

        foreach ($incomeCategory as $value) {
            $pkg->setData('CategoryName', $value);
            $pkg->setData('Payment', true);
            CategoryModel::create($pkg);
            $pkg->setAct('Category_C');
        }

        $pkg->setAct('New_Account_Suc');
        return $pkg;
    }
}
