<?php
namespace Billing\Lib;

use Billing\Model\ListModel;
use Billing\Model\CategoryModel;
use Billing\Model\Package;

class Search
{
    public static function searchList(Package $pkg)
    {
        if (CheckInput::checkStr($pkg->getData('ListName')) === true && $pkg->getAct() === 'List_S') {
            $pkg = ListModel::search($pkg);
            $pkg->delData('Authorization');
        } else {
            $pkg->setAct('List_S_L_Err');
        }
        return $pkg;
    }

    public static function searchCategory(Package $pkg)
    {
        if (CheckInput::checkStr($pkg->getData('CategoryName')) === true && $pkg->getAct() === 'Category_S') {
            $pkg = CategoryModel::search($pkg);
            $pkg->delData('Authorization');
        } else {
            $pkg->setAct('Category_S_L_Err');
        }
        return $pkg;
    }
}
