<?php
namespace Billing\Lib;

class CheckInput
{
    public static function checkId($id)
    {
        if (gettype($id) === 'integer' && $id > 0) {
            $arr = array();
            $pattern = "/(\d){1,11}/";
            preg_match($pattern, $id, $arr);
            if ($id === (int)$arr[0]) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function checkStr($str)
    {
        if (gettype($str) === 'string') {
            $arr = array();
            $pattern = "~[^\x{4e00}-\x{9fa5}_a-zA-Z0-9]+~u";
            preg_match($pattern, $str, $arr);
            return (!isset($arr[0]));
        } else {
            return false;
        }
    }

    public static function checkDate($date)
    {
        try {
            $do = new \DateTime($date);
            return (
                        $date === $do->format('Y-m-d') ||
                        $date === $do->format('Y-m-d H:i:s')
                    ) ? true : false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function checkBool($bool)
    {
        return ($bool === true || $bool === false) ? true : false;
    }
}
