<?php
namespace Billing\Lib;

class WriteRecord
{
    public static function write(\Billing\Model\Package $pkg, array $auth, $client_ip)
    {
        $str = $auth['username'];
        if (substr($pkg->getAct(), -3, 3) === 'Suc') {
            switch ($pkg->getAct()) {
                case 'List_C_Suc':
                    $str = $str.' 新增 ';
                    break;
                case 'List_U_Suc':
                    $str = $str.' 修改 ';
                    break;
                default:
                    return false;
                    break;
            }
            $str = $str.$pkg->getData('ListName').',共 '.$pkg->getData('Amount').' 元';
            $url = SITE_URL."/diary/updateforbilling";
            $date = new \DateTime($pkg->getData('ListDate'));
            $tmp = array(
                'day' => $date->format('Y-m-d'),
                'content' => $str,
                'Authorization' => $auth['Authorization'],
                'clientip' => $client_ip
            );
            // return $tmp;
            return ConnectHTTP::connect($url, $tmp);
        } else {
            return false;
        }
    }
}
