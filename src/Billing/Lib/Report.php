<?php
namespace Billing\Lib;

use Billing\Model\ListModel;
use Billing\Model\CategoryModel;
use Billing\Model\Package;

class Report
{
    public static function getList(Package $pkg)
    {
        $act = $pkg->getAct();
        $pkg->setAct('List_R');
        ListModel::get($pkg);
        if ($pkg->getAct() === 'List_R_Suc') {
            $pkg->setAct($act);
        }
        return $pkg;
    }

    public static function getC(Package $pkg)
    {
        $act = $pkg->getAct();
        $pkg->setAct('Category_R');
        CategoryModel::get($pkg);
        if ($pkg->getAct() === 'Category_R_Suc') {
            $pkg->setAct($act);
        }
        return $pkg;
    }

    public static function getForDate(Package $pkg, $type = null)
    {
        $act = $pkg->getAct();
        $pkg = self::checkDate($pkg, $type);
        $pkg = self::getList($pkg);
        $output = array();
        if ($pkg->getAct() !== 'List_R_Err' && $pkg->getAct() !== 'Date_Input_Err') {
            $larr = $pkg->getData('List');
            $pkg->setAct($act);
            foreach ($larr as $list) {
                if ($list['ListDate'] >= $pkg->getData('Date_S') && $list['ListDate'] < $pkg->getData('Date_E')) {
                    array_push($output, $list);
                }
            }
            $pkg->setData('List', $output);
        } else {
            $pkg->delData('List');
            $pkg->setAct('Report_Get_For_Date_Err');
        }
        
        return $pkg;
    }

    public static function countAmount(Package $pkg, $payment, $type = null)
    {
        if ($pkg->has('List') === false) {
            $pkg = self::getForDate($pkg, $type);
        }
        $sol = 0;
        if ($pkg->getAct() !== 'Report_Get_For_Date_Err') {
            if (CheckInput::checkBool($payment) === true) {
                foreach ($pkg->getData('List') as $list) {
                    if ($list['Payment'] == $payment) {
                        $sol = $sol + $list['Amount'];
                    }
                }

                if ($payment) {
                    $pkg->setData('Income', $sol);
                } else {
                    $pkg->setData('Pay', $sol);
                }
            } else {
                $pkg->setAct('Report_CA_Err');
            }
        }
        return $pkg;
    }

    public static function countCategory(Package $pkg, $type = null)
    {
        if ($pkg->has('List') === false) {
            $pkg = self::getForDate($pkg, $type);
        }
        if ($pkg->has('Total') === false) {
            $pkg = self::total($pkg, $type);
        }
        $pkg = self::getC($pkg);
        $output = array();
        if ($pkg->getAct() !== 'Report_Get_For_Date_Err' && $pkg->getAct() !== 'Report_CA_Err') {
            foreach ($pkg->getData('Category') as $cl) {

                $sum = 0;

                foreach ($pkg->getData('List') as $list) {
                    if ($list['Category'] === $cl['Category']) {
                        $sum = $sum + $list['Amount'];
                    }
                }

                $cl['CategoryTotal'] = $sum;
                if ($cl['Payment']) {
                    if ($pkg->getData('Income') == 0) {
                        $cl['percent'] = number_format((float)$sum/1*100, 1)."%";
                    } else {
                        $cl['percent'] = number_format((float)$sum/$pkg->getData('Income')*100, 1)."%";
                    }
                    $cl['PaymentType'] = $cl['Payment'];
                    $cl['Payment'] = '收入';
                } else {
                    if ($pkg->getData('Pay') == 0) {
                        $cl['percent'] = number_format((float)$sum/1*100, 1)."%";
                    } else {
                        $cl['percent'] = number_format((float)$sum/$pkg->getData('Pay')*100, 1)."%";
                    }
                    $cl['PaymentType'] = $cl['Payment'];
                    $cl['Payment'] = '支出';
                }

                if ($sum > 0) {
                    $cl['status'] = 'haslist';
                } else {
                    $cl['status'] = 'nohaslist';
                }
                array_push($output, $cl);
            }
            $pkg->setData('Category', $output);
        } else {
            $pkg->setAct('Report_CC_Err');
        }
        return $pkg;
    }

    public static function income(Package $pkg, $type = null)
    {
        return ($pkg->has('Income')) ? $pkg : self::countAmount($pkg, true, $type);
    }

    public static function pay(Package $pkg, $type = null)
    {
        return ($pkg->has('Pay')) ? $pkg : self::countAmount($pkg, false, $type);
    }

    public static function total(Package $pkg, $type = null)
    {
        if ($pkg->getAct() !== 'Report_CA_Err' && $pkg->getAct() !== 'Report_Get_For_Date_Err') {
            if ($pkg->has('Income') && $pkg->has('Pay')) {
                $total = $pkg->getData('Income') - $pkg->getData('Pay');
            } else {
                $total = self::income($pkg, $type)->getData('Income') - self::pay($pkg, $type)->getData('Pay');
            }
            $pkg->setData('Total', $total);
        }
        return $pkg;
    }

    public static function showReport(Package $pkg, $type = null)
    {
        if ($pkg->getAct() === 'Report_Show') {
            $pkg = self::countCategory($pkg, $type);
            if (substr($pkg->getAct(), -3) !== 'Err') {
                $pkg->setAct('Report_Show_Suc');
                $pkg->delData('Authorization');
            } else {
                $pkg->delAllData();
            }
        } else {
            $pkg->setAct('Report_Show_Err');
        }
        return $pkg;
    }

    public static function checkDate(Package $pkg, $act = null)
    {
        $date_s = null;
        $date_e = null;
        if ($pkg->has('Date_S') === true) {
            if (CheckInput::checkDate($pkg->getData('Date_S')) === true) {
                $date_s = $pkg->getData('Date_S');
                if ($pkg->has('Date_E') === true) {
                    if (CheckInput::checkDate($pkg->getData('Date_E')) === true) {
                        $date_e = $pkg->getData('Date_E');
                    } else {
                        $pkg->setAct('Date_Input_Err');
                    }
                } else {
                    $date_e = $date_s;
                }
            } else {
                $pkg->setAct('Date_Input_Err');
            }
        } else {
            if ($pkg->has('Date_E') === true) {
                if (CheckInput::checkDate($pkg->getData('Date_E')) === true) {
                    $date_e = $pkg->getData('Date_E');
                    $date_s = $date_e;
                } else {
                    $pkg->setAct('Date_Input_Err');
                }
            } else {
                $date_s = date('Y-m-d');
                $date_e = date('Y-m-d');
            }
        }

        $date_s = new \DateTime($date_s);
        $date_e = new \DateTime($date_e);

        if ($act === 'Day' || $act === null) {
            $date_e->modify('+1 day');
            $pkg->setData('Date_S', $date_s->format('Y-m-d'));
            $pkg->setData('Date_E', $date_e->format('Y-m-d'));
        } elseif ($act === 'Week') {
            $w = $date_s->format('W');
            $y = $date_s->format('Y');
            $date = new \DateTime();

            $pkg->setData('Date_S', $date->setISODate($y, $w, 1)->format('Y-m-d'));
            $pkg->setData('Date_E', $date->setISODate($y, $w, 8)->format('Y-m-d'));
        } elseif ($act === 'Month') {
            $date = new \DateTime($date_s->format('Y-m-d'));
            $date->modify('+1 month');
            $pkg->setData('Date_S', $date_s->format('Y-m'));
            $pkg->setData('Date_E', $date->format('Y-m'));
        } elseif ($act === 'Year') {
            $date = new \DateTime($date_s->format('Y-m-d'));
            $date->modify('+1 year');
            $pkg->setData('Date_S', $date_s->format('Y'));
            $pkg->setData('Date_E', $date->format('Y'));
        } else {
            $pkg->setAct('Date_Input_Err');
        }

        return $pkg;
    }
}
