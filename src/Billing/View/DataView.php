<?php
namespace Billing\View;

use Billing\System\View;
use Billing\View\MsgArray;
use Billing\Model\Package;

class DataView
{
    public static function render(Package $pkg, $authorization)
    {
        $send = array(
            'detail' => null,
            'status' => null,
            'Authorization' => $authorization
        );

        if ($pkg->getAct() === 'Report_Show_Suc' ||
            $pkg->getAct() === 'List_S_Suc' ||
            $pkg->getAct() === 'Category_S_Suc'
        ) {
            $send['detail'] = $pkg->getAllData();
        } else {
            $send['detail'] = MsgArray::get($pkg->getAct());
        }
        $send['status'] = $pkg->getAct();
        return View::render($send);
    }
}
