<?php
namespace Billing\View;

class MsgArray
{
    public static function get($str)
    {
        $tmp = array(
            //正確的
            'List_C_Suc' => '新增明細成功',
            'List_R_Suc' => '取得明細成功',
            'List_U_Suc' => '修改明細成功',
            'List_D_Suc' => '刪除明細成功',
            'Category_C_Suc' => '新增分類成功',
            'Category_R_Suc' => '取得分類成功',
            'Category_U_Suc' => '修改分類成功',
            'Category_D_Suc' => '刪除分類成功',
            'Report_Show_Suc' => '取得報表成功',
            //錯誤
            'List_C_Err' => '新增明細發生錯誤',
            'List_R_Err' => '取得明細發生錯誤',
            'List_U_Err' => '修改明細發生錯誤',
            'List_D_Err' => '刪除明細發生錯誤',
            'Category_C_Err' => '新增分類發生錯誤',
            'Category_R_Err' => '取得分類發生錯誤',
            'Category_U_Err' => '修改分類發生錯誤',
            'Category_D_Err' => '刪除分類發生錯誤',
            'Report_Show_Err' => '取得報表失敗',
            'Date_Input_Err' => '輸入時間錯誤',
            'Report_Get_For_Date_Err' => '因為時間輸入錯誤，導致報表取得錯誤',
            'Report_CA_Err' => '計算加總錯誤',
            'Report_CC_Err' => '計算分類加總錯誤',
            'Category_C_L_Err' => '因為輸入錯誤，導致新增分類失敗',
            'Category_U_L_Err' => '因為輸入錯誤，導致修改分類失敗',
            'Category_D_L_Err' => '因為輸入錯誤，導致刪除分類失敗',
            'List_C_L_Err' => '因為輸入錯誤，導致新增明細失敗',
            'List_U_L_Err' => '因為輸入錯誤，導致修改明細失敗',
            'List_D_L_Err' => '因為輸入錯誤，導致刪除明細失敗',
            //檢查
            'List_H_Suc' => '有這個明細序號',
            'List_H_Err' => '沒有這個明細序號',
            'Category_H_Suc' => '有這個分類序號',
            'Category_H_Err' => '沒有這個分類序號',

            //search
            'List_S_Err' => '搜尋明細失敗',
            'List_S_L_Err' => '搜尋明細失敗',
            'Category_S_Err' => '搜尋分類失敗',
            'Category_S_L_Err' => '搜尋分類失敗',

            'New_Account_Suc' => '新帳號新增類別成功'

        );
        return (isset($tmp[$str])) ? $tmp[$str] : '不知名錯誤';
    }
}
