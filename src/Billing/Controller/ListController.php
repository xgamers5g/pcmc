<?php
namespace Billing\Controller;

use Billing\Lib\LCCUD;
use Billing\View\DataView;
use Billing\System\Controller;
use Auth\Model\DataBox;
use Billing\Model\Package;

class ListController extends Controller
{
    public function __construct()
    {
        Controller::init();
    }

    public function create()
    {
        $pkg = Package::load('List_C', self::$authdata);
        $pkg->setArrayData(self::$data);
        $pkg = LCCUD::load($pkg)->createList();
        $res = \Billing\Lib\WriteRecord::write($pkg, self::$authdata, $_SERVER['REMOTE_ADDR']);
        // echo \Billing\System\View::render($res);
        if ($res === false) {
            echo DataView::render($pkg, self::$authdata['Authorization']);
        } else {
            echo DataView::render($pkg, $res['Authorization']);
        }
    }

    public function update()
    {
        $pkg = Package::load('List_U', self::$authdata);
        $pkg->setArrayData(self::$data);
        $pkg = LCCUD::load($pkg)->updateList();
        $res = \Billing\Lib\WriteRecord::write($pkg, self::$authdata, $_SERVER['REMOTE_ADDR']);
        // echo \Billing\System\View::render($res);
        if ($res === false) {
            echo DataView::render($pkg, self::$authdata['Authorization']);
        } else {
            echo DataView::render($pkg, $res['Authorization']);
        }
    }

    public function delete()
    {
        $pkg = Package::load('List_D', self::$authdata);
        $pkg->setArrayData(self::$data);
        $pkg = LCCUD::load($pkg)->deleteList();
        echo DataView::render($pkg, self::$authdata['Authorization']);
    }
}
