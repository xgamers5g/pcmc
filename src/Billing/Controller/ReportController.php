<?php
namespace Billing\Controller;

use Billing\Lib\Report;
use Billing\Lib\LCCUD;
use Billing\View\DataView;
use Billing\System\Controller;
use Auth\Model\DataBox;
use Billing\Model\Package;

class ReportController extends Controller
{
    public function __construct()
    {
        Controller::init();
    }

    public function showDayReport()
    {
        $pkg = Package::load('Report_Show', self::$authdata);
        $pkg->setArrayData(self::$data);
        $pkg = Report::showReport($pkg);
        echo DataView::render($pkg, self::$authdata['Authorization']);
    }

    public function showWeekReport()
    {
        $pkg = Package::load('Report_Show', self::$authdata);
        $pkg->setArrayData(self::$data);
        $pkg = Report::showReport($pkg, 'Week');
        echo DataView::render($pkg, self::$authdata['Authorization']);
    }

    public function showMonthReport()
    {
        $pkg = Package::load('Report_Show', self::$authdata);
        $pkg->setArrayData(self::$data);
        $pkg = Report::showReport($pkg, 'Month');
        echo DataView::render($pkg, self::$authdata['Authorization']);
    }

    public function showYearReport()
    {
        $pkg = Package::load('Report_Show', self::$authdata);
        $pkg->setArrayData(self::$data);
        $pkg = Report::showReport($pkg, 'Year');
        echo DataView::render($pkg, self::$authdata['Authorization']);
    }

    public function hasList()
    {
        $pkg = Package::load('List_H', self::$authdata);
        $pkg->setArrayData(self::$data);
        $pkg = LCCUD::hasList($pkg);
        echo DataView::render($pkg, self::$authdata['Authorization']);
    }

    public function hasCategory()
    {
        $pkg = Package::load('Category_H', self::$authdata);
        $pkg->setArrayData(self::$data);
        $pkg = LCCUD::hasCategory($pkg);
        echo DataView::render($pkg, self::$authdata['Authorization']);
    }
}
