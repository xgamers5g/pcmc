<?php
namespace Billing\Controller;

use Billing\Lib\LCCUD;
use Billing\View\DataView;
use Billing\System\Controller;
use Auth\Model\DataBox;
use Billing\Model\Package;

class CategoryController extends Controller
{
    public function __construct()
    {
        Controller::init();
    }

    public function create()
    {
        $pkg = Package::load('Category_C', self::$authdata);
        $pkg->setArrayData(self::$data);
        $pkg = LCCUD::load($pkg)->createCategory();
        echo DataView::render($pkg, self::$authdata['Authorization']);
    }

    public function update()
    {
        $pkg = Package::load('Category_U', self::$authdata);
        $pkg->setArrayData(self::$data);
        $pkg = LCCUD::load($pkg)->updateCategory();
        echo DataView::render($pkg, self::$authdata['Authorization']);
    }

    public function delete()
    {
        $pkg = Package::load('Category_D', self::$authdata);
        $pkg->setArrayData(self::$data);
        $pkg = LCCUD::load($pkg)->deleteCategory();
        echo DataView::render($pkg, self::$authdata['Authorization']);
    }

    public function createAtNewAccount()
    {
        $pkg = Package::load('Category_C', self::$authdata);
        $pkg->setArrayData(self::$data);
        $pkg = LCCUD::createAtNewAccount($pkg);
        echo DataView::render($pkg, self::$authdata['Authorization']);
    }
}
