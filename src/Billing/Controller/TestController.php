<?php
namespace Billing\Controller;

class TestController extends \Billing\System\Controller
{
    public static $data = null;
    public function __construct()
    {
        self::init();
    }

    public function connect()
    {
        // echo json_encode(self::$data);
        self::$data['Authorization'] = self::$authdata['Authorization'];
        echo json_encode(\Billing\Lib\ConnectHTTP::connect('http://pcmctest/list/has', self::$data));
    }
}
