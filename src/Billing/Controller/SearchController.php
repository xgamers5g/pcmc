<?php
namespace Billing\Controller;

use Billing\Lib\Search;
use Billing\View\DataView;
use Billing\System\Controller;
use Auth\Model\DataBox;
use Billing\Model\Package;

class SearchController extends Controller
{
    public function __construct()
    {
        Controller::init();
    }

    public function searchList()
    {
        $pkg = Package::load('List_S', self::$authdata);
        $pkg->setArrayData(self::$data);
        $pkg = Search::searchList($pkg);
        echo DataView::render($pkg, self::$authdata['Authorization']);
    }

    public function searchCategory()
    {
        $pkg = Package::load('Category_S', self::$authdata);
        $pkg->setArrayData(self::$data);
        $pkg = Search::searchCategory($pkg);
        echo DataView::render($pkg, self::$authdata['Authorization']);
    }
}
