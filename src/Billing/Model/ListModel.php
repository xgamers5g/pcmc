<?php
namespace Billing\Model;

use Billing\System\Model;
use PDO;
use PDOStatement;

class ListModel extends Model
{
    private static $pkg = null;

    protected function __construct(Package $pkg)
    {
        Model::init();
        if (substr($pkg->getAct(), 0, 4) === 'List') {
            self::$pkg = $pkg;
        } else {
            $pkg->setAct('List_Err');
            return $pkg;
        }
    }

    public static function load(Package $pkg)
    {
        return new self($pkg);
    }

    public static function get(Package $pkg)
    {
        Model::init();
        if ($pkg->getAct() === "List_R") {
            $qs = "SELECT lid AS ListID,
                           list.cid AS Category,
                           cname AS CategoryName,
                           ctype AS Payment,
                           lname AS ListName,
                           lamount AS Amount,
                           ldate AS ListDate
                    FROM `list`, `category`
                    WHERE list.cid = category.cid and list.uid = ?";
            $stmt= self::$db->prepare($qs);
            $stmt->bindValue(1, $pkg->getUser(), PDO::PARAM_INT);
            if ($stmt->execute()) {
                $pkg->setData('List', $stmt->fetchAll(PDO::FETCH_ASSOC));
                $pkg->setAct('List_R_Suc');
            } else {
                $pkg->setAct('List_R_Err');
            }
            $stmt->closeCursor();
        } else {
            $pkg->setAct('List_R_Err');
        }
        return $pkg;
    }

    public static function create(Package $pkg)
    {
        Model::init();
        if ($pkg->getAct() === "List_C") {
            $qs = "INSERT INTO `list` (cid, lname, lamount, ldate, uid)
                    VALUES (?, ?, ?, ?, ?)";
            $stmt= self::$db->prepare($qs);
            $stmt->bindValue(1, $pkg->getData('Category'), PDO::PARAM_INT);
            $stmt->bindValue(2, $pkg->getData('ListName'), PDO::PARAM_STR);
            $stmt->bindValue(3, $pkg->getData('Amount'), PDO::PARAM_INT);
            $stmt->bindValue(4, $pkg->getData('ListDate'), PDO::PARAM_STR);
            $stmt->bindValue(5, $pkg->getUser(), PDO::PARAM_INT);
            if ($stmt->execute()) {
                $pkg->setAct('List_C_Suc');
            } else {
                $pkg->setAct('List_C_Err');
            }
        } else {
            $pkg->setAct('List_C_Err');
        }
        return $pkg;
    }

    public function update()
    {
        if (self::$pkg->getAct() === "List_U") {
            $qs = "UPDATE `list` 
                   SET cid = ?, lname = ?, lamount = ?, ldate = ?
                   WHERE lid = ?";
            $stmt= self::$db->prepare($qs);
            $stmt->bindValue(1, self::$pkg->getData('Category'), PDO::PARAM_INT);
            $stmt->bindValue(2, self::$pkg->getData('ListName'), PDO::PARAM_STR);
            $stmt->bindValue(3, self::$pkg->getData('Amount'), PDO::PARAM_INT);
            $stmt->bindValue(4, self::$pkg->getData('ListDate'), PDO::PARAM_STR);
            $stmt->bindValue(5, self::$pkg->getData('ListID'), PDO::PARAM_INT);
            if ($stmt->execute()) {
                self::$pkg->setAct('List_U_Suc');
            } else {
                self::$pkg->setAct('List_U_Err');
            }
        } else {
            self::$pkg->setAct('List_U_Err');
        }
        return self::$pkg;
    }

    public function delete()
    {
        if (self::$pkg->getAct() === "List_D") {
            $qs = "DELETE FROM `list` WHERE lid = ?";
            $stmt= self::$db->prepare($qs);
            $stmt->bindValue(1, self::$pkg->getData('ListID'), PDO::PARAM_INT);
            if ($stmt->execute()) {
                self::$pkg->setAct('List_D_Suc');
            } else {
                self::$pkg->setAct('List_D_Err');
            }
        } else {
            self::$pkg->setAct('List_D_Err');
        }
        return self::$pkg;
    }

    public static function has($lid)
    {
        Model::init();
        if ($lid > 0) {
            $qs = "SELECT * FROM `list` WHERE lid = ?";
            $stmt = self::$db->prepare($qs);
            $stmt->bindValue(1, $lid, PDO::PARAM_INT);
            $stmt->execute();
            return ($stmt->rowCount() === 1) ? true : false;
        } else {
            $pkg->setAct('List_H_Err');
            return false;
        }
    }

    public static function search(Package $pkg)
    {
        Model::init();

        if ($pkg->getAct() === 'List_S') {
            $qs = 'SELECT lid AS ListID,
                           list.cid AS Category,
                           cname AS CategoryName,
                           ctype AS Payment,
                           lname AS ListName,
                           lamount AS Amount,
                           ldate AS ListDate
                    FROM `list`, `category`
                    WHERE list.cid = category.cid and list.uid = ? and list.lname LIKE ?';
            $stmt = self::$db->prepare($qs);
            $stmt->bindValue(1, $pkg->getUser(), PDO::PARAM_INT);
            $stmt->bindValue(2, '%'.$pkg->getData('ListName').'%', PDO::PARAM_STR);
            if ($stmt->execute()) {
                $pkg->setData('List', $stmt->fetchAll(PDO::FETCH_ASSOC));
                $pkg->setAct('List_S_Suc');
            } else {
                $pkg->setAct('List_S_Err');
                $pkg->delAllData();
            }
        } else {
            $pkg->setAct('List_S_Err');
            $pkg->delAllData();
        }
        return $pkg;
    }
}
