<?php
namespace Billing\Model;

use Billing\System\Model;
use PDO;
use PDOStatement;

class CategoryModel extends Model
{
    private static $pkg = null;

    protected function __construct(Package $pkg)
    {
        Model::init();
        if (substr($pkg->getAct(), 0, 8) === 'Category') {
            self::$pkg = $pkg;
        } else {
            $pkg->setAct('Category_Err');
            return $pkg;
        }
    }

    public static function load(Package $pkg)
    {
        return new self($pkg);
    }

    public static function get(Package $pkg)
    {
        Model::init();
        if ($pkg->getAct() === "Category_R") {
            $qs = "SELECT cid AS Category,
                           cname AS CategoryName,
                           ctype AS Payment
                    FROM `category`
                    WHERE uid = ?";
            $stmt= self::$db->prepare($qs);
            $stmt->bindValue(1, $pkg->getUser(), PDO::PARAM_INT);
            if ($stmt->execute()) {
                $pkg->setData('Category', $stmt->fetchAll(PDO::FETCH_ASSOC));
                $pkg->setAct('Category_R_Suc');
            } else {
                $pkg->setAct('Category_R_Err');
            }
            $stmt->closeCursor();
        } else {
            $pkg->setAct('Category_R_Err');
        }
        return $pkg;
    }

    public static function create(Package $pkg)
    {
        Model::init();
        if ($pkg->getAct() === "Category_C") {
            $qs = "INSERT INTO `category` (cname, ctype, uid)
                    VALUES (?, ?, ?)";
            $stmt= self::$db->prepare($qs);
            $stmt->bindValue(1, $pkg->getData('CategoryName'), PDO::PARAM_STR);
            $stmt->bindValue(2, $pkg->getData('Payment'), PDO::PARAM_INT);
            $stmt->bindValue(3, $pkg->getUser(), PDO::PARAM_INT);
            if ($stmt->execute()) {
                $pkg->setAct('Category_C_Suc');
            } else {
                $pkg->setAct('Category_C_Err');
            }
        } else {
            $pkg->setAct('Category_C_Err');
        }
        return $pkg;
    }

    public function update()
    {
        if (self::$pkg->getAct() === "Category_U") {
            $qs = "UPDATE `category`
                   SET cname = ?, ctype = ?
                   WHERE cid = ?";
            $stmt= self::$db->prepare($qs);
            $stmt->bindValue(1, self::$pkg->getData('CategoryName'), PDO::PARAM_STR);
            $stmt->bindValue(2, self::$pkg->getData('Payment'), PDO::PARAM_INT);
            $stmt->bindValue(3, self::$pkg->getData('Category'), PDO::PARAM_INT);
            if ($stmt->execute()) {
                self::$pkg->setAct('Category_U_Suc');
            } else {
                self::$pkg->setAct('Category_U_Err');
            }
        } else {
            self::$pkg->setAct('Category_U_Err');
        }
        return self::$pkg;
    }

    public function delete()
    {
        if (self::$pkg->getAct() === "Category_D") {
            $qs = "DELETE FROM `category` WHERE cid = ?";
            $stmt= self::$db->prepare($qs);
            $stmt->bindValue(1, self::$pkg->getData('Category'), PDO::PARAM_INT);
            if ($stmt->execute()) {
                self::$pkg->setAct('Category_D_Suc');
            } else {
                self::$pkg->setAct('Category_D_Err');
            }
        } else {
            self::$pkg->setAct('Category_D_Err');
        }
        return self::$pkg;
    }

    public static function has($cid)
    {
        Model::init();
        if ($cid > 0) {
            $qs = "SELECT * FROM `category` WHERE cid = ?";
            $stmt = self::$db->prepare($qs);
            $stmt->bindValue(1, $cid, PDO::PARAM_INT);
            $stmt->execute();
            return ($stmt->rowCount() === 1) ? true : false;
        } else {
            $pkg->setAct('Category_H_Err');
            return false;
        }
    }

    public static function search(Package $pkg)
    {
        Model::init();

        if ($pkg->getAct() === 'Category_S') {
            $qs = 'SELECT lid AS ListID,
                           list.cid AS Category,
                           cname AS CategoryName,
                           ctype AS Payment,
                           lname AS ListName,
                           lamount AS Amount,
                           ldate AS ListDate
                    FROM `list`, `category`
                    WHERE list.cid = category.cid and list.uid = ? and cname LIKE ?';
            $stmt = self::$db->prepare($qs);
            $stmt->bindValue(1, $pkg->getUser(), PDO::PARAM_INT);
            $stmt->bindValue(2, '%'.$pkg->getData('CategoryName').'%', PDO::PARAM_STR);
            if ($stmt->execute()) {
                $pkg->setData('Category', $stmt->fetchAll(PDO::FETCH_ASSOC));
                $pkg->setAct('Category_S_Suc');
            } else {
                $pkg->setAct('Category_S_Err');
                $pkg->delAllData();
            }
        } else {
            $pkg->setAct('Category_S_Err');
            $pkg->delAllData();
        }
        return $pkg;
    }
}
