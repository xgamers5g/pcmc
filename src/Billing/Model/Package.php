<?php
namespace Billing\Model;

use Auth\Model\DataBox;
use Billing\Model\Package;

class Package
{
    private static $data = null;
    private static $act = null;
    private static $user = null;

    protected function __construct($str, $user)
    {
        self::$data = array();

        if (is_string($str)) {
            self::$act = $str;
        } else {
            self::$act = "Action_Err";
        }

        if (isset($user['id'])) {
            self::$user = $user['id'];
        } else {
            self::$act = "User_Err";
        }
    }

    public static function load($str, $user)
    {
        return new self($str, $user);
    }

    /*
    * 單機版本
    */
    // protected function __construct($str, $user)
    // {
    //     self::$data = array();

    //     if (is_string($str)) {
    //         self::$act = $str;
    //     } else {
    //         self::$act = "Action_Err";
    //     }

    //     if (is_integer($user)) {
    //         self::$user = $user;
    //     } else {
    //         self::$act = "User_Err";
    //     }
    // }

    // public static function load($str, $user)
    // {
    //     return new self($str, $user);
    // }

    public function setAct($str)
    {
        if (is_string($str)) {
            self::$act = $str;
        } else {
            self::$act = "Action_Err";
        }
    }

    public function getAct()
    {
        return self::$act;
    }

    public function setArrayData(array $data)
    {
        self::$data = $data;
    }

    public function setData($key, $value)
    {
        if (is_string($key) || is_integer($key)) {
            self::$data[$key] = $value;
        } else {
            self::$act = "Set_Data_Err";
        }
    }

    public function getData($key)
    {
        if (isset(self::$data[$key])) {
            return self::$data[$key];
        } else {
            return false;
        }
    }

    public function has($key)
    {
        return isset(self::$data[$key]);
    }

    public function getAllData()
    {
        return self::$data;
    }

    public function delData($key)
    {
        if (isset(self::$data[$key])) {
            unset(self::$data[$key]);
        } else {
            return false;
        }
    }

    public function delAllData()
    {
        self::$data = null;
    }

    public function getUser()
    {
        return self::$user;
    }

    public function reset()
    {
        self::$data = null;
        self::$act = null;
        self::$user = null;
    }
}
