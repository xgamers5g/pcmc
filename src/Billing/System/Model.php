<?php
namespace Billing\System;

use Fruit\Seed;
use Fruit\Config;
use PDOStatement;

class Model extends Seed
{
    /**
    * 儲存db連線的變數
    */
    public static $db = null;
    
    /**
    * 建立DB連線
    */
    public static function init()
    {
        if (is_null(self::$db)) {
            if (self::getConfig()->getDb() != null) {
                self::$db = self::getConfig()->getDb();
            } else {
                throw new Exception("error9");
            }
        }
    }
}
