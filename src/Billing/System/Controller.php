<?php
namespace Billing\System;

class Controller
{
    /**
     * @param $authdata 使用者資訊
     */
    protected static $authdata = null;
    /**
    * 收取資料的變數
    */
    public static $data = null;
    
    /**
    * 取得收到的資料
    */
    public static function init()
    {
        if (file_get_contents("php://input") != null) {
            self::$data = json_decode(file_get_contents("php://input"), true);
        }
        $core = \Auth\System\Core::getInstance();
        $facade = $core->getAuthFacade($core);
        self::$authdata = $facade->getUserData();
        if (!isset(self::$authdata['id'])) {
            return self::$authdata;
        }

        // protected static $friendlist = null;

        // $friendcore = \AuthFriend\System\Core::getInstance();
        // $friendfacade = $friendcore->getAuthFriendFacade($friendcore);
        // self::$friendlist = $friendfacade->getFriendList();
        // if (!self::$friendlist) {
        //     return self::$friendlist;
        // }
    }
}
