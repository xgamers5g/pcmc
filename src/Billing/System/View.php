<?php
namespace Billing\System;

class View
{
    /**
    * 回傳JSON格式的資料
    * @param $arg 欲送出的資料
    * @return json 格式的資料
    */
    public static function render($arg)
    {
        return json_encode($arg, JSON_UNESCAPED_UNICODE);
    }
}
