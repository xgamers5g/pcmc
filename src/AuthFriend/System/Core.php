<?php
namespace AuthFriend\System;

/**
 * AuthFriend模組的核心，
 * 
 * 直接繼承Core來取得需要使用的工具組件。
 *
 * Garth Wang 2014.11.14 心情非常糟
 */

class Core extends \Auth\System\Core
{
    private static $authfriendmodel = null;
    private static $authfriendfacade = null;
    protected function __construct()
    {
        //nothing to do
    }
    public static function getInstance()
    {
        if (!self::$config) {
            self::$config = include(BASE_DIR.'/config/config.php');
        }
        return new self;
    }
    /**
     * 初始化AuthFriendModel物件並返回。
     * @return Object 返回\AuthFriend\Model\AuthFriendModel
     */
    public function getAuthFriendModel(
        \PDO $pdo,
        \Redis $redis,
        \Auth\Model\Response $response,
        $uid,
        $fid = null
    ) {
        if (self::$authfriendmodel instanceof \AuthFriend\Model\AuthFriendModel) {
            return self::$authfriendmodel;
        }
        self::$authfriendmodel = \AuthFriend\Model\AuthFriendModel::getInstance(
            $pdo,
            $redis,
            $response,
            $uid,
            $fid
        );
        return self::$authfriendmodel;
    }
    public function getAuthFriendFacade(\AuthFriend\System\Core $core, $uid, $authorization)
    {
        if (self::$authfriendfacade instanceof \AuthFriend\System\AuthFriendFacade) {
            return self::$authfriendfacade;
        }
        self::$authfriendfacade = \AuthFriend\Facade\AuthFriendFacade::getInstance(
            $core,
            $uid,
            $authorization
        );
        return self::$authfriendfacade;
    }
}
