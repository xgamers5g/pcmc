<?php
namespace AuthFriend\Facade;

class AuthFriendFacade
{
    private static $core = null;
    private static $ufm = null;
    private static $authorization = null;
    private function __construct()
    {
        //nothing to do
    }
    public static function getInstance(\AuthFriend\System\Core $core, $uid, $authorization)
    {
        self::$core = $core;
        $data = $core->getPostHandler()->getJSONData();
        $fid = null;
        if (isset($data['fid'])) {
            $fid = $data['fid'];
        }
        self::$ufm = $core->getAuthFriendModel(
            $core->getDb(),
            $core->getRedis(),
            $core->getResponse(),
            $uid,
            $fid
        );
        //echo $fid;
        self::$authorization = $authorization;
        return new self;
    }
    public function getFriend()
    {

        $data = self::$ufm->get();
        $data['Authorization'] = self::$authorization;
        return $data;
    }
    public function addFriend()
    {
        $data = self::$ufm->add();
        $data['Authorization'] = self::$authorization;
        return $data;
    }
    public function deleteFriend()
    {
        $data = self::$ufm->delete();
        $data['Authorization'] = self::$authorization;
        return $data;
    }
}
