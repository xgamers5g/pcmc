<?php
namespace AuthFriend\Model;

class AuthFriendModel
{
    /**
     * 積極的人不用別人講，自己會去學。
     *
     * 不想學的人就算給他世界上最好的老師，結果可想而知。
     *
     * 我為人建立好友名單，可是我沒有朋友。
     *
     * Garth_Wang 2014.11.14 心情非常糟
     */
    private static $pdo = null;
    private static $redis = null;
    private static $response = null;
    private static $uid = null;
    private static $fid = null;
    private function __construct()
    {
        //nothing to do
    }
    public static function getInstance(
        \PDO $pdo,
        \Redis $redis,
        \Auth\Model\Response $response,
        $uid,
        $fid = null
    ) {
        self::$pdo = $pdo;
        self::$redis = $redis;
        self::$response = $response;
        self::$uid = $uid;
        if ($fid) {
            self::$fid = $fid;
        }
        // echo self::$fid;
        return new self();
    }
    /**
     * 新增好友
     * 先寫入redis，再寫入mysql
     */
    public function add()
    {
        if (!self::$fid) {
            return self::$response->generateResponse('dataerror');
        }
        if (self::$redis->exists('userfriend::userid::'.self::$uid)) {
            $data = json_decode(self::$redis->get('userfriend::userid::'.self::$uid), true);
            $data[count($data)] = self::$fid;
            self::$redis->set('userfriend::userid::'.self::$uid, json_encode($data));
        } else {
            self::$redis->set(
                'userfriend::userid::'.self::$uid,
                json_encode(array(0 => self::$fid))
            );
        }
        //echo 123;
        $qs = 'insert into user_friend(userid, friendid) VALUES (?, ?)';
        $stmt = self::$pdo->prepare($qs);
        $stmt->bindValue(1, self::$uid, \PDO::PARAM_INT);
        $stmt->bindValue(2, self::$fid, \PDO::PARAM_INT);
        $res = $stmt->execute();
        $stmt->closeCursor();
        if ($res) {
            return self::$response->generateResponse('success');
        } else {
            ///echo 123;
            return self::$response->generateResponse('databaseerror');
        }
    }
    /**
     * 取得好友名單
     * 先從redis找，再從mysql找
     * @return Databox $dbox
     */
    public function get()
    {
        if (self::$redis->exists('userfriend::userid::'.self::$uid)) {
            $data = self::$response->generateResponse('success');
            $data['friendlist'] = json_decode(self::$redis->get('userfriend::userid::'.self::$uid), true);
            return $data;
        } else {
            $qs = 'select friendid from user_friend where userid = ?';
            $stmt = self::$pdo->prepare($qs);
            $stmt->bindValue(1, self::$uid, \PDO::PARAM_INT);
            $stmt->execute();
            $res = $stmt->fetchAll(\PDO::FETCH_COLUMN);
            if ($res) {
                $data = self::$response->generateResponse('success');
                $data['friendlist'] = $res;
                return $data;
            } else {
                return self::$response->generateResponse('databaseerror');
            }
        }
    }
    /**
     * 刪除好友
     * 先刪Redis，再刪Mysql
     * @return DataBox $dbox
     */
    public function delete()
    {
        if (self::$fid==null) {
            return self::$response->generateResponse('dataerror');
        }
        if (self::$redis->exists('userfriend::userid::'.self::$uid)) {
            $data = json_decode(self::$redis->get('userfriend::userid::'.self::$uid), true);
            $count = count($data);
            for ($i=0; $i<$count; $i++) {
                if ($data[$i] == self::$fid) {
                    unset($data[$i]);
                    break;
                }
            }
            if (count($data)==0) {
                self::$redis->delete('userfriend::userid::'.self::$uid);
            } else {
                self::$redis->set('userfriend::userid::'.self::$uid, json_encode(array_values($data)));
            }
        }
        $qs = 'delete from user_friend where userid = ? and friendid = ?';
        $stmt = self::$pdo->prepare($qs);
        $stmt->bindValue(1, self::$uid, \PDO::PARAM_INT);
        $stmt->bindValue(2, self::$fid, \PDO::PARAM_INT);
        $res = $stmt->execute();
        $stmt->closeCursor();
        if ($res) {
            return self::$response->generateResponse('success');
        } else {
            return self::$response->generateResponse('databaseerror');
        }
    }
    public function __destruct()
    {
        self::$pdo = null;
        self::$redis = null;
        self::$response = null;
        self::$uid = null;
        self::$fid = null;
    }
}
