<?php
namespace AuthFriend\Controller;

/**
 * 好友系統
 *
 * 2014.10.29 Garth_Wang 30大壽
 */

class AuthFriendController
{
    private $authdata = null;
    private $friendfacade = null;
    public function __construct($facade = null, $friendfacade = null)
    {
        if (!$facade) {
            $core = \Auth\System\Core::getInstance();
            $facade = $core->getAuthFacade($core);
        }
        $this->authdata = $facade->getUserData();
        //在建構式中，return只能打斷下方程式執行，並不能控制其回傳值。
        if (!isset($this->authdata['id'])) {
            return;
        }
        if ($friendfacade) {
            $this->friendfacade = $friendfacade;
        } else {
            $friendcore = \AuthFriend\System\Core::getInstance();
            //var_dump($this->authdata);
            $this->friendfacade = $friendcore->getAuthFriendFacade(
                $friendcore,
                $this->authdata['id'],
                $this->authdata['Authorization']
            );
        }
    }
    /**
     * 加好友
     *
     *      Endpoints
     *
     *          json Array(
     *              'Authorization' => 使用者的登入令牌
     *              'fid' => 好友id
     *          )
     *
     *      Callback 
     *
     *          json Array(
     *              'statuscode'    => 狀態碼,
     *              'detail'        => 狀態內容,
     *              'Authorization' => 使用者的登入令牌
     *          )
     */
    public function addFriendAction()
    {
        if (!isset($this->authdata['id'])) {
            return json_encode($this->authdata, JSON_UNESCAPED_UNICODE);
        }
        return json_encode($this->friendfacade->addFriend(), JSON_UNESCAPED_UNICODE);
    }
    /**
     * 取得好友名單
     *
     *      Endpoints
     *
     *          json Array(
     *              'Authorization' => 使用者的登入令牌
     *          )
     *
     *      Callback 
     *
     *          json Array(
     *              'statuscode'    => 狀態碼,
     *              'detail'        => 狀態內容,
     *              'Authorization' => 使用者的登入令牌
     *              'friendlist' => array(
     *                  索引值(0) => 好友id,
     *                  索引值(1) => 好友id...以此類推
     *              )
     *          )
     */
    public function getFriendAction()
    {
        if (!isset($this->authdata['id'])) {
            return json_encode($this->authdata, JSON_UNESCAPED_UNICODE);
        }
        return json_encode($this->friendfacade->getFriend(), JSON_UNESCAPED_UNICODE);
    }
    /**
     * 刪好友
     *
     *      Endpoints
     *
     *          json Array(
     *              'Authorization' => 使用者的登入令牌
     *              'fid' => 好友id
     *          )
     *
     *      Callback 
     *
     *          json Array(
     *              'statuscode'    => 狀態碼,
     *              'detail'        => 狀態內容,
     *              'Authorization' => 使用者的登入令牌
     *          )
     */
    public function deleteFriendAction()
    {
        if (!isset($this->authdata['id'])) {
            return json_encode($this->authdata, JSON_UNESCAPED_UNICODE);
        }
        return json_encode($this->friendfacade->deleteFriend(), JSON_UNESCAPED_UNICODE);
    }
}
