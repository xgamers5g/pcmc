<?php
namespace Diary\System;

use Fruit\Seed;

class Controller extends Seed
{
    /**
     * @param $authdata 使用者資訊
     */
    protected static $authdata = null;
    /**
    * @param $data 儲存前端送來資料的變數
    */
    public static $data = null;

    /**
    * 取得前端送來的資料
    */
    public static function init()
    {
        if (file_get_contents("php://input") != null) {
            self::$data = json_decode(file_get_contents("php://input"));
        }
        $core = \Auth\System\Core::getInstance();
        $facade = $core->getAuthFacade($core);
        self::$authdata = $facade->getUserData();
        if (!isset(self::$authdata['id'])) {
            return self::$authdata;
        }
    }
}
