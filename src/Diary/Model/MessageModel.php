<?php
namespace Diary\Model;

use PDO;
use Diary\System\Model;

class MessageModel extends Model
{
    /**
    * 取得DB連線
    */
    protected function __construct()
    {
        self::set();
    }

    /**
    * 新增此類別為物件的開口
    * @return new self()
    */
    public static function load()
    {
        return new self();
    }

    /**
    * 新增留言
    * @return 回傳狀態
    */
    public static function create($arr, $scode)
    {
        Model::set();
        $result = null;
        $qs = 'INSERT INTO diary_message (dmessage, dmdatetime, did, uid, status) VALUES (?, now(), ?, ?, ?)';
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $arr['dmessage'], PDO::PARAM_STR);
        $stmt->bindValue(2, $arr['did'], PDO::PARAM_INT);
        $stmt->bindValue(3, $arr['uid'], PDO::PARAM_INT);
        $stmt->bindValue(4, $arr['mmstatus'], PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = $scode['success'];
        } else {
            $result = $scode['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 查詢留言
    * @return 成功回傳查詢資料，失敗回傳狀態
    */
    public static function select($arr, $scode)
    {
        Model::set();
        $result = null;
        $qs = 'SELECT diary_message.id AS messageid,
        diary_message.dmessage AS message,
        diary_message.dmdatetime AS messagetime,
        diary_message.did AS diaryid,
        diary_message.uid AS user,
        diary_message.dmreply AS reply,
        diary_message.dmrdatetime AS replytime, 
        diary_message.status AS status3 
        FROM diary_message JOIN diary 
        WHERE diary_message.did = diary.id 
        AND diary_message.did = ? AND diary.uid = ?
        OR diary_message.did = ? AND diary_message.uid = ?
        OR diary_message.did = ? AND diary_message.status = 1
        GROUP BY diary_message.id';
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $arr['did'], PDO::PARAM_INT);
        $stmt->bindValue(2, $arr['uid'], PDO::PARAM_INT);
        $stmt->bindValue(3, $arr['did'], PDO::PARAM_INT);
        $stmt->bindValue(4, $arr['uid'], PDO::PARAM_INT);
        $stmt->bindValue(5, $arr['did'], PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (empty($result)) {
                $result = $scode['datanot'];
            }
        } else {
            $result = $scode['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 回覆留言
    * @return 回傳狀態
    */
    public static function update($arr, $scode)
    {
        Model::set();
        $result = null;
        $qs = 'UPDATE diary_message JOIN diary
        SET diary_message.dmreply = ?,
        diary_message.dmrdatetime = now()
        WHERE diary_message.did = diary.id
        AND diary_message.id = ?
        AND diary.uid = ?';
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $arr['dmreply'], PDO::PARAM_STR);
        $stmt->bindValue(2, $arr['mid'], PDO::PARAM_INT);
        $stmt->bindValue(3, $arr['uid'], PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = $scode['success'];
            if ($stmt->rowCount() == 0) {
                $result = $scode['datanot'];
            }
        } else {
            $result = $scode['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 刪除單筆留言
    * @return 回傳狀態
    */
    public static function delete($arr, $scode)
    {
        Model::set();
        $result = null;
        $qs = 'DELETE diary_message FROM diary_message 
        INNER JOIN diary ON diary_message.did = diary.id 
        WHERE diary_message.id = ? AND diary.uid = ?';
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $arr['mid'], PDO::PARAM_INT);
        $stmt->bindValue(2, $arr['uid'], PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = $scode['success'];
            if ($stmt->rowCount() == 0) {
                $result = $scode['datanot'];
            }
        } else {
            $result = $scode['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 刪除日記所有留言
    * diary delete function use
    * @return 回傳狀態
    */
    public static function deletedid($arr, $scode)
    {
        Model::set();
        $result = null;
        $qs = 'DELETE diary_message FROM diary_message 
        INNER JOIN diary ON diary_message.did = diary.id 
        WHERE diary_message.did = ? AND diary.uid = ?';
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $arr['did'], PDO::PARAM_INT);
        $stmt->bindValue(2, $arr['uid'], PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = true;
        } else {
            $result = $scode['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }
}
