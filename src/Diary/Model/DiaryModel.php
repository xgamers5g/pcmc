<?php
namespace Diary\Model;

use PDO;
use Diary\System\Model;

class DiaryModel extends Model
{
    /**
    * 取得DB連線
    */
    protected function __construct()
    {
        self::set();
    }

    /**
    * 新增此類別為物件的開口
    * @return new self()
    */
    public static function load()
    {
        return new self();
    }

    /**
    * 新增日記
    * @return 回傳狀態
    */
    public static function create($arr, $scode)
    {
        Model::set();
        $result = null;
        $qs = 'INSERT INTO diary (cday, oday, eday, dtext, uid, pstatus, mstatus)
         VALUES (now(), ?, now(), ?, ?, ?, ?)';
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $arr['oday'], PDO::PARAM_STR);
        $stmt->bindValue(2, $arr['dtext'], PDO::PARAM_STR);
        $stmt->bindValue(3, $arr['uid'], PDO::PARAM_INT);
        $stmt->bindValue(4, $arr['pstatus'], PDO::PARAM_INT);
        $stmt->bindValue(5, $arr['mstatus'], PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = $scode['success'];
        } else {
            $result = $scode['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 修改日記
    * @return 回傳狀態
    */
    public static function update($arr, $scode)
    {
        Model::set();
        $result = null;
        $qs = 'UPDATE diary SET oday = ?, eday = now(), dtext = ?, pstatus = ?, mstatus = ? WHERE oday = ? AND uid = ?';
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $arr['soday'], PDO::PARAM_STR);
        $stmt->bindValue(2, $arr['dtext'], PDO::PARAM_STR);
        $stmt->bindValue(3, $arr['pstatus'], PDO::PARAM_INT);
        $stmt->bindValue(4, $arr['mstatus'], PDO::PARAM_INT);
        $stmt->bindValue(5, $arr['oday'], PDO::PARAM_INT);
        $stmt->bindValue(6, $arr['uid'], PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = $scode['success'];
            if ($stmt->rowCount() == 0) {
                $result = $scode['datanot'];
            }
        } else {
            $result = $scode['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 刪除單筆日記
    * @return 回傳狀態
    */
    public static function delete($arr, $scode)
    {
        Model::set();
        $result = null;
        $qs = 'DELETE FROM diary WHERE id = ? AND uid = ?';
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $arr['did'], PDO::PARAM_INT);
        $stmt->bindValue(2, $arr['uid'], PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = $scode['success'];
            if ($stmt->rowCount() == 0) {
                $result = $scode['datanot'];
            }
        } else {
            $result = $scode['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 查詢日記(依日記日期)
    * diary create function use
    * @return 成功回傳查詢資料，失敗回傳狀態
    */
    public static function selectday($arr, $scode)
    {
        Model::set();
        $result = null;
        $qs = 'SELECT * FROM diary WHERE oday = ? AND uid = ?';
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $arr['oday'], PDO::PARAM_STR);
        $stmt->bindValue(2, $arr['uid'], PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!is_array($result)) {
                $result = true;
            } else {
                $result = $scode['datarepeat'];
            }
        } else {
            $result = $scode['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 查詢日記(依日記編號)
    * diary delete function use
    * @return 成功回傳查詢資料，失敗回傳狀態
    */
    public static function selectid($arr, $scode)
    {
        Model::set();
        $result = null;
        $qs = 'SELECT * FROM diary WHERE id = ? AND uid = ?';
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $arr['did'], PDO::PARAM_INT);
        $stmt->bindValue(2, $arr['uid'], PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if (is_array($result)) {
                $result = true;
            } else {
                $result = $scode['datanot'];
            }
        } else {
            $result = $scode['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 查詢所有日記
    * @return 成功回傳查詢資料，失敗回傳狀態
    */
    public static function selectall($arr, $scode)
    {
        Model::set();
        $result = null;
        $qs = 'SELECT id AS diaryid, cday AS firstday, oday AS day, eday AS lastdatetime,
        dtext AS content, pstatus AS status1, mstatus AS status2, uid AS user
        FROM diary WHERE uid = ?';
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $arr['uid'], PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (empty($result)) {
                $result = $scode['datanot'];
            }
        } else {
            $result = $scode['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }
    
    /**
    * 查詢公開及擁有者私密日記
    * @return 成功回傳查詢資料，失敗回傳狀態
    */
    public static function selectpublic($arr, $scode)
    {
        Model::set();
        $qs = 'SELECT id AS diaryid, cday AS firstday, oday AS day, eday AS lastdatetime,
        dtext AS content, pstatus AS status1, mstatus AS status2, uid AS user
        FROM diary WHERE pstatus = 1 OR uid = ?';
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $arr['uid'], PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = $stmt->fetchall(PDO::FETCH_ASSOC);
            if (empty($result)) {
                $result = $scode['datanot'];
            }
        } else {
            $result = $scode['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 查詢可留言日記
    * message create function use
    * @return 成功回傳true，失敗回傳狀態
    */
    public static function selectmstatus($arr, $scode)
    {
        Model::set();
        $qs = 'SELECT * FROM diary WHERE id = ? AND mstatus = 1';
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $arr['did'], PDO::PARAM_INT);
        if ($stmt->execute()) {
            $rb = $stmt->fetch(PDO::FETCH_ASSOC);
            if (is_array($rb)) {
                $result = true;
            } else {
                $result = $scode['datamstatusnot'];
            }
        } else {
            $result = $scode['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 修改日記內容-理財系統使用
    * @return 回傳狀態
    */
    public static function updateforbill($arr, $scode)
    {
        Model::set();
        $result = null;
        $qs = 'UPDATE diary SET eday = now(), dtext = CONCAT(?, "\n", ?) WHERE oday = ? AND uid = ?';
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $arr['content'], PDO::PARAM_STR);
        $stmt->bindValue(2, $arr['dtext'], PDO::PARAM_STR);
        $stmt->bindValue(3, $arr['oday'], PDO::PARAM_STR);
        $stmt->bindValue(4, $arr['uid'], PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = $scode['success'];
            if ($stmt->rowCount() == 0) {
                $result = $scode['datanot'];
            }
        } else {
            $result = $scode['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 查詢日記(依日記日期)-理財系統使用
    * diary updateforbill use
    * @return 成功回傳一維陣列，失敗回傳狀態
    */
    public static function selectdayforbill($arr, $scode)
    {
        Model::set();
        $result = null;
        $qs = 'SELECT dtext AS content FROM diary WHERE oday = ? AND uid = ?';
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $arr['oday'], PDO::PARAM_STR);
        $stmt->bindValue(2, $arr['uid'], PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!is_array($result)) {
                $result = $scode['datanot'];
            }
        } else {
            $result = $scode['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 修改日記內容-理財系統使用-Transaction
    * @return 回傳狀態
    */
    public static function updateforbilling($arr, $scode)
    {
        try {
            $pdo = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
            $pdo->beginTransaction();
            $qs = 'SELECT dtext AS content FROM diary WHERE oday = ? AND uid = ?';
            $stmt = $pdo->prepare($qs);
            $stmt->bindValue(1, $arr['oday'], PDO::PARAM_STR);
            $stmt->bindValue(2, $arr['uid'], PDO::PARAM_INT);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_COLUMN);
            $stmt->closeCursor();
            if ($result === false) {
                return $scode['datanot'];
            }
            $qs = 'UPDATE diary SET eday = now(), dtext = CONCAT(?, "\n", ?) WHERE oday = ? AND uid = ?';
            $stmt = $pdo->prepare($qs);
            $stmt->bindValue(1, $result, PDO::PARAM_STR);
            $stmt->bindValue(2, $arr['dtext'], PDO::PARAM_STR);
            $stmt->bindValue(3, $arr['oday'], PDO::PARAM_STR);
            $stmt->bindValue(4, $arr['uid'], PDO::PARAM_INT);
            $stmt->execute();
            $rc = $pdo->commit();
            if ($rc === true) {
                $msg = $scode['success'];
            }
        } catch (PDOException $e) {
            $pdo->rollback();
            $msg = $scode['error'];
        }
        $stmt->closeCursor();
        return $msg;
    }
}
