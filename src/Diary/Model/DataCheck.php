<?php
namespace Diary\Model;

use Diary\Lib\CheckInput;

class DataCheck
{
    /**
    * 判斷使用者輸入內容,格式正確才帶入陣列
    */
    public static function userset($arr, $scode)
    {
        $uarr = array();
        $uarr['uid'] = $arr['user'];
        if (CheckInput::checkDate($arr['day'], $scode) === true) {
            $uarr['oday'] = $arr['day'];
        }
        if (CheckInput::checkDate($arr['sday'], $scode) === true) {
            $uarr['soday'] = $arr['sday'];
        }
        if (is_string($arr['content'])) {
            $uarr['dtext'] = $arr['content'];
        }
        if ($arr['status1'] === 1 || $arr['status1'] === 0) {
            $uarr['pstatus'] = $arr['status1'];
        }
        if ($arr['status2'] === 1 || $arr['status2'] === 0) {
            $uarr['mstatus'] = $arr['status2'];
        }
        if (is_string($arr['message'])) {
            $uarr['dmessage'] = $arr['message'];
        }
        if (is_int($arr['diaryid'])) {
            $uarr['did'] = $arr['diaryid'];
        }
        if (is_int($arr['messageid'])) {
            $uarr['mid'] = $arr['messageid'];
        }
        if (is_string($arr['reply'])) {
            $uarr['dmreply'] = $arr['reply'];
        }
        if ($arr['status3'] === 1 || $arr['status3'] === 0) {
            $uarr['mmstatus'] = $arr['status3'];
        }
        return $uarr;
    }

    /**
    * 判斷新增日記資料是否完整
    */
    public static function diarycreate($arr, $scode)
    {
        if (isset($arr['uid']) && isset($arr['dtext']) && isset($arr['pstatus'])
         && isset($arr['mstatus']) && isset($arr['oday'])) {
            return true;
        } else {
            return $scode['dataerror'];
        }
    }

    /**
    * 判斷修改日記資料是否完整
    */
    public static function diaryupdate($arr, $scode)
    {
        if (isset($arr['uid']) && isset($arr['oday']) && isset($arr['soday'])
         && isset($arr['dtext']) && isset($arr['pstatus']) && isset($arr['mstatus'])) {
            return true;
        } else {
            return $scode['dataerror'];
        }
    }

    /**
    * 判斷刪除日記資料是否完整
    */
    public static function diarydelete($arr, $scode)
    {
        if (isset($arr['uid']) && isset($arr['did'])) {
            return true;
        } else {
            return $scode['dataerror'];
        }
    }

    /**
    * 判斷顯示日記(uid)資料是否完整
    */
    public static function diaryselectall($arr, $scode)
    {
        if (isset($arr['uid'])) {
            return true;
        } else {
            return $scode['dataerror'];
        }
    }

     /**
    * 判斷顯示公開及擁有者私密日記資料是否完整
    */
    public static function diaryselectpublic($arr, $scode)
    {
        if (isset($arr['uid'])) {
            return true;
        } else {
            return $scode['dataerror'];
        }
    }
       
    /**
    * 判斷新增留言資料是否完整
    */
    public static function messagecreate($arr, $scode)
    {
        if (isset($arr['uid']) && isset($arr['dmessage'])
         && isset($arr['mmstatus']) && isset($arr['did'])) {
            return true;
        } else {
            return $scode['dataerror'];
        }
    }

    /**
    * 判斷查詢留言資料是否完整
    */
    public static function messageselect($arr, $scode)
    {
        if (isset($arr['uid']) && isset($arr['did'])) {
            return true;
        } else {
            return $scode['dataerror'];
        }
    }

    /**
    * 判斷回覆留言資料是否完整
    */
    public static function messageupdate($arr, $scode)
    {
        if (isset($arr['uid']) && isset($arr['dmreply']) && isset($arr['mid'])) {
            return true;
        } else {
            return $scode['dataerror'];
        }
    }

    /**
    * 判斷刪除留言資料是否完整
    */
    public static function messagedelete($arr, $scode)
    {
        if (isset($arr['uid']) && isset($arr['mid'])) {
            return true;
        } else {
            return $scode['dataerror'];
        }
    }

    /**
    * 判斷修改日記內容-理財系統使用輸入資料是否完整
    */
    public static function diaryupdateforbill($arr, $scode)
    {
        if (isset($arr['uid']) && isset($arr['dtext']) && isset($arr['oday'])) {
            return true;
        } else {
            return $scode['dataerror'];
        }
    }
}
