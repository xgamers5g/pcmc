<?php
namespace Diary\Model;

use Diary\Model\DataCheck;
use Diary\Lib\CheckInput;
use Diary\Model\DiaryModel;
use Diary\Model\MessageModel;

class DiaryAssembly
{
    /**
    * 新增日記
    * @return 回傳狀態
    */
    public static function create($arr, $scode)
    {
        $dc = DataCheck::diarycreate($arr, $scode);
        if ($dc === true) {
            $dmsd = DiaryModel::selectday($arr, $scode);
            if ($dmsd === true) {
                $ci = CheckInput::check7day($arr['oday'], $scode);
                if ($ci === true) {
                    return DiaryModel::create($arr, $scode);
                } else {
                    return $ci;
                }
            } else {
                return $dmsd;
            }
        } else {
            return $dc;
        }
    }

    /**
    * 修改日記
    * @return 回傳狀態
    */
    public static function update($arr, $scode)
    {
        $dc = DataCheck::diaryupdate($arr, $scode);
        if ($dc === true) {
            $ci = CheckInput::check7day($arr['oday'], $scode);
            if ($ci === true) {
                return DiaryModel::update($arr, $scode);
            } else {
                return $ci;
            }
        } else {
            return $dc;
        }
    }

    /**
    * 刪除單筆日記
    * @return 回傳狀態
    */
    public static function delete($arr, $scode)
    {
        $dc = DataCheck::diarydelete($arr, $scode);
        if ($dc === true) {
            $dmsi = DiaryModel::selectid($arr, $scode);
            if ($dmsi === true) {
                $md = MessageModel::deletedid($arr, $scode);
                if ($md === true) {
                    return DiaryModel::delete($arr, $scode);
                } else {
                    return $md;
                }
            } else {
                return $dmsi;
            }
        } else {
            return $dc;
        }
    }

    /**
    * 顯示日記(uid)
    * @return 成功回傳資料(二維陣列)，失敗回傳狀態
    */
    public static function selectall($arr, $scode)
    {
        $dc = DataCheck::diaryselectall($arr, $scode);
        if ($dc === true) {
                return DiaryModel::selectall($arr, $scode);
        } else {
            return $dc;
        }
    }

    /**
    * 顯示公開及擁有者私密日記
    * @return 成功回傳資料(二維陣列)，失敗回傳狀態
    */
    public static function selectpublic($arr, $scode)
    {
        $dc = DataCheck::diaryselectpublic($arr, $scode);
        if ($dc === true) {
                return DiaryModel::selectpublic($arr, $scode);
        } else {
            return $dc;
        }
    }

    /**
    * 新增日記
    * @return 回傳狀態
    */
    public static function createmessage($arr, $scode)
    {
        $dc = DataCheck::messagecreate($arr, $scode);
        if ($dc === true) {
            $dmsm = DiaryModel::selectmstatus($arr, $scode);
            if ($dmsm === true) {
                return MessageModel::create($arr, $scode);
            } else {
                return $dmsm;
            }
        } else {
            return $dc;
        }
    }

    /**
    * 查詢留言
    * @return 成功回傳資料(二維陣列)，失敗回傳狀態
    */
    public static function selectmessage($arr, $scode)
    {
        $dc = DataCheck::messageselect($arr, $scode);
        if ($dc === true) {
            return MessageModel::select($arr, $scode);
        } else {
            return $dc;
        }
    }

    /**
    * 回覆留言
    * @return 回傳狀態
    */
    public static function replymessage($arr, $scode)
    {
        $dc = DataCheck::messageupdate($arr, $scode);
        if ($dc === true) {
            return MessageModel::update($arr, $scode);
        } else {
            return $dc;
        }
    }

    /**
    * 刪除單筆留言
    * @return 回傳狀態
    */
    public static function deletemessage($arr, $scode)
    {
        $dc = DataCheck::messagedelete($arr, $scode);
        if ($dc === true) {
            return MessageModel::delete($arr, $scode);
        } else {
            return $dc;
        }
    }

    /**
    * 修改日記內容-理財系統使用
    * @return 回傳狀態
    */
    public static function updateforbill($arr, $scode)
    {
        $dc = DataCheck::diaryupdateforbill($arr, $scode);
        if ($dc === true) {
            $ci = CheckInput::check7day($arr['oday'], $scode);
            if ($ci === true) {
                $dmsdb = DiaryModel::selectdayforbill($arr, $scode);
                if (count($dmsdb) === 1) {
                    return DiaryModel::updateforbill(array_merge($arr, $dmsdb), $scode);
                } else {
                    return $dmsdb;
                }
            } else {
                return $ci;
            }
        } else {
            return $dc;
        }
    }

    /**
    * 修改日記內容-理財系統使用-Transaction
    * @return 回傳狀態
    */
    public static function updateforbilling($arr, $scode)
    {
        $dc = DataCheck::diaryupdateforbill($arr, $scode);
        if ($dc === true) {
            $ci = CheckInput::check7day($arr['oday'], $scode);
            if ($ci === true) {
                // sleep(8);
                return DiaryModel::updateforbilling($arr, $scode);
            } else {
                return $ci;
            }
        } else {
            return $dc;
        }
    }
}
