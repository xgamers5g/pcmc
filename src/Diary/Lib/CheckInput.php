<?php
namespace Diary\Lib;

use DateTime;
use Exception;

class CheckInput
{
    /**
    * 檢查時間是否符合格式
    * @param $date 為輸入的日期
    * @return 成功回傳true, 失敗回傳狀態
    */
    public static function checkDate($date, $scode)
    {
        try {
            $dt = new DateTime($date);
            $ck = $dt->format("Y-m-d");
            if ($ck == $date) {
                return true;
            } else {
                return $scode['dataerror'];
            }
        } catch (Exception $e) {
            return $scode['dataerror'];
        }
    }

    /**
    * 檢查時間是否為七天前或未來
    * @param $date 為現在時間
    * @return 成功回傳true, 失敗回傳狀態
    */
    public static function check7day($day, $scode)
    {
        $date = new DateTime('now');
        if ($day > $date->format('Y-m-d') || $day < $date->modify('-7 day')->format('Y-m-d')) {
            return $scode['datanotday'];
        } else {
            return true;
        }
    }
}
