<?php
namespace Diary\Lib;

class StatusCode
{
    /**
    * 定義所有輸出會用到的狀態碼及說明
    */
    public static function responserule()
    {
        return $detail = array(
            'success' => array(
                'statuscode' => '200',
                'detail' => '執行成功'
            ),
            'datanot' => array(
                'statuscode' => '401',
                'detail' => '沒有資料'
            ),
            'datanotday' => array(
                'statuscode' => '401',
                'detail' => '日記日期太遙遠或為未來日記'
            ),
            'dataerror' => array(
                'statuscode' => '401',
                'detail' => '資料錯誤或不完整'
            ),
            'datarepeat' => array(
                'statuscode' => '401',
                'detail' => '已有該日日記'
            ),
            'datamstatusnot' => array(
                'statuscode' => '401',
                'detail' => '此日記未開放留言'
            ),
            'error' => array(
                'statuscode' => '404',
                'detail' => '系統異常,請聯絡管理員'
            ),
            'databaseerror' => array(
                'statuscode' => '599',
                'detail' => '資料庫異常,請聯絡管理員'
            )
        );
    }
}
