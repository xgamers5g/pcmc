<?php
namespace Diary\Controller;

use Diary\System\Controller;
use Diary\Lib\StatusCode;
use Diary\Lib\JsonCode;
use Diary\Model\DiaryAssembly;
use Diary\Model\DataCheck;

class DiaryController extends Controller
{
    /**
     * 使用者輸入內容
     */
    public function __construct()
    {
        self::init();
        $arr = [
            'day' => self::$data->day,
            'sday' => self::$data->sday,
            'content' => self::$data->content,
            'user' => self::$authdata['id'],
            'status1' => self::$data->status1,
            'status2' => self::$data->status2,
            'diaryid' => self::$data->diaryid,
            'message' => self::$data->message,
            'status3' => self::$data->status3,
            'reply' => self::$data->reply,
            'messageid' => self::$data->messageid
        ];
        $this->scode = StatusCode::responserule();
        $this->uarr = DataCheck::userset($arr, $this->scode);
        $this->auth = array('Authorization' => self::$authdata['Authorization']);
    }

    /**
    * 新增日記
    * @return 回傳狀態
    */
    public function create()
    {
        echo JsonCode::jsonencode(array_merge($this->auth, DiaryAssembly::create($this->uarr, $this->scode)));
    }

    /**
    * 修改日記
    * @return 回傳狀態
    */
    public function update()
    {
        echo JsonCode::jsonencode(array_merge($this->auth, DiaryAssembly::update($this->uarr, $this->scode)));
    }

    /**
    * 刪除單筆日記
    * @return 回傳狀態
    */
    public function delete()
    {
        echo JsonCode::jsonencode(array_merge($this->auth, DiaryAssembly::delete($this->uarr, $this->scode)));
    }

    /**
    * 顯示日記(uid)
    * @return 成功回傳資料(二維陣列)，失敗回傳狀態
    */
    public function selectall()
    {
        echo JsonCode::jsonencode(array_merge($this->auth, DiaryAssembly::selectall($this->uarr, $this->scode)));
    }

    /**
    * 顯示公開及擁有者私密日記
    * @return 成功回傳資料(二維陣列)，失敗回傳狀態
    */
    public function selectpublic()
    {
        echo JsonCode::jsonencode(array_merge($this->auth, DiaryAssembly::selectpublic($this->uarr, $this->scode)));
    }
    
    /**
    * 新增日記
    * @return 回傳狀態
    */
    public function createmessage()
    {
        echo JsonCode::jsonencode(array_merge($this->auth, DiaryAssembly::createmessage($this->uarr, $this->scode)));
    }

    /**
    * 查詢留言
    * @return 成功回傳資料(二維陣列)，失敗回傳狀態
    */
    public function selectmessage()
    {
        echo JsonCode::jsonencode(array_merge($this->auth, DiaryAssembly::selectmessage($this->uarr, $this->scode)));
    }

    /**
    * 回覆留言
    * @return 回傳狀態
    */
    public function replymessage()
    {
        echo JsonCode::jsonencode(array_merge($this->auth, DiaryAssembly::replymessage($this->uarr, $this->scode)));
    }

    /**
    * 刪除單筆留言
    * @return 回傳狀態
    */
    public function deletemessage()
    {
        echo JsonCode::jsonencode(array_merge($this->auth, DiaryAssembly::deletemessage($this->uarr, $this->scode)));
    }

    /**
    * 修改日記內容-理財系統使用
    * @return 回傳狀態
    */
    public function updateforbill()
    {
        echo JsonCode::jsonencode(array_merge($this->auth, DiaryAssembly::updateforbill($this->uarr, $this->scode)));
    }
    
    /**
    * 修改日記內容-理財系統使用-Transaction
    * @return 回傳狀態
    */
    public function updateforbilling()
    {
        echo JsonCode::jsonencode(array_merge($this->auth, DiaryAssembly::updateforbilling($this->uarr, $this->scode)));
    }
}
