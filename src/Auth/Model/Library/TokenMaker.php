<?php
namespace Auth\Model\Library;

/**
 * TokenMaker class
 *
 * 這是一個令牌(token)製造機
 *
 * 借自bshaffer/oauth2.0 server Framework中的access_token運算方式
 *
 * 再加入長度控制增加其使用彈性。
 *
 * 2014.09.29 Garth_Wang
 */

class TokenMaker
{
    public static function getInstance()
    {
        return new self;
    }
    /**
     * 產出Token
     * @param int $len 表示Token的字串長度
     * @return string 返回Token字串
     */
    public function generateToken($len)
    {
        $randomData = mt_rand() . mt_rand() . mt_rand() . mt_rand() . microtime(true) . uniqid(mt_rand(), true);
        return substr(hash('sha512', $randomData), 0, $len);
    }
}
