<?php
namespace Auth\Model\Library;

/**
 * 用正規式驗證資料欄位的類別
 */

class ElementValidator
{
    /**
     * @return 回傳ElementValidator物件
     */
    public static function getInstance()
    {
        return new self;
    }
    /**
     * 驗證Authorization Token
     *
     * 規則: 長度必定是40，A-Z, a-z, 0-9
     *
     * @param String $value 傳入要驗證的字串
     *
     * @return boolean 通過驗證回傳true,失敗false
     */
    public function validateAccessToken($value)
    {
        if (!preg_match("/^[A-Za-z0-9]+$/", $value)) {
            return false;
        }
        if (strlen($value) != 40) {
            return false;
        }
        return true;
    }
    /**
     * 驗證UserName
     *
     * 規則: 長度要大於5, A-Z, a-z, 0-9
     *
     * @param String $value 傳入要驗證的字串
     *
     * @return boolean 通過驗證回傳true,失敗false
     */
    public function validateUserName($value)
    {
        if (!preg_match("/^[A-Za-z0-9]+$/", $value)) {
            return false;
        }
        if (strlen($value) < 5) {
            return false;
        }
        return true;
    }
    /**
     * 驗證UserSecret
     *
     * 規則: 長度要大於5, A-Z, a-z, 0-9
     *
     * @param String $value 傳入要驗證的字串
     *
     * @return boolean 通過驗證回傳true,失敗false
     */
    public function validateUserSecret($value)
    {
        if (!preg_match("/^[A-Za-z0-9]+$/", $value)) {
            return false;
        }
        if (strlen($value) < 5) {
            return false;
        }
        return true;
    }
    /**
     * 驗證email
     *
     * 規則: 只要是email就行
     *
     * @param String $value 傳入要驗證的字串
     *
     * @return boolean 通過驗證回傳true,失敗false
     */
    public function validateEmail($value)
    {
        if (filter_var($value, FILTER_VALIDATE_EMAIL) && strlen($value)>=10) {
            return true;
        } else {
            return false;
        }
    }
}
