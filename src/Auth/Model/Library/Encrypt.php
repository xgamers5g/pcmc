<?php
namespace Auth\Model\Library;

/**
 * 密碼加密工具(library)
 */

class Encrypt
{
    public static function getInstance()
    {
        return new self;
    }
    /**
     * 多型
     * 單傳入secret，回傳usersecret與salthash
     * @param string $secret secret
     * @return array() {
     *      'salthash' => string//encrypt
     *      'usersecret' => string//hash
     * }
     * 傳入 secret, salthash，回傳usersecret
     * @param string $secret secret
     * @param string $salthash salthash
     * @return string $usersecret usersecret
     */
    public function secretEncode($secret, $salthash = null)
    {
        //只給secret代表產生，新增時調用。
        if (!$secret) {
            return;
        }
        //有給salt直接回傳密碼，比對時調用。
        if ($salthash) {
            return hash_pbkdf2("sha256", $secret, base64_decode($salthash), 1000, 30);
        } else {
            $newsalt = substr(md5(uniqid(rand(), true)), 0, 6);
            return array(
                'salthash' => base64_encode($newsalt),
                'usersecret' => hash_pbkdf2("sha256", $secret, $newsalt, 1000, 30)
            );
        }
    }
    /**
     * 拿到比對密碼用的salt
     * @param string $username username
     */
    public function getSaltHash(\PDO $pdo, $email)
    {
        $qs = "select salthash from user where email = ?";
        $stmt = $pdo->prepare($qs);
        $stmt->bindValue(1, $email, \PDO::PARAM_STR);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        if ($res) {
            return $res['salthash'];
        } else {
            return false;
        }
    }
}
