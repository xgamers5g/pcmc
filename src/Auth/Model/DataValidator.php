<?php
namespace Auth\Model;

/**
 * 這是用來進行傳入資料結構驗證的類別
 *
 * 只對Facade負責。
 */

class DataValidator
{
    ///裝載要進行驗證的資料
    private static $data = null;
    ///Auth\Model\Response物件
    private static $response = null;
    ///Auth\Model\Library\ElementValidator物件
    private static $elementvalidator = null;
    private function __construct()
    {
        //nothing to do
    }
    /**
     * 取回DataValidator物件
     *
     * @param \Auth\Model\Response $response 傳入response物件
     * @param \Auth\Model\Library\ElementValidator $elementvalidator
     * 傳入elementvalidator
     * @param Array $data 傳入要驗證的資料
     * @return Object 回傳DataValidator物件
     */
    public static function getInstance(
        \Auth\Model\Response $response,
        \Auth\Model\Library\ElementValidator $elementvalidator,
        $data = null
    ) {
        if (is_array($data)) {
            self::$data = $data;
        }
        self::$response = $response;
        self::$elementvalidator = $elementvalidator;
        return new self;
    }
    /**
     * 驗證註冊用的資料
     * 
     * 驗證通過
     * @return true
     *
     * 驗證失敗
     * @return array(
     *      'statuscode' => 狀態碼,
     *      'detail' => 狀態細節
     * )
     */
    public function validateCreateUserData()
    {
        $data = self::$data;
        //驗證是否三個值都存在，有才進入深層驗證。
        if (isset($data['email']) && isset($data['username']) && isset($data['usersecret'])) {
            //這裡開始驗證各項參數，只要沒過就會回傳狀態，都過了就回傳true
            //驗證email
            if (!self::$elementvalidator->validateEmail($data['email'])) {
                return self::$response->generateResponse('emailformaterror');
            }
            //驗證username
            if (!self::$elementvalidator->validateUserName($data['username'])) {
                return self::$response->generateResponse('usernameformaterror');
            }
            //驗證usersecret
            if (!self::$elementvalidator->validateUserSecret($data['usersecret'])) {
                return self::$response->generateResponse('usersecretformaterror');
            }
            return true;
            //資料有缺就直接回傳狀態。
        } else {
            return self::$response->generateResponse('dataerror');
        }
    }
    /**
     * 驗證登入用的資料
     * 
     * 驗證通過
     * @return true
     *
     * 驗證失敗
     * @return array(
     *      'statuscode' => 狀態碼,
     *      'detail' => 狀態細節
     * )
     */
    public function validateLoginData()
    {
        $data = self::$data;
        if (isset($data['email']) && isset($data['usersecret'])) {
            if (!self::$elementvalidator->validateEmail($data['email'])) {
                return self::$response->generateResponse('emailformaterror');
            }
            if (!self::$elementvalidator->validateUserSecret($data['usersecret'])) {
                return self::$response->generateResponse('usersecretformaterror');
            }
            return true;
        } else {
            return self::$response->generateResponse('dataerror');
        }
    }
    /**
     * 驗證更換密碼用的資料
     * 
     * 驗證通過
     * @return true
     *
     * 驗證失敗
     * @return array(
     *      'statuscode' => 狀態碼,
     *      'detail' => 狀態細節
     * )
     */
    public function validateSetSecretData()
    {
        $data = self::$data;
        if (isset($data['usersecret'])) {
            if (!self::$elementvalidator->validateUserSecret($data['usersecret'])) {
                return self::$response->generateResponse('usersecretformaterror');
            }
            return true;
        } else {
            return self::$response->generateResponse('dataerror');
        }
    }
}
