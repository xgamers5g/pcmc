<?php
namespace Auth\Model;

/**
 * UserTokenModel負責usertoken資料表的資料操作。
 * 
 * usertoken的資料儲存在Redis(1)中，前綴為usertoken。
 */

class UserTokenModel
{
    private static $redis = null;
    private static $response = null;
    private static $tokenmaker = null;
    private static $uid = null;
    private static $ip = null;
    private static $useragent = null;
    private static $authorization = null;
    private function __construct()
    {
        //nothing to do
    }
    /**
     * 用uid取回物件
     * @param \Redis $redis 傳入Redis物件
     * @param \Auth\Model\Response $response 傳入Response物件
     * @param \Auth\Model\Library\TokenMaker $tokenmaker 傳入TokenMaker物件
     * @param Integer $uid 傳入使用者id
     * @param String $ip 傳入HttpRequest的Ip
     * @param String $useragent 傳入HttpRequest的User-Agent
     *
     * 查有uid時
     * @return Object 返回UserTokenModel物件
     *
     * 查無uid時
     * @return null
     */
    public static function loadById(
        \Redis $redis,
        \Auth\Model\Response $response,
        \Auth\Model\Library\TokenMaker $tokenmaker,
        $uid,
        $ip,
        $useragent
    ) {
        //echo $uid;
        self::$redis = $redis;
        self::$response = $response;
        self::$tokenmaker = $tokenmaker;
        self::$uid = $uid;
        self::$ip = $ip;
        self::$useragent = $useragent;
        $obj = new self;
        $res = $obj->create();
        if (isset($res['Authorization'])) {
            self::$authorization = $res['Authorization'];
            return $obj;
        } else {
            return false;
        }
    }
    /**
     * 用Authorization Token取回物件
     * @param \Redis $redis 傳入Redis物件
     * @param \Auth\Model\Response $response 傳入Response物件
     * @param \Auth\Model\Library\TokenMaker $tokenmaker 傳入TokenMaker物件
     * @param String $token 傳入Authorization Token
     * @param String $ip 傳入HttpRequest的Ip
     * @param String $useragent 傳入HttpRequest的User-Agent
     *
     * 查有Authorization Token時
     * @return Object 返回UserTokenModel物件
     *
     * 查無Authorization Token或過期
     * @return array(
     *      'statuscode' => 狀態碼,
     *      'detail' 狀態細節
     * )
     */
    public static function loadByToken(
        \Redis $redis,
        \Auth\Model\Response $response,
        \Auth\Model\Library\TokenMaker $tokenmaker,
        $token,
        $ip,
        $useragent
        //這裡可以重構成用物件傳入
    ) {
        if ($redis->exists('usertoken'.$token)) {
            //usertoken_tokenkey
            $tk = json_decode(
                $redis->get('usertoken'.$token),
                true
            );
            //var_dump($tk);
            //usertoken_idkey
            $ik = json_decode(
                $redis->get('usertoken'.$tk['userid']),
                true
            );
            //echo 123;
            //var_dump($ik);
            if (time() <= $ik['expire']) {
                if ($ip == $ik['ip']) {
                    if ($useragent == $ik['useragent']) {
                        self::$redis = $redis;
                        self::$response = $response;
                        self::$tokenmaker = $tokenmaker;
                        self::$uid = $tk['userid'];
                        self::$ip = $ip;
                        self::$useragent = $useragent;
                        self::$authorization = $token;
                        $obj = new self;
                        $res = $obj->update();
                        if (!isset($res['Authorization'])) {
                            return $res;
                        }
                        self::$authorization = $res['Authorization'];
                    } else {
                        $obj = $response->generateResponse('tokenerror');
                    }
                } else {
                    $obj = $response->generateResponse('tokenerror');
                }
            } else {
                $obj = $response->generateResponse('tokenerror');
            }
        } else {
            $obj = $response->generateResponse('tokenerror');
        }
        return $obj;
    }
    private function create()
    {
        $newtoken = self::$tokenmaker->generateToken(40);
        //multi事務模式
        $res = self::$redis->multi()
            ->set(
                'usertoken'.self::$uid,
                json_encode(
                    array(
                        'ip' => self::$ip,
                        'useragent' => self::$useragent,
                        'access_token' => $newtoken,
                        'expire' => time() + 600
                    )
                )
            )
            ->set(
                'usertoken'.$newtoken,
                json_encode(
                    array(
                        'userid' => self::$uid
                    )
                )
            )
            //執行事務
            ->exec();
        if ($res[0] && $res[1]) {
            $data = self::$response->generateResponse('success');
            $data['login'] = true;
            $data['Authorization'] = $newtoken;
            return $data;
        } else {
            return self::$response->generateResponse('databaseerror');
        }
    }
    private function update()
    {
        $newtoken = self::$tokenmaker->generateToken(40);
        //watch+multi事務模式
        self::$redis->watch('usertoken'.self::$authorization);
        self::$redis->watch('usertoken'.self::$uid);
        $res = self::$redis->multi()
            ->delete('usertoken'.self::$authorization)
            ->set(
                'usertoken'.self::$uid,
                json_encode(
                    array(
                        'ip' => self::$ip,
                        'useragent' => self::$useragent,
                        'access_token' => $newtoken,
                        'expire' => time() + 600
                    )
                )
            )
            ->set(
                'usertoken'.$newtoken,
                json_encode(
                    array(
                        'userid' => self::$uid
                    )
                )
            )
            //事務執行
            ->exec();
        if ($res[0] && $res[1] && $res[2]) {
            $data = self::$response->generateResponse('success');
            $data['login'] = true;
            $data['Authorization'] = $newtoken;
            return $data;
        } else {
            return self::$response->generateResponse('databaseerror');
        }
    }
    /**
     * 刪除Authorization Token
     * @return array(
     *      'statuscode' => 狀態碼,
     *      'detail' => 狀態細節
     * )
     */
    public function delete()
    {
        self::$redis->watch('usertoken'.self::$uid);
        self::$redis->watch('usertoken'.self::$authorization);
        $res = self::$redis->multi()
            ->delete('usertoken'.self::$uid)
            ->delete('usertoken'.self::$authorization)
            ->exec();
        self::$authorization = null;
        if ($res[0] && $res[1]) {
            return self::$response->generateResponse('success');
        } else {
            return self::$response->generateResponse('databaseerror');
        }
    }
    /**
     * 查詢Authorization Token是否存在
     * @return boolean
     */
    public function isTokenExist()
    {
        $res = self::$redis->multi()
            ->exists('usertoken'.self::$uid)
            ->exists('usertoken'.self::$authorization)
            ->exec();
        if ($res[0] && $res[1]) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * 取回Uid
     * @return Integer
     */
    public function getUid()
    {
        return self::$uid;
    }
    /**
     * 取回Authorization Token
     * @return String
     */
    public function getToken()
    {
        return self::$authorization;
    }
}
