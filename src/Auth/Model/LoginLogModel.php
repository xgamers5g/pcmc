<?php
namespace Auth\Model;

/**
 * 這是用來寫登入紀錄的model
 *
 * 2014.10.29 Garth_Wang 30大壽無人問津SAD
 */

class LoginLogModel
{
    private static $pdo = null;
    private static $redis = null;
    private static $datetime = null;
    private static $email = null;
    private static $ip = null;
    private static $useragent = null;
    private static $action = null;
    private function __construct()
    {
        //nothing to do
    }
    /**
     * SiteAdministrator 限定
     */
    public static function getInstance(
        \PDO $pdo,
        \Redis $redis,
        \DateTime $datetime,
        $email,
        $ip,
        $useragent,
        $action
    ) {
        self::$pdo = $pdo;
        self::$redis = $redis;
        self::$datetime = $datetime;
        self::$email = $email;
        self::$ip = $ip;
        self::$useragent = $useragent;
        self::$action = $action;
        return new self;
    }
    public function createLog()
    {
        $qs = 'insert into loginlog(email, ip, useragent, action) values (?, ?, ?, ?)';
        $stmt = self::$pdo->prepare($qs);
        $stmt->bindValue(1, self::$email, \PDO::PARAM_STR);
        $stmt->bindValue(2, self::$ip, \PDO::PARAM_STR);
        $stmt->bindValue(3, self::$useragent, \PDO::PARAM_STR);
        $stmt->bindValue(4, self::$action, \PDO::PARAM_STR);
        $res = $stmt->execute();
        $stmt->closeCursor();
        if ($res) {
            $res = self::$redis->set(
                'loginlog'.self::$pdo->lastInsertId(),
                json_encode(
                    array(
                        'email' => self::$email,
                        'time' => self::$datetime->format('Y-m-d H:i:s'),
                        'ip' => self::$ip,
                        'useragent' => self::$useragent,
                        'action' => self::$action
                    )
                )
            );
            if ($res) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function getLogByDay()
    {

    }
    public function getLogByWeek()
    {

    }
    public function getLogByMonth()
    {

    }
    public function flushlog()
    {

    }
}
