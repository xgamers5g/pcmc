<?php
namespace Auth\Model;

/**
 * UserModel負責database中user資料表的資料操作。
 *
 * user的資料儲存在Mysql:pcmc:usertoken中。
 */

class UserModel
{
    private static $uid = null;
    private static $pdo = null;
    protected function __construct()
    {
        //nothing to do
    }
    /**
     * 用Login的方式取回物件
     * @param \PDO $pdo 傳入pdo物件
     * @param \Auth\Model\Library\Encrypt $encrypt 傳入Encrypt物件
     * @param \Auth\Model\Response $response 傳入Response物件
     * @param String $email 傳入登入用的email
     * @param String $usersecret 傳入登入用的secret
     * 
     * 登入成功
     * @return Object 回傳UserModel物件
     *
     * 登入失敗
     * @return null 回傳null
     */
    public static function loadByLogin(
        \PDO $pdo,
        \Auth\Model\Library\Encrypt $encrypt,
        \Auth\Model\Response $response,
        $email,
        $usersecret
    ) {
        $salt = $encrypt->getSaltHash($pdo, $email);
        $qs = 'select id from user where email = ? and usersecret = ?';
        $stmt = $pdo->prepare($qs);
        $stmt->bindValue(1, $email, \PDO::PARAM_INT);
        $stmt->bindValue(2, $encrypt->secretEncode($usersecret, $salt), \PDO::PARAM_STR);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (!$res) {
            return;
        }
        self::$uid = $res['id'];
        self::$pdo = $pdo;
        return new self;
    }
    /**
     * 用傳入uid的方式取回物件
     * @param \PDO $pdo pdo物件
     * @param Integer $uid 傳入使用者id
     * 
     * 查無uid時
     * @return null
     *
     * 查有uid時
     * @return Object 回傳UserModel物件
     */
    public static function loadById(\PDO $pdo, $uid)
    {
        $qs = 'select id from user where id = ?';
        $stmt = $pdo->prepare($qs);
        $stmt->bindValue(1, $uid, \PDO::PARAM_INT);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (!$res) {
            return;
        }
        self::$uid = $uid;
        self::$pdo = $pdo;
        return new self();
    }
    /**
     * 改密碼
     * @param \Auth\Model\Library\Encrypt $encrypt 傳入Encrypt物件
     * @param \Auth\Model\Response $response 傳入Response物件
     * @param Stinrg $secret 傳入新密碼
     * @return Array (
     *      'statuscode' => 狀態碼,
     *      'detail' => 狀態細節
     * )
     */
    public function setSecret(
        \Auth\Model\Library\Encrypt $encrypt,
        \Auth\Model\Response $response,
        $secret
    ) {
        //這裡用Encrypt類別把傳入的$secret作加密處理。
        $secret = $encrypt->secretEncode($secret);
        $qs = 'update user set salthash = ?, usersecret = ? where id = ?';
        $stmt = self::$pdo->prepare($qs);
        $stmt->bindValue(1, $secret['salthash']);
        $stmt->bindValue(2, $secret['usersecret'], \PDO::PARAM_STR);
        $stmt->bindValue(3, self::$uid, \PDO::PARAM_INT);
        $res = $stmt->execute();
        $stmt->closeCursor();
        if ($res) {
            return $response->generateResponse('success');
        } else {
            return $response->generateResponse('databaseerror');
        }
    }
    /**
     * 刪除使用者
     * @param \Auth\Model\Response $response 傳入Response物件
     * @return Array (
     *      'statuscode' => 狀態碼,
     *      'detail' => 狀態細節
     * )
     */
    public function deleteUser(\Auth\Model\Response $response)
    {
        $qs = 'delete user from user where id = ?';
        $stmt = self::$pdo->prepare($qs);
        $stmt->bindValue(1, self::$uid, \PDO::PARAM_INT);
        $res = $stmt->execute();
        $stmt->closeCursor();
        if ($res) {
            return $response->generateResponse('success');
        } else {
            return $response->generateResponse('databaseerror');
        }
    }
    /**
     * 取得User的資訊，包括id、email、username
     * @param \Auth\Model\Response $response 傳入Response物件
     * @return Array (
     *      'statuscode' => 狀態碼,
     *      'detail' => 狀態細節,
     *      'id' => 使用者id,
     *      'email' => 使用者email,
     *      'username' => 使用者名稱
     * )
     */
    public function getUserData(\Auth\Model\Response $response)
    {
        $qs ="select id,username,email from user where id = ?";
        $stmt = self::$pdo->prepare($qs);
        $stmt->bindValue(1, self::$uid, \PDO::PARAM_STR);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        if ($res) {
            $data = $response->generateResponse('success');
            $data['id'] = $res['id'];
            $data['email'] = $res['email'];
            $data['username'] = $res['username'];
            return $data;
        } else {
            return $response->generateResponse('databaseerror');
        }
    }
}
