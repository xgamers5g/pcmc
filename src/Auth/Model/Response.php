<?php
namespace Auth\Model;

/**
 * 這個類別負責系統狀態的產生
 *
 * 這裡定義了所有輸出會用到的狀態碼及說明
 */

class Response
{
    private static $detail = array(
        'success' => array(
            'statuscode' => '200',
            'detail' => '執行成功'
        ),
        'dataerror' => array(
            'statuscode' => '400',
            'detail' => '請求資料格式錯誤'
        ),
        'databaseerror' => array(
            'statuscode' => '599',
            'detail' => '資料庫已經爆炸了'
        ),
        'tokenerror' => array(
            'statuscode' => '401',
            'detail' => 'access_token不存在、不合法或過期'
        ),
        'tokenformaterror' => array(
            'statuscode' => '401',
            'detail' => '這是偽造的access_token'
        ),
        'loginerror' => array(
            'statuscode' => '401',
            'detail' => '登入帳號或是密碼錯誤'
        ),
        'emptyemail' => array(
            'statuscode' => '401',
            'detail' => '沒有輸入使用者email'
        ),
        'emptyusername' => array(
            'statuscode' => '401',
            'detail' => '沒有輸入使用者帳號'
        ),
        'useremailexist' => array(
            'statuscode' => '401',
            'detail' => '該電郵已經被使用，請嘗試其它電郵。'
        ),
        'usernameexist' => array(
            'statuscode' => '401',
            'detail' => '該帳號已經被使用，請嘗試其它帳號。'
        ),
        'useremailallow' => array(
            'statuscode' => '200',
            'detail' => '此電郵可以註冊'
        ),
        'usernameallow' => array(
            'statuscode' => '200',
            'detail' => '此帳號可以註冊'
        ),
        'usernameformaterror' => array(
            'statuscode' => '401',
            'detail' => '帳號格式不正確, 本系統只支援0-9,a-z,A-Z，長度必需超過5'
        ),
        'emptyusersecret' => array(
            'statuscode' => '401',
            'detail' => '沒有輸入密碼'
        ),
        'usersecretformaterror' => array(
            'statuscode' => '401',
            'detail' => '密碼格式不正確, 本系統只支援0-9,a-z,A-Z，長度必需超過5'
        ),
        'emailformaterror' => array(
            'statuscode' => '401',
            'detail' => 'email信箱格式不正確'
        ),
        'usersecretallow' => array(
            'statuscode' => '200',
            'detail' => '此密碼可以使用'
        ),
        'isnotlogin' => array(
            'statuscode' => '401',
            'detail' => '不在登入狀態'
        ),
        'index' => array(
            'statuscode' => '404',
            'detail' => '不正確的請求uri(get?post?)'
        ),
        'nofriend' => array(
            'statuscode' => '404',
            'detail' => '你沒有朋友'
        ),
        'islogin' => array(
            'statuscode' => '401',
            'detail' => '請先登出'
        )
    );
    /**
     *  @return Object 回傳Response物件
     */
    public static function getInstance()
    {
        return new self;
    }
    /**
     * 取得系統狀態
     *
     * @param String $status 傳入狀態代號，定義在$detail中。
     * @return Array 回傳系統狀態
     */
    public function generateResponse($status)
    {
        return self::$detail[$status];
    }
}
