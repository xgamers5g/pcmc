<?php
namespace Auth\Model;

/**
 * 這支class主要是用來取得HttpRequest的IP以及User-Agent
 *
 * 2014.10.6 Garth_Wang
 */

class UserStatus
{
    ///存放使用者ip
    public $ip = null;
    ///存放使用者的瀏覽器資訊
    public $useragent = null;
    private function __construct()
    {
        if (isset($_SERVER['REMOTE_ADDR'])) {
            $this->ip = $_SERVER['REMOTE_ADDR'];
        }
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $this->useragent = $_SERVER['HTTP_USER_AGENT'];
        }
    }
    /**
     * @return Object 返回 UserStatus物件
     */
    public static function getInstance()
    {
        return new self;
    }
    /**
     * 取回ip、User-Agent
     * @param String $ip 傳入ip，測試時DI用。
     * @param String $useragent 傳入User-Agent，測試時DI用。
     * @return array (
     *      'ip' => HttpRequest的ip,
     *      'useragent' => HttpRequest的useragent
     *)
     */
    public function getUserStatus($ip = null, $useragent = null)
    {
        if ($ip) {
            $this->ip = $ip;
        }
        if ($useragent) {
            $this->useragent = $useragent;
        }
        return array(
            'ip' => $this->ip,
            'useragent' => $this->useragent
        );
    }
}
