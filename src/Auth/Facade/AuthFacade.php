<?php
namespace Auth\Facade;

/**
 * AuthFacade 主要是用來統一接口的Class,
 *
 * 同時整合了全系統各Model的元件，
 *
 * 此Interface主要為「其它子系統」，以及「Action Class」提供服務。
 */

class AuthFacade
{
    private static $core = null;
    private static $login = null;
    private static $data = null;
    private static $utm = null;
    private function __construct()
    {
        //facade應為單例
    }
    /**
     * 取得Facade物件實例，
     *
     * 取得環境變數、從環境變數判定出本次Request所代表的身份。
     *
     * @param \Auth\System\Core $core 直接把模組核心傳入
     *
     * @return 回傳AuthFacade物件
     */
    public static function getInstance(\Auth\System\Core $core)
    {
        self::$core = $core;
        $postdata = $core->getPostHandler()->getJsonData();
        //如果postdata為array，與getUserStatus()進行merge後指定給self::$data
        if (is_array($postdata)) {
            self::$data = array_merge(
                $postdata,
                $core->getUserStatus()->getUserStatus()
            );
            //否則直接把getUserStatus直接指定給self::$data
        } else {
            self::$data = $core->getUserStatus()->getUserStatus();
        }
        //這裡加入本機登入判斷。
        if (isset(self::$data['clientip']) && self::$data['ip'] == '127.0.0.1') {
            self::$data['ip'] = self::$data['clientip'];
        }
        //初始化UserTokenModel，裡面儲存了登入狀態，並指定給self::$utm
        //這裡下判斷式，驗證是否有Authorization。
        if (isset(self::$data['Authorization'])) {
            self::$utm = $core->getUserTokenModelByToken(
                $core->getRedis(),
                $core->getResponse(),
                $core->getTokenMaker(),
                self::$data
            );
        }
        //如果self::$utm類型為UserTokenModel物件，
        //把uid及Authorization合併到self::$data中
        if (self::$utm instanceof \Auth\Model\UserTokenModel) {
            self::$data['uid'] = self::$utm->getUid();
            self::$data['Authorization'] = self::$utm->getToken();
            self::$login = true;
        }
        //驗證登入狀態
        return new self;
    }
    /**
     * 取得登入狀態
     * @return boolean
     *
     * true表示已登入，null表示為登入。
     */
    public function getloginState()
    {
        return self::$login;
    }
    /**
     * 取得使用者的資料
     *
     * 未登入時:
     * @return array (
     *      'statuscode' => '狀態碼',
     *      'detail' => '狀態細節'
     *)
     *
     * 偵測到登入時:
     * @return array (
     *      'statuscode' => '狀態碼',
     *      'detail' => '狀態細節',
     *      'uid' => '使用者id',
     *      'email' => '使用者email',
     *      'username' => '使用者名稱',
     *      'Authorization' => '身份令牌'
     * )
     */
    public function getUserData()
    {
        //登入鎖
        if (!self::$login) {
            return self::$core->getResponse()->generateResponse('isnotlogin');
        }
        //從core中取出usermodel,再從usermodel取出userdata，並指定給$data
        $data = self::$core
            ->getUserModelById(self::$core->getDb(), self::$data['uid'])
            ->getUserData(self::$core->getResponse());
        //把Authorization組進$data
        $data['Authorization'] = self::$data['Authorization'];
        return $data;
    }
    /**
     * 登出
     *
     * 未登入時:
     * @return array (
     *      'statuscode' => '狀態碼',
     *      'detail' => '狀態細節'
     *)
     *
     * 偵測到登入時:
     * @return array (
     *      'statuscode' => '狀態碼',
     *      'detail' => '狀態細節',
     * )
     */
    public function logout()
    {
        //登入鎖
        if (!self::$login) {
            return self::$core->getResponse()->generateResponse('isnotlogin');
        }
        //把資料庫中的Authorization砍掉，就完成登出作業了。
        $data = self::$utm->delete();
        return $data;
    }
    /**
     * 砍帳號
     *
     * 未登入時:
     * @return array (
     *      'statuscode' => '狀態碼',
     *      'detail' => '狀態細節'
     *)
     *
     * 偵測到登入時:
     * @return array (
     *      'statuscode' => '狀態碼',
     *      'detail' => '狀態細節'
     * )
     */
    public function deleteUser()
    {
        //登入鎖
        if (!self::$login) {
            return self::$core->getResponse()->generateResponse('isnotlogin');
        }
        //從core裡取出usermodel,再從usermodel執行deleteuser
        $data = self::$core
            ->getUserModelById(self::$core->getDb(), self::$data['uid'])
            ->deleteUser(self::$core->getResponse());
        //然後還要再把Authorization砍掉，就完美刪除了。
        self::$utm->delete();
        return $data;
    }
    /**
     * 註冊帳號
     * @return array(
     *      'statuscode' => '狀態碼',
     *      'detail' => '狀態細節'
     * )
     */
    public function createUser()
    {
        //登出鎖
        if (self::$login) {
            return self::$core->getResponse()->generateResponse('islogin');
        }
        //資料驗證
        //從core取出DataValidator，執行ValidateCreateUserData
        //並將其回傳值指定給$res
        $res = self::$core->getDataValidator(
            self::$core->getResponse(),
            self::$core->getElementValidator(),
            self::$data
        )->ValidateCreateUserData();
        //如果$res不是true代表驗證沒有過，就會將其return。
        //(裡面會是response狀態)
        if ($res!==true) {
            return $res;
        }
        //調用core的createUser，並return其回傳結果。
        return self::$core->createUser(
            self::$core->getDb(),
            self::$core->getEncrypt(),
            self::$core->getResponse(),
            self::$data['email'],
            self::$data['username'],
            self::$data['usersecret']
        );
    }
    /**
     * 砍帳號
     *
     * 登入失敗時:
     * @return array (
     *      'statuscode' => '狀態碼',
     *      'detail' => '狀態細節'
     * )
     *
     * 登入成功時:
     * @return array (
     *      'statuscode' => '狀態碼',
     *      'detail' => '狀態細節',
     *      'Authorization' => '身份令牌'
     * )
     */
    public function login()
    {
        //資料驗證
        $res = self::$core->getDataValidator(
            self::$core->getResponse(),
            self::$core->getElementValidator(),
            self::$data
        )->ValidateLoginData();
        if ($res!==true) {
            return $res;
        }
        //UserModel
        $um = self::$core->getUserModelByLogin(
            self::$core->getDb(),
            self::$core->getEncrypt(),
            self::$core->getResponse(),
            self::$data['email'],
            self::$data['usersecret']
        );
        if (!$um instanceof \Auth\Model\UserModel) {
            //建立登入失敗紀錄
            self::$core
                ->getLoginLogModel(
                    self::$core->getDb(),
                    self::$core->getRedis(),
                    self::$core->getDateTime(),
                    self::$data['email'],
                    self::$data['ip'],
                    self::$data['useragent'],
                    'fail'
                )
                ->createLog();
            return self::$core->getResponse()->generateResponse('loginerror');
        }
        //UserTokenModel
        $utm = self::$core->getUserTokenModelById(
            self::$core->getRedis(),
            self::$core->getResponse(),
            self::$core->getTokenMaker(),
            $um->getUserData(self::$core->getResponse())['id'],
            self::$data['ip'],
            self::$data['useragent']
        );
        //echo $um->getUserData(self::$core->getResponse())['id'];
        //var_dump($utm);
        if ($utm instanceof \Auth\Model\UserTokenModel) {
            $res = self::$core->getResponse()->generateResponse('success');
            $res['Authorization'] = $utm->getToken();
            //建立登入成功紀錄
            self::$core
                ->getLoginLogModel(
                    self::$core->getDb(),
                    self::$core->getRedis(),
                    self::$core->getDateTime(),
                    self::$data['email'],
                    self::$data['ip'],
                    self::$data['useragent'],
                    'success'
                )
                ->createLog();
            return $res;
        }
    }
    /**
     * 改密碼
     * @return array (
     *      'statuscode' => '狀態碼',
     *      'detail' => '狀態細節',
     *      'Authorization' => '身份令牌'
     * )
     */
    public function setSecret()
    {
        //登入鎖
        if (!self::$login) {
            return self::$core->getResponse()->generateResponse('isnotlogin');
        }
        //資料驗證
        $res = self::$core->getDataValidator(
            self::$core->getResponse(),
            self::$core->getElementValidator(),
            self::$data
        )->ValidateSetSecretData();
        if ($res!==true) {
            return $res;
        }
        //echo 123;
        //這邊執行setSecret
        $data = self::$core
            ->getUserModelById(self::$core->getDb(), self::$data['uid'])
            ->setSecret(
                self::$core->getEncrypt(),
                self::$core->getResponse(),
                self::$data['usersecret']
            );
        $data['Authorization'] = self::$data['Authorization'];
        return $data;
    }
    /**
     * 使用到get Method的時候會觸發index
     * @return array(
     *      'statuscode' => '狀態碼',
     *      'detail' => '狀態細節'
     * )
     */
    public function index()
    {
        return self::$core->getResponse()->generateResponse('index');
    }
    /**
     * Email欄位驗證
     * 規則：只要是Email就會過。
     * @return array(
     *      'statuscode' => '狀態碼',
     *      'detail' => '狀態細節'         
     * )
     */
    public function checkUserEmail()
    {
        if (!isset(self::$data['email'])) {
            return self::$core->getResponse()->generateResponse('emptyemail');
        }
        return self::$core->checkUserEmailExistForClient(
            self::$core->getDb(),
            self::$core->getResponse(),
            self::$core->getElementValidator(),
            self::$data['email']
        );
    }
    /**
     * UserName欄位驗證
     * 規則：A-Z,a-z,0-9，至少要有5個字元
     * @return array(
     *      'statuscode' => '狀態碼',
     *      'detail' => '狀態細節'         
     * )
     */
    public function checkUserName()
    {
        if (!isset(self::$data['username'])) {
            return self::$core->getResponse()->generateResponse('emptyusername');
        }
        return self::$core->checkUserNameExistForClient(
            self::$core->getDb(),
            self::$core->getResponse(),
            self::$core->getElementValidator(),
            self::$data['username']
        );
    }
    /**
     * Secret欄位驗證
     * 規則：A-Z,a-z,0-9，至少要有5個字元
     * @return array(
     *      'statuscode' => '狀態碼',
     *      'detail' => '狀態細節'         
     * )
     */
    public function checkUserSecret()
    {
        if (!isset(self::$data['usersecret'])) {
            return self::$core->getResponse()->generateResponse('emptyusersecret');
        }
        return self::$core->checkUserSecretForClient(
            self::$core->getResponse(),
            self::$core->getElementValidator(),
            self::$data['usersecret']
        );
    }
    public function __destruct()
    {
        self::$core = null;
        self::$login = null;
        self::$data = null;
        self::$utm = null;
    }
}
