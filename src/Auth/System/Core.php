<?php
namespace Auth\System;

use PDO;
use Redis;

/**
 * Core是整個系統的核心，包含了所有類別的初始化物件功能。
 * 以及重要的public method。
 *
 * Garth_Wang 2014.11.11
 */

class Core
{
    public static $config = null;
    protected static $redis = null;
    protected static $db = null;
    protected static $response = null;
    protected static $dbox = null;
    protected static $encrypt = null;
    protected static $tokenmaker = null;
    protected static $elementvalidator = null;
    protected static $posthandler = null;
    protected static $userstatus = null;
    protected static $usertokenmodel = null;
    protected static $usermodel = null;
    protected static $datavalidator = null;
    protected static $authfacade = null;
    protected static $datetime = null;
    protected static $loginlogmodel = null;
    protected function __construct()
    {
        //nothing to do
    }
    /**
     *  @return 回傳\Auth\System\Core物件
     */
    public static function getInstance()
    {
        if (!self::$config) {
            self::$config = include(BASE_DIR.'/config/config.php');
        }
        return new self;
    }
    /**
     * 更換Core用的Config
     * @param String $filename 指定設定檔檔名
     */
    public static function setConfig($filename)
    {
        self::$config = include(BASE_DIR.'/config/'.$filename.'.php');
    }
    /**
     * 初始化PDO物件，並回傳。
     * @return Object 返回PDO物件
     */
    public function getDb()
    {
        if (self::$db instanceof PDO) {
            return self::$db;
        }
        try {
            self::$db = new PDO(
                self::$config['db']['default']['constr'],
                self::$config['db']['default']['user'],
                self::$config['db']['default']['pass']
            );
        } catch (\Exception $e) {
            return;
        }
        return self::$db;
    }
    /**
     * 初始化Redis物件，並回傳。
     * @return Object 返回Redis物件
     */
    public function getRedis()
    {
        if (self::$redis instanceof Redis) {
            return self::$redis;
        }
        try {
            self::$redis = new Redis();
            self::$redis->pconnect(self::$config['redis']['location']);
            self::$redis->select(self::$config['redis']['database']);
        } catch (\Exception $e) {
            return;
        }
        return self::$redis;
    }
    /**
     * 初始化\Auth\Model\Response物件，並回傳。
     * @return Object 返回Redis物件
     */
    public function getResponse()
    {
        if (self::$response instanceof \Auth\Model\Response) {
            return self::$response;
        }
        self::$response = \Auth\Model\Response::getInstance();
        return self::$response;
    }
    /**
     * 初始化\Auth\Model\Library\Encrypt物件，並回傳。
     * @return Object 返回Encrypt物件
     */
    public function getEncrypt()
    {
        if (self::$encrypt instanceof \Auth\Model\Library\Encrypt) {
            return self::$encrypt;
        }
        self::$encrypt = \Auth\Model\Library\Encrypt::getInstance();
        return self::$encrypt;
    }
    /**
     * 初始化\Auth\Model\Library\TokenMaker物件，並回傳。
     * @return Object 返回TokenMaker物件
     */
    public function getTokenMaker()
    {
        if (self::$tokenmaker instanceof \Auth\Model\Library\TokenMaker) {
            return self::$tokenmaker;
        }
        self::$tokenmaker = \Auth\Model\Library\TokenMaker::getInstance();
        return self::$tokenmaker;
    }
    /**
     * 初始化\Auth\Model\Library\ElementValidator物件，並回傳。
     * @return Object 返回ElementValidator物件
     */
    public function getElementValidator()
    {
        if (self::$elementvalidator instanceof \Auth\Model\Library\ElementValidator) {
            return self::$elementvalidator;
        }
        self::$elementvalidator = \Auth\Model\Library\ElementValidator::getInstance();
        return self::$elementvalidator;
    }
    /**
     * 初始化\Auth\Model\PostHandler物件，並回傳。
     * @return Object 返回PostHandler物件
     */
    public function getPostHandler()
    {
        if (self::$posthandler instanceof \Auth\Model\PostHandler) {
            return self::$posthandler;
        }
        self::$posthandler = \Auth\Model\PostHandler::getInstance();
        return self::$posthandler;
    }
    /**
     * 初始化\Auth\Model\UserStatus物件，並回傳。
     * @return Object 返回UserStatus物件
     */
    public function getUserStatus()
    {
        if (self::$userstatus instanceof \Auth\Model\UserStatus) {
            return self::$userstatus;
        }
        self::$userstatus = \Auth\Model\UserStatus::getInstance();
        return self::$userstatus;
    }
    /**
     * 以Token初始化\Auth\Model\TokenModel物件，並回傳。
     * @param Redis $redis 傳入Redis物件
     * @param \Auth\Model\Response $response 傳入Response物件
     * @param \Auth\Model\Library\TokenMaker $tokenmaker 傳入tokenmaker物件
     * @param Array $data 傳入包含authorization、ip、useragent的陣列
     * @return Object 返回UserTokenModel物件
     */
    public function getUserTokenModelByToken(
        Redis $redis,
        \Auth\Model\Response $response,
        \Auth\Model\Library\TokenMaker $tokenmaker,
        $data = null
    ) {
        if (self::$usertokenmodel instanceof \Auth\Model\UserTokenModel) {
            return self::$usertokenmodel;
        }
        self::$usertokenmodel = \Auth\Model\UserTokenModel::loadByToken(
            $redis,
            $response,
            $tokenmaker,
            $data['Authorization'],
            $data['ip'],
            $data['useragent']
        );
        return self::$usertokenmodel;
    }
    /**
     * 以Uid初始化\Auth\Model\UserModel物件，並回傳。
     * @param \PDO $pdo 傳入PDO物件
     * @param Integer $uid 傳入使用者id
     * @return Object 返回UserModel物件
     */
    public function getUserModelById(\PDO $pdo, $uid)
    {
        if (self::$usermodel instanceof \Auth\Model\UserModel) {
            return self::$usermodel;

        }
        self::$usermodel = \Auth\Model\UserModel::loadById($pdo, $uid);
        return self::$usermodel;
    }
    /**
     * 創建使用者
     * @param \PDO $pdo 傳入PDO物件
     * @param \Auth\Model\Library\Encrypt $encrypt 傳入Encrypt物件
     * @param \Auth\Model\Response $response 傳入Response物件
     * @param String $email 傳入註冊用email
     * @param String @username 傳入註冊用UserName
     * @param String @usersecret 傳入註冊用UserSecret
     * @return array(
     *      'statuscode' => 狀態碼,
     *      'detail' => 狀態細節
     * )
     */
    public function createUser(
        \PDO $pdo,
        \Auth\Model\Library\Encrypt $encrypt,
        \Auth\Model\Response $response,
        $email,
        $username,
        $usersecret
    ) {
        $secret = $encrypt->secretEncode($usersecret);
        //var_dump($secret);
        $qs = "insert into user (email, salthash, username, usersecret) values(?, ?, ?, ?)";
        $stmt = $pdo->prepare($qs);
        $stmt->bindValue(1, $email, \PDO::PARAM_STR);
        $stmt->bindValue(2, $secret['salthash'], \PDO::PARAM_STR);
        $stmt->bindValue(3, $username, \PDO::PARAM_STR);
        $stmt->bindValue(4, $secret['usersecret'], \PDO::PARAM_STR);
        $res = $stmt->execute();
        $stmt->closeCursor();
        if ($res) {
            return $response->generateResponse('success');
        } else {
            return $response->generateResponse('databaseerror');
        }
    }
    /**
     * 初始化\Auth\Model\DataValidator物件，並回傳。
     * @return Object 返回DataValidator物件。
     */
    public function getDataValidator(
        \Auth\Model\Response $response,
        \Auth\Model\Library\ElementValidator $elementvalidator,
        $data = null
    ) {
        if (self::$datavalidator instanceof \Auth\Model\DataValidator) {
            return self::$datavalidator;
        }
        self::$datavalidator = \Auth\Model\DataValidator::getInstance($response, $elementvalidator, $data);
        return self::$datavalidator;
    }
    /**
     * 用Login的方式取回UserModel物件，並回傳。
     * @param \PDO $pdo 傳入PDO物件
     * @param \Auth\Model\Library\Encrypt $encrypt 傳入Encrypt物件。
     * @param \Auth\Model\Response $response 傳入Response物件。
     * @param String $email 傳入登入用email
     * @param String $usersecret 傳入登入用UserSecret
     */
    public function getUserModelByLogin(
        \PDO $pdo,
        \Auth\Model\Library\Encrypt $encrypt,
        \Auth\Model\Response $response,
        $email,
        $usersecret
    ) {
        if (self::$usermodel instanceof \Auth\Model\UserModel) {
            return self::$usermodel;
        }
        self::$usermodel = \Auth\Model\UserModel::loadByLogin($pdo, $encrypt, $response, $email, $usersecret);
        return self::$usermodel;
    }
    /**
     * 用Uid的方式取回UserTokenModel物件，並回傳。
     * @param \Redis $redis 傳入Redis物件
     * @param \Auth\Model\Response $response 傳入Response物件
     * @param \Auth\Model\Library\TokenMaker $tokenmaker 傳入TokenMaker物件
     * @param Interger $uid 傳入使用者id
     * @param String $ip 傳入HttpRequest的ip
     * @param String $useragent 傳入HttpRequest的useragent
     * @return Object 返回UserTokenModel物件
     */
    public function getUserTokenModelById(
        \Redis $redis,
        \Auth\Model\Response $response,
        \Auth\Model\Library\TokenMaker $tokenmaker,
        $uid,
        $ip,
        $useragent
    ) {
        //echo $uid;
        if (self::$usertokenmodel instanceof \Auth\Model\UserTokemModel) {
            return self::$usertokenmodel;
        }
        self::$usertokenmodel = \Auth\Model\UserTokenModel::loadById(
            $redis,
            $response,
            $tokenmaker,
            $uid,
            $ip,
            $useragent
        );
        return self::$usertokenmodel;
    }
    /**
     * 初始化\Auth\Facade\AuthFacade物件，並回傳。
     * @param \Auth\System\Core $core 傳入core物件。
     * @return Object 返回AuthFacade物件。
     */
    public function getAuthFacade(\Auth\System\Core $core)
    {
        if (self::$authfacade instanceof \Auth\Facade\AuthFacade) {
            return self::$authfacade;
        }
        self::$authfacade = \Auth\Facade\AuthFacade::getInstance($core);
        return self::$authfacade;
    }
    /**
     * 驗證Email
     * @param \PDO $pdo 傳入PDO物件
     * @param \Auth\Model\Response $response 傳入Response物件
     * @param \Auth\Model\Library\ElementValidator $elementvalidator 傳入ElementValidator物件
     * @param String $email 傳入要驗證的email
     * @return array(
     *      'statuscode' => 狀態碼,
     *      'detail' => 狀態細節
     * )
     */
    public function checkUserEmailExistForClient(
        \PDO $pdo,
        \Auth\Model\Response $response,
        \Auth\Model\Library\ElementValidator $elementvalidator,
        $email
    ) {
        if (!$email) {
            return $response->generateResponse('emptyemail');
        } elseif (!$elementvalidator->validateEmail($email)) {
            return $response->generateResponse('emailformaterror');
        }
        $qs = 'select email from user where email = ?';
        $stmt = $pdo->prepare($qs);
        $stmt->bindValue(1, $email, \PDO::PARAM_STR);
        if (!$stmt->execute()) {
            return $response->generateResponse('databaseerror');
        }
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        if ($res != null) {
            return $response->generateResponse('useremailexist');
        } else {
            return $response->generateResponse('useremailallow');
        }
    }
    /**
     * 驗證UserName
     * @param \PDO $pdo 傳入PDO物件
     * @param \Auth\Model\Response $response 傳入Response物件
     * @param \Auth\Model\Library\ElementValidator $elementvalidator 傳入ElementValidator物件
     * @param String $username 傳入要驗證的UserName
     * @return array(
     *      'statuscode' => 狀態碼,
     *      'detail' => 狀態細節
     * )
     */
    public function checkUserNameExistForClient(
        \PDO $pdo,
        \Auth\Model\Response $response,
        \Auth\Model\Library\ElementValidator $elementvalidator,
        $username
    ) {
        if (!$username) {
            return $response->generateResponse('emptyusername');
        } elseif (!$elementvalidator->validateUserName($username)) {
            return $response->generateResponse('usernameformaterror');
        }
        $qs = 'select username from user where username = ?';
        $stmt = $pdo->prepare($qs);
        $stmt->bindValue(1, $username, \PDO::PARAM_STR);
        if (!$stmt->execute()) {
            return $response->generateResponse('databaseerror');
        }
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        if ($res != null) {
            return $response->generateResponse('usernameexist');
        } else {
            return $response->generateResponse('usernameallow');
        }
    }
    /**
     * 驗證UserSecret
     * @param \Auth\Model\Response $response 傳入Response物件
     * @param \Auth\Model\Library\ElementValidator $elementvalidator 傳入ElementValidator物件
     * @param String $usersecret 傳入要驗證的UserSecret
     * @return array(
     *      'statuscode' => 狀態碼,
     *      'detail' => 狀態細節
     * )
     */
    public function checkUserSecretForClient(
        \Auth\Model\Response $response,
        \Auth\Model\Library\ElementValidator $elementvalidator,
        $usersecret
    ) {
        if (!$usersecret) {
            return $response->generateResponse('emptyusersecret');
        } elseif (!$elementvalidator->validateUserSecret($usersecret)) {
            return $response->generateResponse('usersecretformaterror');
        } else {
            return $response->generateResponse('usersecretallow');
        }
    }
    public function getDateTime()
    {
        if (self::$datetime instanceof \DateTime) {
            return self::$datetime;
        }
        self::$datetime = new \DateTime();
        return self::$datetime;
    }

    public function getLoginLogModel(
        \PDO $pdo,
        \Redis $redis,
        \DateTime $datetime,
        $email,
        $ip,
        $useragent,
        $action
    ) {
        if (self::$loginlogmodel instanceof \Auth\Model\LoginLogModel) {
            return self::$loginlogmodel;
        }
        self::$loginlogmodel = \Auth\Model\LoginLogModel::getInstance(
            $pdo,
            $redis,
            $datetime,
            $email,
            $ip,
            $useragent,
            $action
        );
        return self::$loginlogmodel;
    }
    public function __destruct()
    {
        self::$config = null;
        self::$redis = null;
        self::$db = null;
        self::$response = null;
        self::$dbox = null;
        self::$encrypt = null;
        self::$tokenmaker = null;
        self::$elementvalidator = null;
        self::$posthandler = null;
        self::$userstatus = null;
        self::$usertokenmodel = null;
        self::$usermodel = null;
        self::$datavalidator = null;
        self::$authfacade = null;
        self::$datetime = null;
        self::$loginlogmodel = null;
    }
}
