<?php
namespace Auth\Controller;

use Auth\Model\AuthValidator;
use Auth\Model\AuthModel;
use Auth\Model\UserTokenModel;
use Auth\Model\DataBox;
use Auth\Tools\ToolGetUserStatus;
use Auth\Tools\ToolPostHandler;
use Auth\View\ViewRender;
use Auth\Model\Response;

/**
 * 這是整個系統對Client端服務的Action Class
 *
 * 所有的行為都調用了Facade的Interface
 *
 * 2014.10.6 Garth_Wang
 */

class MainController
{
    private $facade = null;
    public function __construct($facade = null)
    {
        if ($facade) {
            $this->facade = $facade;
        } else {
            $res = \Auth\System\Core::getInstance();
            $this->facade = $res->getAuthFacade($res);
        }
    }
    /**
     * 錯誤使用URI時大部份會導向這個地方來提醒Client端 HttpMethod設定錯誤
     *
     *      Callback 
     *
     *          json Array(
     *              'statuscode' => 狀態碼,
     *              'detail'     => 狀態內容
     *          )
     */
    public function indexAction()
    {
        return json_encode($this->facade->index(), JSON_UNESCAPED_UNICODE);
    }
    /**
     * 登入
     *
     *      Endpoints
     *
     *          json Array(
     *              'email' => 使用者email,
     *              'usersecret' => 使用者密碼
     *          )
     *
     *      Callback 
     *
     *          json Array(
     *              'statuscode'    => 狀態碼,
     *              'detail'        => 狀態內容,
     *              'Authorization' => 使用者的登入令牌
     *          )
     */
    public function loginAction()
    {
        return json_encode($this->facade->login(), JSON_UNESCAPED_UNICODE);
    }
    /**
     * 登出
     *
     *      Endpoints
     *
     *          json Array(
     *              'Authorization' => 使用者的登入令牌
     *          )
     *
     *      Callback 
     *
     *          json Array(
     *              'statuscode'    => 狀態碼,
     *              'detail'        => 狀態內容,
     *              'Authorization' => 使用者的登入令牌
     *          )
     */
    public function logoutAction()
    {
        return json_encode($this->facade->logout(), JSON_UNESCAPED_UNICODE);
    }
    /**
     * 註冊
     *
     *      Endpoints
     *
     *          json Array(
     *              'email'        => 使用者要註冊的email,
     *              'username'     => 使用者要註冊的用戶名稱,
     *              'usersecret'   => 使用者要登入的密碼
     *          )
     *
     *      Callback 
     *
     *          json Array(
     *              'statuscode'    => 狀態碼,
     *              'detail'        => 狀態內容
     *          )
     */
    public function createAction()
    {
        return json_encode($this->facade->createUser(), JSON_UNESCAPED_UNICODE);
    }
    /**
     * 更改密碼
     *
     *      Endpoints
     *
     *          json Array(
     *              'usersecret'     => 使用者的新密碼,
     *              'Authorization'  => 使用者的登入令牌
     *          )
     *
     *      Callback 
     *
     *          json Array(
     *              'statuscode'     => 狀態碼,
     *              'detail'         => 狀態內容,
     *              'Authorization'  => 使用者的登入令牌
     *          )
     */
    public function setsecretAction()
    {
        return json_encode($this->facade->setSecret(), JSON_UNESCAPED_UNICODE);
    }
    /**
     * 自砍帳號
     *
     *      Endpoints
     *
     *          json Array(
     *              'Authorization'  => 使用者的登入令牌
     *          )
     *
     *      Callback 
     *
     *          json Array(
     *              'statuscode'     => 狀態碼,
     *              'detail'         => 狀態內容,
     *          )
     */
    public function deleteAction()
    {
        return json_encode($this->facade->deleteUser(), JSON_UNESCAPED_UNICODE);
    }
    /**
     * 取得使用者資訊
     *
     *      Endpoints
     *
     *          json Array(
     *              'Authorization'  => 使用者的登入令牌
     *          )
     *
     *      Callback 
     *
     *          json Array(
     *              'statuscode'     => 狀態碼,
     *              'detail'         => 狀態內容,
     *              'Authorization'  => 使用者的登入令牌,
     *              'email'          => 用戶,
     *              'id'             => 用戶id,
     *              'username'       => 用戶名
     *          )
     */
    public function getuserdataAction()
    {
        return json_encode($this->facade->getUserData(), JSON_UNESCAPED_UNICODE);
    }
    /**
     * 驗證email格式，以及是否重覆。
     *
     *      Endpoints
     *
     *          json Array(
     *              'email'  => 要驗證的email
     *          )
     *
     *      Callback 
     *
     *          json Array(
     *              'statuscode'     => 狀態碼,
     *              'detail'         => 狀態內容
     *          )
     */
    public function checkUserEmailAction()
    {
        return json_encode($this->facade->checkUserEmail(), JSON_UNESCAPED_UNICODE);
    }
    /**
     * 驗證用戶名格式，以及是否重覆。
     *
     *      Endpoints
     *
     *          json Array(
     *              'username'  => 要驗證的用戶名
     *          )
     *
     *      Callback 
     *
     *          json Array(
     *              'statuscode'     => 狀態碼,
     *              'detail'         => 狀態內容
     *          )
     */
    public function checkUserNameAction()
    {
        return json_encode($this->facade->checkUserName(), JSON_UNESCAPED_UNICODE);
    }
     /**
     * 驗證密碼格式。
     *
     *      Endpoints
     *
     *          json Array(
     *              'usersecret'  => 要驗證的密碼格式
     *          )
     *
     *      Callback 
     *
     *          json Array(
     *              'statuscode'     => 狀態碼,
     *              'detail'         => 狀態內容
     *          )
     */
    public function checkUserSecretAction()
    {
        return json_encode($this->facade->checkUserSecret(), JSON_UNESCAPED_UNICODE);
    }
}
