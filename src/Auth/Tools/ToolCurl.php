<?php
namespace Auth\Tools;

/**
 * ToolCurl
 * 
 * 這是CURL的包覆類別
 *
 * 主要是為了建立帶有SessionCookie的Curl測試環境
 *
 * It's work for SessionCookie
 *
 * first, ToolCurl::load($uri),you'll get an object
 *
 * if you want to touch the url just object->get()
 *
 * if you want to get the call back data just object->get(true);
 *
 * 要讀取header object->get(true, true)
 *
 * 要加入Authorization到HEADER裡的話 object->get(true, true, $value)
 *
 * then if you want to post JSON data just object->postJSON($postdata)
 *
 * with call back data : object->postJSON($postdata, true)
 *
 * 要讀取header object->postJSON($postdata, true, true)
 *
 * 要加入Authorization到HEADER裡的話 object->postJSON($postdata, true, true, $value)
 *
 * 2014.10.6 Garth_Wang
 */

class ToolCurl
{
    /** 
     * @param static $ci 裝載curl_init()物件
     */
    private static $ci = null;
    /** 
     * @param static $uri 裝載目標uri的網址
     */
    private static $uri = null;
    
    /// @brief 建構式，開啟cookiesession環境
    private function __construct()
    {
        //指定cookie位置
        $cookie_file='./mycookie.tmp';
        //如果沒有cookie的話就創建
        if (!file_exists($cookie_file)) {
            $fp = fopen($cookie_file, 'w+');
            fclose($fp);
            chmod($cookie_file, 0777);
        }
        //創建cookie的儲存容器
        self::$ci = curl_init(self::$uri);
        curl_setopt(self::$ci, CURLOPT_COOKIEFILE, $cookie_file);
        curl_setopt(self::$ci, CURLOPT_COOKIEJAR, $cookie_file);
        curl_setopt(self::$ci, CURLOPT_USERAGENT, 'I am Garth!');
        curl_setopt(self::$ci, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt(self::$ci, CURLOPT_SSL_VERIFYPEER, 0);
    }
    /**
     * 回傳ToolCurl物件
     * @param string $uri 初始化物件必需要有目標uri
     */
    public static function init($uri)
    {
        self::$uri = $uri;
        return new self();
    }
    public static function post($postdata, $return = null, $head = null)
    {
        curl_setopt(self::$ci, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt(self::$ci, CURLOPT_POSTFIELDS, $postdata);
        if ($return == true) {
            curl_setopt(self::$ci, CURLOPT_RETURNTRANSFER, 1);
            if ($head == true) {
                curl_setopt(self::$ci, CURLOPT_HEADER, 1);
            }
            return curl_exec(self::$ci);
        } else {
            curl_exec(self::$ci);
        }
    }
    /**
     * 以postJSON物件的方式打uri
     * @param JSON $postdata 要送的post的參數
     * @param bool $return 填入true則回傳curl回來的結果
     */
    public function postJSON($postdata, $return = null, $head = null)
    {
        curl_setopt(self::$ci, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt(self::$ci, CURLOPT_POSTFIELDS, $postdata);
            curl_setopt(
                self::$ci,
                CURLOPT_HTTPHEADER,
                array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($postdata)
                )
            );
        if ($return == true) {
            curl_setopt(self::$ci, CURLOPT_RETURNTRANSFER, 1);
            if ($head == true) {
                curl_setopt(self::$ci, CURLOPT_HEADER, 1);
            }
            return curl_exec(self::$ci);
        } else {
            curl_exec(self::$ci);
        }
    }
    /**
     *  直接不帶參數打uri
     *  @param bool $return 填入true則回傳curl回來的結果
     */
    public function get($return = null, $head = null, $token = null)
    {
        if ($token) {
            curl_setopt(self::$ci, CURLOPT_HTTPHEADER, array('Authorization: ' . $token));
        }
        if ($return == true) {
            curl_setopt(self::$ci, CURLOPT_RETURNTRANSFER, 1);
            if ($head == true) {
                curl_setopt(self::$ci, CURLOPT_HEADER, 1);
            }
            return curl_exec(self::$ci);
        } else {
            curl_exec(self::$ci);
        }
    }
    /// 刪除cookie檔案，目前使用上能刪掉。
    /// 但不知為何又被創建出來~_~. 先做記號等著debug.
    public static function deleteCookie()
    {
        $cookie_file='mycookie.tmp';
        chmod($cookie_file, 0777);
        unlink($cookie_file);
    }
    public static function getHeadersFromCurlResponse($response)
    {
        $headers = array();
        $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));
        foreach (explode("\r\n", $header_text) as $i => $line) {
            if ($i === 0) {
                $headers['http_code'] = $line;
            } else {
                list ($key, $value) = explode(': ', $line);
                $headers[$key] = $value;
            }
        }
        return $headers;
    }
}
