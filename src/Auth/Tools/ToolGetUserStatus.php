<?php
namespace Auth\Tools;

use Auth\Model\DataBox;

/**
 * 這支class主要是用來取得用戶的IP
 *
 * 以及User-Agent
 *
 * 2014.10.6 Garth_Wang
 */

class ToolGetUserStatus
{
    public static function getUserStatus(DataBox $dbox)
    {
        $dbox->addData('ip', $_SERVER['REMOTE_ADDR']);
        $dbox->addData('useragent', $_SERVER['HTTP_USER_AGENT']);
        return $dbox;
    }
}
