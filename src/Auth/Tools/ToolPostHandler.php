<?php
namespace Auth\Tools;

use Auth\Model\DataBox;

/**
 * ToolPostHandler
 *
 * 專門拿來解析HttpRequest的POST資料
 *
 * The Tool for recieve the data from HttpMethod 'POST'
 *
 * We can start ToolPostHandler with ToolPostHandler::init()
 *
 * it will return a ToolPostHandler Object
 *
 * if you want to get data when Content-Type isn't multipart/form-data
 *
 * you can try ToolPostHandler::init()->getData()
 * 
 * if you want to get multipart/form-data or origin form post Variable
 *
 * you can try ToolPostHandler::init()->getDataByKey($key)
 *
 * if you want to get JSON data , use this ToolPostHandler::init()->getJSONData()
 *
 * 2014.09.25 Garth_Wang
 */

class ToolPostHandler
{
    private static $httpmethod = null;
    private static $postdata = null;
    private function __construct()
    {
        //nothing to do
    }
    /**
     * 取得POST過來的JSON物件
     * @return array $postdata 返回json_decode後的陣列
     */
    public static function getJSONData(DataBox $dbox)
    {
        if (strtoupper(getenv('REQUEST_METHOD') == 'POST')) {
            if (file_get_contents("php://input") != null) {
                self::$postdata =  json_decode(file_get_contents("php://input"), true);
                $dbox->addArrayData(self::$postdata);
            } else {
                self::$postdata = null;
            }
        }
        return $dbox;
    }
    /**
     * 取得無索引的POST陣列
     * @return array $postdata 
     */
    public static function getData()
    {
        if (strtoupper(getenv('REQUEST_METHOD') == 'POST')) {
            if (file_get_contents("php://input") != null) {
                self::$postdata =  file_get_contents("php://input");
            } else {
                self::$postdata = null;
            }
        }
        return self::$postdata;
    }
    /**
     * 取得有索引的POST欄位值
     * @param string $key 
     * @return string $postdata 返回單一欄位
     */
    public static function getDataByKey($key)
    {
        if (strtoupper(getenv('REQUEST_METHOD') == 'POST')) {
            if (isset($_POST[$key])) {
                self::$postdata = $_POST[$key];
            } else {
                self::$postdata = null;
            }
        }
        return self::$postdata;
    }
}
