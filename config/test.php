<?php

return array(
    'general' => array(
        'router' => 'Fruit\PuxRouter'
    ),

    'pux' => array(
        'mux' => implode(DIRECTORY_SEPARATOR, ['route', 'mux.php'])
    ),

    'twig' => array(
        'template_dir' => BASE_DIR . '/templates',
        'debug'        => true,
        'cache_dir'    => '/tmp'
    ),
    
    'db' => array(
        'default' => array(
            'constr' => 'mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8',
            'user'   => 'root',
            'pass'   => '1234'
        ),
    ),
    
    'redis' => array(
        'location' => '/tmp/redis.sock',
        'database' => 1
    ),

    'upload' => array(
        'dir' => BASE_DIR . '/files',
        'prefix' => 'ul_'
    )
);
