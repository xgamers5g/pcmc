<?php
namespace AuthTest\Facade\UnitTest;

use PHPUnit_Framework_TestCase;

class AuthFacadeUnitTest extends PHPUnit_Framework_TestCase
{
    public function testgetInstance()
    {
        //模仿Redis物件
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        //模仿Response物件
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        //模仿TokenMaker物件
        $tokenmaker = $this->getMock('Auth\Model\Library\TokenMaker', array(), array(), '', false, true, true);
        //模仿PostHandler物件
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        //設定PostHandler::getJSONData在任易時刻的回傳值為 array('Authorization' => 'abc')
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            //->with($param1',$param2) 如果還要綁參數的話可以用with來限定入參數
            ->will($this->returnValue(array('Authorization' => 'abc')));
        //模仿UserStatus物件
        $userstatus = $this->getMock('\Auth\Model\UserStatus', array(), array(), '', false, true, true);
        //設定UserStatus::getUserStatus的回傳值
        $userstatus
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue(array('ip' => '123.123.123.123', 'useragent' => 'asdf')));
        //模仿Core物件
        $core = $this->getMock('\Auth\System\core', array(), array(), '', false, true, true);
        //設定Core::getPostHandler回傳$posthandler假物件
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        //設定Core::getUserStatus回傳$userstatus假物件
        $core
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue($userstatus));
        //設定Core::getRedis回傳$redis假物件
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        //設定Core::getResponse回傳$response假物件
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        //設定Core::getTokenMaker回傳tokenmaker假物件
        $core
            ->expects($this->any())
            ->method('getTokenMaker')
            ->will($this->returnValue($tokenmaker));
        //將假的$core注入\Auth\Facade\AuthFacade::getInstance，
        //如此一來getInstance的執行全部都會用假的來跑。
        $res = \Auth\Facade\AuthFacade::getInstance($core);
        $this->assertEquals(true, $res instanceof \Auth\Facade\AuthFacade);
    }
    public function testgetloginStateWhenAuthorized()
    {
        $redis = $this->getMock('\Redis');
        $response = $this->getMock('\Auth\Model\Response');
        $tokenmaker = $this->getMock('Auth\Model\Library\TokenMaker');
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array('Authorization' => 'abc')));
        $userstatus = $this->getMock('\Auth\Model\UserStatus', array(), array(), '', false, true, true);
        $userstatus
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue(array('ip' => '123.123.123.123', 'useragent' => 'asdf')));
        $utm = $this->getMock('\Auth\Model\UserTokenModel', array(), array(), '', false, true, true);
        $utm
            ->expects($this->at(0))
            ->method('getUid')
            ->will($this->returnValue(5));
        $utm
            ->expects($this->at(1))
            ->method('getToken')
            ->will($this->returnValue('abc'));
        $core = $this->getMock('\Auth\System\core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $core
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue($userstatus));
        $core
            ->expects($this->any())
            ->method('getUserTokenModelByToken')
            ->will($this->returnValue($utm));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getTokenMaker')
            ->will($this->returnValue($tokenmaker));
        $res = \Auth\Facade\AuthFacade::getInstance($core);
        $this->assertEquals(true, $res->getloginState());
    }
    public function testgetloginStateWhenUnAuthorized()
    {
        $redis = $this->getMock('\Redis');
        $response = $this->getMock('\Auth\Model\Response');
        $tokenmaker = $this->getMock('Auth\Model\Library\TokenMaker');
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array('Authorization' => 'abc')));
        $userstatus = $this->getMock('\Auth\Model\UserStatus', array(), array(), '', false, true, true);
        $userstatus
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue(array('ip' => '123.123.123.123', 'useragent' => 'asdf')));
        $core = $this->getMock('\Auth\System\core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $core
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue($userstatus));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getTokenMaker')
            ->will($this->returnValue($tokenmaker));
        $res = \Auth\Facade\AuthFacade::getInstance($core);
        $this->assertEquals(null, $res->getloginState());
    }
    public function testgetUserData()
    {
        $usermodel = $this->getMock('\Auth\Model\UserModel', array(), array(), '', false, true, true);
        $usermodel
            ->expects($this->any())
            ->method('getUserData')
            ->will($this->returnValue(array('id' => 8, 'email' => 'a@b.c', 'username' => 'asdf')));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $redis = $this->getMock('\Redis');
        $response = $this->getMock('\Auth\Model\Response');
        $tokenmaker = $this->getMock('Auth\Model\Library\TokenMaker');
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array('Authorization' => 'abc')));
        $userstatus = $this->getMock('\Auth\Model\UserStatus', array(), array(), '', false, true, true);
        $userstatus
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue(array('ip' => '123.123.123.123', 'useragent' => 'asdf')));
        $utm = $this->getMock('\Auth\Model\UserTokenModel', array(), array(), '', false, true, true);
        //設定第0次呼叫UserTokenModel時的method為getUid，並指定其回傳值為5。
        $utm
            ->expects($this->at(0))
            ->method('getUid')
            ->will($this->returnValue(5));
        //設定第1次呼叫UserTokenModel時的method為getToken，並指定其回傳值為'abc'。
        $utm
            ->expects($this->at(1))
            ->method('getToken')
            ->will($this->returnValue('abc'));
        $core = $this->getMock('\Auth\System\core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $core
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue($userstatus));
        $core
            ->expects($this->any())
            ->method('getUserTokenModelByToken')
            ->will($this->returnValue($utm));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getTokenMaker')
            ->will($this->returnValue($tokenmaker));
        $core
            ->expects($this->any())
            ->method('getDb')
            ->will($this->returnValue($pdo));
        $core
            ->expects($this->any())
            ->method('getUserModelById')
            ->will($this->returnValue($usermodel));
        //加入response,pdo,
        $res = \Auth\Facade\AuthFacade::getInstance($core);
        $assertData = array(
            'id' => 8,
            'email' => 'a@b.c',
            'username' => 'asdf',
            'Authorization' => 'abc'
        );
        $this->assertEquals($assertData, $res->getUserData());
    }
    public function testlogout()
    {
        $redis = $this->getMock('\Redis');
        $response = $this->getMock('\Auth\Model\Response');
        $tokenmaker = $this->getMock('Auth\Model\Library\TokenMaker');
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array('Authorization' => 'abc')));
        $userstatus = $this->getMock('\Auth\Model\UserStatus', array(), array(), '', false, true, true);
        $userstatus
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue(array('ip' => '123.123.123.123', 'useragent' => 'asdf')));
        $utm = $this->getMock('\Auth\Model\UserTokenModel', array(), array(), '', false, true, true);
        $utm
            ->expects($this->at(0))
            ->method('getUid')
            ->will($this->returnValue(5));
        $utm
            ->expects($this->at(1))
            ->method('getToken')
            ->will($this->returnValue('abc'));
        $utm
            ->expects($this->any())
            ->method('delete')
            ->will($this->returnValue(array('statuscode' => '200', 'detail' => '測試成功')));
        $core = $this->getMock('\Auth\System\core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $core
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue($userstatus));
        $core
            ->expects($this->any())
            ->method('getUserTokenModelByToken')
            ->will($this->returnValue($utm));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getTokenMaker')
            ->will($this->returnValue($tokenmaker));
        $res = \Auth\Facade\AuthFacade::getInstance($core);
        $assertData = array(
            'statuscode' => 200,
            'detail' => '測試成功'
        );
        $this->assertEquals($assertData, $res->logout());
    }
    public function testdeleteUser()
    {
        $usermodel = $this->getMock('\Auth\Model\UserModel', array(), array(), '', false, true, true);
        $usermodel
            ->expects($this->any())
            ->method('deleteUser')
            ->will($this->returnValue(array('statuscode' => '200', 'detail' => '測試成功')));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $redis = $this->getMock('\Redis');
        $response = $this->getMock('\Auth\Model\Response');
        $tokenmaker = $this->getMock('Auth\Model\Library\TokenMaker');
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array('Authorization' => 'abc')));
        $userstatus = $this->getMock('\Auth\Model\UserStatus', array(), array(), '', false, true, true);
        $userstatus
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue(array('ip' => '123.123.123.123', 'useragent' => 'asdf')));
        $utm = $this->getMock('\Auth\Model\UserTokenModel', array(), array(), '', false, true, true);
        $utm
            ->expects($this->at(0))
            ->method('getUid')
            ->will($this->returnValue(5));
        $utm
            ->expects($this->at(1))
            ->method('getToken')
            ->will($this->returnValue('abc'));
        $core = $this->getMock('\Auth\System\core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $core
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue($userstatus));
        $core
            ->expects($this->any())
            ->method('getUserTokenModelByToken')
            ->will($this->returnValue($utm));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getTokenMaker')
            ->will($this->returnValue($tokenmaker));
        $core
            ->expects($this->any())
            ->method('getDb')
            ->will($this->returnValue($pdo));
        $core
            ->expects($this->any())
            ->method('getUserModelById')
            ->will($this->returnValue($usermodel));
        //加入response,pdo,
        $res = \Auth\Facade\AuthFacade::getInstance($core);
        $assertData = array(
            'statuscode' => '200',
            'detail' => '測試成功'
        );
        $this->assertEquals($assertData, $res->deleteUser());
    }
    public function testcreateUserSuccess()
    {
        //這邊因為會調用到public static method
        //所以我們必需做個機制，讓測試的時候可以調到測試用的method
        //所以，我們從core動手腳。
        $redis = $this->getMock('\Redis');
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $encrypt = $this->getMock('\Auth\Model\Library\Encrypt', array(), array(), '', false, true, true);
        $response = $this->getMock('\Auth\Model\Response');
        $tokenmaker = $this->getMock('Auth\Model\Library\TokenMaker');
        $datavalidator = $this->getMock('Auth\Model\DataValidator', array(), array(), '', false, true, true);
        $datavalidator
            ->expects($this->any())
            ->method('validateCreateUserData')
            ->will($this->returnValue(true));
        $elementvalidator = $this->getMock(
            'Auth\Model\Library\ElementValidator',
            array(),
            array(),
            '',
            false,
            true,
            true
        );
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array('email' => 'abc', 'username'=> '123', 'usersecret'=>'123')));
        $userstatus = $this->getMock('\Auth\Model\UserStatus', array(), array(), '', false, true, true);
        $userstatus
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue(array('ip' => '123.123.123.123', 'useragent' => 'asdf')));
        $core = $this->getMock('\Auth\System\core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $core
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue($userstatus));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getTokenMaker')
            ->will($this->returnValue($tokenmaker));
        $core
            ->expects($this->any())
            ->method('createUser')
            ->will($this->returnValue('success'));
        $core
            ->expects($this->any())
            ->method('getDataValidator')
            ->will($this->returnValue($datavalidator));
        $core
            ->expects($this->any())
            ->method('getElementValidator')
            ->will($this->returnValue($elementvalidator));
        $core
            ->expects($this->any())
            ->method('getDb')
            ->will($this->returnValue($pdo));
        $core
            ->expects($this->any())
            ->method('getEncrypt')
            ->will($this->returnvalue($encrypt));
        $res = \Auth\Facade\AuthFacade::getInstance($core);
        //var_dump($res->createUser());
        $this->assertEquals('success', $res->createUser());
    }
    public function testcreateUserFail()
    {
        //這邊因為會調用到public static method
        //所以我們必需做個機制，讓測試的時候可以調到測試用的method
        //所以，我們從core動手腳。
        $redis = $this->getMock('\Redis');
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $encrypt = $this->getMock('\Auth\Model\Library\Encrypt', array(), array(), '', false, true, true);
        $response = $this->getMock('\Auth\Model\Response');
        $tokenmaker = $this->getMock('Auth\Model\Library\TokenMaker');
        $datavalidator = $this->getMock('Auth\Model\DataValidator', array(), array(), '', false, true, true);
        $datavalidator
            ->expects($this->any())
            ->method('validateCreateUserData')
            ->will($this->returnValue(false));
        $elementvalidator = $this->getMock(
            'Auth\Model\Library\ElementValidator',
            array(),
            array(),
            '',
            false,
            true,
            true
        );
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array('email' => 'abc', 'username'=> '123', 'usersecret'=>'123')));
        $userstatus = $this->getMock('\Auth\Model\UserStatus', array(), array(), '', false, true, true);
        $userstatus
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue(array('ip' => '123.123.123.123', 'useragent' => 'asdf')));
        $core = $this->getMock('\Auth\System\core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $core
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue($userstatus));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getTokenMaker')
            ->will($this->returnValue($tokenmaker));
        $core
            ->expects($this->any())
            ->method('createUser')
            ->will($this->returnValue('success'));
        $core
            ->expects($this->any())
            ->method('getDataValidator')
            ->will($this->returnValue($datavalidator));
        $core
            ->expects($this->any())
            ->method('getElementValidator')
            ->will($this->returnValue($elementvalidator));
        $core
            ->expects($this->any())
            ->method('getDb')
            ->will($this->returnValue($pdo));
        $core
            ->expects($this->any())
            ->method('getEncrypt')
            ->will($this->returnvalue($encrypt));
        $res = \Auth\Facade\AuthFacade::getInstance($core);
        //var_dump($res->createUser());
        $this->assertEquals(false, $res->createUser());
    }
    public function testLoginSuccess()
    {
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $redis = $this->getMock('\Redis');
        $encrypt = $this->getMock('\Auth\Model\Library\Encrypt', array(), array(), '', false, true, true);
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('success')
            ->will($this->returnValue(array('statuscode' => '200', 'detail' => '執行成功')));
        $tokenmaker = $this->getMock('Auth\Model\Library\TokenMaker');
        $datavalidator = $this->getMock('Auth\Model\DataValidator', array(), array(), '', false, true, true);
        $datavalidator
            ->expects($this->any())
            ->method('validateLoginData')
            ->will($this->returnValue(true));
        $elementvalidator = $this->getMock(
            'Auth\Model\Library\ElementValidator',
            array(),
            array(),
            '',
            false,
            true,
            true
        );
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array('email' => 'abc', 'username'=> '123', 'usersecret'=>'123')));
        $userstatus = $this->getMock('\Auth\Model\UserStatus', array(), array(), '', false, true, true);
        $userstatus
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue(array('ip' => '123.123.123.123', 'useragent' => 'asdf')));
        $usermodel = $this->getMock('\Auth\Model\UserModel', array(), array(), '', false, true, true);
        $usermodel
            ->expects($this->any())
            ->method('getUserData')
            ->will($this->returnValue(array('id' => 1)));
        $usertokenmodel = $this->getMock('\Auth\Model\UserTokenModel', array(), array(), '', false, true, true);
        $usertokenmodel
            ->expects($this->any())
            ->method('getToken')
            ->will($this->returnValue('testtoken'));
        $datetime = $this->getMock('\DateTime', array(), array(), '', false, true, true);
        $loginlogmodel = $this->getMock('\Auth\Model\LoginLogModel', array(), array(), '', false, true, true);
        $core = $this->getMock('\Auth\System\core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $core
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue($userstatus));
        $core
            ->expects($this->any())
            ->method('getDb')
            ->will($this->returnValue($pdo));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getEncrypt')
            ->will($this->returnValue($encrypt));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getTokenMaker')
            ->will($this->returnValue($tokenmaker));
        $core
            ->expects($this->any())
            ->method('getUserModelByLogin')
            ->will($this->returnValue($usermodel));
        $core
            ->expects($this->any())
            ->method('getUserTokenModelById')
            ->will($this->returnValue($usertokenmodel));
        $core
            ->expects($this->any())
            ->method('getDataValidator')
            ->will($this->returnValue($datavalidator));
        $core
            ->expects($this->any())
            ->method('getElementValidator')
            ->will($this->returnValue($elementvalidator));
        $core
            ->expects($this->any())
            ->method('getDateTime')
            ->will($this->returnValue($datetime));
        $core
            ->expects($this->any())
            ->method('getLoginLogModel')
            ->will($this->returnValue($loginlogmodel));

        $res = \Auth\Facade\AuthFacade::getInstance($core);
        $this->assertEquals(
            array(
                'statuscode' => '200',
                'detail' => '執行成功',
                'Authorization' => 'testtoken'
            ),
            $res->login()
        );
    }
    public function testLoginFail()
    {
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $redis = $this->getMock('\Redis');
        $encrypt = $this->getMock('\Auth\Model\Library\Encrypt', array(), array(), '', false, true, true);
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('loginerror')
            ->will($this->returnValue(array('statuscode' => '401', 'detail' => '登入帳號或是密碼錯誤')));
        $tokenmaker = $this->getMock('Auth\Model\Library\TokenMaker');
        $datavalidator = $this->getMock('Auth\Model\DataValidator', array(), array(), '', false, true, true);
        $datavalidator
            ->expects($this->any())
            ->method('validateLoginData')
            ->will($this->returnValue(true));
        $elementvalidator = $this->getMock(
            'Auth\Model\Library\ElementValidator',
            array(),
            array(),
            '',
            false,
            true,
            true
        );
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array('email' => 'abc', 'username'=> '123', 'usersecret'=>'123')));
        $userstatus = $this->getMock('\Auth\Model\UserStatus', array(), array(), '', false, true, true);
        $userstatus
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue(array('ip' => '123.123.123.123', 'useragent' => 'asdf')));
        $usermodel = $this->getMock('\Auth\Model\UserModel', array(), array(), '', false, true, true);
        $usermodel
            ->expects($this->any())
            ->method('getUserData')
            ->will($this->returnValue(array('id' => 1)));
        $usertokenmodel = $this->getMock('\Auth\Model\UserTokenModel', array(), array(), '', false, true, true);
        $usertokenmodel
            ->expects($this->any())
            ->method('getToken')
            ->will($this->returnValue('testtoken'));
        $datetime = $this->getMock('\DateTime', array(), array(), '', false, true, true);
        $loginlogmodel = $this->getMock('\Auth\Model\LoginLogModel', array(), array(), '', false, true, true);
        $core = $this->getMock('\Auth\System\core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $core
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue($userstatus));
        $core
            ->expects($this->any())
            ->method('getDb')
            ->will($this->returnValue($pdo));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getEncrypt')
            ->will($this->returnValue($encrypt));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getTokenMaker')
            ->will($this->returnValue($tokenmaker));
        $core
            ->expects($this->any())
            ->method('getUserModelByLogin')
            ->will($this->returnValue(false));
        $core
            ->expects($this->any())
            ->method('getUserTokenModelById')
            ->will($this->returnValue($usertokenmodel));
        $core
            ->expects($this->any())
            ->method('getDataValidator')
            ->will($this->returnValue($datavalidator));
        $core
            ->expects($this->any())
            ->method('getElementValidator')
            ->will($this->returnValue($elementvalidator));
        $core
            ->expects($this->any())
            ->method('getDateTime')
            ->will($this->returnValue($datetime));
        $core
            ->expects($this->any())
            ->method('getLoginLogModel')
            ->will($this->returnValue($loginlogmodel));
        $res = \Auth\Facade\AuthFacade::getInstance($core);
        $this->assertEquals(
            array(
                'statuscode' => '401',
                'detail' => '登入帳號或是密碼錯誤'
            ),
            $res->login()
        );
    }
    public function testSetSecret()
    {
        $usermodel = $this->getMock('\Auth\Model\UserModel', array(), array(), '', false, true, true);
        $usermodel
            ->expects($this->any())
            ->method('setSecret')
            ->will($this->returnValue(array('statuscode' => '200', 'detail' => '執行成功')));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $redis = $this->getMock('\Redis');
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('success')
            ->will($this->returnValue(array('statuscode' => '200', 'detail' => '執行成功')));
        $datavalidator = $this->getMock('Auth\Model\DataValidator', array(), array(), '', false, true, true);
        $datavalidator
            ->expects($this->any())
            ->method('validateSetSecretData')
            ->will($this->returnValue(true));
        $elementvalidator = $this->getMock(
            'Auth\Model\Library\ElementValidator',
            array(),
            array(),
            '',
            false,
            true,
            true
        );
        $encrypt = $this->getMock('\Auth\Model\Library\Encrypt', array(), array(), '', false, true, true);
        $tokenmaker = $this->getMock('Auth\Model\Library\TokenMaker');
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array('Authorization' => 'asdf', 'usersecret' => '12345')));
        $userstatus = $this->getMock('\Auth\Model\UserStatus', array(), array(), '', false, true, true);
        $userstatus
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue(array('ip' => '123.123.123.123', 'useragent' => 'asdf')));
        $utm = $this->getMock('\Auth\Model\UserTokenModel', array(), array(), '', false, true, true);
        $utm
            ->expects($this->at(0))
            ->method('getUid')
            ->will($this->returnValue(5));
        $utm
            ->expects($this->at(1))
            ->method('getToken')
            ->will($this->returnValue('abc'));
        $core = $this->getMock('\Auth\System\core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $core
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue($userstatus));
        $core
            ->expects($this->any())
            ->method('getUserTokenModelByToken')
            ->will($this->returnValue($utm));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getTokenMaker')
            ->will($this->returnValue($tokenmaker));
        $core
            ->expects($this->any())
            ->method('getDb')
            ->will($this->returnValue($pdo));
        $core
            ->expects($this->any())
            ->method('getUserModelById')
            ->will($this->returnValue($usermodel));
        $core
            ->expects($this->any())
            ->method('getDataValidator')
            ->will($this->returnValue($datavalidator));
        $core
            ->expects($this->any())
            ->method('getElementValidator')
            ->will($this->returnValue($elementvalidator));
        $core
            ->expects($this->any())
            ->method('getEncrypt')
            ->will($this->returnValue($encrypt));
        //加入response,pdo,
        $res = \Auth\Facade\AuthFacade::getInstance($core);
        $assertData = array(
            'statuscode' => '200',
            'detail' => '執行成功',
            'Authorization' => 'abc'
        );
        $this->assertEquals($assertData, $res->setSecret());
    }
    public function testIndex()
    {
        $redis = $this->getMock('\Redis');
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('index')
            ->will($this->returnValue(array('statuscode' => '404', 'detail' => '不正確的請求uri(get?post?)')));
        $tokenmaker = $this->getMock('Auth\Model\Library\TokenMaker');
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array('Authorization' => 'abc')));
        $userstatus = $this->getMock('\Auth\Model\UserStatus', array(), array(), '', false, true, true);
        $userstatus
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue(array('ip' => '123.123.123.123', 'useragent' => 'asdf')));
        $core = $this->getMock('\Auth\System\core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $core
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue($userstatus));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getTokenMaker')
            ->will($this->returnValue($tokenmaker));
        $res = \Auth\Facade\AuthFacade::getInstance($core);
        $assertData = array(
            'statuscode' => '404',
            'detail' => '不正確的請求uri(get?post?)'
        );
        $this->assertEquals($assertData, $res->index());
    }
    public function testcheckUserEmailWithNoEmail()
    {
        $redis = $this->getMock('\Redis');
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('emptyemail')
            ->will($this->returnValue(array('statuscode' => '401', 'detail' => '沒有輸入使用者email')));
        $tokenmaker = $this->getMock('Auth\Model\Library\TokenMaker');
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array('Authorization' => 'abc')));
        $userstatus = $this->getMock('\Auth\Model\UserStatus', array(), array(), '', false, true, true);
        $userstatus
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue(array('ip' => '123.123.123.123', 'useragent' => 'asdf')));
        $core = $this->getMock('\Auth\System\core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $core
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue($userstatus));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getTokenMaker')
            ->will($this->returnValue($tokenmaker));
        $assertData = array(
            'statuscode' => '401',
            'detail' => '沒有輸入使用者email'
        );
        $res = \Auth\Facade\AuthFacade::getInstance($core);
        $this->assertEquals($assertData, $res->checkUserEmail());
    }
    public function testcheckUserEmailWithEmailAllow()
    {

        $redis = $this->getMock('\Redis');
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $tokenmaker = $this->getMock('Auth\Model\Library\TokenMaker');
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array('email' => 'abc')));
        $userstatus = $this->getMock('\Auth\Model\UserStatus', array(), array(), '', false, true, true);
        $userstatus
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue(array('ip' => '123.123.123.123', 'useragent' => 'asdf')));
        $core = $this->getMock('\Auth\System\core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $core
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue($userstatus));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getTokenMaker')
            ->will($this->returnValue($tokenmaker));
        $core
            ->expects($this->any())
            ->method('checkUserEmailExistForClient')
            ->will($this->returnValue(array('statuscode' => '200', 'detail' => '此電郵可以註冊')));
        $core
            ->expects($this->any())
            ->method('getDb')
            ->will($this->returnValue($pdo));
        $core
            ->expects($this->any())
            ->method('getElementValidator')
            ->will($this->returnValue($elementvalidator));
        $assertData = array(
            'statuscode' => '200',
            'detail' => '此電郵可以註冊'
        );
        $res = \Auth\Facade\AuthFacade::getInstance($core);
        $this->assertEquals($assertData, $res->checkUserEmail());
    }
}
