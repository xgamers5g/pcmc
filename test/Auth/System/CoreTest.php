<?php
namespace AuthTest\System\UnitTest;

use PHPUnit_Framework_TestCase;

class CoreTest extends PHPUnit_Framework_TestCase
{
    public function testgetInstance()
    {
        $res = \Auth\System\Core::getInstance();
        $this->assertTrue($res instanceof \Auth\System\Core);
    }
    public function testgetdb()
    {
        $res = \Auth\System\Core::getInstance('test');
        $this->assertTrue($res->getDb() instanceof \PDO);
    }
    public function testgetredis()
    {
        $res = \Auth\System\Core::getInstance('test');
        $this->assertTrue($res->getRedis() instanceof \Redis);
    }
    public function testgetresponse()
    {
        $res = \Auth\System\Core::getInstance('test');
        $this->assertTrue($res->getResponse() instanceof \Auth\Model\Response);
    }
    public function testgetEncrypt()
    {
        $res = \Auth\System\Core::getInstance('test');
        $this->assertTrue($res->getEncrypt() instanceof \Auth\Model\Library\Encrypt);
    }
    public function testgetTokenMaker()
    {
        $res = \Auth\System\Core::getInstance('test');
        $this->assertTrue($res->getTokenMaker() instanceof \Auth\Model\Library\TokenMaker);
    }
    public function testgetPostHandler()
    {
        $res = \Auth\System\Core::getInstance('test');
        $this->assertEquals(true, $res->getPostHandler() instanceof \Auth\Model\PostHandler);
    }
    public function testgetUserStatus()
    {
        $res = \Auth\System\Core::getInstance('test');
        $this->assertEquals(true, $res->getUserStatus() instanceof \Auth\Model\UserStatus);
    }
    public function testgetUserTokenModelByToken()
    {
        $data = array(
            'Authorization' => '123',
            'ip' => '123.123.123.123',
            'useragent' => 'asdf'
        );
        $redis20 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis20
            ->expects($this->any())
            ->method("exec")
            ->will($this->returnValue(array(0 => true, 1 => true, 2 => true)));
        $redis21 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis21
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis20));
        $redis22 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis22
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis21));
        $redis23 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis23
            ->expects($this->any())
            ->method("delete")
            ->will($this->returnValue($redis22));

        $redis24 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis24
            ->expects($this->at(0))
            ->method("exists")
            ->will($this->returnValue(true));
        $redis24
            ->expects($this->at(2))
            ->method("get")
            ->will(
                $this->returnValue(
                    json_encode(
                        array(
                            'expire' => time()+600,
                            'ip' => '123.123.123.123',
                            'useragent' => 'asdf',
                            'userid' => 1
                        )
                    )
                )
            );
        $redis24
            ->expects($this->any())
            ->method("multi")
            ->will($this->returnValue($redis23));

        $response = $this
            ->getMockBuilder("\Auth\Model\Response")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('success')
            ->will($this->returnValue(array()));
        $tokenmaker = $this
            ->getMockBuilder("\Auth\Model\Library\TokenMaker")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $tokenmaker
             ->expects($this->any())
             ->method("generateToken")
             ->will($this->returnValue('abc'));
        $res = \Auth\System\Core::getInstance('test');
        $this->assertEquals(
            true,
            $res->getUserTokenModelByToken(
                $redis24,
                $response,
                $tokenmaker,
                $data
            ) instanceof \Auth\Model\UserTokenModel
        );
    }
    public function testgetUserModelById()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->at(2))
            ->method("fetch")
            ->will($this->returnValue(array('uid' => 1)));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method("prepare")
            ->will($this->returnValue($pdostatement));
        $res = \Auth\System\Core::getInstance('test');
        $um = $res->getUserModelById($pdo, 1);
        $this->assertEquals(true, $um instanceof \Auth\Model\UserModel);
    }
    public function testcreateUser()
    {
        $fetchAllMock = $this
            ->getMockBuilder("\PDOStatement")
            ->setMethods(array("execute"))
            ->getMock();
        $fetchAllMock
            ->expects($this->at(0))->method("execute")
            ->will($this->returnValue(array('salthash' => '123')));
        $fetchAllMock
            ->expects($this->at(1))->method("execute")
            ->will($this->returnValue(null));
        $pdo = $this
            ->getMockBuilder("\PDO")
            ->disableOriginalConstructor()
            ->setMethods(array("prepare"))
            ->getMock();
        $pdo
            ->expects($this->any())
            ->method("prepare")
            ->will($this->returnValue($fetchAllMock));
        $encrypt = $this
            ->getMockBuilder("\Auth\Model\Library\Encrypt")
            ->disableOriginalConstructor()
            ->getMock();
        $response = $this
            ->getMockBuilder("\Auth\Model\Response")
            ->disableOriginalConstructor()
            ->setMethods(array("generateResponse"))
            ->getMock();
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('success')
            ->will($this->returnValue(true));
        $response
            ->expects($this->at(1))
            ->method("generateResponse")
            ->with('databaseerror')
            ->will($this->returnValue(true));
        $res = \Auth\System\Core::getInstance('test');
        $this->assertEquals(
            true,
            $res->createUser($pdo, $encrypt, $response, '1', '2', '3')
        );
        $this->assertEquals(
            true,
            $res->createUser($pdo, $encrypt, $response, '1', '2', '3')
        );
    }
    public function testgetDataValidator()
    {
        $response = $this->getMock('\Auth\Model\Response');
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $data = array();
        $res = \Auth\System\Core::getInstance();
        $this->assertEquals(
            true,
            $res->getDataValidator($response, $elementvalidator, $data) instanceof \Auth\Model\DataValidator
        );
    }
    public function testgetUserModelByLogin()
    {
        $pdostatement = $this
            ->getMockBuilder("\PDOStatement")
            ->setMethods(array("fetch"))
            ->getMock();
        $pdostatement
            ->expects($this->any())
            ->method("fetch")
            ->will($this->returnValue(array('id' => 1)));
        $pdo = $this
            ->getMockBuilder("\PDO")
            ->disableOriginalConstructor()
            ->setMethods(array("prepare"))
            ->getMock();
        $pdo
            ->expects($this->any())
            ->method("prepare")
            ->will($this->returnValue($pdostatement));
        $encrypt = $this
            ->getMockBuilder("\Auth\Model\Library\Encrypt")
            ->disableOriginalConstructor()
            ->getMock();
        $response = $this
            ->getMockBuilder("\Auth\Model\Response")
            ->disableOriginalConstructor()
            ->setMethods(array("generateResponse"))
            ->getMock();
        $res = \Auth\System\Core::getInstance('test');
        $this->assertEquals(
            true,
            $res->getUserModelByLogin($pdo, $encrypt, $response, 'garth', '12345') instanceof \Auth\Model\UserModel
        );
    }
    public function testgetUserTokenModelById()
    {
        $redis1 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis1
             ->expects($this->at(0))
             ->method("exec")
             ->will($this->returnValue(array(0=>true, 1=>true)));
        $redis2 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis2
             ->expects($this->any())
             ->method("set")
             ->will($this->returnValue($redis1));
        $redis3 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis3
             ->expects($this->any())
             ->method("set")
             ->will($this->returnValue($redis2));
        $redis4 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis4
             ->expects($this->any())
             ->method("multi")
             ->will($this->returnValue($redis3));
        $response = $this
            ->getMockBuilder("\Auth\Model\Response")
            ->disableOriginalConstructor()
            ->setMethods(array("generateResponse"))
            ->getMock();
        $tokenmaker = $this
            ->getMockBuilder("\Auth\Model\Library\TokenMaker")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $tokenmaker
             ->expects($this->any())
             ->method("generateToken")
             ->will($this->returnValue('abc'));
        $res = \Auth\System\Core::getInstance('test');
        $this->assertEquals(
            true,
            $res->getUserTokenModelById($redis4, $response, $tokenmaker, 1, '123.123.123.123', 'test')
            instanceof \Auth\Model\UserTokenModel
        );
    }
    public function testgetAuthFacade()
    {
        $redis = $this->getMock('\Redis');
        $response = $this->getMock('\Auth\Model\Response');
        $tokenmaker = $this->getMock('Auth\Model\Library\TokenMaker');
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array('Authorization' => 'abc')));
        $userstatus = $this->getMock('\Auth\Model\UserStatus', array(), array(), '', false, true, true);
        $userstatus
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue(array('ip' => '123.123.123.123', 'useragent' => 'asdf')));
        $core = $this->getMock('\Auth\System\core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $core
            ->expects($this->any())
            ->method('getUserStatus')
            ->will($this->returnValue($userstatus));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getTokenMaker')
            ->will($this->returnValue($tokenmaker));
        $res = \Auth\System\Core::getInstance('test');
        $afm = $res->getAuthFacade($core);
        $this->assertEquals(true, $afm instanceof \Auth\Facade\AuthFacade);
    }
    public function testcheckUserEmailExistForClientWithEmptyEmail()
    {
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('emptyemail')
            ->will($this->returnValue('noemail'));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        // $pdo
        //     ->expects($this->any())
        //     ->method("prepare")
        //     ->will($this->returnValue($pdostatement));
        $res = \Auth\System\Core::getInstance('test');
        $assertData = 'noemail';
        $this->assertEquals(
            $assertData,
            $res->checkUserEmailExistForClient($pdo, $response, $elementvalidator, '')
        );
    }
    public function testcheckUserEmailExistForClientWithEmailFormatError()
    {
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('emailformaterror')
            ->will($this->returnValue('noemail'));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        // $pdo
        //     ->expects($this->any())
        //     ->method("prepare")
        //     ->will($this->returnValue($pdostatement));
        $res = \Auth\System\Core::getInstance('test');
        $assertData = 'noemail';
        $this->assertEquals(
            $assertData,
            $res->checkUserEmailExistForClient($pdo, $response, $elementvalidator, 'asdfasdf')
        );
    }
    public function testcheckUserEmailExistForClientWithDatabaseError()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->any())
            ->method('execute')
            ->will($this->returnValue(false));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('databaseerror')
            ->will($this->returnValue('nodb'));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->will($this->returnValue($pdostatement));
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $elementvalidator
            ->expects($this->any())
            ->method('validateEmail')
            ->will($this->returnValue(true));
        // $pdo
        //     ->expects($this->any())
        //     ->method("prepare")
        //     ->will($this->returnValue($pdostatement));
        $res = \Auth\System\Core::getInstance('test');
        $assertData = 'nodb';
        $this->assertEquals(
            $assertData,
            $res->checkUserEmailExistForClient($pdo, $response, $elementvalidator, 'asdfasdf')
        );
    }
    public function testcheckUserEmailExistForClientWithUserEmailExist()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->any())
            ->method('execute')
            ->will($this->returnValue(true));
        $pdostatement
            ->expects($this->any())
            ->method('fetch')
            ->will($this->returnValue(true));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('useremailexist')
            ->will($this->returnValue('nodb'));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->will($this->returnValue($pdostatement));
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $elementvalidator
            ->expects($this->any())
            ->method('validateEmail')
            ->will($this->returnValue(true));
        // $pdo
        //     ->expects($this->any())
        //     ->method("prepare")
        //     ->will($this->returnValue($pdostatement));
        $res = \Auth\System\Core::getInstance('test');
        $assertData = 'nodb';
        $this->assertEquals(
            $assertData,
            $res->checkUserEmailExistForClient($pdo, $response, $elementvalidator, 'asdfasdf')
        );
    }
    public function testcheckUserEmailExistForClientWithUserEmailAllow()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->any())
            ->method('execute')
            ->will($this->returnValue(true));
        $pdostatement
            ->expects($this->any())
            ->method('fetch')
            ->will($this->returnValue(null));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('useremailallow')
            ->will($this->returnValue('nodb'));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->will($this->returnValue($pdostatement));
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $elementvalidator
            ->expects($this->any())
            ->method('validateEmail')
            ->will($this->returnValue(true));
        // $pdo
        //     ->expects($this->any())
        //     ->method("prepare")
        //     ->will($this->returnValue($pdostatement));
        $res = \Auth\System\Core::getInstance('test');
        $assertData = 'nodb';
        $this->assertEquals(
            $assertData,
            $res->checkUserEmailExistForClient($pdo, $response, $elementvalidator, 'asdfasdf')
        );
    }
    public function testcheckUserEmailExistForClientWithUserNameExist()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->any())
            ->method('execute')
            ->will($this->returnValue(true));
        $pdostatement
            ->expects($this->any())
            ->method('fetch')
            ->will($this->returnValue(true));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('usernameexist')
            ->will($this->returnValue('nodb'));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->will($this->returnValue($pdostatement));
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $elementvalidator
            ->expects($this->any())
            ->method('validateUserName')
            ->will($this->returnValue(true));
        // $pdo
        //     ->expects($this->any())
        //     ->method("prepare")
        //     ->will($this->returnValue($pdostatement));
        $res = \Auth\System\Core::getInstance('test');
        $assertData = 'nodb';
        $this->assertEquals(
            $assertData,
            $res->checkUserNameExistForClient($pdo, $response, $elementvalidator, 'asdfasdf')
        );
    }
    public function testcheckUserEmailExistForClientWithUserNameAllow()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->any())
            ->method('execute')
            ->will($this->returnValue(true));
        $pdostatement
            ->expects($this->any())
            ->method('fetch')
            ->will($this->returnValue(null));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('usernameallow')
            ->will($this->returnValue('nodb'));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->will($this->returnValue($pdostatement));
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $elementvalidator
            ->expects($this->any())
            ->method('validateUserName')
            ->will($this->returnValue(true));
        // $pdo
        //     ->expects($this->any())
        //     ->method("prepare")
        //     ->will($this->returnValue($pdostatement));
        $res = \Auth\System\Core::getInstance('test');
        $assertData = 'nodb';
        $this->assertEquals(
            $assertData,
            $res->checkUserNameExistForClient($pdo, $response, $elementvalidator, 'asdfasdf')
        );
    }
    public function testcheckUserSecretAllow()
    {
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('usersecretallow')
            ->will($this->returnValue('nodb'));
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $elementvalidator
            ->expects($this->any())
            ->method('validateUserSecret')
            ->will($this->returnValue(true));
        $res = \Auth\System\Core::getInstance('test');
        $assertData = 'nodb';
        $this->assertEquals(
            $assertData,
            $res->checkUserSecretForClient($response, $elementvalidator, 'asdfasdf')
        );
    }
    public function testgetDateTime()
    {
        $res = \Auth\System\Core::getInstance('test');
        $this->assertEquals(true, $res->getDateTime() instanceof \DateTime);
    }
    public function testgetLoginLogModel()
    {
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $datetime = $this->getMock('\DateTime', array(), array(), '', false, true, true);
        $res = \Auth\System\Core::getInstance();
        $this->assertEquals(
            true,
            $res->getLoginLogModel(
                $pdo,
                $redis,
                $datetime,
                'testemail',
                'testip',
                'testuseragent',
                'testaction'
            ) instanceof \Auth\Model\LoginLogModel
        );
    }
}
