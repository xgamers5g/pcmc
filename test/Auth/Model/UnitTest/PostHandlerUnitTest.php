<?php
namespace AuthTest\Model\UnitTest;

use PHPUnit_Framework_TestCase;

class PostHandlerUnitTest extends PHPUnit_Framework_TestCase
{
    public function testgetInstance()
    {
        $res = \Auth\Model\PostHandler::getInstance();
        $this->assertEquals(true, $res instanceof \Auth\Model\PostHandler);
    }
    public function testgetJSONDataNotEmpty()
    {
        $res = \Auth\Model\PostHandler::getInstance();
        $this->assertEquals(
            array(
                'statuscode' => 200,
                'detail' => '這是測試檔案'
            ),
            $res->getJSONData(TEST_DIR.'/Auth/Model/UnitTest/mypostdata.dat', 'POST')
        );
    }
    public function testgetJSONDataIsEmpty()
    {
        $res = \Auth\Model\PostHandler::getInstance();
        $this->assertEquals(
            array(),
            $res->getJSONData(null, 'POST')
        );
    }
    public function testgetJSONDataNotPost()
    {
        $res = \Auth\Model\PostHandler::getInstance();
        $this->assertEquals(
            null,
            $res->getJSONData()
        );
    }
}
