<?php
namespace AuthTest\Model\Library\UnitTest;

use PHPUnit_Framework_TestCase;

class EncryptUnitTest extends PHPUnit_Framework_TestCase
{
    const SECRET = '12345';
    public function testgetInstance()
    {
        $this->assertTrue(
            \Auth\Model\Library\Encrypt::getInstance() instanceof \Auth\Model\Library\Encrypt
        );
    }
    public function testsecretEncode()
    {
        $res = \Auth\Model\Library\Encrypt::getInstance();
        $this->assertEquals(null, $res->secretEncode(null));
        for ($i=0; $i<300; $i++) {
            $this->assertNotEquals(
                array(
                    'salthash' => 'YmVkZjAy',
                    'usersecret' => 'f4ad1dc858725af4133f2e17159979'
                ),
                $res->secretEncode(self::SECRET),
                'nonono'
            );
        }
        $this->assertEquals('f4ad1dc858725af4133f2e17159979', $res->secretEncode('12345', 'YmVkZjAy'));
    }
    public function testgetSaltHash()
    {
        $fetchAllMock = $this
           ->getMockBuilder("\PDOStatement")
           ->setMethods(array("fetch"))
           ->getMock();
        $fetchAllMock
           ->expects($this->at(0))
           ->method("fetch")
           ->will($this->returnValue(array('salthash' => '123')));
        $fetchAllMock
           ->expects($this->at(1))
           ->method("fetch")
           ->will($this->returnValue(null));
        $mock = $this
           ->getMockBuilder("\PDO")
           ->disableOriginalConstructor()
           ->setMethods(array("prepare"))
           ->getMock();
        $mock
           ->expects($this->any())
           ->method("prepare")
           ->will($this->returnValue($fetchAllMock));
        
        $res = \Auth\Model\Library\Encrypt::getInstance()->getSaltHash($mock, '123');
        $this->assertEquals(123, $res);
        $res = \Auth\Model\Library\Encrypt::getInstance()->getSaltHash($mock, '123');
        $this->assertEquals(null, $res);
    }
}
