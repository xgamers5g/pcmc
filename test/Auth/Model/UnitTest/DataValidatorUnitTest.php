<?php
namespace AuthTest\Model\UnitTest;

use PHPUnit_Framework_TestCase;

class DataValidatorUnitTest extends PHPUnit_Framework_TestCase
{
    public function testgetInstance()
    {
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $data = array();
        $this->assertEquals(
            true,
            \Auth\Model\DataValidator::getInstance($response, $elementvalidator, $data)
            instanceof \Auth\Model\DataValidator
        );
    }
    public function testvalidateCreateUserDataSuccessWithDataError()
    {
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('dataerror')
            ->will($this->returnValue(true));
        $data = array(
        );
        $dv = \Auth\Model\DataValidator::getInstance($response, $elementvalidator, $data);
        $res = $dv->validateCreateUserData();
        $this->assertEquals(true, $res);
    }
    public function testvalidateCreateUserDataWithEmailFormateError()
    {
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $elementvalidator
            ->expects($this->at(0))
            ->method("validateEmail")
            ->will($this->returnValue(false));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('emailformaterror')
            ->will($this->returnValue(true));
        $data = array(
            'email' => '123',
            'username' => 'asdfg',
            'usersecret' => '9876544'
        );
        $dv = \Auth\Model\DataValidator::getInstance($response, $elementvalidator, $data);
        $res = $dv->validateCreateUserData();
        $this->assertEquals(true, $res);
    }
    public function testvalidateCreateUserDataWithUserNameFormatError()
    {
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $elementvalidator
            ->expects($this->at(0))
            ->method("validateEmail")
            ->will($this->returnValue(true));
        $elementvalidator
            ->expects($this->at(1))
            ->method("validateUserName")
            ->will($this->returnValue(false));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('usernameformaterror')
            ->will($this->returnValue(true));
        $data = array(
            'email' => '123',
            'username' => 'asdfg',
            'usersecret' => '9876544'
        );
        $dv = \Auth\Model\DataValidator::getInstance($response, $elementvalidator, $data);
        $res = $dv->validateCreateUserData();
        $this->assertEquals(true, $res);
    }
    public function testvalidateCreateUserDataWithUserSecretFormatError()
    {
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $elementvalidator
            ->expects($this->at(0))
            ->method("validateEmail")
            ->will($this->returnValue(true));
        $elementvalidator
            ->expects($this->at(1))
            ->method("validateUserName")
            ->will($this->returnValue(true));
        $elementvalidator
            ->expects($this->at(2))
            ->method("validateUserSecret")
            ->will($this->returnValue(false));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('usersecretformaterror')
            ->will($this->returnValue(true));
        $data = array(
            'email' => '123',
            'username' => 'asdfg',
            'usersecret' => '9876544'
        );
        $dv = \Auth\Model\DataValidator::getInstance($response, $elementvalidator, $data);
        $res = $dv->validateCreateUserData();
        $this->assertEquals(true, $res);
    }
    public function testvalidateCreateUserDataSuccess()
    {
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $elementvalidator
            ->expects($this->at(0))
            ->method("validateEmail")
            ->will($this->returnValue(true));
        $elementvalidator
            ->expects($this->at(1))
            ->method("validateUserName")
            ->will($this->returnValue(true));
        $elementvalidator
            ->expects($this->at(2))
            ->method("validateUserSecret")
            ->will($this->returnValue(true));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        // $response
        //     ->expects($this->at(0))
        //     ->method("generateResponse")
        //     ->with('usersecretformaterror')
        //     ->will($this->returnValue(true));
        $data = array(
            'email' => '123',
            'username' => 'asdfg',
            'usersecret' => '9876544'
        );
        $dv = \Auth\Model\DataValidator::getInstance($response, $elementvalidator, $data);
        $res = $dv->validateCreateUserData();
        $this->assertEquals(true, $res);
    }
    public function testvalidateLoginDataSuccessWithDataError()
    {
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('dataerror')
            ->will($this->returnValue(true));
        $data = array(
        );
        $dv = \Auth\Model\DataValidator::getInstance($response, $elementvalidator, $data);
        $res = $dv->validateLoginData();
        $this->assertEquals(true, $res);
    }
    public function testvalidateLoginDataWithEmailFormateError()
    {
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $elementvalidator
            ->expects($this->at(0))
            ->method("validateEmail")
            ->will($this->returnValue(false));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('emailformaterror')
            ->will($this->returnValue(true));
        $data = array(
            'email' => '123',
            'usersecret' => '9876544'
        );
        $dv = \Auth\Model\DataValidator::getInstance($response, $elementvalidator, $data);
        $res = $dv->validateLoginData();
        $this->assertEquals(true, $res);
    }
    public function testvalidateLoginDataWithUserSecretFormateError()
    {
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $elementvalidator
            ->expects($this->at(0))
            ->method("validateEmail")
            ->will($this->returnValue(true));
        $elementvalidator
            ->expects($this->at(1))
            ->method("validateUserSecret")
            ->will($this->returnValue(false));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('usersecretformaterror')
            ->will($this->returnValue(true));
        $data = array(
            'email' => '123',
            'usersecret' => '9876544'
        );
        $dv = \Auth\Model\DataValidator::getInstance($response, $elementvalidator, $data);
        $res = $dv->validateLoginData();
        $this->assertEquals(true, $res);
    }
    public function testvalidateLoginDataSuccess()
    {
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $elementvalidator
            ->expects($this->at(0))
            ->method("validateEmail")
            ->will($this->returnValue(true));
        $elementvalidator
            ->expects($this->at(1))
            ->method("validateUserSecret")
            ->will($this->returnValue(true));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $data = array(
            'email' => '123',
            'usersecret' => '9876544'
        );
        $dv = \Auth\Model\DataValidator::getInstance($response, $elementvalidator, $data);
        $res = $dv->validateLoginData();
        $this->assertEquals(true, $res);
    }
    public function testvalidateSetSecretDataSuccessWithDataError()
    {
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('dataerror')
            ->will($this->returnValue(true));
        $data = array(
        );
        $dv = \Auth\Model\DataValidator::getInstance($response, $elementvalidator, $data);
        $res = $dv->validateSetSecretData();
        $this->assertEquals(true, $res);
    }
    public function testvalidateSetSecretDataWithUserSecretFormateError()
    {
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $elementvalidator
            ->expects($this->at(0))
            ->method("validateUserSecret")
            ->will($this->returnValue(false));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('usersecretformaterror')
            ->will($this->returnValue(true));
        $data = array(
            'usersecret' => '9876544'
        );
        $dv = \Auth\Model\DataValidator::getInstance($response, $elementvalidator, $data);
        $res = $dv->validateSetSecretData();
        $this->assertEquals(true, $res);
    }
    public function testvalidateSetSecretDataSuccess()
    {
        $elementvalidator = $this->getMock('\Auth\Model\Library\ElementValidator');
        $elementvalidator
            ->expects($this->at(0))
            ->method("validateUserSecret")
            ->will($this->returnValue(true));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $data = array(
            'usersecret' => '9876544'
        );
        $dv = \Auth\Model\DataValidator::getInstance($response, $elementvalidator, $data);
        $res = $dv->validateSetSecretData();
        $this->assertEquals(true, $res);
    }
}
