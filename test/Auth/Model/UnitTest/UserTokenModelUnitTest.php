<?php
namespace AuthTest\Model\UnitTest;

use PHPUnit_Framework_TestCase;

class UserTokenModelUnitTest extends PHPUnit_Framework_TestCase
{
    public function testloadById()
    {
        $redis1 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis1
             ->expects($this->at(0))
             ->method("exec")
             ->will($this->returnValue(array(0=>true, 1=>true)));
        $redis1
             ->expects($this->at(1))
             ->method("exec")
             ->will($this->returnValue(array(0=>false, 1=>true)));
        $redis2 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis2
             ->expects($this->any())
             ->method("set")
             ->will($this->returnValue($redis1));
        $redis3 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis3
             ->expects($this->any())
             ->method("set")
             ->will($this->returnValue($redis2));
        $redis4 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis4
             ->expects($this->any())
             ->method("multi")
             ->will($this->returnValue($redis3));
        $redis4
             ->expects($this->at(0))
             ->method("exists")
             ->will($this->returnValue(false));
        $redis4
             ->expects($this->at(1))
             ->method("exists")
             ->will($this->returnValue(true));
        $response = $this
            ->getMockBuilder("\Auth\Model\Response")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('success')
            ->will($this->returnValue(array()));
        $response
            ->expects($this->at(1))
            ->method("generateResponse")
            ->with('databaseerror')
            ->will($this->returnValue('123'));

        $tokenmaker = $this
            ->getMockBuilder("\Auth\Model\Library\TokenMaker")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $tokenmaker
             ->expects($this->any())
             ->method("generateToken")
             ->will($this->returnValue('abc'));
        $res = \Auth\Model\UserTokenModel::loadById($redis4, $response, $tokenmaker, 1, '123.123.123.123', 'asdf');
        $this->assertEquals(
            true,
            $res instanceof \Auth\Model\UserTokenModel,
            'UserTokenModel::loadById成功路線'
        );
        $res = \Auth\Model\UserTokenModel::loadById($redis4, $response, $tokenmaker, 1, '123.123.123.123', 'asdf');
        $this->assertEquals(
            false,
            $res,
            'UserTokenModel::loadById失敗路線'
        );
    }

    public function testloadByTokenSuccess()
    {
        $redis20 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis20
            ->expects($this->any())
            ->method("exec")
            ->will($this->returnValue(array(0 => true, 1 => true, 2 => true)));
        $redis21 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis21
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis20));
        $redis22 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis22
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis21));
        $redis23 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis23
            ->expects($this->any())
            ->method("delete")
            ->will($this->returnValue($redis22));

        $redis24 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis24
            ->expects($this->at(0))
            ->method("exists")
            ->will($this->returnValue(true));
        $redis24
            ->expects($this->at(2))
            ->method("get")
            ->will(
                $this->returnValue(
                    json_encode(
                        array(
                            'expire' => time()+600,
                            'ip' => '123.123.123.123',
                            'useragent' => 'asdf',
                            'userid' => 1
                        )
                    )
                )
            );
        $redis24
            ->expects($this->any())
            ->method("multi")
            ->will($this->returnValue($redis23));

        $response = $this
            ->getMockBuilder("\Auth\Model\Response")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('success')
            ->will($this->returnValue(array()));
        $tokenmaker = $this
            ->getMockBuilder("\Auth\Model\Library\TokenMaker")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $tokenmaker
             ->expects($this->any())
             ->method("generateToken")
             ->will($this->returnValue('abc'));
        $res = \Auth\Model\UserTokenModel::loadByToken(
            $redis24,
            $response,
            $tokenmaker,
            '123',
            '123.123.123.123',
            'asdf'
        );
        $this->assertEquals(true, $res instanceof \Auth\Model\UserTokenModel);
    }

    public function testloadByTokenTokenError()
    {
        $redis1 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis1
            ->expects($this->at(0))
            ->method("exists")
            ->will($this->returnValue(false));
        $response = $this
            ->getMockBuilder("\Auth\Model\Response")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('tokenerror')
            ->will($this->returnValue(array()));
        $tokenmaker = $this
            ->getMockBuilder("\Auth\Model\Library\TokenMaker")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $tokenmaker
             ->expects($this->any())
             ->method("generateToken")
             ->will($this->returnValue('abc'));
        $res = \Auth\Model\UserTokenModel::loadByToken(
            $redis1,
            $response,
            $tokenmaker,
            '123',
            '123.123.123.123',
            'asdf'
        );
        $this->assertEquals(array(), $res);
    }

    public function testloadByTokenRedisError()
    {
        $redis30 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis30
            ->expects($this->any())
            ->method("exec")
            ->will($this->returnValue(array(0 => false, 1 => false, 2 => false)));
        $redis31 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis31
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis30));
        $redis32 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis32
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis31));
        $redis33 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis33
            ->expects($this->any())
            ->method("delete")
            ->will($this->returnValue($redis32));
        $redis34 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis34
            ->expects($this->at(0))
            ->method("exists")
            ->will($this->returnValue(true));
        $redis34
            ->expects($this->at(2))
            ->method("get")
            ->will(
                $this->returnValue(
                    json_encode(
                        array(
                            'expire' => time()+600,
                            'ip' => '123.123.123.123',
                            'useragent' => 'asdf',
                            'userid' => 1
                        )
                    )
                )
            );
        $redis34
            ->expects($this->any())
            ->method("multi")
            ->will($this->returnValue($redis33));

        $response2 = $this
            ->getMockBuilder("\Auth\Model\Response")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $response2
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('databaseerror')
            ->will($this->returnValue(array()));
        $tokenmaker = $this
            ->getMockBuilder("\Auth\Model\Library\TokenMaker")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $tokenmaker
             ->expects($this->any())
             ->method("generateToken")
             ->will($this->returnValue('abc'));
        $res = \Auth\Model\UserTokenModel::loadByToken(
            $redis34,
            $response2,
            $tokenmaker,
            '123',
            '123.123.123.123',
            'asdf'
        );
        //echo $res;
        $this->assertEquals(array(), $res);
    }

    public function testdeleteSuccess()
    {
        $redis21 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis21
            ->expects($this->any())
            ->method("exec")
            ->will($this->returnValue(array(0 => true, 1 => true)));
        $redis22 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis22
            ->expects($this->any())
            ->method("delete")
            ->will($this->returnValue($redis21));
        $redis23 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis23
            ->expects($this->any())
            ->method("delete")
            ->will($this->returnValue($redis22));
        $redis30 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis30
            ->expects($this->any())
            ->method("exec")
            ->will($this->returnValue(array(0 => true, 1 => true, 2 => true)));
        $redis31 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis31
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis30));
        $redis32 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis32
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis31));
        $redis33 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis33
            ->expects($this->any())
            ->method("delete")
            ->will($this->returnValue($redis32));

        $redis34 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis34
            ->expects($this->at(0))
            ->method("exists")
            ->will($this->returnValue(true));
        $redis34
            ->expects($this->at(2))
            ->method("get")
            ->will(
                $this->returnValue(
                    json_encode(
                        array(
                            'expire' => time()+600,
                            'ip' => '123.123.123.123',
                            'useragent' => 'asdf',
                            'userid' => 1
                        )
                    )
                )
            );
        $redis34
            ->expects($this->at(5))
            ->method("multi")
            ->will($this->returnValue($redis33));
        $redis34
            ->expects($this->at(6))
            ->method("watch")
            ->with('usertoken');
        $redis34
            ->expects($this->at(7))
            ->method("watch")
            ->with('usertokenabc');
        $redis34
            ->expects($this->at(8))
            ->method("multi")
            ->will($this->returnValue($redis23));

        $response = $this
            ->getMockBuilder("\Auth\Model\Response")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $response
            ->expects($this->at(1))
            ->method("generateResponse")
            ->with('success')
            ->will($this->returnValue(1));
        
        $tokenmaker = $this
            ->getMockBuilder("\Auth\Model\Library\TokenMaker")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $tokenmaker
             ->expects($this->any())
             ->method("generateToken")
             ->will($this->returnValue('abc'));

        $res = \Auth\Model\UserTokenModel::loadByToken(
            $redis34,
            $response,
            $tokenmaker,
            '123',
            '123.123.123.123',
            'asdf'
        );
        $this->assertEquals(1, $res->delete());
    }

    public function testdeleteFail()
    {
        $redis21 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis21
            ->expects($this->any())
            ->method("exec")
            ->will($this->returnValue(array(0 => false, 1 => true)));
        $redis22 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis22
            ->expects($this->any())
            ->method("delete")
            ->will($this->returnValue($redis21));
        $redis23 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis23
            ->expects($this->any())
            ->method("delete")
            ->will($this->returnValue($redis22));
        $redis30 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis30
            ->expects($this->any())
            ->method("exec")
            ->will($this->returnValue(array(0 => true, 1 => true, 2 => true)));

        $redis31 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis31
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis30));
        $redis32 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis32
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis31));
        $redis33 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis33
            ->expects($this->any())
            ->method("delete")
            ->will($this->returnValue($redis32));
        $redis34 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis34
            ->expects($this->at(0))
            ->method("exists")
            ->will($this->returnValue(true));
        $redis34
            ->expects($this->at(2))
            ->method("get")
            ->will(
                $this->returnValue(
                    json_encode(
                        array(
                            'expire' => time()+600,
                            'ip' => '123.123.123.123',
                            'useragent' => 'asdf',
                            'userid' => 1
                        )
                    )
                )
            );
        $redis34
            ->expects($this->at(5))
            ->method("multi")
            ->will($this->returnValue($redis33));
        $redis34
            ->expects($this->at(6))
            ->method("watch")
            ->with('usertoken');
        $redis34
            ->expects($this->at(7))
            ->method("watch")
            ->with('usertokenabc');
        $redis34
            ->expects($this->at(8))
            ->method("multi")
            ->will($this->returnValue($redis23));

        $response = $this
            ->getMockBuilder("\Auth\Model\Response")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $response
            ->expects($this->at(1))
            ->method("generateResponse")
            ->with('databaseerror')
            ->will($this->returnValue(1));
        $tokenmaker = $this
            ->getMockBuilder("\Auth\Model\Library\TokenMaker")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $tokenmaker
             ->expects($this->any())
             ->method("generateToken")
             ->will($this->returnValue('abc'));

        $res = \Auth\Model\UserTokenModel::loadByToken(
            $redis34,
            $response,
            $tokenmaker,
            '123',
            '123.123.123.123',
            'asdf'
        );
        $this->assertEquals(1, $res->delete());
    }

    public function testisTokenExistTrue()
    {
        $redis21 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis21
            ->expects($this->any())
            ->method("exec")
            ->will($this->returnValue(array(0 => true, 1 => true)));
        $redis22 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis22
            ->expects($this->any())
            ->method("exists")
            ->will($this->returnValue($redis21));
        $redis23 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis23
            ->expects($this->any())
            ->method("exists")
            ->will($this->returnValue($redis22));
        $redis30 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis30
            ->expects($this->any())
            ->method("exec")
            ->will($this->returnValue(array(0 => true, 1 => true, 2 => true)));
        $redis31 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis31
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis30));
        $redis32 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis32
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis31));

        $redis33 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis33
            ->expects($this->any())
            ->method("delete")
            ->will($this->returnValue($redis32));
        $redis34 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis34
            ->expects($this->at(0))
            ->method("exists")
            ->will($this->returnValue(true));
        $redis34
            ->expects($this->at(2))
            ->method("get")
            ->will(
                $this->returnValue(
                    json_encode(
                        array(
                            'expire' => time()+600,
                            'ip' => '123.123.123.123',
                            'useragent' => 'asdf',
                            'userid' => 1
                        )
                    )
                )
            );
        $redis34
            ->expects($this->at(5))
            ->method("multi")
            ->will($this->returnValue($redis33));
        $redis34
            ->expects($this->at(6))
            ->method("multi")
            ->will($this->returnValue($redis23));
        $response = $this
            ->getMockBuilder("\Auth\Model\Response")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $tokenmaker = $this
            ->getMockBuilder("\Auth\Model\Library\TokenMaker")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $tokenmaker
             ->expects($this->any())
             ->method("generateToken")
             ->will($this->returnValue('abc'));
        $res = \Auth\Model\UserTokenModel::loadByToken(
            $redis34,
            $response,
            $tokenmaker,
            '123',
            '123.123.123.123',
            'asdf'
        );
        $this->assertEquals(true, $res->isTokenExist());
    }
    public function testisTokenExistFalse()
    {
        $redis21 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis21
            ->expects($this->any(0))
            ->method("exec")
            ->will($this->returnValue(array(0 => false, 1 => true)));
        $redis22 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis22
            ->expects($this->any())
            ->method("exists")
            ->will($this->returnValue($redis21));
        $redis23 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis23
            ->expects($this->any())
            ->method("exists")
            ->will($this->returnValue($redis22));
        $redis30 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis30
            ->expects($this->any())
            ->method("exec")
            ->will($this->returnValue(array(0 => true, 1 => true, 2 => true)));
        $redis31 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis31
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis30));
        $redis32 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis32
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis31));

        $redis33 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis33
            ->expects($this->any())
            ->method("delete")
            ->will($this->returnValue($redis32));
        $redis34 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis34
            ->expects($this->at(0))
            ->method("exists")
            ->will($this->returnValue(true));
        $redis34
            ->expects($this->at(2))
            ->method("get")
            ->will(
                $this->returnValue(
                    json_encode(
                        array(
                            'expire' => time()+600,
                            'ip' => '123.123.123.123',
                            'useragent' => 'asdf',
                            'userid' => 1
                        )
                    )
                )
            );
        $redis34
            ->expects($this->at(5))
            ->method("multi")
            ->will($this->returnValue($redis33));
        $redis34
            ->expects($this->at(6))
            ->method("multi")
            ->will($this->returnValue($redis23));
        $response = $this
            ->getMockBuilder("\Auth\Model\Response")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $tokenmaker = $this
            ->getMockBuilder("\Auth\Model\Library\TokenMaker")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $tokenmaker
             ->expects($this->any())
             ->method("generateToken")
             ->will($this->returnValue('abc'));
        $res = \Auth\Model\UserTokenModel::loadByToken(
            $redis34,
            $response,
            $tokenmaker,
            '123',
            '123.123.123.123',
            'asdf'
        );
        $this->assertEquals(false, $res->isTokenExist());
    }

    public function testgetUid()
    {
        $redis30 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis30
            ->expects($this->any())
            ->method("exec")
            ->will($this->returnValue(array(0 => true, 1 => true, 2 => true)));
        $redis31 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis31
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis30));
        $redis32 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis32
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis31));

        $redis33 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis33
            ->expects($this->any())
            ->method("delete")
            ->will($this->returnValue($redis32));
        $redis34 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis34
            ->expects($this->at(0))
            ->method("exists")
            ->will($this->returnValue(true));
        $redis34
            ->expects($this->at(1))
            ->method("get")
            ->will(
                $this->returnValue(
                    json_encode(
                        array(
                            'userid' => 1
                        )
                    )
                )
            );
        $redis34
            ->expects($this->at(2))
            ->method("get")
            ->will(
                $this->returnValue(
                    json_encode(
                        array(
                            'expire' => time()+600,
                            'ip' => '123.123.123.123',
                            'useragent' => 'asdf',
                            'userid' => 1
                        )
                    )
                )
            );
        $redis34
            ->expects($this->at(5))
            ->method("multi")
            ->will($this->returnValue($redis33));
        $response = $this
            ->getMockBuilder("\Auth\Model\Response")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $tokenmaker = $this
            ->getMockBuilder("\Auth\Model\Library\TokenMaker")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $tokenmaker
             ->expects($this->any())
             ->method("generateToken")
             ->will($this->returnValue('abc'));
        $res = \Auth\Model\UserTokenModel::loadByToken(
            $redis34,
            $response,
            $tokenmaker,
            '123',
            '123.123.123.123',
            'asdf'
        );
        $this->assertEquals(1, $res->getUid());
    }
    public function testgetToken()
    {
        $redis30 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis30
            ->expects($this->any())
            ->method("exec")
            ->will($this->returnValue(array(0 => true, 1 => true, 2 => true)));
        $redis31 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis31
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis30));
        $redis32 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis32
            ->expects($this->any())
            ->method("set")
            ->will($this->returnValue($redis31));

        $redis33 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis33
            ->expects($this->any())
            ->method("delete")
            ->will($this->returnValue($redis32));
        $redis34 = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis34
            ->expects($this->at(0))
            ->method("exists")
            ->will($this->returnValue(true));
        $redis34
            ->expects($this->at(1))
            ->method("get")
            ->will(
                $this->returnValue(
                    json_encode(
                        array(
                            'userid' => 1
                        )
                    )
                )
            );
        $redis34
            ->expects($this->at(2))
            ->method("get")
            ->will(
                $this->returnValue(
                    json_encode(
                        array(
                            'expire' => time()+600,
                            'ip' => '123.123.123.123',
                            'useragent' => 'asdf',
                            'userid' => 1
                        )
                    )
                )
            );
        $redis34
            ->expects($this->at(5))
            ->method("multi")
            ->will($this->returnValue($redis33));
        $response = $this
            ->getMockBuilder("\Auth\Model\Response")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $tokenmaker = $this
            ->getMockBuilder("\Auth\Model\Library\TokenMaker")
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
        $tokenmaker
             ->expects($this->any())
             ->method("generateToken")
             ->will($this->returnValue('abc'));
        $res = \Auth\Model\UserTokenModel::loadByToken(
            $redis34,
            $response,
            $tokenmaker,
            '123',
            '123.123.123.123',
            'asdf'
        );
        $this->assertEquals('abc', $res->getToken());
    }
}
