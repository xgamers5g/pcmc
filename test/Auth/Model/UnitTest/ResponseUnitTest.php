<?php
namespace AuthTest\Model\UnitTest;

use PHPUnit_Framework_TestCase;

class ResponseUnitTest extends PHPUnit_Framework_TestCase
{
    public function testgenerateResponse()
    {
        $response = \Auth\Model\Response::getInstance();
        $status = array(
            0 => 'success',
            1 => 'dataerror',
            2 => 'databaseerror',
            3 => 'tokenerror',
            4 => 'tokenformaterror',
            5 => 'loginerror',
            6 => 'emptyemail',
            7 => 'emptyusername',
            8 => 'useremailexist',
            9 => 'usernameexist',
            10 => 'useremailallow',
            11 => 'usernameallow',
            12 => 'usernameformaterror',
            13 => 'emptyusersecret',
            14 => 'usersecretformaterror',
            15 => 'emailformaterror',
            16 => 'usersecretallow',
            17 => 'isnotlogin',
            18 => 'index',
            19 => 'nofriend',
            20 => 'islogin'
        );
        $res = array(
            0 => array(
                'statuscode' => '200',
                'detail' => '執行成功'
            ),
            1 => array(
                'statuscode' => '400',
                'detail' => '請求資料格式錯誤'
            ),
            2 => array(
                'statuscode' => '599',
                'detail' => '資料庫已經爆炸了'
            ),
            3 => array(
                'statuscode' => '401',
                'detail' => 'access_token不存在、不合法或過期'
            ),
            4 => array(
                'statuscode' => '401',
                'detail' => '這是偽造的access_token'
            ),
            5 => array(
                'statuscode' => '401',
                'detail' => '登入帳號或是密碼錯誤'
            ),
            6 => array(
                'statuscode' => '401',
                'detail' => '沒有輸入使用者email'
            ),
            7 => array(
                'statuscode' => '401',
                'detail' => '沒有輸入使用者帳號'
            ),
            8 => array(
                'statuscode' => '401',
                'detail' => '該電郵已經被使用，請嘗試其它電郵。'
            ),
            9 => array(
                'statuscode' => '401',
                'detail' => '該帳號已經被使用，請嘗試其它帳號。'
            ),
            10 => array(
                'statuscode' => '200',
                'detail' => '此電郵可以註冊'
            ),
            11 => array(
                'statuscode' => '200',
                'detail' => '此帳號可以註冊'
            ),
            12 => array(
                'statuscode' => '401',
                'detail' => '帳號格式不正確, 本系統只支援0-9,a-z,A-Z，長度必需超過5'
            ),
            13 => array(
                'statuscode' => '401',
                'detail' => '沒有輸入密碼'
            ),
            14 => array(
                'statuscode' => '401',
                'detail' => '密碼格式不正確, 本系統只支援0-9,a-z,A-Z，長度必需超過5'
            ),
            15 => array(
                'statuscode' => '401',
                'detail' => 'email信箱格式不正確'
            ),
            16 => array(
                'statuscode' => '200',
                'detail' => '此密碼可以使用'
            ),
            17 => array(
                'statuscode' => '401',
                'detail' => '不在登入狀態'
            ),
            18 => array(
                'statuscode' => '404',
                'detail' => '不正確的請求uri(get?post?)'
            ),
            19 => array(
                'statuscode' => '404',
                'detail' => '你沒有朋友'
            ),
            20 => array(
                'statuscode' => '401',
                'detail' => '請先登出'
            )
        );
        for ($i=0; $i<21; $i++) {
            $this->assertEquals($res[$i], $response->generateResponse($status[$i]));
        }
    }
}
