<?php
namespace AuthTest\Model\Library\UnitTest;

use PHPUnit_Framework_TestCase;

class ElementValidatorUnitTest extends PHPUnit_Framework_TestCase
{
    public function testgetInstance()
    {
        $res = \Auth\Model\Library\ElementValidator::getInstance();
        $this->assertEquals(true, $res instanceof \Auth\Model\Library\ElementValidator);
    }
    public function testValidateAccessToken()
    {
        $value1 = 'abcdefghijklmnopqrstuvwxyz12341234567890';
        $value2 = 'abcdefghijklmnopqrstuvwxyz123412345678901';
        $value3 = 'abcdefghijklmnopqrstuvwxyz1234123456789#';
        $this->assertEquals(
            true,
            \Auth\Model\Library\ElementValidator::getInstance()->validateAccessToken($value1)
        );
        $this->assertEquals(
            false,
            \Auth\Model\Library\ElementValidator::getInstance()->validateAccessToken($value2)
        );
        $this->assertEquals(
            false,
            \Auth\Model\Library\ElementValidator::getInstance()->validateAccessToken($value3)
        );
    }
    public function testValidateUserName()
    {
        $value1 = 'asdfa';
        $value2 = '1aasd';
        $value3 = 'a99';
        $value4 = 123;
        $value5 = 'qqa###$';
        $ev = \Auth\Model\Library\ElementValidator::getInstance();
        $this->assertEquals(true, $ev->validateUserName($value1));
        $this->assertEquals(true, $ev->validateUserName($value2));
        $this->assertEquals(false, $ev->validateUserName($value3));
        $this->assertEquals(false, $ev->validateUserName($value4));
        $this->assertEquals(false, $ev->validateUserName($value5));
    }
    public function testValidateUserSecret()
    {
        $value1 = 'asdfa';
        $value2 = '1aasd';
        $value3 = 'a99';
        $value4 = 123;
        $value5 = 'qqa###$';
        $ev = \Auth\Model\Library\ElementValidator::getInstance();
        $this->assertEquals(true, $ev->validateUserSecret($value1));
        $this->assertEquals(true, $ev->validateUserSecret($value2));
        $this->assertEquals(false, $ev->validateUserSecret($value3));
        $this->assertEquals(false, $ev->validateUserSecret($value4));
        $this->assertEquals(false, $ev->validateUserSecret($value5));
    }
    public function testValidateEmail()
    {
        $value1 = 'xgamers5g@hotmail.com';
        $value2 = 'xgamers5g@jizzlab.uni.me';
        $value3 = '1@3.c';
        $value4 = 'aasdf8333';
        $value5 = 'select * from user';
        $ev = \Auth\Model\Library\ElementValidator::getInstance();
        $this->assertEquals(true, $ev->validateEmail($value1));
        $this->assertEquals(true, $ev->validateEmail($value2));
        $this->assertEquals(false, $ev->validateEmail($value3));
        $this->assertEquals(false, $ev->validateEmail($value4));
        $this->assertEquals(false, $ev->validateEmail($value5));
    }
}
