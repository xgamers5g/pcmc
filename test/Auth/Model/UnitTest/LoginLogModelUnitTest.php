<?php
namespace AuthTest\Model\UnitTest;

use PHPUnit_Framework_TestCase;

class LoginLogModelUnitTest extends PHPUnit_Framework_TestCase
{
    public function testgetInstance()
    {
        
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $datetime = $this->getMock('\DateTime', array(), array(), '', false, true, true);

        $res = \Auth\Model\LoginLogModel::getInstance(
            $pdo,
            $redis,
            $datetime,
            'testemail',
            'testip',
            'testuseragent',
            'testaction'
        );
        $this->assertEquals(true, $res instanceof \Auth\Model\LoginLogModel);
    }
    public function testcreateLogWithPdoSuccessAndRedisSuccess()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->at(0))
            ->method('bindValue')
            ->with(1, 'testemail', 2);
        $pdostatement
            ->expects($this->at(1))
            ->method('bindValue')
            ->with(2, 'testip', 2);
        $pdostatement
            ->expects($this->at(2))
            ->method('bindValue')
            ->with(3, 'testuseragent', 2);
        $pdostatement
            ->expects($this->at(3))
            ->method('bindValue')
            ->with(4, 'testaction', 2);
        $pdostatement
            ->expects($this->at(4))
            ->method('execute')
            ->will($this->returnValue(true));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->with('insert into loginlog(email, ip, useragent, action) values (?, ?, ?, ?)')
            ->will($this->returnValue($pdostatement));
        $pdo
            ->expects($this->any())
            ->method('lastInsertId')
            ->will($this->returnValue(1));
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $data = array(
            "email" => "testemail",
            "time" => "testtime",
            "ip" => "testip",
            "useragent" => "testuseragent",
            "action" => "testaction"
        );
        $redis
            ->expects($this->at(0))
            ->method('set')
            ->with(
                'loginlog1',
                json_encode($data)
            )
            ->will($this->returnValue(true));
        $datetime = $this->getMock('\DateTime', array(), array(), '', false, true, true);
        $datetime
            ->expects($this->any())
            ->method('format')
            ->with('Y-m-d H:i:s')
            ->will($this->returnValue('testtime'));

        $res = \Auth\Model\LoginLogModel::getInstance(
            $pdo,
            $redis,
            $datetime,
            'testemail',
            'testip',
            'testuseragent',
            'testaction'
        );
        $this->assertEquals(true, $res->createlog());
    }
    public function testcreateLogWithPdoSuccessAndRedisFail()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->at(0))
            ->method('bindValue')
            ->with(1, 'testemail', 2);
        $pdostatement
            ->expects($this->at(1))
            ->method('bindValue')
            ->with(2, 'testip', 2);
        $pdostatement
            ->expects($this->at(2))
            ->method('bindValue')
            ->with(3, 'testuseragent', 2);
        $pdostatement
            ->expects($this->at(3))
            ->method('bindValue')
            ->with(4, 'testaction', 2);
        $pdostatement
            ->expects($this->at(4))
            ->method('execute')
            ->will($this->returnValue(true));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->with('insert into loginlog(email, ip, useragent, action) values (?, ?, ?, ?)')
            ->will($this->returnValue($pdostatement));
        $pdo
            ->expects($this->any())
            ->method('lastInsertId')
            ->will($this->returnValue(1));
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $data = array(
            "email" => "testemail",
            "time" => "testtime",
            "ip" => "testip",
            "useragent" => "testuseragent",
            "action" => "testaction"
        );
        $redis
            ->expects($this->at(0))
            ->method('set')
            ->with(
                'loginlog1',
                json_encode($data)
            )
            ->will($this->returnValue(false));
        $datetime = $this->getMock('\DateTime', array(), array(), '', false, true, true);
        $datetime
            ->expects($this->any())
            ->method('format')
            ->with('Y-m-d H:i:s')
            ->will($this->returnValue('testtime'));

        $res = \Auth\Model\LoginLogModel::getInstance(
            $pdo,
            $redis,
            $datetime,
            'testemail',
            'testip',
            'testuseragent',
            'testaction'
        );
        $this->assertEquals(false, $res->createlog());
    }
    public function testcreateLogWithPdoFail()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->at(0))
            ->method('bindValue')
            ->with(1, 'testemail', 2);
        $pdostatement
            ->expects($this->at(1))
            ->method('bindValue')
            ->with(2, 'testip', 2);
        $pdostatement
            ->expects($this->at(2))
            ->method('bindValue')
            ->with(3, 'testuseragent', 2);
        $pdostatement
            ->expects($this->at(3))
            ->method('bindValue')
            ->with(4, 'testaction', 2);
        $pdostatement
            ->expects($this->at(4))
            ->method('execute')
            ->will($this->returnValue(false));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->with('insert into loginlog(email, ip, useragent, action) values (?, ?, ?, ?)')
            ->will($this->returnValue($pdostatement));
        $pdo
            ->expects($this->any())
            ->method('lastInsertId')
            ->will($this->returnValue(1));
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $datetime = $this->getMock('\DateTime', array(), array(), '', false, true, true);
        $res = \Auth\Model\LoginLogModel::getInstance(
            $pdo,
            $redis,
            $datetime,
            'testemail',
            'testip',
            'testuseragent',
            'testaction'
        );
        $this->assertEquals(false, $res->createlog());

    }
}
