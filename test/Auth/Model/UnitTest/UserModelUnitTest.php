<?php
namespace AuthTest\Model\UnitTest;

use PHPUnit_Framework_TestCase;

class UserModelUnitTest extends PHPUnit_Framework_TestCase
{
    public function testloadById()
    {
        $fetchAllMock = $this
            ->getMockBuilder("\PDOStatement")
            ->setMethods(array("fetch"))
            ->getMock();
        $fetchAllMock
            ->expects($this->at(0))->method("fetch")
            ->will($this->returnValue(array('salthash' => '123')));
        $fetchAllMock
            ->expects($this->at(1))->method("fetch")
            ->will($this->returnValue(null));
        $mock = $this
            ->getMockBuilder("\PDO")
            ->disableOriginalConstructor()
            ->setMethods(array("prepare"))
            ->getMock();
        $mock
            ->expects($this->any())
            ->method("prepare")
            ->will($this->returnValue($fetchAllMock));
        $res = \Auth\Model\UserModel::loadById($mock, 1);
        $this->assertEquals(true, $res instanceof \Auth\Model\UserModel);
        $res = \Auth\Model\UserModel::loadById($mock, 1);
        $this->assertEquals(null, $res);
    }

    public function testsetSecret()
    {
        $fetchAllMock = $this
            ->getMockBuilder("\PDOStatement")
            ->setMethods(array("fetch"))
            ->getMock();
        $fetchAllMock
            ->expects($this->at(0))->method("fetch")
            ->will($this->returnValue(true));
        $fetchAllMock2 = $this
            ->getMockBuilder("\PDOStatement")
            ->setMethods(array("execute"))
            ->getMock();
        $fetchAllMock2
           ->expects($this->at(0))->method("execute")
           ->will($this->returnValue(true));
        $fetchAllMock3 = $this
            ->getMockBuilder("\PDOStatement")
            ->setMethods(array("execute"))
            ->getMock();
        $fetchAllMock3
           ->expects($this->at(0))->method("execute")
           ->will($this->returnValue(false));
        $pdo = $this
           ->getMockBuilder("\PDO")
           ->disableOriginalConstructor()
           ->setMethods(array("prepare"))
           ->getMock();
        $pdo
           ->expects($this->at(0))
           ->method("prepare")
           ->will($this->returnValue($fetchAllMock));
        $pdo
           ->expects($this->at(1))
           ->method("prepare")
           ->will($this->returnValue($fetchAllMock2));
        $pdo
           ->expects($this->at(2))
           ->method("prepare")
           ->will($this->returnValue($fetchAllMock3));
        $encrypt = $this
           ->getMockBuilder("\Auth\Model\Library\Encrypt")
           ->disableOriginalConstructor()
           ->getMock();
        $response = $this
           ->getMockBuilder("\Auth\Model\Response")
           ->disableOriginalConstructor()
           ->setMethods(array("generateResponse"))
           ->getMock();
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('success')
            ->will($this->returnValue(true));
        $response
            ->expects($this->at(1))
            ->method("generateResponse")
            ->with('databaseerror')
            ->will($this->returnValue(true));
        $um = \Auth\Model\UserModel::loadById($pdo, 1);
        //var_dump($um);
        $this->assertEquals(
            true,
            $um->setSecret($encrypt, $response, '1')
        );
        $this->assertEquals(
            true,
            $um->setSecret($encrypt, $response, '1')
        );
    }

    public function testdeleteUser()
    {
        $fetchAllMock = $this
            ->getMockBuilder("\PDOStatement")
            ->setMethods(array("fetch"))
            ->getMock();
        $fetchAllMock
            ->expects($this->at(0))->method("fetch")
            ->will($this->returnValue(true));
        $fetchAllMock2 = $this
            ->getMockBuilder("\PDOStatement")
            ->setMethods(array("execute"))
            ->getMock();
        $fetchAllMock2
           ->expects($this->at(0))->method("execute")
           ->will($this->returnValue(true));
        $fetchAllMock3 = $this
            ->getMockBuilder("\PDOStatement")
            ->setMethods(array("execute"))
            ->getMock();
        $fetchAllMock3
           ->expects($this->at(0))->method("execute")
           ->will($this->returnValue(false));
        $pdo = $this
           ->getMockBuilder("\PDO")
           ->disableOriginalConstructor()
           ->setMethods(array("prepare"))
           ->getMock();
        $pdo
           ->expects($this->at(0))
           ->method("prepare")
           ->will($this->returnValue($fetchAllMock));
        $pdo
           ->expects($this->at(1))
           ->method("prepare")
           ->will($this->returnValue($fetchAllMock2));
        $pdo
           ->expects($this->at(2))
           ->method("prepare")
           ->will($this->returnValue($fetchAllMock3));
        $encrypt = $this
           ->getMockBuilder("\Auth\Model\Library\Encrypt")
           ->disableOriginalConstructor()
           ->getMock();
        $response = $this
           ->getMockBuilder("\Auth\Model\Response")
           ->disableOriginalConstructor()
           ->setMethods(array("generateResponse"))
           ->getMock();
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('success')
            ->will($this->returnValue(true));
        $response
            ->expects($this->at(1))
            ->method("generateResponse")
            ->with('databaseerror')
            ->will($this->returnValue(true));
        $um = \Auth\Model\UserModel::loadById($pdo, 1);
        $this->assertEquals(
            true,
            $um->deleteUser($response)
        );
        $this->assertEquals(
            true,
            $um->deleteUser($response)
        );
    }

    public function testgetUserData()
    {
        $fetchAllMock = $this
            ->getMockBuilder("\PDOStatement")
            ->setMethods(array("fetch"))
            ->getMock();
        $fetchAllMock
            ->expects($this->at(0))->method("fetch")
            ->will($this->returnValue(true));
        $fetchAllMock2 = $this
            ->getMockBuilder("\PDOStatement")
            ->setMethods(array("fetch"))
            ->getMock();
        $fetchAllMock2
           ->expects($this->at(0))->method("fetch")
           ->will($this->returnValue(array('id'=>1, 'email'=>'2', 'username'=>'3')));
        $fetchAllMock3 = $this
            ->getMockBuilder("\PDOStatement")
            ->setMethods(array("fetch"))
            ->getMock();
        $fetchAllMock3
           ->expects($this->at(0))->method("fetch")
           ->will($this->returnValue(false));
        $pdo = $this
           ->getMockBuilder("\PDO")
           ->disableOriginalConstructor()
           ->setMethods(array("prepare"))
           ->getMock();
        $pdo
           ->expects($this->at(0))
           ->method("prepare")
           ->will($this->returnValue($fetchAllMock));
        $pdo
           ->expects($this->at(1))
           ->method("prepare")
           ->will($this->returnValue($fetchAllMock2));
        $pdo
           ->expects($this->at(2))
           ->method("prepare")
           ->will($this->returnValue($fetchAllMock3));
        $response = $this
           ->getMockBuilder("\Auth\Model\Response")
           ->disableOriginalConstructor()
           ->setMethods(array("generateResponse"))
           ->getMock();
        $response
            ->expects($this->at(0))
            ->method("generateResponse")
            ->with('success')
            ->will($this->returnValue(array('statuscode'=>'123','detail'=>'789')));
        $response
            ->expects($this->at(1))
            ->method("generateResponse")
            ->with('databaseerror')
            ->will($this->returnValue(true));
        $um = \Auth\Model\UserModel::loadById($pdo, 1);
        $this->assertEquals(
            array(
              'statuscode' => '123',
              'detail' => '789',
              'id' => 1,
              'email' => '2',
              'username' => '3'
            ),
            $um->getUserData($response)
        );
        $this->assertEquals(
            true,
            $um->getUserData($response)
        );
    }
    public function testloadByLoginSuccess()
    {
        $fetchAllMock = $this
            ->getMockBuilder("\PDOStatement")
            ->setMethods(array("fetch"))
            ->getMock();
        $fetchAllMock
            ->expects($this->at(0))->method("fetch")
            ->will($this->returnValue(array('id' => 1)));
        $pdo = $this
            ->getMockBuilder("\PDO")
            ->disableOriginalConstructor()
            ->setMethods(array("prepare"))
            ->getMock();
        $pdo
            ->expects($this->any())
            ->method("prepare")
            ->will($this->returnValue($fetchAllMock));
        $encrypt = $this
            ->getMockBuilder("\Auth\Model\Library\Encrypt")
            ->disableOriginalConstructor()
            ->getMock();
        $response = $this
            ->getMockBuilder("\Auth\Model\Response")
            ->disableOriginalConstructor()
            ->setMethods(array("generateResponse"))
            ->getMock();
        
        $um = \Auth\Model\UserModel::loadByLogin(
            $pdo,
            $encrypt,
            $response,
            'xgamers5g@hotmail.com',
            '12345'
        );
        $this->assertEquals(true, $um instanceof \Auth\Model\UserModel);
    }

    public function testloadByLoginFail()
    {
        $fetchAllMock = $this
            ->getMockBuilder("\PDOStatement")
            ->setMethods(array("fetch"))
            ->getMock();
        $fetchAllMock
            ->expects($this->at(0))->method("fetch")
            ->will($this->returnValue(null));
        $pdo = $this
            ->getMockBuilder("\PDO")
            ->disableOriginalConstructor()
            ->setMethods(array("prepare"))
            ->getMock();
        $pdo
            ->expects($this->any())
            ->method("prepare")
            ->will($this->returnValue($fetchAllMock));
        $encrypt = $this
            ->getMockBuilder("\Auth\Model\Library\Encrypt")
            ->disableOriginalConstructor()
            ->getMock();
        $response = $this
            ->getMockBuilder("\Auth\Model\Response")
            ->disableOriginalConstructor()
            ->setMethods(array("generateResponse"))
            ->getMock();
        
        $um = \Auth\Model\UserModel::loadByLogin(
            $pdo,
            $encrypt,
            $response,
            'xgamers5g@hotmail.com',
            '12345'
        );
        $this->assertEquals(null, $um);
    }
}
