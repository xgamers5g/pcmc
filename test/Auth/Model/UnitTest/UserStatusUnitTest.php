<?php
namespace AuthTest\Model\UnitTest;

use PHPUnit_Framework_TestCase;

class UserStatusUnitTest extends PHPUnit_Framework_TestCase
{
    public function testgetInstance()
    {
        $res = \Auth\Model\UserStatus::getInstance();
        $this->assertEquals(true, $res instanceof \Auth\Model\UserStatus);
    }
    public function testgetUserStatusDI()
    {
        $res = \Auth\Model\UserStatus::getInstance();
        $this->assertEquals(
            array(
                'ip' => '123.123.123.123',
                'useragent' => 'garthsuperman'
            ),
            $res->getUserStatus('123.123.123.123', 'garthsuperman')
        );
    }
    public function testgetUserStatusWithoutIp()
    {
        $res = \Auth\Model\UserStatus::getInstance();
        $this->assertEquals(
            array(
                'ip' => null,
                'useragent' => 'garthsuperman'
            ),
            $res->getUserStatus(null, 'garthsuperman')
        );
    }
    public function testgetUserStatusWithoutUserAgent()
    {
        $res = \Auth\Model\UserStatus::getInstance();
        $this->assertEquals(
            array(
                'ip' => '123.123.123.123',
                'useragent' => null
            ),
            $res->getUserStatus('123.123.123.123')
        );
    }
    public function testgetUserStatusWithoutAll()
    {
        $res = \Auth\Model\UserStatus::getInstance();
        $this->assertEquals(
            array(
                'ip' => null,
                'useragent' => null
            ),
            $res->getUserStatus()
        );
    }
}
