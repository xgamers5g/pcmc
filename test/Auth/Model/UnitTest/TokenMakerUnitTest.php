<?php
namespace AuthTest\Model\Library\UnitTest;

use PHPUnit_Framework_TestCase;

class TokenMakerUnitTest extends PHPUnit_Framework_TestCase
{
    public function testgetInstance()
    {
        $res = \Auth\Model\Library\TokenMaker::getInstance();
        $this->assertEquals(
            get_class($res),
            'Auth\Model\Library\TokenMaker'
        );
    }
    public function testgenerateToken()
    {
        $res = \Auth\Model\Library\TokenMaker::getInstance();
        $this->assertEquals(
            40,
            strlen($res->generateToken(40))
        );
        $this->assertEquals(
            true,
            preg_match("/^[A-Za-z0-9]+$/", $res->generateToken(40))
        );
    }
}
