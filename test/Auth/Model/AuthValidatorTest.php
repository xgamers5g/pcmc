<?php
namespace AuthTest\Model;

use PHPUnit_Extensions_Database_TestCase;
use AuthTest\DataSet;
use Fruit\Seed;
use Auth\Model\AuthValidator;
use Auth\Model\AuthModel;
use Auth\Model\DataBox;
use Auth\Model\Encrypt;
use Redis;
use PDO;

class AuthValidatorTest extends PHPUnit_Extensions_Database_TestCase
{
    ///為此測試設置一個PDO資料庫連線
    public function getConnection()
    {
        $conn = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }
    ///為此測試設置一組資料
    public function getDataSet()
    {
        return new DataSet(
            [
                'user' => [
                    [
                        'email' => 'xgamers5g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'garth',
                        'usersecret' => Encrypt::secretEncode('12345', 'Mzk5ZmFl')
                    ],
                    [
                        'email' => 'xgamers6g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'louis',
                        'usersecret' => Encrypt::secretEncode('12345', 'Mzk5ZmFl')
                    ]
                ],
            ]
        );
    }
    public function testDataSet()
    {
        $this->assertEquals(2, $this->getConnection()->getRowCount('user'));
    }
    public function testvalidateToken()
    {
        $redis = new Redis();
        $redis->pconnect('/tmp/redis.sock');
        $redis->select(1);
        $redis->flushDB();
        $redis->set(
            'usertoken1',
            json_encode(
                array(
                    'ip' => '125.210.188.36',
                    'useragent' => 'I am Garth!',
                    'access_token' => 'garth12345678901234567890123456789012345',
                    'expire' => time()
                )
            )
        );
        $redis->set(
            'usertokengarth12345678901234567890123456789012345',
            json_encode(
                array(
                    'userid' => 1
                )
            )
        );
        $redis->set(
            'usertoken2',
            json_encode(
                array(
                    'ip' => '125.210.188.36',
                    'useragent' => 'I am Garth!',
                    'access_token' => 'louis12345678901234567890123456789012345',
                    'expire' => time()-601
                )
            )
        );
        $redis->set(
            'usertokenlouis12345678901234567890123456789012345',
            json_encode(
                array(
                    'userid' => 2
                )
            )
        );
        $dbox = DataBox::getDataBox();
        $res = AuthValidator::validateToken($dbox);
        $this->assertEquals(null, $res->getData('statuscode'));
        $dbox->addData('Authorization', 'garth12345678901234567890123456789012345');
        $res = AuthValidator::validateToken($dbox);
        //var_dump($res->getData());
        $this->assertEquals('401', $res->getData('statuscode'));
        $dbox->addData('Authorization', 'garth12345678901234567890123456789012345');
        $dbox->addData('ip', '125.210.188.36');
        $res = AuthValidator::validateToken($dbox);
        $this->assertEquals('401', $res->getData('statuscode'));
        $dbox->addData('Authorization', 'garth12345678901234567890123456789012345');
        $dbox->addData('ip', '125.210.188.36');
        $dbox->addData('useragent', 'I am Garth!');
        $res = AuthValidator::validateToken($dbox);
        $this->assertEquals(true, $res->getLogin());
        $dbox->addData('Authorization', 'louis12345678901234567890123456789012345');
        $dbox->addData('ip', '125.210.188.36');
        $dbox->addData('useragent', 'I am Garth!');
        $res = AuthValidator::validateToken($dbox);
        //var_dump($res->getData());
        $this->assertEquals(false, $res->getLogin());
    }
    public function testvalidateLogin()
    {
        $redis = new Redis();
        $redis->pconnect('/tmp/redis.sock');
        $redis->select(1);
        $redis->flushDB();
        $dbox = DataBox::getDataBox();
        $res = AuthValidator::validateLogin($dbox);
        $this->assertEquals('400', $res->getData('statuscode'));
        $dbox->setType('login');
        $res = AuthValidator::validateLogin($dbox);
        $this->assertEquals('599', $res->getData('statuscode'));
        $dbox->setType('login');
        $dbox->addData('email', 'xgamers6g@hotmail.com');
        $res = AuthValidator::validateLogin($dbox);
        $dbox->setType('login');
        $dbox->addData('email', 'xgamers6g@hotmail.com');
        $dbox->addData('usersecret', '12345');
        $dbox->addData('ip', '125.210.188.36');
        $dbox->addData('useragent', 'I am Garth!');
        $res = AuthValidator::validateLogin($dbox);
        $this->assertEquals(true, $res->getLogin());
    }
    public function testvalidateUserNameExistForClient()
    {
        $dbox = DataBox::getDataBox();
        $res = AuthValidator::validateUserNameExistForClient($dbox);
        $this->assertEquals('沒有輸入使用者帳號', $res->getData('detail'));
        $dbox->reset();
        $dbox->addData('username', 'garth');
        $res = AuthValidator::validateUserNameExistForClient($dbox);
        $this->assertEquals('該帳號已經被使用，請嘗試其它帳號。', $res->getData('detail'));
        $dbox->reset();
        $dbox->addData('username', 'KKBOX');
        $res = AuthValidator::validateUserNameExistForClient($dbox);
        $this->assertEquals('此帳號可以註冊', $res->getData('detail'));
        $dbox->reset();
        $dbox->addData('username', 'abcd');
        $res = AuthValidator::validateUserNameExistForClient($dbox);
        $this->assertEquals(
            '帳號格式不正確, 本系統只支援0-9,a-z,A-Z，長度必需超過5',
            $res->getData('detail')
        );
        $dbox->reset();
        $dbox->addData('username', 'abcd##$1a');
        $res = AuthValidator::validateUserNameExistForClient($dbox);
        $this->assertEquals(
            '帳號格式不正確, 本系統只支援0-9,a-z,A-Z，長度必需超過5',
            $res->getData('detail')
        );
    }
    public function testvalidateUserSecretForClient()
    {
        $dbox = DataBox::getDataBox();
        $res = AuthValidator::validateUserSecretForClient($dbox);
        $this->assertEquals('沒有輸入密碼', $res->getData('detail'));
        $dbox->reset();
        $dbox->addData('usersecret', '1234');
        $res = AuthValidator::validateUserSecretForClient($dbox);
        $this->assertEquals(
            '密碼格式不正確, 本系統只支援0-9,a-z,A-Z，長度必需超過5',
            $res->getData('detail')
        );
        $dbox->reset();
        $dbox->addData('usersecret', '1234###');
        $res = AuthValidator::validateUserSecretForClient($dbox);
        $this->assertEquals(
            '密碼格式不正確, 本系統只支援0-9,a-z,A-Z，長度必需超過5',
            $res->getData('detail')
        );
        $dbox->reset();
        $dbox->addData('usersecret', '12345');
        $res = AuthValidator::validateUserSecretForClient($dbox);
        $this->assertEquals('此密碼可以使用', $res->getData('detail'));
    }
    public function testvalidateActionData()
    {
        $dbox = DataBox::getDataBox();
        $dbox->setLogin(true);
        $dbox->addData('usersecret', '12345');
        $res = AuthValidator::validateActionData($dbox);
        $this->assertEquals('setsecret', $res->getType());
        $dbox->reset();
        $dbox->addData('email', 'xgamers5g@hotmail.com');
        $dbox->addData('usersecret', '12345');
        $res = AuthValidator::validateActionData($dbox);
        //var_dump($res->getData());
        $this->assertEquals('login', $res->getType());
        $dbox->reset();
        $dbox->addData('email', 'james@hotmail.com');
        $dbox->addData('username', 'james');
        $dbox->addData('usersecret', '12345');
        $res = AuthValidator::validateActionData($dbox);
        $this->assertEquals('createuser', $res->getType());
    }
}
