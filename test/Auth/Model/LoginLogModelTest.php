<?php
namespace AuthTest\Model;

use PHPUnit_Extensions_Database_TestCase;
use AuthTest\DataSet;
use Auth\Model\LoginLogModel;
use Auth\Model\DataBox;
use Auth\Model\Encrypt;
use Redis;
use DateTime;
use PDO;

class LoginLogModelTest extends PHPUnit_Extensions_Database_TestCase
{
    ///為此測試設置一個PDO資料庫連線
    public function getConnection()
    {
        $conn = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }
    ///為此測試設置一組資料
    public function getDataSet()
    {
        return new DataSet(
            [
                'user' => [
                    [
                        'email' => 'xgamers5g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'garth',
                        'usersecret' => Encrypt::secretEncode('12345', 'Mzk5ZmFl')
                    ],
                    [
                        'email' => 'xgamers6g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'louis',
                        'usersecret' => Encrypt::secretEncode('12345', 'Mzk5ZmFl')
                    ],
                    [
                        'email' => 'xgamers7g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'peter',
                        'usersecret' => Encrypt::secretEncode('12345', 'Mzk5ZmFl')
                    ],
                    [
                        'email' => 'xgamers8g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'mikasa',
                        'usersecret' => Encrypt::secretEncode('12345', 'Mzk5ZmFl')
                    ]
                ]
            ]
        );
    }
    public function testDataSet()
    {
        $this->assertEquals(4, $this->getConnection()->getRowCount('user'));
    }
    public function testcreateLog()
    {
        $dbox = DataBox::getDataBox();
        $dbox->addData('email', 'xgamers5g@hotmail.com');
        $dbox->addData('ip', '125.210.188.36');
        $dbox->addData('useragent', 'I am Garth!');
        $dbox->addData('action', 'success');
        $res = LoginLogModel::createLog($dbox);
        $this->assertFalse($res instanceof DataBox);
        $this->assertTrue($res);
    }
}
