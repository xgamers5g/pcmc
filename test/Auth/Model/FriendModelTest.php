<?php
namespace AuthTest\Model;

use PHPUnit_Extensions_Database_TestCase;
use AuthTest\DataSet;
use Fruit\Seed;
use Auth\Model\TokenMaker;
use Auth\Model\FriendModel;
use Auth\Model\DataBox;
use Auth\Model\Encrypt;
use Redis;
use PDO;

class FriendModelTest extends PHPUnit_Extensions_Database_TestCase
{
    ///為此測試設置一個PDO資料庫連線
    public function getConnection()
    {
        $conn = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }
    ///為此測試設置一組資料
    public function getDataSet()
    {
        return new DataSet(
            [
                'user' => [
                    [
                        'email' => 'xgamers5g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'garth',
                        'usersecret' => Encrypt::secretEncode('12345', 'Mzk5ZmFl')
                    ],
                    [
                        'email' => 'xgamers6g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'louis',
                        'usersecret' => Encrypt::secretEncode('12345', 'Mzk5ZmFl')
                    ],
                    [
                        'email' => 'xgamers7g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'peter',
                        'usersecret' => Encrypt::secretEncode('12345', 'Mzk5ZmFl')
                    ],
                    [
                        'email' => 'xgamers8g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'mikasa',
                        'usersecret' => Encrypt::secretEncode('12345', 'Mzk5ZmFl')
                    ]
                ],
                'user_friend' => [
                    [
                        'userid' => 1,
                        'friendid' => 2
                    ],
                    [
                        'userid' => 1,
                        'friendid' => 3
                    ],
                    [
                        'userid' => 1,
                        'friendid' => 4
                    ],
                    [
                        'userid' => 2,
                        'friendid' => 1
                    ],
                    [
                        'userid' => 2,
                        'friendid' => 3
                    ],
                    [
                        'userid' => 3,
                        'friendid' => 1
                    ]
                ]
            ]
        );
    }
    public function testDataSet()
    {
        $this->assertEquals(4, $this->getConnection()->getRowCount('user'));
        $this->assertEquals(6, $this->getConnection()->getRowCount('user_friend'));
    }
    public function testload()
    {
        $dbox = DataBox::getDataBox();
        $dbox->setType('friendmodel');
        $dbox->addData('id', 3);
        $dbox->addData('ip', '125.210.188.36');
        $dbox->addData('useragent', 'I am Garth!');
        $fm = FriendModel::load($dbox);
        $this->assertTrue($fm instanceof FriendModel);
    }
    public function testadd()
    {
        //先寫入redis，再寫入mysql。
        //redis:
        //userfreind::userid -> 裡面放friendid陣列，以userid為主索引，用while方式人工搜尋。
        //找不到userfriend::userid時執行query取回資料。
        //mysql
        //$qs = 'insert into user_friend';
        $redis = new Redis();
        $redis->pconnect('/tmp/redis.sock');
        $redis->select(1);
        $redis->flushDB();
        $redis->set(
            'userfriend::userid::1',
            json_encode(
                array(
                    0 => 2,
                    1 => 3,
                    2 => 4
                )
            )
        );
        $redis->set(
            'userfriend::userid::2',
            json_encode(
                array(
                    0 => 1,
                    1 => 3
                )
            )
        );
        $redis->set(
            'userfriend::userid::3',
            json_encode(
                array(
                    0 => 1
                )
            )
        );
        $dbox = DataBox::getDataBox();
        $dbox->setType('friendmodel');
        $dbox->addData('id', 3);
        $dbox->addData('ip', '125.210.188.36');
        $dbox->addData('useragent', 'I am Garth!');
        $fm = FriendModel::load($dbox);
        $dbox->setType('addfriend');
        $dbox->addData('fid', 2);
        $dbox = $fm->add($dbox);
        //var_dump($dbox->getData());
        $this->assertEquals('200', $dbox->getData('statuscode'));
        $this->assertEquals(2, json_decode($redis->get('userfriend::userid::3'), true)[1]);
    }
    public function testget()
    {
        //先從redis找，沒有再從mysql找。
        $redis = new Redis();
        $redis->pconnect('/tmp/redis.sock');
        $redis->select(1);
        $redis->flushDB();
        $redis->set(
            'userfriend::userid::1',
            json_encode(
                array(
                    0 => 2,
                    1 => 3,
                    2 => 4
                )
            )
        );
        $fakeres = array(
            0 => 2,
            1 => 3,
            2 => 4
        );
        $fakeres2 = array(
            0 => 1,
            1 => 3
        );
        $dbox = DataBox::getDataBox();
        $dbox->setType('friendmodel');
        $dbox->addData('id', 1);
        $dbox->addData('ip', '125.210.188.36');
        $dbox->addData('useragent', 'I am Garth!');
        $fm = FriendModel::load($dbox);
        $friendlist = $fm->get();
        $this->assertEquals(
            $fakeres,
            $friendlist->getData('friendlist')
        );
        $dbox->reset();
        $dbox->setType('friendmodel');
        $dbox->addData('id', 2);
        $dbox->addData('ip', '125.210.188.36');
        $dbox->addData('useragent', 'I am Garth!');
        $fm = FriendModel::load($dbox);
        $dbox = $fm->get();
        $this->assertEquals(
            $fakeres2,
            $dbox->getData('friendlist')
        );
        $this->assertEquals('200', $dbox->getData('statuscode'));
    }
    public function testdelete()
    {
        //先移除redis的內容,再移除mysql的內容。
        $redis = new Redis();
        $redis->pconnect('/tmp/redis.sock');
        $redis->select(1);
        $redis->flushDB();
        $redis->set(
            'userfriend::userid::1',
            json_encode(
                array(
                    0 => 2,
                    1 => 3,
                    2 => 4
                )
            )
        );
        $redis->set(
            'userfriend::userid::2',
            json_encode(
                array(
                    0 => 1,
                    1 => 3
                )
            )
        );
        $redis->set(
            'userfriend::userid::3',
            json_encode(
                array(
                    0 => 1
                )
            )
        );
        $dbox = DataBox::getDataBox();
        $dbox->setType('friendmodel');
        $dbox->addData('id', 3);
        $dbox->addData('fid', 1);
        $dbox->addData('ip', '125.210.188.36');
        $dbox->addData('useragent', 'I am Garth!');
        $fm = FriendModel::load($dbox);
        $dbox = $fm->delete($dbox);
        $this->assertEquals('200', $dbox->getData('statuscode'));
        $this->assertFalse($redis->exists('userfriend::userid::3'));
    }
}
