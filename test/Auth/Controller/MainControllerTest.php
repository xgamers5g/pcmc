<?php
namespace AuthTest\Controller;

use PHPUnit_Extensions_Database_TestCase;
use AuthTest\DataSet;
use Auth\Tools\ToolCurl;
use Fruit;
use PDO;

class MainControllerTest extends PHPUnit_Extensions_Database_TestCase
{
    ///為此測試設置一個PDO資料庫連線
    public function getConnection()
    {
        $conn = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }
    public function getDataSet()
    {
        $core = \Auth\System\Core::getInstance();
        return new DataSet(
            [
                'user' => [
                    [
                        'email' => 'xgamers5g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'garth',
                        'usersecret' => $core->getEncrypt()->secretEncode('12345', 'Mzk5ZmFl')
                    ],
                    [
                        'email' => 'louis@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'louis',
                        'usersecret' => $core->getEncrypt()->secretEncode('12345', 'Mzk5ZmFl')
                    ],
                    [
                        'email' => 'yurak@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'yurak',
                        'usersecret' => $core->getEncrypt()->secretEncode('12345', 'Mzk5ZmFl')
                    ],
                    [
                        'email' => 'james@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'james',
                        'usersecret' => $core->getEncrypt()->secretEncode('12345', 'Mzk5ZmFl')
                    ]
                ]
            ]
        );
    }
    public function testDataSet()
    {
        $this->assertEquals(4, $this->getConnection()->getRowCount('user'));
    }
    
    public function testloginAndlogoutAction()
    {
        $postdata = json_encode(
            array(
                'email' => 'louis@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->postJSON(null, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 401 Unauthorized', $res['http_code']);
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        //$authorizationdata = json_decode($authorizationdata, true);
        $authorizationdata['Authorization'] = $body['Authorization'];
        $authorizationdata = json_encode($authorizationdata);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->postJSON($authorizationdata, true, true);
        //echo $res;
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 200 OK', $res['http_code']);
    }

    public function testcreateAction()
    {
        $postdata = json_encode(
            array(
                'email' => 'louis@hotmail.com',
                'username' => 'louis',
                'usersecret' => '12345'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/create');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 599', $res['http_code']);
        $postdata = json_encode(
            array(
                'username' => 'louis'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/create');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 400 Bad Request', $res['http_code']);
        $postdata = json_encode(
            array(
                'secret' => '12345'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/create');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 400 Bad Request', $res['http_code']);
        $postdata = json_encode(
            array(
                'email' => 'peter@hotmail.com',
                'username' => 'peter',
                'usersecret' => '12345'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/create');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 200 OK', $res['http_code']);
    }

    public function testsetSecretAction()
    {
        $secretdata = json_encode(
            array(
                'usersecret' => '79979'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/edit/setsecret');
        $res = $tc->postJSON($secretdata, true, true);
        //echo $res;
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 401 Unauthorized', $res['http_code']);
        $postdata = json_encode(
            array(
                'email' => 'louis@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $secretdata = json_decode($secretdata, true);
        $secretdata['Authorization'] = $body['Authorization'];
        $secretdata = json_encode($secretdata);
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 200 OK', $res['http_code']);
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/edit/setsecret');
        $res = $tc->postJSON($secretdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 200 OK', $res['http_code']);
        
        $body = json_decode($body, true);
        //$authorizationdata = json_decode($authorizationdata, true);
        $authorizationdata['Authorization'] = $body['Authorization'];
        $authorizationdata = json_encode($authorizationdata);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->postJSON($authorizationdata, true, true);
    }
    
    public function testdeleteAction()
    {
        $tc = ToolCurl::init(SITE_URL . '/auth/edit/delete');
        $res = $tc->postJSON(null, true, true);
        //echo $res;
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 401 Unauthorized', $res['http_code']);
        $postdata = json_encode(
            array(
                'email' => 'james@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        //$authorizationdata = json_decode($authorizationdata, true);
        $authorizationdata['Authorization'] = $body['Authorization'];
        $authorizationdata = json_encode($authorizationdata);
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 200 OK', $res['http_code']);
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/edit/delete');
        $res = $tc->post($authorizationdata, true, true);
        //echo $res;
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 200 OK', $res['http_code']);
    }
    
    public function testgetUserDataAction()
    {
        $tc = ToolCurl::init(SITE_URL . '/auth/getuserdata');
        $res = $tc->postJSON(null, true, true);
        //echo $res;
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 401 Unauthorized', $res['http_code']);
        $postdata = json_encode(
            array(
                'email' => 'james@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        //$authorizationdata = json_decode($authorizationdata, true);
        $authorizationdata['Authorization'] = $body['Authorization'];
        $authorizationdata = json_encode($authorizationdata);
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 200 OK', $res['http_code']);
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/getuserdata');
        $res = $tc->postJSON($authorizationdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 200 OK', $res['http_code']);
        $this->assertEquals('james', $body['username']);
        $this->assertEquals('4', $body['id']);
        $authorizationdata = json_decode($authorizationdata, true);
        $authorizationdata['Authorization'] = $body['Authorization'];
        $authorizationdata = json_encode($authorizationdata);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->postJSON($authorizationdata, true, true);
    }
    public function testcheckUserEmailAction()
    {
        $postdata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/check/email');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 401 Unauthorized', $res['http_code']);
        $this->assertEquals('該電郵已經被使用，請嘗試其它電郵。', $body['detail']);
        $postdata = json_encode(
            array(
                'email' => 'xgamers6g@hotmail.com'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/check/email');
        $res = $tc->postJSON($postdata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 200 OK', $res['http_code']);
        $postdata = json_encode(
            array(
                'email' => 'xgamers5g'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/check/email');
        $res = $tc->postJSON($postdata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 401 Unauthorized', $res['http_code']);
        $this->assertEquals(
            'email信箱格式不正確',
            $body['detail']
        );
    }
    public function testcheckUserNameAction()
    {
        $postdata = json_encode(
            array(
                'username' => 'garth'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/check/username');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 401 Unauthorized', $res['http_code']);
        $this->assertEquals('該帳號已經被使用，請嘗試其它帳號。', $body['detail']);
        $postdata = json_encode(
            array(
                'username' => 'windgirl'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/check/username');
        $res = $tc->postJSON($postdata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 200 OK', $res['http_code']);
        $postdata = json_encode(
            array(
                'username' => 'wi###ndg$%^&irl'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/check/username');
        $res = $tc->postJSON($postdata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 401 Unauthorized', $res['http_code']);
        $this->assertEquals(
            '帳號格式不正確, 本系統只支援0-9,a-z,A-Z，長度必需超過5',
            $body['detail']
        );
    }
    public function testcheckUserSecretAction()
    {
        $postdata = json_encode(
            array(
                'usersecret' => '0836'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/check/usersecret');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 401 Unauthorized', $res['http_code']);
        $this->assertEquals(
            '密碼格式不正確, 本系統只支援0-9,a-z,A-Z，長度必需超過5',
            $body['detail']
        );
        $postdata = json_encode(
            array(
                'usersecret' => '11windgirl'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/check/usersecret');
        $res = $tc->postJSON($postdata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 200 OK', $res['http_code']);
        $postdata = json_encode(
            array(
                'usersecret' => 'wi###ndg$%^&irl'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/check/usersecret');
        $res = $tc->postJSON($postdata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('HTTP/1.1 401 Unauthorized', $res['http_code']);
        $this->assertEquals(
            '密碼格式不正確, 本系統只支援0-9,a-z,A-Z，長度必需超過5',
            $body['detail']
        );
    }
}
