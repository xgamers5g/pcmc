<?php
namespace AuthTest\Model\UnitTest;

use PHPUnit_Framework_TestCase;

class MainControllerUnitTest extends PHPUnit_Framework_TestCase
{
    public function testindexAction()
    {
        $authfacade = $this->getMock('\Auth\Facade\AuthFacade', array(), array(), '', false, true, true);
        $authfacade
            ->expects($this->any())
            ->method('index')
            ->will($this->returnValue(array('test' => "test1")));
        $mc = new \Auth\Controller\MainController($authfacade);
        $assertData = json_encode(
            array(
                "test" => "test1"
            ),
            JSON_UNESCAPED_UNICODE
        );
        $this->assertEquals($assertData, $mc->indexAction());
    }
    public function testloginAction()
    {
        $authfacade = $this->getMock('\Auth\Facade\AuthFacade', array(), array(), '', false, true, true);
        $authfacade
            ->expects($this->any())
            ->method('login')
            ->will($this->returnValue(array('test' => "test1")));
        $mc = new \Auth\Controller\MainController($authfacade);
        $assertData = json_encode(
            array(
                "test" => "test1"
            ),
            JSON_UNESCAPED_UNICODE
        );
        $this->assertEquals($assertData, $mc->loginAction());
    }
    public function testlogoutAction()
    {
        $authfacade = $this->getMock('\Auth\Facade\AuthFacade', array(), array(), '', false, true, true);
        $authfacade
            ->expects($this->any())
            ->method('logout')
            ->will($this->returnValue(array('test' => "test1")));
        $mc = new \Auth\Controller\MainController($authfacade);
        $assertData = json_encode(
            array(
                "test" => "test1"
            ),
            JSON_UNESCAPED_UNICODE
        );
        $this->assertEquals($assertData, $mc->logoutAction());
    }
    public function testcreatAction()
    {
        $authfacade = $this->getMock('\Auth\Facade\AuthFacade', array(), array(), '', false, true, true);
        $authfacade
            ->expects($this->any())
            ->method('createUser')
            ->will($this->returnValue(array('test' => "test1")));
        $mc = new \Auth\Controller\MainController($authfacade);
        $assertData = json_encode(
            array(
                "test" => "test1"
            ),
            JSON_UNESCAPED_UNICODE
        );
        $this->assertEquals($assertData, $mc->createAction());
    }
    public function testsetsecretAction()
    {
        $authfacade = $this->getMock('\Auth\Facade\AuthFacade', array(), array(), '', false, true, true);
        $authfacade
            ->expects($this->any())
            ->method('setSecret')
            ->will($this->returnValue(array('test' => "test1")));
        $mc = new \Auth\Controller\MainController($authfacade);
        $assertData = json_encode(
            array(
                "test" => "test1"
            ),
            JSON_UNESCAPED_UNICODE
        );
        $this->assertEquals($assertData, $mc->setsecretAction());
    }
    public function testdeleteAction()
    {
        $authfacade = $this->getMock('\Auth\Facade\AuthFacade', array(), array(), '', false, true, true);
        $authfacade
            ->expects($this->any())
            ->method('deleteUser')
            ->will($this->returnValue(array('test' => "test1")));
        $mc = new \Auth\Controller\MainController($authfacade);
        $assertData = json_encode(
            array(
                "test" => "test1"
            ),
            JSON_UNESCAPED_UNICODE
        );
        $this->assertEquals($assertData, $mc->deleteAction());
    }
    public function testgetUserDataAction()
    {
        $authfacade = $this->getMock('\Auth\Facade\AuthFacade', array(), array(), '', false, true, true);
        $authfacade
            ->expects($this->any())
            ->method('getUserData')
            ->will($this->returnValue(array('test' => "test1")));
        $mc = new \Auth\Controller\MainController($authfacade);
        $assertData = json_encode(
            array(
                "test" => "test1"
            ),
            JSON_UNESCAPED_UNICODE
        );
        $this->assertEquals($assertData, $mc->getuserdataAction());
    }
    public function testcheckUserEmailAction()
    {
        $authfacade = $this->getMock('\Auth\Facade\AuthFacade', array(), array(), '', false, true, true);
        $authfacade
            ->expects($this->any())
            ->method('checkUserEmail')
            ->will($this->returnValue(array('test' => "test1")));
        $mc = new \Auth\Controller\MainController($authfacade);
        $assertData = json_encode(
            array(
                "test" => "test1"
            ),
            JSON_UNESCAPED_UNICODE
        );
        $this->assertEquals($assertData, $mc->checkUserEmailAction());
    }
}
