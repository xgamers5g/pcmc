<?php
namespace DiaryTest\Controller;

use PDO;
use PHPUnit_Extensions_Database_TestCase;
use AuthTest\DataSet;
use Auth\Tools\ToolCurl;

class DiaryControllerTest extends PHPUnit_Extensions_Database_TestCase
{
    public function getConnection()
    {
        $conn = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }
    public function getDataSet()
    {
        $encrypt = \Auth\System\Core::getInstance()->getEncrypt();
        return new DataSet(
            [
                'user' => [
                    [
                        'email' => 'xgamers5g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'garth',
                        'usersecret' => $encrypt->secretEncode('12345', 'Mzk5ZmFl')
                    ],
                    [
                        'email' => 'louis@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'louis',
                        'usersecret' => $encrypt->secretEncode('12345', 'Mzk5ZmFl')
                    ],
                    [
                        'email' => 'peter@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'peter',
                        'usersecret' => $encrypt->secretEncode('12345', 'Mzk5ZmFl')
                    ],
                ],
                'diary' => [
                    [
                        'cday' => date("Y-m-d"),
                        'oday' => date("Y-m-d"),
                        'eday' => date("Y-m-d H:i:s"),
                        'dtext' => 'garthgood',
                        'uid' => 1,
                        'pstatus' => 1,
                        'mstatus' => 1
                    ],
                    [
                        'cday' => date("Y-m-d"),
                        'oday' => "2014-10-26",
                        'eday' => date("Y-m-d H:i:s"),
                        'dtext' => 'aaaa',
                        'uid' => 1,
                        'pstatus' => 1,
                        'mstatus' => 1
                    ],
                    [
                        'cday' => date("Y-m-d"),
                        'oday' => date("Y-m-d"),
                        'eday' => date("Y-m-d H:i:s"),
                        'dtext' => 'peterbyebye',
                        'uid' => 3,
                        'pstatus' => 0,
                        'mstatus' => 0
                    ]
                ],
                'diary_message' => [
                    [
                        'dmessage' => 'testmessagemodel1',
                        'dmdatetime' => date("Y-m-d H:i:s"),
                        'did' => 2,
                        'uid' => 3,
                        'status' => 1
                    ],
                    [
                        'dmessage' => 'testmessagemodel2',
                        'dmdatetime' => date("Y-m-d H:i:s"),
                        'did' => 3,
                        'uid' => 2,
                        'status' => 0
                    ],
                    [
                        'dmessage' => 'testmessagemodel3',
                        'dmdatetime' => date("Y-m-d H:i:s"),
                        'did' => 1,
                        'uid' => 2,
                        'status' => 1
                    ],
                    [
                        'dmessage' => 'testmessagemodel4',
                        'dmdatetime' => date("Y-m-d H:i:s"),
                        'did' => 1,
                        'uid' => 3,
                        'status' => 0
                    ]
                ]
            ]
        );
    }

    public function testCreate()
    {
        $postdata = json_encode(
            array(
                'email' => 'louis@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $arr = json_encode(
            array(
                "day" => date("Y-m-d"),
                "content" => "test",
                'status1' => 0,
                'status2' => 0
                )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $arr = json_decode($arr, true);
        $arr['Authorization'] = $body['Authorization'];
        $arr = json_encode($arr);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/diary/create');
        $res = $tc->postJSON($arr, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals(200, $body['statuscode']);
    }

    public function testUpdate()
    {
        $postdata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $arr = json_encode(
            array(
                "day" => date("Y-m-d"),
                "sday" => date("Y-m-d"),
                "content" => "test",
                'status1' => 1,
                'status2' => 1
                )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $arr = json_decode($arr, true);
        $arr['Authorization'] = $body['Authorization'];
        $arr = json_encode($arr);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/diary/update');
        $res = $tc->postJSON($arr, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals(200, $body['statuscode']);
    }

    public function testDelete()
    {
        $postdata = json_encode(
            array(
                'email' => 'peter@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $arr = json_encode(
            array(
                "diaryid" => 3
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $arr = json_decode($arr, true);
        $arr['Authorization'] = $body['Authorization'];
        $arr = json_encode($arr);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/diary/delete');
        $res = $tc->postJSON($arr, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals(200, $body['statuscode']);
    }

    public function testSelectAll()
    {
        $postdata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $arr = json_encode(array());

        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $arr = json_decode($arr, true);
        $arr['Authorization'] = $body['Authorization'];
        $arr = json_encode($arr);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/diary/selectall');
        $res = $tc->postJSON($arr, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals(false, isset($body['statuscode']));
    }

    public function testSelectPublic()
    {
        $postdata = json_encode(
            array(
                'email' => 'peter@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $arr = json_encode(array());

        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $arr = json_decode($arr, true);
        $arr['Authorization'] = $body['Authorization'];
        $arr = json_encode($arr);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/diary/selectpublic');
        $res = $tc->postJSON($arr, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals(false, isset($body['statuscode']));
    }

    public function testCreateMessage()
    {
        $postdata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $arr = json_encode(
            array(
                "message" => "testMessageController",
                "diaryid" => 1,
                'status3' => 1
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $arr = json_decode($arr, true);
        $arr['Authorization'] = $body['Authorization'];
        $arr = json_encode($arr);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/diary/createmessage');
        $res = $tc->postJSON($arr, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals(200, $body['statuscode']);
    }

    public function testSelectMessage()
    {
        $postdata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $arr = json_encode(array("diaryid" => 1));

        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $arr = json_decode($arr, true);
        $arr['Authorization'] = $body['Authorization'];
        $arr = json_encode($arr);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/diary/selectmessage');
        $res = $tc->postJSON($arr, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals(false, isset($body['statuscode']));
    }

    public function testReplyMessage()
    {
        $postdata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $arr = json_encode(
            array(
                "reply" => "reply",
                'messageid' => 1
                )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $arr = json_decode($arr, true);
        $arr['Authorization'] = $body['Authorization'];
        $arr = json_encode($arr);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/diary/replymessage');
        $res = $tc->postJSON($arr, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals(200, $body['statuscode']);
    }

    public function testDeleteMessage()
    {
        $postdata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $arr = json_encode(
            array(
                "messageid" => 1
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $arr = json_decode($arr, true);
        $arr['Authorization'] = $body['Authorization'];
        $arr = json_encode($arr);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/diary/deletemessage');
        $res = $tc->postJSON($arr, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals(200, $body['statuscode']);
    }

    public function testUpdateforbill()
    {
        $postdata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $arr = json_encode(
            array(
                "day" => date("Y-m-d"),
                "content" => "bill",
                )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $arr = json_decode($arr, true);
        $arr['Authorization'] = $body['Authorization'];
        $arr = json_encode($arr);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/diary/updateforbill');
        $res = $tc->postJSON($arr, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals(200, $body['statuscode']);
    }

    public function testUpdateforbilling()
    {
        $postdata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $arr = json_encode(
            array(
                "day" => date("Y-m-d"),
                "content" => "bill",
                )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $arr = json_decode($arr, true);
        $arr['Authorization'] = $body['Authorization'];
        $arr = json_encode($arr);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/diary/updateforbilling');
        $res = $tc->postJSON($arr, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals(200, $body['statuscode']);
    }
}
