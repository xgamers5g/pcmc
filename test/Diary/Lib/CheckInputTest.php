<?php
namespace DiaryTest\Lib;

use PHPUnit_Framework_TestCase;
use Diary\Lib\CheckInput;

class CheckInputTest extends PHPUnit_Framework_TestCase
{
    public function testcheckDate()
    {
        $date1 = "2014-10-20";
        $date2 = "2017";
        $date3 = "abcd";
        $scode = array(
                    'dataerror' => array(
                    'statuscode' => '401',
                    'detail' => '資料錯誤或不完整'
                    )
                 );
        $this->assertEquals(true, CheckInput::checkDate($date1, $scode));
        $this->assertEquals($scode['dataerror'], CheckInput::checkDate($date2, $scode));
        $this->assertEquals($scode['dataerror'], CheckInput::checkDate($date3, $scode));
    }

    public function testcheck7day()
    {
        $date1 = date('Y-m-d');
        $date2 = "2017-01-01";
        $date3 = "2014-10-01";
        $scode = array(
                    'datanotday' => array(
                    'statuscode' => '401',
                    'detail' => '日記日期太遙遠或為未來日記'
                    )
                 );
        $this->assertEquals(true, CheckInput::check7day($date1, $scode));
        $this->assertEquals($scode['datanotday'], CheckInput::check7day($date2, $scode));
        $this->assertEquals($scode['datanotday'], CheckInput::check7day($date3, $scode));
    }
}
