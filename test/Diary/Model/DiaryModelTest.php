<?php
namespace DiaryTest\Model;

use PDO;
use Diary\Model\DiaryModel;
use PHPUnit_Extensions_Database_TestCase;
use AuthTest\DataSet;

class DiaryModelTest extends PHPUnit_Extensions_Database_TestCase
{
    public function getConnection()
    {
        $conn = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }
    public function getDataSet()
    {
        return new DataSet(
            [
                'diary' => [
                    [
                        'cday' => date('Y-m-d'),
                        'oday' => date('Y-m-d'),
                        'eday' => date('Y-m-d H:i:s'),
                        'dtext' => 'diarymodeltest1',
                        'uid' => 1,
                        'pstatus' => 1,
                        'mstatus' => 1
                    ],
                    [
                        'cday' => date('Y-m-d'),
                        'oday' => '2014-10-30',
                        'eday' => date('Y-m-d H:i:s'),
                        'dtext' => 'diarymodeltest2',
                        'uid' => 1,
                        'pstatus' => 0,
                        'mstatus' => 1
                    ],
                    [
                        'cday' => date('Y-m-d'),
                        'oday' => date('Y-m-d'),
                        'eday' => date('Y-m-d H:i:s'),
                        'dtext' => 'diarymodeltest3',
                        'uid' => 3,
                        'pstatus' => 0,
                        'mstatus' => 0
                    ]
                ]
            ]
        );
    }

    public function testCreate()
    {
        $arr1 = array(
            'oday' => date('Y-m-d'),
            'dtext' => 'testmodel1105',
            'uid' => 2,
            'pstatus' => 0,
            'mstatus' => 1
            );
        $scode = array(
                    'success' => array(
                        'statuscode' => '200',
                        'detail' => '執行成功'
                    ),
                    'databaseerror' => array(
                        'statuscode' => '599',
                        'detail' => '資料庫異常,請聯絡管理員'
                    )
                 );
        $this->assertEquals($scode['success'], DiaryModel::create($arr1, $scode));
        // $this->assertEquals($scode['databaseerror'], DiaryModel::create($arr2, $scode));
    }

    public function testUpdate()
    {
        $arr1 = array(
            'oday' => date('Y-m-d'),
            'soday' => date('Y-m-d'),
            'dtext' => 'testdiary',
            'uid' => 1,
            'pstatus' => 0,
            'mstatus' => 0
            );
        $arr2 = array(
            'oday' => date('Y-m-d'),
            'soday' => date('Y-m-d'),
            'dtext' => 'testdiary',
            'uid' => 2,
            'pstatus' => 1,
            'mstatus' => 0
            );
        $scode = array(
                    'success' => array(
                        'statuscode' => '200',
                        'detail' => '執行成功'
                    ),
                    'datanot' => array(
                        'statuscode' => '401',
                        'detail' => '沒有資料'
                    ),
                    'databaseerror' => array(
                        'statuscode' => '599',
                        'detail' => '資料庫異常,請聯絡管理員'
                    )
                 );
        $this->assertEquals($scode['success'], DiaryModel::update($arr1, $scode));
        $this->assertEquals($scode['datanot'], DiaryModel::update($arr2, $scode));
        // $this->assertEquals($scode['databaseerror'], DiaryModel::update($arr3, $scode));
    }

    public function testDelete()
    {
        $arr1 = array(
            'did' => 1,
            'uid' => 1
            );
        $arr2 = array(
            'did' => 10,
            'uid' => 1
            );
        $scode = array(
                    'success' => array(
                        'statuscode' => '200',
                        'detail' => '執行成功'
                    ),
                    'datanot' => array(
                        'statuscode' => '401',
                        'detail' => '沒有資料'
                    ),
                    'databaseerror' => array(
                        'statuscode' => '599',
                        'detail' => '資料庫異常,請聯絡管理員'
                    )
                 );
        $this->assertEquals($scode['success'], DiaryModel::delete($arr1, $scode));
        $this->assertEquals($scode['datanot'], DiaryModel::delete($arr2, $scode));
        // $this->assertEquals($scode['databaseerror'], DiaryModel::delete($arr3, $scode));
    }

    public function testSelectDay()
    {
        $arr1 = array(
            'oday' => date('Y-m-d'),
            'uid' => 10
        );
        $arr2 = array(
            'oday' => date('Y-m-d'),
            'uid' => 1
        );
        $scode = array(
                    'datarepeat' => array(
                        'statuscode' => '401',
                        'detail' => '已有該日日記'
                    ),
                    'databaseerror' => array(
                        'statuscode' => '599',
                        'detail' => '資料庫異常,請聯絡管理員'
                    )
                 );
        $this->assertEquals(true, DiaryModel::selectday($arr1, $scode));
        $this->assertEquals($scode['datarepeat'], DiaryModel::selectday($arr2, $scode));
        // $this->assertEquals($scode['databaseerror'], DiaryModel::selectday($arr3, $scode));
    }

    public function testSelectID()
    {
        $arr1 = array(
            'did' => 1,
            'uid' => 1
        );
        $arr2 = array(
            'did' => 10,
            'uid' => 1
        );
        $scode = array(
                    'datanot' => array(
                        'statuscode' => '401',
                        'detail' => '沒有資料'
                    ),
                    'databaseerror' => array(
                        'statuscode' => '599',
                        'detail' => '資料庫異常,請聯絡管理員'
                    )
                 );
        $this->assertEquals(true, DiaryModel::selectid($arr1, $scode));
        $this->assertEquals($scode['datanot'], DiaryModel::selectid($arr2, $scode));
        // $this->assertEquals($scode['databaseerror'], DiaryModel::selectid($arr3, $scode));
    }

    public function testSelectAll()
    {
        $arr1 = array(
            'uid' => 1
        );
        $arr2 = array(
            'uid' => 10
        );
        $scode = array(
                    'datanot' => array(
                        'statuscode' => '401',
                        'detail' => '沒有資料'
                    ),
                    'databaseerror' => array(
                        'statuscode' => '599',
                        'detail' => '資料庫異常,請聯絡管理員'
                    )
                 );
        $dmsl = DiaryModel::selectall($arr1, $scode);
        // var_dump($dmsl);
        $this->assertEquals(false, isset($dmsl['statuscode']));
        $this->assertEquals($scode['datanot'], DiaryModel::selectall($arr2, $scode));
        // $this->assertEquals($scode['databaseerror'], DiaryModel::selectall($arr3, $scode));
    }

    public function testSelectPublic()
    {
        $arr1 = array(
            'uid' => 1
            );
        $arr2 = array(
            'uid' => 10
            );
        $scode = array(
                    'datanot' => array(
                        'statuscode' => '401',
                        'detail' => '沒有資料'
                    ),
                    'databaseerror' => array(
                        'statuscode' => '599',
                        'detail' => '資料庫異常,請聯絡管理員'
                    )
                 );
        $dmsp = DiaryModel::selectpublic($arr1, $scode);
        // var_dump($dmsp);
        $this->assertEquals(false, isset($dmsp['statuscode']));
        $this->testUpdate();
        $this->assertEquals($scode['datanot'], DiaryModel::selectpublic($arr2, $scode));
        // $this->assertEquals($scode['databaseerror'], DiaryModel::selectpublic($arr3, $scode));
    }

    public function testSelectMstatus()
    {
        $arr1 = array(
            'did' => 1
        );
        $arr2 = array(
            'did' => 3
        );
        $scode = array(
                    'datamstatusnot' => array(
                        'statuscode' => '401',
                        'detail' => '此日記未開放留言'
                    ),
                    'databaseerror' => array(
                        'statuscode' => '599',
                        'detail' => '資料庫異常,請聯絡管理員'
                    )
                 );
        $this->assertEquals(true, DiaryModel::selectmstatus($arr1, $scode));
        $this->assertEquals($scode['datamstatusnot'], DiaryModel::selectmstatus($arr2, $scode));
        // $this->assertEquals($scode['databaseerror'], DiaryModel::selectmstatus($arr3, $scode));
    }

    public function testUpdateForBill()
    {
        $arr1 = array(
            'oday' => date('Y-m-d'),
            'dtext' => 'testdiary',
            'uid' => 1,
            'content' => '原本的資料'
            );

        $arr2 = array(
            'oday' => '2010-01-01',
            'dtext' => 'testdiary',
            'uid' => 1,
            'content' => '原本的資料'
            );
        $scode = array(
                    'success' => array(
                        'statuscode' => '200',
                        'detail' => '執行成功'
                    ),
                    'datanot' => array(
                        'statuscode' => '401',
                        'detail' => '沒有資料'
                    ),
                    'databaseerror' => array(
                        'statuscode' => '599',
                        'detail' => '資料庫異常,請聯絡管理員'
                    )
                 );

        $this->assertEquals($scode['success'], DiaryModel::updateforbill($arr1, $scode));
        $this->assertEquals($scode['datanot'], DiaryModel::updateforbill($arr2, $scode));
        // $this->assertEquals($scode['databaseerror'], DiaryModel::updateforbill($arr3, $scode));
    }

    public function testSelectDayForBill()
    {
        $arr1 = array(
            'uid' => 1,
            'oday' => date('Y-m-d')
        );
        $arr2 = array(
            'uid' => 10,
            'oday' => date('2010-01-01')
        );
        $scode = array(
                    'datanot' => array(
                        'statuscode' => '401',
                        'detail' => '沒有資料'
                    ),
                    'databaseerror' => array(
                        'statuscode' => '599',
                        'detail' => '資料庫異常,請聯絡管理員'
                    )
                 );
        $dmsdb = DiaryModel::selectdayforbill($arr1, $scode);
        // var_dump($dmsl);
        $this->assertEquals(false, isset($dmsdb['statuscode']));
        $this->assertEquals($scode['datanot'], DiaryModel::selectdayforbill($arr2, $scode));
        // $this->assertEquals($scode['databaseerror'], DiaryModel::selectdayforbill($arr3, $scode));
    }

    public function testUpdateForBilling()
    {
        $arr1 = array(
            'oday' => date('Y-m-d'),
            'dtext' => 'testdiary',
            'uid' => 1,
            'content' => '原本的資料'
            );

        $arr2 = array(
            'oday' => '2010-01-01',
            'dtext' => 'testdiary',
            'uid' => 1,
            'content' => '原本的資料'
            );
        $scode = array(
                    'success' => array(
                        'statuscode' => '200',
                        'detail' => '執行成功'
                    ),
                    'datanot' => array(
                        'statuscode' => '401',
                        'detail' => '沒有資料'
                    ),
                    'databaseerror' => array(
                        'statuscode' => '599',
                        'detail' => '資料庫異常,請聯絡管理員'
                    )
                 );
        $this->assertEquals($scode['success'], DiaryModel::updateforbilling($arr1, $scode));
        $this->assertEquals($scode['datanot'], DiaryModel::updateforbilling($arr2, $scode));
        // $this->assertEquals($scode['databaseerror'], DiaryModel::updateforbill($arr3, $scode));
    }
}
