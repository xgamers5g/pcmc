<?php
namespace DiaryTest\Model;

use PDO;
use PHPUnit_Extensions_Database_TestCase;
use AuthTest\DataSet;
use Diary\Model\DiaryAssembly;
use Diary\Lib\StatusCode;

class DiaryAssemblyTest extends PHPUnit_Extensions_Database_TestCase
{
    public function __construct()
    {
        $this->scode = StatusCode::responserule();
    }

    public function getConnection()
    {
        $conn = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }
    public function getDataSet()
    {
        return new DataSet(
            [
                'diary' => [
                    [
                        'cday' => date('Y-m-d'),
                        'oday' => date('Y-m-d'),
                        'eday' => date('Y-m-d H:i:s'),
                        'dtext' => 'testcontroller1',
                        'uid' => 1,
                        'pstatus' => 1,
                        'mstatus' => 1
                    ],
                    [
                        'cday' => date('Y-m-d'),
                        'oday' => '2014-10-26',
                        'eday' => date('Y-m-d H:i:s'),
                        'dtext' => 'testcontroller2',
                        'uid' => 1,
                        'pstatus' => 1,
                        'mstatus' => 1
                    ],
                    [
                        'cday' => date('Y-m-d'),
                        'oday' => date('Y-m-d'),
                        'eday' => date('Y-m-d H:i:s'),
                        'dtext' => 'testcontroller3',
                        'uid' => 3,
                        'pstatus' => 0,
                        'mstatus' => 0
                    ]
                ],
                'diary_message' => [
                    [
                        'dmessage' => 'testmessage1',
                        'dmdatetime' => date('Y-m-d H:i:s'),
                        'did' => 2,
                        'uid' => 3,
                        'status' => 1
                    ],
                    [
                        'dmessage' => 'testmessage2',
                        'dmdatetime' => date('Y-m-d H:i:s'),
                        'did' => 3,
                        'uid' => 2,
                        'status' => 0
                    ],
                    [
                        'dmessage' => 'testmessage3',
                        'dmdatetime' => date('Y-m-d H:i:s'),
                        'did' => 1,
                        'uid' => 2,
                        'status' => 1
                    ],
                    [
                        'dmessage' => 'testmessage4',
                        'dmdatetime' => date('Y-m-d H:i:s'),
                        'did' => 1,
                        'uid' => 3,
                        'status' => 0
                    ]
                ]
            ]
        );
    }

    public function testCreate()
    {
        $this->uarr = array(
                    'oday' => date('Y-m-d'),
                    'dtext' => 'testdiarycreate',
                    'pstatus' => 1,
                    'mstatus' => 1,
                    'uid' => 2
                );
        $this->assertEquals($this->scode['success'], DiaryAssembly::create($this->uarr, $this->scode));
        $this->assertEquals($this->scode['datarepeat'], DiaryAssembly::create($this->uarr, $this->scode));
        $this->uarr = array(
                    'oday' => '2015-01-01',
                    'dtext' => 'testdiarycreate',
                    'pstatus' => 1,
                    'mstatus' => 1,
                    'uid' => 2
                );
        $this->assertEquals($this->scode['datanotday'], DiaryAssembly::create($this->uarr, $this->scode));
        array_pop($this->uarr);
        $this->assertEquals($this->scode['dataerror'], DiaryAssembly::create($this->uarr, $this->scode));
    }

    public function testUpdate()
    {
        $this->uarr = array(
                    'oday' => date('Y-m-d'),
                    'soday' => date('Y-m-d'),
                    'dtext' => 'testdiaryupdate',
                    'pstatus' => 0,
                    'mstatus' => 0,
                    'uid' => 1
                );
        $this->assertEquals($this->scode['success'], DiaryAssembly::update($this->uarr, $this->scode));
        $this->uarr = array(
                    'oday' => "2010-01-01",
                    'soday' => date('Y-m-d'),
                    'dtext' => 'testdiaryupdate',
                    'pstatus' => 0,
                    'mstatus' => 0,
                    'uid' => 1
                );
        $this->assertEquals($this->scode['datanotday'], DiaryAssembly::update($this->uarr, $this->scode));
        array_pop($this->uarr);
        $this->assertEquals($this->scode['dataerror'], DiaryAssembly::update($this->uarr, $this->scode));
    }

    public function testDelete()
    {
        $this->uarr = array(
                    'did' => 1,
                    'uid' => 1
                );
        $this->assertEquals($this->scode['success'], DiaryAssembly::delete($this->uarr, $this->scode));
        $this->uarr = array(
                    'did' => 1,
                    'uid' => 2
                );
        $this->assertEquals($this->scode['datanot'], DiaryAssembly::delete($this->uarr, $this->scode));
        array_pop($this->uarr);
        $this->assertEquals($this->scode['dataerror'], DiaryAssembly::delete($this->uarr, $this->scode));
    }

    public function testSelectAll()
    {
        $this->uarr = array(
                    'uid' => 1
                );
        $dcsa = DiaryAssembly::selectall($this->uarr, $this->scode);
        // var_dump($dcsa);
        $this->assertEquals(false, isset($dcsa['statuscode']));
        array_pop($this->uarr);
        $this->assertEquals($this->scode['dataerror'], DiaryAssembly::selectall($this->uarr, $this->scode));
    }

    public function testSelectPublic()
    {
        $this->uarr = array(
                    'uid' => 1
                );
        $dcsp = DiaryAssembly::selectpublic($this->uarr, $this->scode);
        // var_dump($dcsp);
        $this->assertEquals(false, isset($dcsp['statuscode']));
        array_pop($this->uarr);
        $this->assertEquals($this->scode['dataerror'], DiaryAssembly::selectpublic($this->uarr, $this->scode));
    }

    public function testCreateMessage()
    {
        $this->uarr = array(
                    'dmessage' => 'testmessagecreate',
                    'did' => 1,
                    'mmstatus' => 1,
                    'uid' => 2
                );
        $this->assertEquals($this->scode['success'], DiaryAssembly::createmessage($this->uarr, $this->scode));
        $this->uarr = array(
                    'dmessage' => 'testmessagecreate',
                    'did' => 3,
                    'mmstatus' => 1,
                    'uid' => 2
                );
        $this->assertEquals($this->scode['datamstatusnot'], DiaryAssembly::createmessage($this->uarr, $this->scode));
        array_pop($this->uarr);
        $this->assertEquals($this->scode['dataerror'], DiaryAssembly::createmessage($this->uarr, $this->scode));
    }

    public function testSelectMessage()
    {
        $this->uarr = array(
                    'uid' => 1,
                    'did' => 1
                );
        $dcsp = DiaryAssembly::selectmessage($this->uarr, $this->scode);
        // var_dump($dcsp);
        $this->assertEquals(false, isset($dcsp['statuscode']));
        array_pop($this->uarr);
        $this->assertEquals($this->scode['dataerror'], DiaryAssembly::selectmessage($this->uarr, $this->scode));
    }

    public function testReplyMessage()
    {
        $this->uarr = array(
                    'uid' => 1,
                    'mid' => 1,
                    'dmreply' => 'Assemblyreplymessage'
                );
        $this->assertEquals($this->scode['success'], DiaryAssembly::replymessage($this->uarr, $this->scode));
        array_pop($this->uarr);
        $this->assertEquals($this->scode['dataerror'], DiaryAssembly::replymessage($this->uarr, $this->scode));
    }

    public function testDeleteMessage()
    {
        $this->uarr = array(
                    'mid' => 1,
                    'uid' => 1
                );
        $this->assertEquals($this->scode['success'], DiaryAssembly::deletemessage($this->uarr, $this->scode));
        array_pop($this->uarr);
        $this->assertEquals($this->scode['dataerror'], DiaryAssembly::deletemessage($this->uarr, $this->scode));
    }

    public function testUpdateforbill()
    {
        $this->uarr = array(
                    'oday' => date('Y-m-d'),
                    'dtext' => 'bill',
                    'uid' => 1
                );
        $this->assertEquals($this->scode['success'], DiaryAssembly::updateforbill($this->uarr, $this->scode));
        $this->uarr = array(
                    'oday' => '2010-01-01',
                    'dtext' => 'testdiaryupdate',
                    'uid' => 1
                );
        //datanot
        $this->assertEquals($this->scode['datanotday'], DiaryAssembly::updateforbill($this->uarr, $this->scode));
        array_pop($this->uarr);
        $this->assertEquals($this->scode['dataerror'], DiaryAssembly::updateforbill($this->uarr, $this->scode));
    }

    public function testUpdateforbilling()
    {
        $this->uarr = array(
                    'oday' => date('Y-m-d'),
                    'dtext' => 'billnew',
                    'uid' => 1
                );
        $this->assertEquals($this->scode['success'], DiaryAssembly::updateforbilling($this->uarr, $this->scode));
        $this->uarr = array(
                    'oday' => '2010-01-01',
                    'dtext' => 'testdiaryupdate',
                    'uid' => 1
                );
        //datanot
        $this->assertEquals($this->scode['datanotday'], DiaryAssembly::updateforbilling($this->uarr, $this->scode));
        array_pop($this->uarr);
        $this->assertEquals($this->scode['dataerror'], DiaryAssembly::updateforbilling($this->uarr, $this->scode));
    }
}
