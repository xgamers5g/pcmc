<?php
namespace DiaryTest\Model;

use PHPUnit_Framework_TestCase;
use Diary\Model\DataCheck;
use Diary\Lib\StatusCode;

class DataCheckTest extends PHPUnit_Framework_TestCase
{
    public function __construct()
    {
        $this->scode = StatusCode::responserule();
    }

    public function testuserset()
    {
        $arr = array(
                    'user' => 1,                //未設判斷
                    'day' => "2014-11-07",      //正確格式
                    'sday' => 1,
                    'content' => "這是文字",    //正確格式
                    'status1' => 9999,
                    'status2' => 9999,
                    'diaryid' => 1,             //正確格式
                    'message' => 1,
                    'status3' => 0,             //正確格式
                    'reply' => 1,
                    'messageid' => 1            //正確格式
            );
        $dc = DataCheck::userset($arr, $this->scode);
        // var_dump($dc);
        $this->assertEquals(6, count($dc));
    }

    public function testdiarycreate()
    {
        $arr = array(
                'dtext' => "abcd",
                'pstatus' => 1,
                'mstatus' => 1,
                'oday' => "2014-11-05",
                'uid' => 1
            );
        $this->assertEquals(true, DataCheck::diarycreate($arr, $this->scode));
        array_pop($arr);
        $this->assertEquals($this->scode['dataerror'], DataCheck::diarycreate($arr, $this->scode));
    }

    public function testdiaryupdate()
    {
        $arr = array(
                'oday' => "2014-11-05",
                'soday' => "2014-11-04",
                'dtext' => "test",
                'pstatus' => 1,
                'mstatus' => 1,
                'uid' => 1
            );
        $this->assertEquals(true, DataCheck::diaryupdate($arr, $this->scode));
        array_pop($arr);
        $this->assertEquals($this->scode['dataerror'], DataCheck::diaryupdate($arr, $this->scode));
    }

    public function testdiarydelete()
    {
        $arr = array(
                'did' => 1,
                'uid' => 1
            );
        $this->assertEquals(true, DataCheck::diarydelete($arr, $this->scode));
        array_pop($arr);
        $this->assertEquals($this->scode['dataerror'], DataCheck::diarydelete($arr, $this->scode));
    }

    public function testdiaryselectall()
    {
        $arr = array(
                'uid' => 1
            );
        $this->assertEquals(true, DataCheck::diaryselectall($arr, $this->scode));
        array_pop($arr);
        $this->assertEquals($this->scode['dataerror'], DataCheck::diaryselectall($arr, $this->scode));
    }

    public function testdiaryselectpublic()
    {
        $arr = array(
                'uid' => 1
            );
        $this->assertEquals(true, DataCheck::diaryselectpublic($arr, $this->scode));
        array_pop($arr);
        $this->assertEquals($this->scode['dataerror'], DataCheck::diaryselectpublic($arr, $this->scode));
    }

    public function testmessagecreate()
    {
        $arr = array(
                'dmessage' => "test",
                'mmstatus' => 1,
                'did' => 1,
                'uid' => 1
            );
        $this->assertEquals(true, DataCheck::messagecreate($arr, $this->scode));
        array_pop($arr);
        $this->assertEquals($this->scode['dataerror'], DataCheck::messagecreate($arr, $this->scode));
    }

    public function testmessageselect()
    {
        $arr = array(
                'did' => 1,
                'uid' => 1
            );
        $this->assertEquals(true, DataCheck::messageselect($arr, $this->scode));
        array_pop($arr);
        $this->assertEquals($this->scode['dataerror'], DataCheck::messageselect($arr, $this->scode));
    }

    public function testmessageupdate()
    {
        $arr = array(
                'dmreply' => "reply",
                'mid' => 1,
                'uid' => 1
            );
        $this->assertEquals(true, DataCheck::messageupdate($arr, $this->scode));
        array_pop($arr);
        $this->assertEquals($this->scode['dataerror'], DataCheck::messageupdate($arr, $this->scode));
    }

    public function testmessagedelete()
    {
        $arr = array(
                'mid' => 1,
                'uid' => 1
            );
        $this->assertEquals(true, DataCheck::messagedelete($arr, $this->scode));
        array_pop($arr);
        $this->assertEquals($this->scode['dataerror'], DataCheck::messagedelete($arr, $this->scode));
    }
}
