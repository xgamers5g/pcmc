<?php
namespace DiaryTest\Model;

use PDO;
use PHPUnit_Extensions_Database_TestCase;
use AuthTest\DataSet;
use Diary\Model\MessageModel;

class MessageModelTest extends PHPUnit_Extensions_Database_TestCase
{
    public function getConnection()
    {
        $conn = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }
    public function getDataSet()
    {
        return new DataSet(
            [
                'diary_message' => [
                    [
                        'dmessage' => 'messagemodeltest1',
                        'dmdatetime' => date('Y-m-d H:i:s'),
                        'did' => 1,
                        'uid' => 3,
                        'status' => 1
                    ],
                    [
                        'dmessage' => 'messagemodeltest2',
                        'dmdatetime' => date('Y-m-d H:i:s'),
                        'did' => 2,
                        'uid' => 2,
                        'status' => 0
                    ],
                    [
                        'dmessage' => 'messagemodeltest3',
                        'dmdatetime' => date('Y-m-d H:i:s'),
                        'did' => 1,
                        'uid' => 2,
                        'status' => 1
                    ]
                ]
            ]
        );
    }

    public function testCreate()
    {
        $arr1 = array(
            'did' => 1,
            'dmessage' => 'testmessage1106',
            'uid' => 1,
            'mmstatus' => 1
            );
        $scode = array(
                    'success' => array(
                        'statuscode' => '200',
                        'detail' => '執行成功'
                    ),
                    'databaseerror' => array(
                        'statuscode' => '599',
                        'detail' => '資料庫異常,請聯絡管理員'
                    )
                 );
        $this->assertEquals($scode['success'], MessageModel::create($arr1, $scode));
        // $this->assertEquals($scode['databaseerror'], MessageModel::create($arr2, $scode));
    }

    public function testSelect()
    {
        $arr1 = array(
            'did' => 2,
            'uid' => 2
        );
        $arr2 = array(
            'did' => 2,
            'uid' => 4
        );
        $scode = array(
                    'datanot' => array(
                        'statuscode' => '401',
                        'detail' => '沒有資料'
                    ),
                    'databaseerror' => array(
                        'statuscode' => '599',
                        'detail' => '資料庫異常,請聯絡管理員'
                    )
                 );
        $mms = MessageModel::select($arr1, $scode);
        // var_dump($mms);
        $this->assertEquals(false, isset($mms['statuscode']));
        $this->assertEquals($scode['datanot'], MessageModel::select($arr2, $scode));
        // $this->assertEquals($scode['databaseerror'], MessageModel::select($arr3, $scode));
    }

    public function testUpdate()
    {
        $arr1 = array(
            'dmreply' => 'testreply1106',
            'mid' => 1,
            'uid' => 1
            );
        $arr2 = array(
            'dmreply' => 'testreply1106',
            'mid' => 1,
            'uid' => 2
            );
        $scode = array(
                    'success' => array(
                        'statuscode' => '200',
                        'detail' => '執行成功'
                    ),
                    'datanot' => array(
                        'statuscode' => '401',
                        'detail' => '沒有資料'
                    ),
                    'databaseerror' => array(
                        'statuscode' => '599',
                        'detail' => '資料庫異常,請聯絡管理員'
                    )
                 );
        $this->assertEquals($scode['success'], MessageModel::update($arr1, $scode));
        $this->assertEquals($scode['datanot'], MessageModel::update($arr2, $scode));
        // $this->assertEquals($scode['databaseerror'], MessageModel::update($arr3, $scode));
    }

    public function testDelete()
    {
        $arr1 = array(
            'mid' => 1,
            'uid' => 2
            );
        $arr2 = array(
            'mid' => 1,
            'uid' => 1
            );
        $scode = array(
                    'success' => array(
                        'statuscode' => '200',
                        'detail' => '執行成功'
                    ),
                    'datanot' => array(
                        'statuscode' => '401',
                        'detail' => '沒有資料'
                    ),
                    'databaseerror' => array(
                        'statuscode' => '599',
                        'detail' => '資料庫異常,請聯絡管理員'
                    )
                 );
        $this->assertEquals($scode['datanot'], MessageModel::delete($arr1, $scode));
        $this->assertEquals($scode['success'], MessageModel::delete($arr2, $scode));
        // $this->assertEquals($scode['databaseerror'], MessageModel::delete($arr3, $scode));
    }

    public function testDeleteDid()
    {
        $arr1 = array(
            'did' => 1,
            'uid' => 1
            );
        $scode = array(
                    'databaseerror' => array(
                        'statuscode' => '599',
                        'detail' => '資料庫異常,請聯絡管理員'
                    )
                 );
        $this->assertEquals(true, MessageModel::deletedid($arr1, $scode));
        // $this->assertEquals($scode['databaseerror'], MessageModel::deletedid($arr2, $scode));
    }
}
