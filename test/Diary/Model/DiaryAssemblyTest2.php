<?php
namespace DiaryTest\Model;

use PDO;
use PHPUnit_Extensions_Database_TestCase;
use AuthTest\DataSet;
use Diary\Model\DiaryAssembly;
use Diary\Lib\StatusCode;

class DiaryAssemblyTest2 extends PHPUnit_Extensions_Database_TestCase
{
    public function __construct()
    {
        $this->scode = StatusCode::responserule();
    }

    public function getConnection()
    {
        $conn = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }
    public function getDataSet()
    {
        return new DataSet(
            [
                'diary' => [
                    [
                        'cday' => date('Y-m-d'),
                        'oday' => date('Y-m-d'),
                        'eday' => date('Y-m-d H:i:s'),
                        'dtext' => 'testcontroller1',
                        'uid' => 1,
                        'pstatus' => 1,
                        'mstatus' => 1
                    ],
                    [
                        'cday' => date('Y-m-d'),
                        'oday' => '2014-10-26',
                        'eday' => date('Y-m-d H:i:s'),
                        'dtext' => 'testcontroller2',
                        'uid' => 1,
                        'pstatus' => 1,
                        'mstatus' => 1
                    ],
                    [
                        'cday' => date('Y-m-d'),
                        'oday' => date('Y-m-d'),
                        'eday' => date('Y-m-d H:i:s'),
                        'dtext' => 'testcontroller3',
                        'uid' => 3,
                        'pstatus' => 0,
                        'mstatus' => 0
                    ]
                ],
                'diary_message' => [
                    [
                        'dmessage' => 'testmessage1',
                        'dmdatetime' => date('Y-m-d H:i:s'),
                        'did' => 2,
                        'uid' => 3,
                        'status' => 1
                    ],
                    [
                        'dmessage' => 'testmessage2',
                        'dmdatetime' => date('Y-m-d H:i:s'),
                        'did' => 3,
                        'uid' => 2,
                        'status' => 0
                    ],
                    [
                        'dmessage' => 'testmessage3',
                        'dmdatetime' => date('Y-m-d H:i:s'),
                        'did' => 1,
                        'uid' => 2,
                        'status' => 1
                    ],
                    [
                        'dmessage' => 'testmessage4',
                        'dmdatetime' => date('Y-m-d H:i:s'),
                        'did' => 1,
                        'uid' => 3,
                        'status' => 0
                    ]
                ]
            ]
        );
    }

    public function testUpdateforbill()
    {
        $this->uarr = array(
                    'oday' => date('Y-m-d'),
                    'dtext' => 'bill',
                    'uid' => 1
                );
        $this->assertEquals($this->scode['success'], DiaryAssembly::updateforbill($this->uarr, $this->scode));
        $this->uarr = array(
                    'oday' => '2010-01-01',
                    'dtext' => 'testdiaryupdate',
                    'uid' => 1
                );
        //datanot
        $this->assertEquals($this->scode['datanotday'], DiaryAssembly::updateforbill($this->uarr, $this->scode));
        array_pop($this->uarr);
        $this->assertEquals($this->scode['dataerror'], DiaryAssembly::updateforbill($this->uarr, $this->scode));
    }
}
