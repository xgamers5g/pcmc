<?php
namespace AuthFriendTest\Model;

use PHPUnit_Framework_TestCase;

class AuthFriendModelUnitTest extends PHPUnit_Framework_TestCase
{
    public function testgetInstance()
    {
        //Singleton Class的模擬方法
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $fm = \AuthFriend\Model\AuthFriendModel::getInstance($pdo, $redis, $response, 1);
        $this->assertTrue($fm instanceof \AuthFriend\Model\AuthFriendModel);
    }
    public function testaddWithNoFid()
    {
        //Singleton Class的模擬方法
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('dataerror')
            ->will($this->returnValue(array('statuscode' => '400', 'detail' => '測試成功')));
        $fid = null;
        $assertdata = array(
            'statuscode' => '400',
            'detail' => '測試成功'
        );
        $fm = \AuthFriend\Model\AuthFriendModel::getInstance($pdo, $redis, $response, 1, $fid);
        $this->assertEquals(
            $assertdata,
            $fm->add()
        );
    }
    public function testaddWithCorrectFidAndRedisExistAndPDOSuccess()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->at(0))
            ->method('bindValue')
            ->with(1, 1, 1);
        $pdostatement
            ->expects($this->at(1))
            ->method('bindValue')
            ->with(2, 2, 1);
        $pdostatement
            ->expects($this->at(2))
            ->method('execute')
            ->will($this->returnValue(true));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->with('insert into user_friend(userid, friendid) VALUES (?, ?)')
            ->will($this->returnValue($pdostatement));
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis
            ->expects($this->any())
            ->method('exists')
            ->with('userfriend::userid::1')
            ->will($this->returnValue(true));
        $redis
            ->expects($this->any())
            ->method('get')
            ->with('userfriend::userid::1')
            ->will($this->returnValue('{"0":"3"}'));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('success')
            ->will($this->returnValue(array('statuscode' => '200', 'detail' => '測試成功')));
        $fid = 2;
        $assertdata = array(
            'statuscode' => '200',
            'detail' => '測試成功'
        );
        $fm = \AuthFriend\Model\AuthFriendModel::getInstance($pdo, $redis, $response, 1, $fid);
        $this->assertEquals($assertdata, $fm->add());
    }
    
    public function testaddWithCorrectFidAndRedisNotExistAndPDOSuccess()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->at(0))
            ->method('bindValue')
            ->with(1, 1, 1);
        $pdostatement
            ->expects($this->at(1))
            ->method('bindValue')
            ->with(2, 2, 1);
        $pdostatement
            ->expects($this->at(2))
            ->method('execute')
            ->will($this->returnValue(true));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->with('insert into user_friend(userid, friendid) VALUES (?, ?)')
            ->will($this->returnValue($pdostatement));
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis
            ->expects($this->any())
            ->method('exists')
            ->with('userfriend::userid::1')
            ->will($this->returnValue(false));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('success')
            ->will($this->returnValue(array('statuscode' => '200', 'detail' => '測試成功')));
        $fid = 2;
        $assertdata = array(
            'statuscode' => '200',
            'detail' => '測試成功'
        );
        $fm = \AuthFriend\Model\AuthFriendModel::getInstance($pdo, $redis, $response, 1, $fid);
        $this->assertEquals($assertdata, $fm->add());
    }
    public function testaddWithCorrectFidAndRedisNotExistAndPDOFail()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->at(0))
            ->method('bindValue')
            ->with(1, 1, 1);
        $pdostatement
            ->expects($this->at(1))
            ->method('bindValue')
            ->with(2, 2, 1);
        $pdostatement
            ->expects($this->at(2))
            ->method('execute')
            ->will($this->returnValue(false));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->with('insert into user_friend(userid, friendid) VALUES (?, ?)')
            ->will($this->returnValue($pdostatement));
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis
            ->expects($this->any())
            ->method('exists')
            ->with('userfriend::userid::1')
            ->will($this->returnValue(false));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('databaseerror')
            ->will($this->returnValue(array('statuscode' => '599', 'detail' => '測試成功')));
        $fid = 2;
        $assertdata = array(
            'statuscode' => '599',
            'detail' => '測試成功'
        );
        $fm = \AuthFriend\Model\AuthFriendModel::getInstance($pdo, $redis, $response, 1, $fid);
        $this->assertEquals($assertdata, $fm->add());
    }
    public function testgetWithRedisExists()
    {
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis
            ->expects($this->any())
            ->method('exists')
            ->with('userfriend::userid::1')
            ->will($this->returnValue(true));
        $redis
            ->expects($this->any())
            ->method('get')
            ->with('userfriend::userid::1')
            ->will($this->returnValue('{"0":"2", "1":"3", "2":"4"}'));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('success')
            ->will($this->returnValue(array('statuscode' => '200', 'detail' => '測試成功')));
        $assertdata = array(
            'statuscode' => '200',
            'detail' => '測試成功',
            'friendlist' =>array(
                0 => 2,
                1 => 3,
                2 => 4
            )
        );
        $fm = \AuthFriend\Model\AuthFriendModel::getInstance($pdo, $redis, $response, 1);
        $this->assertEquals($assertdata, $fm->get());
    }
    public function testgetWithRedisNotExistsAndPDOSuccess()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->any())
            ->method('bindValue')
            ->with(1, 1, 1);
        $pdostatement
            ->expects($this->any())
            ->method('fetchALL')
            ->with(7)
            ->will($this->returnValue(array(0=>2, 1=>3, 2=>4)));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->with('select friendid from user_friend where userid = ?')
            ->will($this->returnValue($pdostatement));
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis
            ->expects($this->any())
            ->method('exists')
            ->with('userfriend::userid::1')
            ->will($this->returnValue(false));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('success')
            ->will($this->returnValue(array('statuscode' => '200', 'detail' => '測試成功')));
        $assertdata = array(
            'statuscode' => '200',
            'detail' => '測試成功',
            'friendlist' =>array(
                0 => 2,
                1 => 3,
                2 => 4
            )
        );
        $fm = \AuthFriend\Model\AuthFriendModel::getInstance($pdo, $redis, $response, 1);
        $this->assertEquals($assertdata, $fm->get());
    }
    public function testgetWithRedisNotExistsAndPDOFail()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->any())
            ->method('bindValue')
            ->with(1, 1, 1);
        $pdostatement
            ->expects($this->any())
            ->method('fetchALL')
            ->with(7)
            ->will($this->returnValue(false));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->with('select friendid from user_friend where userid = ?')
            ->will($this->returnValue($pdostatement));
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis
            ->expects($this->any())
            ->method('exists')
            ->with('userfriend::userid::1')
            ->will($this->returnValue(false));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('databaseerror')
            ->will($this->returnValue(array('statuscode' => '599', 'detail' => '測試成功')));
        $assertdata = array(
            'statuscode' => '599',
            'detail' => '測試成功',
        );
        $fm = \AuthFriend\Model\AuthFriendModel::getInstance($pdo, $redis, $response, 1);
        $this->assertEquals($assertdata, $fm->get());
    }
    public function testdeleteWithNoFid()
    {
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        /*
        $redis
            ->expects($this->any())
            ->method('exists')
            ->with('userfriend::userid::1')
            ->will($this->returnValue(false));
         */
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('dataerror')
            ->will($this->returnValue(array('statuscode' => '599', 'detail' => '測試成功')));
        $assertdata = array(
            'statuscode' => '599',
            'detail' => '測試成功',
        );
        $fid = null;
        $fm = \AuthFriend\Model\AuthFriendModel::getInstance($pdo, $redis, $response, 1, $fid);
        $this->assertEquals($assertdata, $fm->delete());
    }
    public function testdeleteWithRedisExistCountDataNotZeroPDOSuccess()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->at(0))
            ->method('bindValue')
            ->with(1, 1, 1);
        $pdostatement
            ->expects($this->at(1))
            ->method('bindValue')
            ->with(2, 2, 1);
        $pdostatement
            ->expects($this->at(2))
            ->method('execute')
            ->will($this->returnValue(true));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->with('delete from user_friend where userid = ? and friendid = ?')
            ->will($this->returnValue($pdostatement));
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis
            ->expects($this->any())
            ->method('exists')
            ->with('userfriend::userid::1')
            ->will($this->returnValue(true));
        $redis
            ->expects($this->any())
            ->method('get')
            ->with('userfriend::userid::1')
            ->will($this->returnValue('{"0":"2", "1":"3", "2":"4"}'));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('success')
            ->will($this->returnValue(array('statuscode' => '200', 'detail' => '測試成功')));
        $assertdata = array(
            'statuscode' => '200',
            'detail' => '測試成功',
        );
        $fid = 2;
        $fm = \AuthFriend\Model\AuthFriendModel::getInstance($pdo, $redis, $response, 1, $fid);
        $this->assertEquals($assertdata, $fm->delete());
    }
    public function testdeleteWithRedisExistCountDataNotZeroPDOFail()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->at(0))
            ->method('bindValue')
            ->with(1, 1, 1);
        $pdostatement
            ->expects($this->at(1))
            ->method('bindValue')
            ->with(2, 2, 1);
        $pdostatement
            ->expects($this->at(2))
            ->method('execute')
            ->will($this->returnValue(false));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->with('delete from user_friend where userid = ? and friendid = ?')
            ->will($this->returnValue($pdostatement));
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis
            ->expects($this->any())
            ->method('exists')
            ->with('userfriend::userid::1')
            ->will($this->returnValue(true));
        $redis
            ->expects($this->any())
            ->method('get')
            ->with('userfriend::userid::1')
            ->will($this->returnValue('["2","3","4"]'));
        $redis
            ->expects($this->any())
            ->method('set')
            ->with('userfriend::userid::1', '["3","4"]');
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('databaseerror')
            ->will($this->returnValue(array('statuscode' => '599', 'detail' => '測試成功')));
        $assertdata = array(
            'statuscode' => '599',
            'detail' => '測試成功',
        );
        $fid = 2;
        $fm = \AuthFriend\Model\AuthFriendModel::getInstance($pdo, $redis, $response, 1, $fid);
        $this->assertEquals($assertdata, $fm->delete());
    }
    public function testdeleteWithRedisExistCountDataIsZeroPDOSuccess()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->at(0))
            ->method('bindValue')
            ->with(1, 1, 1);
        $pdostatement
            ->expects($this->at(1))
            ->method('bindValue')
            ->with(2, 2, 1);
        $pdostatement
            ->expects($this->at(2))
            ->method('execute')
            ->will($this->returnValue(true));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->with('delete from user_friend where userid = ? and friendid = ?')
            ->will($this->returnValue($pdostatement));
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis
            ->expects($this->any())
            ->method('exists')
            ->with('userfriend::userid::1')
            ->will($this->returnValue(true));
        $redis
            ->expects($this->any())
            ->method('get')
            ->with('userfriend::userid::1')
            ->will($this->returnValue('["2"]'));
        $redis
            ->expects($this->any())
            ->method('delete')
            ->with('userfriend::userid::1');
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('success')
            ->will($this->returnValue(array('statuscode' => '200', 'detail' => '測試成功')));
        $assertdata = array(
            'statuscode' => '200',
            'detail' => '測試成功',
        );
        $fid = 2;
        $fm = \AuthFriend\Model\AuthFriendModel::getInstance($pdo, $redis, $response, 1, $fid);
        $this->assertEquals($assertdata, $fm->delete());
    }
    public function testdeleteWithRedisExistCountDataIsZeroPDOFail()
    {
        $pdostatement = $this->getMock('\PDOStatement', array(), array(), '', false, true, true);
        $pdostatement
            ->expects($this->at(0))
            ->method('bindValue')
            ->with(1, 1, 1);
        $pdostatement
            ->expects($this->at(1))
            ->method('bindValue')
            ->with(2, 2, 1);
        $pdostatement
            ->expects($this->at(2))
            ->method('execute')
            ->will($this->returnValue(false));
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $pdo
            ->expects($this->any())
            ->method('prepare')
            ->with('delete from user_friend where userid = ? and friendid = ?')
            ->will($this->returnValue($pdostatement));
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $redis
            ->expects($this->any())
            ->method('exists')
            ->with('userfriend::userid::1')
            ->will($this->returnValue(true));
        $redis
            ->expects($this->any())
            ->method('get')
            ->with('userfriend::userid::1')
            ->will($this->returnValue('["2"]'));
        $redis
            ->expects($this->any())
            ->method('delete')
            ->with('userfriend::userid::1');
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $response
            ->expects($this->any())
            ->method('generateResponse')
            ->with('databaseerror')
            ->will($this->returnValue(array('statuscode' => '599', 'detail' => '測試成功')));
        $assertdata = array(
            'statuscode' => '599',
            'detail' => '測試成功',
        );
        $fid = 2;
        $fm = \AuthFriend\Model\AuthFriendModel::getInstance($pdo, $redis, $response, 1, $fid);
        $this->assertEquals($assertdata, $fm->delete());
    }
}
