<?php
namespace AuthFriendTest\Controller;

use PHPUnit_Extensions_Database_TestCase;
use AuthFriendTest\DataSet;
use Auth\Tools\ToolCurl;
//use Fruit;
use Redis;
use PDO;
use DateTime;

class AuthFriendControllerTest extends PHPUnit_Extensions_Database_TestCase
{
    public function getConnection()
    {
        $conn = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }
    public function getDataSet()
    {
        $encrypt = \Auth\System\Core::getInstance()->getEncrypt();
        return new DataSet(
            [
                'user' => [
                    [
                        'email' => 'xgamers5g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'garth',
                        'usersecret' => $encrypt->secretEncode('12345', 'Mzk5ZmFl')
                    ],
                    [
                        'email' => 'louis@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'louis',
                        'usersecret' => $encrypt->secretEncode('12345', 'Mzk5ZmFl')
                    ],
                    [
                        'email' => 'peter@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'peter',
                        'usersecret' => $encrypt->secretEncode('12345', 'Mzk5ZmFl')
                    ],
                    [
                        'email' => 'mikasa@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'mikasa',
                        'usersecret' => $encrypt->secretEncode('12345', 'Mzk5ZmFl')
                    ]
                ],
                'user_friend' => [
                    [
                        'userid' => 1,
                        'friendid' => 2
                    ],
                    [
                        'userid' => 1,
                        'friendid' => 3
                    ],
                    [
                        'userid' => 1,
                        'friendid' => 4
                    ],
                    [
                        'userid' => 2,
                        'friendid' => 1
                    ],
                    [
                        'userid' => 2,
                        'friendid' => 3
                    ],
                    [
                        'userid' => 3,
                        'friendid' => 1
                    ]
                ]
            ]
        );
    }
    public function testDataSet()
    {
        $this->assertEquals(4, $this->getConnection()->getRowCount('user'));
        $this->assertEquals(6, $this->getConnection()->getRowCount('user_friend'));
    }
    public function testaddFriendAction()
    {
        $postdata = json_encode(
            array(
                'email' => 'peter@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $friend = json_encode(
            array(
                'fid' => 2
            )
        );
        $redis = new Redis();
        $redis->pconnect('/tmp/redis.sock');
        $redis->select(1);
        $redis->flushDB();
        $redis->set(
            'userfriend::userid::3',
            json_encode(
                array(
                    0 => 1
                )
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $friend = json_decode($friend, true);
        $friend['Authorization'] = $body['Authorization'];
        $friend = json_encode($friend);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/friend/add');
        $res = $tc->postJSON($friend, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals(200, $body['statuscode']);
    }
    public function testgetFriendAction()
    {
        $redis = new Redis();
        $redis->pconnect('/tmp/redis.sock');
        $redis->select(1);
        $redis->flushDB();
        $redis->set(
            'userfriend::userid::1',
            json_encode(
                array(
                    0 => 2,
                    1 => 3,
                    2 => 4
                )
            )
        );
        $postdata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        //$friend = json_decode($friend, true);
        $friend['Authorization'] = $body['Authorization'];
        $friend = json_encode($friend);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/friend/get');
        $res = $tc->postJSON($friend, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals(200, $body['statuscode']);
    }
    public function testdeleteFriendAction()
    {
        $redis = new Redis();
        $redis->pconnect('/tmp/redis.sock');
        $redis->select(1);
        $redis->flushDB();
        $redis->set(
            'userfriend::userid::1',
            json_encode(
                array(
                    0 => 2,
                    1 => 3,
                    2 => 4
                )
            )
        );
        $postdata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $friend = json_encode(
            array(
                'fid' => 2
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($postdata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $friend = json_decode($friend, true);
        $friend['Authorization'] = $body['Authorization'];
        $friend = json_encode($friend);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $tc = ToolCurl::init(SITE_URL . '/friend/delete');
        $res = $tc->postJSON($friend, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals(200, $body['statuscode']);
    }
}
