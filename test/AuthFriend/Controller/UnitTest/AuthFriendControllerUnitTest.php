<?php
namespace AuthFriendTest\Controller\UnitTest;

use PHPUnit_Framework_TestCase;

class AuthFriendControllerUnitTest extends PHPUnit_Framework_TestCase
{
    public function testNewWithDI()
    {
        $facade = $this->getMock('\Auth\Facade\AuthFacade', array(), array(), '', false, true, true);
        $friendfacade = $this->getMock(
            '\AuthFriend\Facade\AuthFriendFacade',
            array(),
            array(),
            '',
            false,
            true,
            true
        );
        $res = new \AuthFriend\Controller\AuthFriendController($facade, $friendfacade);
        $this->assertEquals(true, $res instanceof \AuthFriend\Controller\AuthFriendController);
    }
    public function testNewWithoutDI()
    {
        $res = new \AuthFriend\Controller\AuthFriendController();
        $this->assertEquals(true, $res instanceof \AuthFriend\Controller\AuthFriendController);
    }
    public function testgetFriendActionWhenNotLogin()
    {
        $facade = $this->getMock('\Auth\Facade\AuthFacade', array(), array(), '', false, true, true);
        $facade
            ->expects($this->any())
            ->method('getUserData')
            ->will($this->returnValue(array('statuscode' => '401','detail' => '測試成功')));
        $friendfacade = $this->getMock(
            '\AuthFriend\Facade\AuthFriendFacade',
            array(),
            array(),
            '',
            false,
            true,
            true
        );
        $friendfacade
            ->expects($this->any())
            ->method('getFriend')
            ->will($this->returnValue(array('testkey' => 'testvalue')));
        $res = new \AuthFriend\Controller\AuthFriendController($facade, $friendfacade);
        //var_dump($res);
        $this->assertEquals('{"statuscode":"401","detail":"測試成功"}', $res->getFriendAction());
    }
    public function testgetFriendActionWhenLogin()
    {
        $facade = $this->getMock('\Auth\Facade\AuthFacade', array(), array(), '', false, true, true);
        $facade
            ->expects($this->any())
            ->method('getUserData')
            ->will(
                $this->returnValue(
                    array(
                        'statuscode' => '200',
                        'detail' => '測試成功',
                        'id' => 1,
                        'Authorization' => 'asdf'
                    )
                )
            );
        $friendfacade = $this->getMock(
            '\AuthFriend\Facade\AuthFriendFacade',
            array(),
            array(),
            '',
            false,
            true,
            true
        );
        $friendfacade
            ->expects($this->any())
            ->method('getFriend')
            ->will($this->returnValue(array('testkey' => 'testvalue')));
        $res = new \AuthFriend\Controller\AuthFriendController($facade, $friendfacade);
        $this->assertEquals('{"testkey":"testvalue"}', $res->getFriendAction());
    }
    public function testAddFriendActionWhenNotLogin()
    {
        $facade = $this->getMock('\Auth\Facade\AuthFacade', array(), array(), '', false, true, true);
        $facade
            ->expects($this->any())
            ->method('getUserData')
            ->will(
                $this->returnValue(
                    array(
                        'statuscode' => '401',
                        'detail' => '測試成功'
                    )
                )
            );
        $friendfacade = $this->getMock(
            '\AuthFriend\Facade\AuthFriendFacade',
            array(),
            array(),
            '',
            false,
            true,
            true
        );
        $friendfacade
            ->expects($this->any())
            ->method('addFriend')
            ->will($this->returnValue(array('testkey' => 'testvalue')));
        $res = new \AuthFriend\Controller\AuthFriendController($facade, $friendfacade);
        $this->assertEquals('{"statuscode":"401","detail":"測試成功"}', $res->addFriendAction());
    }
    public function testAddFriendActionWhenLogin()
    {
        $facade = $this->getMock('\Auth\Facade\AuthFacade', array(), array(), '', false, true, true);
        $facade
            ->expects($this->any())
            ->method('getUserData')
            ->will(
                $this->returnValue(
                    array(
                        'statuscode' => '200',
                        'detail' => '測試成功',
                        'id' => 1,
                        'Authorization' => 'asdf'
                    )
                )
            );
        $friendfacade = $this->getMock(
            '\AuthFriend\Facade\AuthFriendFacade',
            array(),
            array(),
            '',
            false,
            true,
            true
        );
        $friendfacade
            ->expects($this->any())
            ->method('addFriend')
            ->will($this->returnValue(array('testkey' => 'testvalue')));
        $res = new \AuthFriend\Controller\AuthFriendController($facade, $friendfacade);
        $this->assertEquals('{"testkey":"testvalue"}', $res->addFriendAction());
    }
    public function testDeleteFriendActionWhenNotLogin()
    {
        $facade = $this->getMock('\Auth\Facade\AuthFacade', array(), array(), '', false, true, true);
        $facade
            ->expects($this->any())
            ->method('getUserData')
            ->will(
                $this->returnValue(
                    array(
                        'statuscode' => '401',
                        'detail' => '測試成功'
                    )
                )
            );
        $friendfacade = $this->getMock(
            '\AuthFriend\Facade\AuthFriendFacade',
            array(),
            array(),
            '',
            false,
            true,
            true
        );
        $friendfacade
            ->expects($this->any())
            ->method('deleteFriend')
            ->will($this->returnValue(array('testkey' => 'testvalue')));
        $res = new \AuthFriend\Controller\AuthFriendController($facade, $friendfacade);
        $this->assertEquals('{"statuscode":"401","detail":"測試成功"}', $res->deleteFriendAction());
    }
    public function testDeleteFriendActionWhenLogin()
    {
        $facade = $this->getMock('\Auth\Facade\AuthFacade', array(), array(), '', false, true, true);
        $facade
            ->expects($this->any())
            ->method('getUserData')
            ->will(
                $this->returnValue(
                    array(
                        'statuscode' => '200',
                        'detail' => '測試成功',
                        'id' => 1,
                        'Authorization' => 'asdf'
                    )
                )
            );
        $friendfacade = $this->getMock(
            '\AuthFriend\Facade\AuthFriendFacade',
            array(),
            array(),
            '',
            false,
            true,
            true
        );
        $friendfacade
            ->expects($this->any())
            ->method('deleteFriend')
            ->will($this->returnValue(array('testkey' => 'testvalue')));
        $res = new \AuthFriend\Controller\AuthFriendController($facade, $friendfacade);
        $this->assertEquals('{"testkey":"testvalue"}', $res->deleteFriendAction());
    }
}
