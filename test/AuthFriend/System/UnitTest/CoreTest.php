<?php
namespace AuthFriendTest\System\UnitTest;

use PHPUnit_Framework_TestCase;

class CoreTest extends PHPUnit_Framework_TestCase
{
    public function testGetAuthFriendModel()
    {
        $res = \AuthFriend\System\Core::getInstance();
        $this->assertEquals(true, $res instanceof \AuthFriend\System\Core);
        \AuthFriend\System\Core::setConfig('test');
        $this->assertEquals(true, $res instanceof \AuthFriend\System\Core);
    }
    public function testGetAuthFriendFacade()
    {
        $res = \AuthFriend\System\Core::getInstance();
        $this->assertEquals(
            true,
            $res->getAuthFriendFacade($res, 1, 'asdf')
            instanceof \AuthFriend\Facade\AuthFriendFacade
        );
    }
}
