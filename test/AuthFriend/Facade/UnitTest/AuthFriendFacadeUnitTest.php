<?php
namespace AuthFriendTest\Facade\UnitTest;

use PHPUnit_Framework_TestCase;

class AuthFriendFacadeUnitTest extends PHPUnit_Framework_TestCase
{
    public function testgetInstance()
    {
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array()));
        $authfriendmodel = $this->getMock(
            '\AuthFriend\Model\AuthFriendModel',
            array(),
            array(),
            '',
            false,
            true,
            true
        );
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $core = $this->getMock('\AuthFriend\System\Core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getDb')
            ->will($this->returnValue($pdo));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getAuthFriendModel')
            ->with($core->getDb(), $core->getRedis(), $core->getResponse(), 1)
            ->will($this->returnValue($authfriendmodel));
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $facade = \AuthFriend\Facade\AuthFriendFacade::getInstance($core, 1, 'asdf');
        $this->assertEquals(
            true,
            $facade instanceof \AuthFriend\Facade\AuthFriendFacade
        );
    }
    public function testgetFriend()
    {
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        // $posthandler
        //     ->expects($this->any())
        //     ->method('getJSONData')
        //     ->will($this->returnValue(array("fid" => 2)));
        $authfriendmodel = $this->getMock(
            '\AuthFriend\Model\AuthFriendModel',
            array(),
            array(),
            '',
            false,
            true,
            true
        );
        $authfriendmodel
            ->expects($this->any())
            ->method('get')
            ->will($this->returnValue(array('testkey' => 'testvalue')));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $core = $this->getMock('\AuthFriend\System\Core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getDb')
            ->will($this->returnValue($pdo));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getAuthFriendModel')
            ->with($core->getDb(), $core->getRedis(), $core->getResponse(), 1)
            ->will($this->returnValue($authfriendmodel));
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $facade = \AuthFriend\Facade\AuthFriendFacade::getInstance($core, 1, 'asdf');
        $assertData = array(
            'testkey' => 'testvalue',
            'Authorization' => 'asdf'
        );
        $this->assertEquals($assertData, $facade->getFriend());
    }
    public function testaddFriend()
    {
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array("fid" => 2)));
        $authfriendmodel = $this->getMock(
            '\AuthFriend\Model\AuthFriendModel',
            array(),
            array(),
            '',
            false,
            true,
            true
        );
        $authfriendmodel
            ->expects($this->any())
            ->method('add')
            ->will($this->returnValue(array('testkey' => 'testvalue')));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $core = $this->getMock('\AuthFriend\System\Core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getDb')
            ->will($this->returnValue($pdo));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getAuthFriendModel')
            ->with($core->getDb(), $core->getRedis(), $core->getResponse(), 1)
            ->will($this->returnValue($authfriendmodel));
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $facade = \AuthFriend\Facade\AuthFriendFacade::getInstance($core, 1, 'asdf');
        $assertData = array(
            'testkey' => 'testvalue',
            'Authorization' => 'asdf'
        );
        $this->assertEquals($assertData, $facade->addFriend());
    }
    public function testdeleteFriend()
    {
        $posthandler = $this->getMock('\Auth\Model\PostHandler', array(), array(), '', false, true, true);
        $posthandler
            ->expects($this->any())
            ->method('getJSONData')
            ->will($this->returnValue(array("fid" => 2)));
        $authfriendmodel = $this->getMock(
            '\AuthFriend\Model\AuthFriendModel',
            array(),
            array(),
            '',
            false,
            true,
            true
        );
        $authfriendmodel
            ->expects($this->any())
            ->method('delete')
            ->will($this->returnValue(array('testkey' => 'testvalue')));
        $response = $this->getMock('\Auth\Model\Response', array(), array(), '', false, true, true);
        $redis = $this->getMock('\Redis', array(), array(), '', false, true, true);
        $pdo = $this->getMock('\PDO', array(), array(), '', false, true, true);
        $core = $this->getMock('\AuthFriend\System\Core', array(), array(), '', false, true, true);
        $core
            ->expects($this->any())
            ->method('getDb')
            ->will($this->returnValue($pdo));
        $core
            ->expects($this->any())
            ->method('getRedis')
            ->will($this->returnValue($redis));
        $core
            ->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($response));
        $core
            ->expects($this->any())
            ->method('getAuthFriendModel')
            ->with($core->getDb(), $core->getRedis(), $core->getResponse(), 1)
            ->will($this->returnValue($authfriendmodel));
        $core
            ->expects($this->any())
            ->method('getPostHandler')
            ->will($this->returnValue($posthandler));
        $facade = \AuthFriend\Facade\AuthFriendFacade::getInstance($core, 1, 'asdf');
        $assertData = array(
            'testkey' => 'testvalue',
            'Authorization' => 'asdf'
        );
        $this->assertEquals($assertData, $facade->deleteFriend());
    }
}
