<?php
namespace BillingTest\Model;

use PHPUnit_Framework_TestCase;
use Billing\Model\Package;
use Auth\Model\DataBox;

class PackageTest extends PHPUnit_Framework_TestCase
{
    public function testload()
    {
        $user = DataBox::getDataBox();
        $user->addData('id', 1);
        $act = "test";
        $pkg = Package::load($act, $user);
        $this->assertEquals(true, ($pkg instanceof Package));
        $this->assertEquals("object", gettype($pkg));
    }

    public function testAct()
    {
        $user = DataBox::getDataBox();
        $user->addData('id', 1);
        $act = "test";
        $pkg = Package::load($act, $user);
        $this->assertEquals($act, $pkg->getAct());
        $pkg->setAct("Load");
        $this->assertEquals("Load", $pkg->getAct());
        $pkg->setAct(1);
        $this->assertEquals('Action_Err', $pkg->getAct());
    }

    public function testActError()
    {
        $user = DataBox::getDataBox();
        $user->addData('id', 1);
        $act = 2;
        $pkg = Package::load($act, $user);
        $this->assertEquals('Action_Err', $pkg->getAct());
    }

    public function testUser()
    {
        $user = DataBox::getDataBox();
        $user->addData('id', 1);
        $act = "test";
        $pkg = Package::load($act, $user);
        $this->assertEquals(1, $pkg->getUser());
    }

    public function testData()
    {
        //測試初始化
        $user = DataBox::getDataBox();
        $user->addData('id', 1);
        $act = "test";
        $pkg = Package::load($act, $user);
        $this->assertEquals("array", gettype($pkg->getAllData()));

        //測試是否可以set和get Data
        $pkg->setData("name", "Peter");
        $this->assertEquals('Peter', $pkg->getData('name'));
        $this->assertEquals(false, $pkg->getData('test'));
        $this->assertEquals(1, count($pkg->getAllData()));

        //測試是否可以set和get All Data
        $arr = array('test' => '123');
        $pkg->setArrayData($arr);
        $this->assertEquals('123', $pkg->getData('test'));
        $this->assertEquals(false, $pkg->getData('name'));
        $this->assertEquals(1, count($pkg->getAllData()));

        //測試是否可以delete Data
        $pkg->setData("name", "Peter");
        $pkg->delData('test');
        $this->assertEquals('Peter', $pkg->getData('name'));
        $this->assertEquals(false, $pkg->getData('test'));
        $this->assertEquals(1, count($pkg->getAllData()));

        //測試是否可以delete All Data
        $arr = array('test' => '123', 'name' => 'Peter');
        $pkg->setArrayData($arr);
        $this->assertEquals('Peter', $pkg->getData('name'));
        $this->assertEquals('123', $pkg->getData('test'));
        $this->assertEquals(2, count($pkg->getAllData()));
        $pkg->delAllData();
        $this->assertEquals(0, count($pkg->getAllData()));
    }
}
