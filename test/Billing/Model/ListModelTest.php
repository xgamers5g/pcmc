<?php
namespace BillingTest\Model;

use BillingTest\DataSet;
use Billing\Model\Package;
use Billing\System\Model;
use Billing\Model\ListModel;
use Fruit\Seed;
use Fruit\Config;
use PHPUnit_Extensions_Database_TestCase;
use PDO;
use PDOStatement;
use DateTime;
use Auth\Model\DataBox;

class ListModelTest extends PHPUnit_Extensions_Database_TestCase
{
    private $id = null;
    private $obj = null;

    public function getConnection()
    {
        $conn = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }

    public function getDataSet()
    {
        return new DataSet(
            [
                'category' => [
                    [
                        'cname' => '測試支出分類',
                        'ctype' => 0,
                        'uid' => 1
                    ],
                    [
                        'cname' => '測試收入分類',
                        'ctype' => 1,
                        'uid' => 1
                    ]
                ],
                'list' => [
                    [
                        'lname' => '新增測試明細',
                        'lamount' => 2014,
                        'cid' => 1,
                        'uid' => 1,
                        'ldate' => '2014-10-22'
                    ],
                    [
                        'lname' => '修改測試明細',
                        'lamount' => 10,
                        'cid' => 1,
                        'uid' => 1
                    ],
                    [
                        'lname' => '刪除測試明細',
                        'lamount' => 10,
                        'cid' => 1,
                        'uid' => 1
                    ]
                ]
            ]
        );
    }

    public function testDataSet()
    {
        $this->assertEquals(2, $this->getConnection()->getRowCount('category'));
        $this->assertEquals(3, $this->getConnection()->getRowCount('list'));
    }

    public function testlistAll()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load('List_R', $user);
        $pkgtmp = ListModel::get($pkg);
        $this->assertEquals('List_R_Suc', $pkgtmp->getAct());
        $res = $pkgtmp->getData('List');
        $this->assertEquals(1, $res[0]['ListID']);
        $this->assertEquals('新增測試明細', $res[0]['ListName']);
        $this->assertEquals(2014, $res[0]['Amount']);
        $this->assertEquals(1, $res[0]['Category']);
        $this->assertEquals('測試支出分類', $res[1]['CategoryName']);
        $this->assertEquals(0, $res[0]['Payment']);
    }

    public function testload()
    {
        $user = DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load("List", $user);
        $lo = ListModel::load($pkg);
        $flag = ($lo instanceof ListModel);
        $this->assertEquals(true, $flag);
        $this->assertEquals('object', gettype($lo));
        // $this->assertEquals('List_Err', $pkg->getAct());
    }

    public function testcreate()
    {
        $date = new DateTime('now');
        $arr = array(
            'Category' => 1,
            'ListName' => "測試新增明細成功",
            'ListDate' => $date->format('Y-m-d H:i:s'),
            'Amount' => 2015
        );
        $user = DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load("List_C", $user);
        $pkg->setArrayData($arr);
        ListModel::create($pkg);
        $this->assertEquals('List_C_Suc', $pkg->getAct());
    }

    public function testupdate()
    {
        $date = new DateTime('now');
        $arr = array(
            'ListID' => 2,
            'Category' => 1,
            'ListName' => "測試新增明細成功",
            'ListDate' => $date->format('Y-m-d H:i:s'),
            'Amount' => 2015
        );
        $user = DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load("List_U", $user);
        $pkg->setArrayData($arr);
        $lo = ListModel::load($pkg);
        $this->assertEquals('List_U_Suc', $lo->update()->getAct());
    }

    public function testdelete()
    {
        $arr = array('ListID' => 3);
        $user = DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load("List_D", $user);
        $pkg->setArrayData($arr);
        $lo = ListModel::load($pkg);
        $this->assertEquals('List_D_Suc', $lo->delete()->getAct());
    }

    public function testhas()
    {
        $this->assertEquals(true, ListModel::has(1));
    }

    public function testsearch()
    {
        $arr = array(
            'ListName' => '明細'
        );
        $user = DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load("List_S", $user);
        $pkg->setArrayData($arr);
        $pkg = ListModel::search($pkg);
        $res = $pkg->getData('List');
        $this->assertEquals('List_S_Suc', $pkg->getAct());
        $this->assertEquals(3, count($res));
        $this->assertEquals(7, count($res[0]));
        $pkg->setData('ListName', '修改');
        $pkg->setAct('List_S');
        $pkg = ListModel::search($pkg);
        $res = $pkg->getData('List');
        $this->assertEquals('List_S_Suc', $pkg->getAct());
        $this->assertEquals(1, count($res));
        $this->assertEquals(7, count($res[0]));
    }
}
