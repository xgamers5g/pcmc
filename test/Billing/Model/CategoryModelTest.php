<?php
namespace BillingTest\Model;

use BillingTest\DataSet;
use Billing\Model\Package;
use Billing\System\Model;
use Billing\Model\CategoryModel;
use Fruit\Seed;
use Fruit\Config;
use PHPUnit_Extensions_Database_TestCase;
use PDO;
use PDOStatement;
use DateTime;

class CategoryModelTest extends PHPUnit_Extensions_Database_TestCase
{
    public function getConnection()
    {
        $conn = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }

    public function getDataSet()
    {
        return new DataSet(
            [
                'category' => [
                    [
                        'cname' => '測試支出分類',
                        'ctype' => 0,
                        'uid' => 1
                    ],
                    [
                        'cname' => '測試收入分類',
                        'ctype' => 1,
                        'uid' => 1
                    ]
                ],
                'list' => [
                        [
                            'lname' => '新增測試明細',
                            'lamount' => 2014,
                            'cid' => 2,
                            'uid' => 1,
                            'ldate' => '2014-10-22'
                        ],
                        [
                            'lname' => '修改測試明細',
                            'lamount' => 10,
                            'cid' => 1,
                            'uid' => 1
                        ],
                        [
                            'lname' => '刪除測試明細',
                            'lamount' => 10,
                            'cid' => 1,
                            'uid' => 1
                        ]
                ]
            ]
        );
    }

    public function testDataSet()
    {
        $this->assertEquals(2, $this->getConnection()->getRowCount('category'));
        $this->assertEquals(3, $this->getConnection()->getRowCount('list'));
    }
    public function testlistAll()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load('Category_R', $user);
        $res = CategoryModel::get($pkg)->getData('Category');
        $this->assertEquals(1, $res[0]['Category']);
        $this->assertEquals('測試支出分類', $res[0]['CategoryName']);
        $this->assertEquals(0, $res[0]['Payment']);
    }

    public function testload()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load('Category_R', $user);
        $co = CategoryModel::load($pkg);
        $flag = ($co instanceof CategoryModel);
        $this->assertEquals(true, $flag);
        $this->assertEquals('object', gettype($co));
    }

    public function testcreate()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $cname = "測試新增分類成功";
        $ctype = true;
        $arr = array('CategoryName' => "測試新增分類成功", 'Payment' => true);
        $pkg = Package::load('Category_C', $user);
        $pkg->setArrayData($arr);
        CategoryModel::create($pkg);
        $this->assertEquals('Category_C_Suc', $pkg->getAct());
        $pkg->setAct('Category_R');
        $res = CategoryModel::get($pkg)->getData('Category');
        $this->assertEquals(3, $res[2]['Category']);
        $this->assertEquals('測試新增分類成功', $res[2]['CategoryName']);
        $this->assertEquals(1, $res[2]['Payment']);
    }

    public function testupdate()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $cname = "測試修改分類成功";
        $ctype = false;
        $arr = array('CategoryName' => $cname, 'Payment' => $ctype, 'Category' => 1);
        $pkg = Package::load('Category_U', $user);
        $pkg->setArrayData($arr);
        $co = CategoryModel::load($pkg);
        $this->assertEquals('Category_U_Suc', $co->update()->getAct());
        $pkg->setAct('Category_R');
        $res = CategoryModel::get($pkg)->getData('Category');
        $this->assertEquals(1, $res[0]['Category']);
        $this->assertEquals("測試修改分類成功", $res[0]['CategoryName']);
        $this->assertEquals(0, $res[0]['Payment']);
    }

    public function testdelete()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $arr = array('Category' => 1);
        $pkg = Package::load('Category_D', $user);
        $pkg->setArrayData($arr);
        $co = CategoryModel::load($pkg);
        $this->assertEquals('Category_D_Suc', $co->delete()->getAct());
    }

    public function testhas()
    {
        $this->assertEquals(true, CategoryModel::has(1));
    }

    public function testsearch()
    {
        $arr = array(
            'CategoryName' => '分類'
        );
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load("Category_S", $user);
        $pkg->setArrayData($arr);
        $pkg = CategoryModel::search($pkg);
        $res = $pkg->getData('Category');
        $this->assertEquals('Category_S_Suc', $pkg->getAct());
        $this->assertEquals(3, count($res));
        $this->assertEquals(7, count($res[0]));
        $pkg->setData('CategoryName', '收入');
        $pkg->setAct('Category_S');
        $pkg = CategoryModel::search($pkg);
        $res = $pkg->getData('Category');
        $this->assertEquals('Category_S_Suc', $pkg->getAct());
        $this->assertEquals(1, count($res));
        $this->assertEquals(7, count($res[0]));
    }
}
