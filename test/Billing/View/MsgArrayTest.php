<?php
namespace BillingTest\View;

use PHPUnit_Framework_TestCase;
use Billing\View\MsgArray;

class MsgArrayTest extends PHPUnit_Framework_TestCase
{
    public function testget()
    {
        $this->assertEquals('新增明細發生錯誤', MsgArray::get('List_C_Err'));
        $this->assertEquals('不知名錯誤', MsgArray::get('123'));
    }
}
