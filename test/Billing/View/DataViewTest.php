<?php
namespace BillingTest\View;

use PHPUnit_Framework_TestCase;
use Billing\View\DataView;

class DataViewTest extends PHPUnit_Framework_TestCase
{
    public function testrender()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = \Billing\Model\Package::load('List_C_Err', $user);
        $res = DataView::render($pkg, 123);
        $this->assertEquals('{"detail":"新增明細發生錯誤","status":"List_C_Err","Authorization":123}', $res);
    }
}
