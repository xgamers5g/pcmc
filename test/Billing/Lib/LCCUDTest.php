<?php
namespace BillingTest\Model;

use BillingTest\DataSet;
use Billing\Model\Package;
use Billing\Lib\LCCUD;
use PHPUnit_Extensions_Database_TestCase;
use DateTime;

class LCCUDTest extends PHPUnit_Extensions_Database_TestCase
{
    private $id = null;
    private $obj = null;

    public function getConnection()
    {
        $conn = new \PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }

    public function getDataSet()
    {
        return new DataSet(
            [
                'category' => [
                    [
                        'cname' => '測試支出分類',
                        'ctype' => 0,
                        'uid' => 1
                    ],
                    [
                        'cname' => '測試收入分類',
                        'ctype' => 1,
                        'uid' => 1
                    ]
                ],
                'list' => [
                    [
                        'lname' => '新增測試明細',
                        'lamount' => 2014,
                        'cid' => 1,
                        'uid' => 1,
                        'ldate' => '2014-10-22'
                    ],
                    [
                        'lname' => '修改測試明細',
                        'lamount' => 10,
                        'cid' => 1,
                        'uid' => 1
                    ],
                    [
                        'lname' => '刪除測試明細',
                        'lamount' => 10,
                        'cid' => 1,
                        'uid' => 1
                    ]
                ]
            ]
        );
    }

    public function testDataSet()
    {
        $this->assertEquals(2, $this->getConnection()->getRowCount('category'));
        $this->assertEquals(3, $this->getConnection()->getRowCount('list'));
    }

    public function testload()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load("List", $user);
        $lco = LCCUD::load($pkg);
        $flag = ($lco instanceof LCCUD);
        $this->assertEquals(true, $flag);
        $this->assertEquals('object', gettype($lco));
        // $this->assertEquals('List_Err', $pkg->getAct());
    }

    public function testcreateList()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $date = new DateTime('now');
        $arr = array(
            'Category' => 1,
            'ListName' => "測試新增明細成功",
            'ListDate' => $date->format('Y-m-d H:i:s'),
            'Amount' => 2015
        );
        $pkg = Package::load("List_C", $user);
        $pkg->setArrayData($arr);
        $lco = LCCUD::load($pkg);
        $this->assertEquals('List_C_Suc', $lco->createList()->getAct());
    }

    public function testupdateList()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $date = new DateTime('now');
        $arr = array(
            'ListID' => 2,
            'Category' => 1,
            'ListName' => "測試修改明細成功",
            'ListDate' => $date->format('Y-m-d H:i:s'),
            'Amount' => 2015
        );
        $pkg = Package::load("List_U", $user);
        $pkg->setArrayData($arr);
        $lco = LCCUD::load($pkg);
        $this->assertEquals('List_U_Suc', $lco->updateList()->getAct());
    }

    public function testdeleteList()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $arr = array('ListID' => 3);
        $pkg = Package::load("List_D", $user);
        $pkg->setArrayData($arr);
        $lco = LCCUD::load($pkg);
        $this->assertEquals('List_D_Suc', $lco->deleteList()->getAct());
    }

    public function testcreateC()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $cname = "測試新增分類成功";
        $ctype = true;
        $arr = array('CategoryName' => "測試新增分類成功", 'Payment' => true);
        $pkg = Package::load('Category_C', $user);
        $pkg->setArrayData($arr);
        $this->assertEquals('Category_C_Suc', LCCUD::createCategory($pkg)->getAct());
    }

    public function testupdateC()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $cname = "測試修改分類成功";
        $ctype = false;
        $arr = array('CategoryName' => $cname, 'Payment' => $ctype, 'Category' => 1);
        $pkg = Package::load('Category_U', $user);
        $pkg->setArrayData($arr);
        $lco = LCCUD::load($pkg);
        $this->assertEquals('Category_U_Suc', $lco->updateCategory()->getAct());
    }

    public function testdeleteC()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $arr = array('Category' => 1);
        $pkg = Package::load('Category_D', $user);
        $pkg->setArrayData($arr);
        $lco = LCCUD::load($pkg);
        $this->assertEquals('Category_D_Suc', $lco->deleteCategory()->getAct());
    }

    public function testdeleteCU()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $arr = array('Category' => 1, 'otherCategory' => 2);
        $pkg = Package::load('Category_D', $user);
        $pkg->setArrayData($arr);
        $lco = LCCUD::load($pkg);
        $this->assertEquals('Category_D_Suc', $lco->deleteCategory()->getAct());
    }

    public function testhasTest()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $arr = array('Category' => 1);
        $pkg = Package::load('Category_H', $user);
        $pkg->setArrayData($arr);
        $pkg = LCCUD::hasCategory($pkg);
        $this->assertEquals('Category_H_Suc', $pkg->getAct());
        $pkg->setData('ListID', 4);
        $pkg = LCCUD::hasList($pkg);
        $this->assertEquals('List_H_Err', $pkg->getAct());
    }
    // public function testdeleteAll()
    // {
    //     $arr = array('Category' => 1);
    //     $pkg = Package::load('Category_D', 1);
    //     $pkg->setArrayData($arr);
    //     $lco = LCCUD::load($pkg);
    //     $lco->updateAllList(1, 2);
    //     $this->assertEquals('List_U_Suc', $pkg->getAct());
    //     var_dump($pkg->getAllData());
    // }
}
