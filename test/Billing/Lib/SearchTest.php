<?php
namespace BillingTest\Model;

use BillingTest\DataSet;
use Billing\Model\Package;
use Billing\Lib\Search;
use PHPUnit_Extensions_Database_TestCase;
use DateTime;

class SearchTest extends PHPUnit_Extensions_Database_TestCase
{
    private $id = null;
    private $obj = null;

    public function getConnection()
    {
        $conn = new \PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }

    public function getDataSet()
    {
        return new DataSet(
            [
                'category' => [
                    [
                        'cname' => '測試支出分類',
                        'ctype' => 0,
                        'uid' => 1
                    ],
                    [
                        'cname' => '測試收入分類',
                        'ctype' => 1,
                        'uid' => 1
                    ]
                ],
                'list' => [
                    [
                        'lname' => '新增測試明細',
                        'lamount' => 2014,
                        'cid' => 2,
                        'uid' => 1,
                        'ldate' => '2014-10-22'
                    ],
                    [
                        'lname' => '修改測試明細',
                        'lamount' => 10,
                        'cid' => 1,
                        'uid' => 1
                    ],
                    [
                        'lname' => '刪除測試明細',
                        'lamount' => 10,
                        'cid' => 1,
                        'uid' => 1
                    ]
                ]
            ]
        );
    }

    public function testDataSet()
    {
        $this->assertEquals(2, $this->getConnection()->getRowCount('category'));
        $this->assertEquals(3, $this->getConnection()->getRowCount('list'));
    }

    public function testsearchL()
    {
        $arr = array(
            'ListName' => '明細'
        );
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load("List_S", $user);
        $pkg->setArrayData($arr);
        $pkg = Search::searchList($pkg);
        $res = $pkg->getData('List');
        $this->assertEquals('List_S_Suc', $pkg->getAct());
        $this->assertEquals(3, count($res));
        $this->assertEquals(7, count($res[0]));
        $pkg->setData('ListName', '修改');
        $pkg->setAct('List_S');
        $pkg = Search::searchList($pkg);
        $res = $pkg->getData('List');
        $this->assertEquals('List_S_Suc', $pkg->getAct());
        $this->assertEquals(1, count($res));
        $this->assertEquals(7, count($res[0]));
    }

    public function testsearchC()
    {
        $arr = array(
            'CategoryName' => '分類'
        );
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load("Category_S", $user);
        $pkg->setArrayData($arr);
        $pkg = Search::searchCategory($pkg);
        $res = $pkg->getData('Category');
        $this->assertEquals('Category_S_Suc', $pkg->getAct());
        $this->assertEquals(3, count($res));
        $this->assertEquals(7, count($res[0]));
        $pkg->setData('CategoryName', '收入');
        $pkg->setAct('Category_S');
        $pkg = Search::searchCategory($pkg);
        $res = $pkg->getData('Category');
        $this->assertEquals('Category_S_Suc', $pkg->getAct());
        $this->assertEquals(1, count($res));
        $this->assertEquals(7, count($res[0]));
    }
}
