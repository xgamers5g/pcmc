<?php
namespace BillingTest\Lib;

use PHPUnit_Framework_TestCase;
use Billing\Lib\CheckInput;

class CITest extends PHPUnit_Framework_TestCase
{
    public function testcheckDate()
    {
        $date = date('Y-m-d');
        $this->assertEquals(true, CheckInput::checkDate($date));
        $date = date('Y-m-d H:i:s');
        $this->assertEquals(true, CheckInput::checkDate($date));
        $date = 'aaaa-aa-aa';
        $this->assertEquals(false, CheckInput::checkDate($date));
        $date = '2016-02-29';
        $this->assertEquals(true, CheckInput::checkDate($date));
        $date = '2014-02-29';
        $this->assertEquals(false, CheckInput::checkDate($date));
        $date = null;
        $this->assertEquals(false, CheckInput::checkDate($date));
    }

    public function testcheckBool()
    {
        $bool = true;
        $this->assertEquals(true, CheckInput::checkBool($bool));
        $bool = false;
        $this->assertEquals(true, CheckInput::checkBool($bool));
        $bool = 1;
        $this->assertEquals(false, CheckInput::checkBool($bool));
        $bool = null;
        $this->assertEquals(false, CheckInput::checkBool($bool));
    }

    public function testcheckId()
    {
        $id = 1;
        $this->assertEquals(true, CheckInput::checkId($id));
        $id = null;
        $this->assertEquals(false, CheckInput::checkId($id));
        $id = '1';
        $this->assertEquals(false, CheckInput::checkId($id));
        $id = "1";
        $this->assertEquals(false, CheckInput::checkId($id));
        $id = 'aaa';
        $this->assertEquals(false, CheckInput::checkId($id));
    }

    public function testcheckStr()
    {
        $str = '123aa';
        $this->assertEquals(true, CheckInput::checkStr($str));
        $str = '123_____aa';
        $this->assertEquals(true, CheckInput::checkStr($str));
        $str = '測試中';
        $this->assertEquals(true, CheckInput::checkStr($str));
        $str = 'aaa\\\\aaa';
        $this->assertEquals(false, CheckInput::checkStr($str));
        $str = '(aaaaaa)';
        $this->assertEquals(false, CheckInput::checkStr($str));
        $str = '(aaa測試中aaa)';
        $this->assertEquals(false, CheckInput::checkStr($str));
        $str = '(aaa測試中,aaa)';
        $this->assertEquals(false, CheckInput::checkStr($str));
    }
}
