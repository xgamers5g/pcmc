<?php
namespace BillingTest\Lib;

use BillingTest\DataSet;
use Billing\Model\Package;
use Billing\Lib\Report;
use PHPUnit_Extensions_Database_TestCase;
use DateTime;

class ReportTest extends PHPUnit_Extensions_Database_TestCase
{
    private $id = null;
    private $obj = null;

    public function getConnection()
    {
        $conn = new \PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }

    public function getDataSet()
    {
        $date = new \DateTime();
        $date->modify('-1 month');
        $date2 = new \DateTime();
        $date2->modify('+1 month');
        $date3 = new \DateTime();
        $date3->modify('+1 day');
        return new DataSet(
            [
                'category' => [
                    [
                        'cname' => '測試支出分類',
                        'ctype' => 0,
                        'uid' => 1
                    ],
                    [
                        'cname' => '測試收入分類',
                        'ctype' => 1,
                        'uid' => 1
                    ],
                    [
                        'cname' => '測試支出分類2',
                        'ctype' => 0,
                        'uid' => 1
                    ]
                ],
                'list' => [
                    [
                        'lname' => '新增測試明細',
                        'lamount' => 2014,
                        'cid' => 1,
                        'uid' => 1,
                        'ldate' => $date->format('Y-m-d H:i:s')
                    ],
                    [
                        'lname' => '修改測試明細',
                        'lamount' => 10,
                        'cid' => 1,
                        'uid' => 1,

                    ],
                    [
                        'lname' => '修改測試明細2',
                        'lamount' => 3666,
                        'cid' => 2,
                        'uid' => 1,
                        'ldate' => $date2->format('Y-m-d H:i:s')
                    ],
                    [
                        'lname' => '刪除測試明細',
                        'lamount' => 10,
                        'cid' => 3,
                        'uid' => 1,
                        'ldate' => $date3->format('Y-m-d H:i:s')
                    ]
                ]
            ]
        );
    }

    public function testDataSet()
    {
        $this->assertEquals(3, $this->getConnection()->getRowCount('category'));
        $this->assertEquals(4, $this->getConnection()->getRowCount('list'));
    }

    public function testgetList()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load('List_A', $user);
        Report::getList($pkg);
        $this->assertEquals('List_A', $pkg->getAct());
        $res = $pkg->getData('List');
        $this->assertEquals(1, $res[0]['ListID']);
        $this->assertEquals('新增測試明細', $res[0]['ListName']);
        $this->assertEquals(2014, $res[0]['Amount']);
        $this->assertEquals(1, $res[0]['Category']);
        $this->assertEquals('測試支出分類', $res[1]['CategoryName']);
        $this->assertEquals(0, $res[0]['Payment']);
    }

    public function testgetC()
    {
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load('Category_A', $user);
        $res = Report::getC($pkg)->getData('Category');
        $this->assertEquals('Category_A', $pkg->getAct());
        $this->assertEquals(1, $res[0]['Category']);
        $this->assertEquals('測試支出分類', $res[0]['CategoryName']);
        $this->assertEquals(0, $res[0]['Payment']);
    }

    public function testcheckDate()
    {
        $date = new \DateTime('now');
        $tmp = array(
            'Date_S' => $date->format('Y-m-d')
        );
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load('Report_G', $user);
        $pkg->setArrayData($tmp);
        $res = Report::checkDate($pkg, 'Month')->getAllData();
        $this->assertEquals($date->format('Y-m'), $res['Date_S']);
        $date->modify('+1 month');
        $this->assertEquals($date->format('Y-m'), $res['Date_E']);
        $this->assertEquals('Report_G', $pkg->getAct());
    }

    public function testcheckDateErr()
    {
        $tmp = array(
            'Date_S' => '2014-aa-aa'
        );
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load('Report_G', $user);
        $pkg->setArrayData($tmp);
        $res = Report::checkDate($pkg, 'Month')->getAllData();
        $this->assertEquals('Date_Input_Err', $pkg->getAct());
    }

    public function testgetForDate()
    {
        $date = new \DateTime('now');
        $tmp = array(
            'Date_S' => $date->format('Y-m-d')
        );
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load('Report_G', $user);
        $pkg->setArrayData($tmp);
        $res = Report::getForDate($pkg, 'Day')->getData('List');
        $this->assertEquals(1, count($res));
        $this->assertEquals(7, count($res[0]));
        $this->assertEquals(2, $res[0]['ListID']);
        $this->assertEquals(false, isset($res[1]));
    }

    public function testgetForYear()
    {
        $date = new \DateTime('now');
        $tmp = array(
            'Date_S' => $date->format('Y-m-d')
        );
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load('Report_G', $user);
        $pkg->setArrayData($tmp);
        $res = Report::getForDate($pkg, 'Year')->getData('List');
        $this->assertEquals(4, count($res));
        $this->assertEquals(7, count($res[0]));
        $this->assertEquals(1, $res[0]['ListID']);
    }

    public function testcountAmount()
    {
        $date = new \DateTime('now');
        $tmp = array(
            'Date_S' => $date->format('Y-m-d')
        );
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load('Report_G', $user);
        $pkg->setArrayData($tmp);
        $res = Report::countAmount($pkg, false)->getAllData();
        $this->assertEquals(10, $res['Pay']);
    }

    public function testpay()
    {
        $date = new \DateTime('now');
        $tmp = array(
            'Date_S' => $date->format('Y-m-d')
        );
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load('Report_G', $user);
        $pkg->setArrayData($tmp);
        $res = Report::pay($pkg)->getAllData();
        $this->assertEquals(10, $res['Pay']);
    }

    public function testincome()
    {
        $date = new \DateTime('now');
        $tmp = array(
            'Date_S' => $date->format('Y-m-d')
        );
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load('Report_G', $user);
        $pkg->setArrayData($tmp);
        $res = Report::income($pkg)->getAllData();
        $this->assertEquals(0, $res['Income']);
    }

    public function testTotal()
    {
        $date = new \DateTime('now');
        $tmp = array(
            'Date_S' => $date->format('Y-m-d')
        );
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load('Report_G', $user);
        $pkg->setArrayData($tmp);
        $res = Report::total($pkg)->getAllData();
        $this->assertEquals(-10, $res['Total']);
        $this->assertEquals(0, $res['Income']);
        $this->assertEquals(10, $res['Pay']);
        // var_dump($res);
    }

    public function testcountCategory()
    {
        $date = new \DateTime('now');
        $tmp = array(
            'Date_S' => $date->format('Y-m-d')
        );
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load('Report_G', $user);
        $pkg->setArrayData($tmp);
        $res = Report::countCategory($pkg, 'Year')->getAllData();
        $this->assertEquals(1632, $res['Total']);
        $this->assertEquals(2034, $res['Pay']);
        $this->assertEquals(3666, $res['Income']);
        $this->assertEquals(3, count($res['Category']));
        $this->assertEquals(4, count($res['List']));
        // var_dump($res);
    }

    public function testshowReport()
    {
        $date = new \DateTime('now');
        $tmp = array(
            'Date_S' => $date->format('Y-m-d')
            // 'Date_S' => '2014-aa-aa'
        );
        $user = \Auth\Model\DataBox::getDataBox();
        $user->addData('id', 1);
        $pkg = Package::load('Report_Show', $user);
        $pkg->setArrayData($tmp);
        $res = Report::showReport($pkg, 'Year')->getAllData();
        // var_dump($res);
        // echo $pkg->getAct();
        $this->assertEquals('Report_Show_Suc', $pkg->getAct());
        $this->assertEquals(1632, $res['Total']);
        $this->assertEquals(2034, $res['Pay']);
        $this->assertEquals(3666, $res['Income']);
        $this->assertEquals(3, count($res['Category']));
        $this->assertEquals(4, count($res['List']));
    }
}
