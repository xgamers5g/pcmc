<?php
namespace BillingTest\Lib;

use Billing\Lib\ConnectHTTP;
use BillingTest\DataSet;

class ConnectHTTPTest extends \PHPUnit_Extensions_Database_TestCase
{
    public function getConnection()
    {
        $conn = new \PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }
    public function getDataSet()
    {
        $encrypt = \Auth\System\Core::getInstance()->getEncrypt();
        return new DataSet(
            [
                'user' => [
                    [
                        'email' => 'xgamers5g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'garth',
                        'usersecret' => $encrypt->secretEncode('12345', 'Mzk5ZmFl')
                    ]
                ],
                'category' => [
                    [
                        'cname' => 'classname1',
                        'ctype' => 1,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname2',
                        'ctype' => 1,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname3',
                        'ctype' => 1,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname4',
                        'ctype' => 1,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname5',
                        'ctype' => 1,
                        'uid' => 1
                    ]
                ],
                'list' => [
                    [
                        'cid' => 4,
                        'lname' => 'louislist',
                        'lamount' => 85000,
                        'uid' => 1
                    ],
                    [
                        'cid' => 5,
                        'lname' => 'peterlist',
                        'lamount' => 9000000,
                        'uid' => 1
                    ]
                ]
            ]
        );
    }
    public function testDataSet()
    {
        $this->assertEquals(1, $this->getConnection()->getRowCount('user'));
        $this->assertEquals(5, $this->getConnection()->getRowCount('category'));
        $this->assertEquals(2, $this->getConnection()->getRowCount('list'));
    }

    public function testconnect()
    {
        $tmp = array('email' => 'xgamers5g@hotmail.com');
        $result = ConnectHTTP::connect('http://pcmctest/auth/check/email', $tmp);
        $arr = array('statuscode' => '401', 'detail' => "該電郵已經被使用，請嘗試其它電郵。");
        $this->assertEquals($arr, $result);
    }
}
