<?php
namespace BillingTest\Lib;

use Billing\Lib\WriteRecord;

class WriteRecordTest extends \PHPUnit_Framework_TestCase
{
    public function testwrite()
    {
        $tmp = array(
            'ListName' => 'test',
            'Amount' => 800,
            'ListDate' => date('Y-m-d')
        );
        $auth = array(
            'Authorization' => '123',
            'username' => 'aaa',
            'id' => 1
        );
        $pkg = \Billing\Model\Package::load('List_C_Suc', $auth);
        $pkg->setArrayData($tmp);
        $res = WriteRecord::write($pkg, $auth);
        var_dump($res);
    }
}
