<?php
namespace BillingTest\System;

use Billing\System\Model;
use Fruit\Seed;
use Fruit\Config;
use PHPUnit_Framework_TestCase;
use PDO;
use PDOStatement;

class ModelTest extends PHPUnit_Framework_TestCase
{
    public function testinit()
    {
        $type = null;
        Model::init();
        if (Model::$db instanceof PDOStatement) {
            $type = 1;
        } elseif (Model::$db instanceof PDO) {
            $type = 2;
        } else {
            $type = 3;
        }
        $this->assertEquals(2, $type);
    }
}
