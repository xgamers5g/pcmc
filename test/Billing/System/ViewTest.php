<?php
namespace BillingTest\System;

use Billing\System\View;
use PHPUnit_Framework_TestCase;

class ViewTest extends PHPUnit_Framework_TestCase
{
    public function testreturn()
    {
        $str = "123ada";
        $arr = array("test" => "123ada", "test1" => "123ada");
        $s = View::render($str);
        $this->assertEquals('"123ada"', $s);
        $a = View::render($arr);
        $this->assertEquals('{"test":"123ada","test1":"123ada"}', $a);
    }
}
