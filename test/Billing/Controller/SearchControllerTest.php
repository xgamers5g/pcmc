<?php
namespace BillingTest\Controller;

use PHPUnit_Extensions_Database_TestCase;
use AuthTest\DataSet;
use Auth\Tools\ToolCurl;
use Billing\Model\Package;
use Fruit;
use PDO;
use DateTime;

class SearchControllerTest extends PHPUnit_Extensions_Database_TestCase
{
    public function getConnection()
    {
        $conn = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }
    public function getDataSet()
    {
        $encrypt = \Auth\System\Core::getInstance()->getEncrypt();
        return new DataSet(
            [
                'user' => [
                    [
                        'email' => 'xgamers5g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'garth',
                        'usersecret' => $encrypt->secretEncode('12345', 'Mzk5ZmFl')
                    ]
                ],
                'category' => [
                    [
                        'cname' => 'classname1',
                        'ctype' => true,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname2',
                        'ctype' => true,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname3',
                        'ctype' => false,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname4',
                        'ctype' => true,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname5',
                        'ctype' => true,
                        'uid' => 1
                    ]
                ],
                'list' => [
                    [
                        'cid' => 4,
                        'lname' => 'louislist',
                        'lamount' => 85000,
                        'uid' => 1
                    ],
                    [
                        'cid' => 5,
                        'lname' => 'peterlist',
                        'lamount' => 9000000,
                        'uid' => 1
                    ],
                    [
                        'cid' => 3,
                        'lname' => 'list',
                        'lamount' => 900,
                        'uid' => 1
                    ]
                ]
            ]
        );
    }
    public function testDataSet()
    {
        $this->assertEquals(1, $this->getConnection()->getRowCount('user'));
        $this->assertEquals(5, $this->getConnection()->getRowCount('category'));
        $this->assertEquals(3, $this->getConnection()->getRowCount('list'));
    }

    public function testhasList()
    {
        $date = new DateTime();
        $logindata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $searchdata = json_encode(
            array(
                'ListName' => 'list'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($logindata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $searchdata = json_decode($searchdata, true);
        $searchdata['Authorization'] = $body['Authorization'];
        $searchdata = json_encode($searchdata);
        $tc = ToolCurl::init(SITE_URL . '/list/search');
        $res = $tc->postJSON($searchdata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        //echo $res;
        $body = json_decode($body, true);
        $this->assertEquals('List_S_Suc', $body['status']);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->PostJSON(json_encode(array('Authorization' => $token)), true, true);
    }

    public function testhasCategory()
    {
        $date = new DateTime();
        $logindata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $searchdata = json_encode(
            array(
                'CategoryName' => '3'
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($logindata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $searchdata = json_decode($searchdata, true);
        $searchdata['Authorization'] = $body['Authorization'];
        $searchdata = json_encode($searchdata);
        $tc = ToolCurl::init(SITE_URL . '/category/search');
        $res = $tc->postJSON($searchdata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals('Category_S_Suc', $body['status']);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->PostJSON(json_encode(array('Authorization' => $token)), true, true);
    }
}
