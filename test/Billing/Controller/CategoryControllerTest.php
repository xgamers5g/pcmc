<?php
namespace BillingTest\Controller;

use PHPUnit_Extensions_Database_TestCase;
use AuthTest\DataSet;
use Auth\Tools\ToolCurl;
use Fruit;
use PDO;
use DateTime;

class CategoryControllerTest extends PHPUnit_Extensions_Database_TestCase
{
    public function getConnection()
    {
        $conn = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }
    public function getDataSet()
    {
        $encrypt = \Auth\System\Core::getInstance()->getEncrypt();
        return new DataSet(
            [
                'user' => [
                    [
                        'email' => 'xgamers5g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'garth',
                        'usersecret' => $encrypt->secretEncode('12345', 'Mzk5ZmFl')
                    ]
                ],
                'category' => [
                    [
                        'cname' => 'classname1',
                        'ctype' => 1,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname2',
                        'ctype' => 1,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname3',
                        'ctype' => 1,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname4',
                        'ctype' => 1,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname5',
                        'ctype' => 1,
                        'uid' => 1
                    ]
                ],
                'list' => [
                    [
                        'cid' => 4,
                        'lname' => 'louislist',
                        'lamount' => 85000,
                        'uid' => 1
                    ],
                    [
                        'cid' => 5,
                        'lname' => 'peterlist',
                        'lamount' => 9000000,
                        'uid' => 1
                    ]
                ]
            ]
        );
    }
    public function testDataSet()
    {
        $this->assertEquals(1, $this->getConnection()->getRowCount('user'));
        $this->assertEquals(5, $this->getConnection()->getRowCount('category'));
        $this->assertEquals(2, $this->getConnection()->getRowCount('list'));
    }
    
    public function testCategorycreate()
    {
        $logindata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $categorydata = json_encode(
            array(
                'CategoryName' => 'classname3',
                'Payment' => true,
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($logindata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $categorydata = json_decode($categorydata, true);
        $categorydata['Authorization'] = $body['Authorization'];
        $categorydata = json_encode($categorydata);
        $tc = ToolCurl::init(SITE_URL . '/category/create');
        $res = $tc->postJSON($categorydata, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals('新增分類成功', $body['detail']);
        $this->assertEquals('Category_C_Suc', $body['status']);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->PostJSON(json_encode(array('Authorization' => $token)), true, true);
    }

    public function testCategoryupdate()
    {
        $logindata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $categorydata = json_encode(
            array(
                'Category' => 1,
                'CategoryName' => 'Gaaaaaah',
                'Payment' => true
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($logindata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $categorydata = json_decode($categorydata, true);
        $categorydata['Authorization'] = $body['Authorization'];
        $categorydata = json_encode($categorydata);
        $tc = ToolCurl::init(SITE_URL . '/category/update');
        $res = $tc->postJSON($categorydata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $res = ToolCurl::getHeadersFromCurlResponse($res);
        $this->assertEquals('修改分類成功', $body['detail']);
        $this->assertEquals('Category_U_Suc', $body['status']);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->PostJSON(json_encode(array('Authorization' => $token)), true, true);
    }
    
    public function testCategorydelete()
    {
        $logindata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $categorydata = json_encode(
            array(
                'Category' => 4
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($logindata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $categorydata = json_decode($categorydata, true);
        $categorydata['Authorization'] = $body['Authorization'];
        $categorydata = json_encode($categorydata);
        $tc = ToolCurl::init(SITE_URL . '/category/delete');
        $res = $tc->postJSON($categorydata, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals('刪除分類成功', $body['detail']);
        $this->assertEquals('Category_D_Suc', $body['status']);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->PostJSON(json_encode(array('Authorization' => $token)), true, true);
    }

    public function testCategorydeleteUL()
    {
        $logindata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $categorydata = json_encode(
            array(
                'Category' => 4,
                'otherCategory' => 1
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($logindata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $categorydata = json_decode($categorydata, true);
        $categorydata['Authorization'] = $body['Authorization'];
        $categorydata = json_encode($categorydata);
        $tc = ToolCurl::init(SITE_URL . '/category/delete');
        $res = $tc->postJSON($categorydata, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals('刪除分類成功', $body['detail']);
        $this->assertEquals('Category_D_Suc', $body['status']);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->PostJSON(json_encode(array('Authorization' => $token)), true, true);
    }

    public function testCategoryCreateAtNewAccount()
    {
        $logindata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );

        
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($logindata, true, true);
        //echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $categorydata['Authorization'] = $body['Authorization'];
        $categorydata = json_encode($categorydata);
        $tc = ToolCurl::init(SITE_URL . '/category/newauth');
        $res = $tc->postJSON($categorydata, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        // var_dump($body);
        $this->assertEquals('新帳號新增類別成功', $body['detail']);
        $this->assertEquals('New_Account_Suc', $body['status']);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->PostJSON(json_encode(array('Authorization' => $token)), true, true);
    }
}
