<?php
namespace BillingTest\Controller;

use PHPUnit_Extensions_Database_TestCase;
use AuthTest\DataSet;
use Auth\Tools\ToolCurl;
use Billing\Model\Package;
use Fruit;
use PDO;
use DateTime;

class ListControllerTest extends PHPUnit_Extensions_Database_TestCase
{
    public function getConnection()
    {
        $conn = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }
    public function getDataSet()
    {
        $encrypt = \Auth\System\Core::getInstance()->getEncrypt();
        return new DataSet(
            [
                'user' => [
                    [
                        'email' => 'xgamers5g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'garth',
                        'usersecret' => $encrypt->secretEncode('12345', 'Mzk5ZmFl')
                    ]
                ],
                'category' => [
                    [
                        'cname' => 'classname1',
                        'ctype' => 1,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname2',
                        'ctype' => 1,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname3',
                        'ctype' => 1,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname4',
                        'ctype' => 1,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname5',
                        'ctype' => 1,
                        'uid' => 1
                    ]
                ],
                'list' => [
                    [
                        'cid' => 4,
                        'lname' => 'louislist',
                        'lamount' => 85000,
                        'uid' => 1
                    ],
                    [
                        'cid' => 5,
                        'lname' => 'peterlist',
                        'lamount' => 9000000,
                        'uid' => 1
                    ]
                ],
                'diary' => [
                    [
                        'cday' => date("Y-m-d"),
                        'oday' => date("Y-m-d"),
                        'eday' => date("Y-m-d H:i:s"),
                        'dtext' => 'garthgood',
                        'uid' => 1,
                        'pstatus' => 1,
                        'mstatus' => 1
                    ]
                ]
            ]
        );
    }
    public function testDataSet()
    {
        $this->assertEquals(1, $this->getConnection()->getRowCount('user'));
        $this->assertEquals(5, $this->getConnection()->getRowCount('category'));
        $this->assertEquals(2, $this->getConnection()->getRowCount('list'));
        $this->assertEquals(1, $this->getConnection()->getRowCount('diary'));
    }
    
    public function testListcreate()
    {
        $logindata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $listdata = json_encode(
            array(
                'Category' => 3,
                'ListName' => 'garthlist',
                'Amount' => 8000
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($logindata, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $listdata = json_decode($listdata, true);
        $listdata['Authorization'] = $body['Authorization'];
        $listdata = json_encode($listdata);
        $tc = ToolCurl::init(SITE_URL . '/list/create');
        $res = $tc->postJSON($listdata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        // echo $res;
        $this->assertEquals('新增明細成功', $body['detail']);
        $this->assertEquals('List_C_Suc', $body['status']);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->PostJSON(json_encode(array('Authorization' => $token)), true, true);
    }
    
    public function testListupdate()
    {
        $date = new DateTime();
        $logindata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $listdata = json_encode(
            array(
                'ListID' => 1,
                'Category' => 4,
                'ListName' => '許功蓋',
                'Amount' => 950000,
                'ListDate' => $date->format('Y-m-d')
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($logindata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $listdata = json_decode($listdata, true);
        $listdata['Authorization'] = $body['Authorization'];
        $listdata = json_encode($listdata);
        $tc = ToolCurl::init(SITE_URL . '/list/update');
        $res = $tc->postJSON($listdata, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals('修改明細成功', $body['detail']);
        $this->assertEquals('List_U_Suc', $body['status']);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->PostJSON(json_encode(array('Authorization' => $token)), true, true);
    }
    
    // public function testListDelete()
    // {
    //     $date = new DateTime();
    //     $logindata = json_encode(
    //         array(
    //             'email' => 'xgamers5g@hotmail.com',
    //             'usersecret' => '12345'
    //         )
    //     );
    //     $listdata = json_encode(
    //         array(
    //             'ListID' => 2
    //         )
    //     );
    //     $tc = ToolCurl::init(SITE_URL . '/auth/login');
    //     $res = $tc->postJSON($logindata, true, true);
    //     list($header, $body) = explode("\r\n\r\n", $res, 2);
    //     $body = json_decode($body, true);
    //     $listdata = json_decode($listdata, true);
    //     $listdata['Authorization'] = $body['Authorization'];
    //     $listdata = json_encode($listdata);
    //     $tc = ToolCurl::init(SITE_URL . '/list/delete');
    //     $res = $tc->postJSON($listdata, true, true);
    //     //echo $res;
    //     list($header, $body) = explode("\r\n\r\n", $res, 2);
    //     $body = json_decode($body, true);
    //     $this->assertEquals('刪除明細成功', $body['detail']);
    //     $this->assertEquals('List_D_Suc', $body['status']);
    //     $this->assertEquals(true, array_key_exists('Authorization', $body));
    //     $token = $body['Authorization'];
    //     $tc = ToolCurl::init(SITE_URL . '/auth/logout');
    //     $res = $tc->PostJSON(json_encode(array('Authorization' => $token)), true, true);
    // }
}
