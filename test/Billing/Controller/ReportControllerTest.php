<?php
namespace BillingTest\Controller;

use PHPUnit_Extensions_Database_TestCase;
use AuthTest\DataSet;
use Auth\Tools\ToolCurl;
use Billing\Model\Package;
use Fruit;
use PDO;
use DateTime;

class ReportControllerTest extends PHPUnit_Extensions_Database_TestCase
{
    public function getConnection()
    {
        $conn = new PDO('mysql:host=127.0.0.1;dbname=pcmctest;charset=utf8', 'root', '1234');
        return $this->createDefaultDBConnection($conn);
    }
    public function getDataSet()
    {
        $encrypt = \Auth\System\Core::getInstance()->getEncrypt();
        return new DataSet(
            [
                'user' => [
                    [
                        'email' => 'xgamers5g@hotmail.com',
                        'salthash' => 'Mzk5ZmFl',
                        'username' => 'garth',
                        'usersecret' => $encrypt->secretEncode('12345', 'Mzk5ZmFl')
                    ]
                ],
                'category' => [
                    [
                        'cname' => 'classname1',
                        'ctype' => true,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname2',
                        'ctype' => true,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname3',
                        'ctype' => false,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname4',
                        'ctype' => true,
                        'uid' => 1
                    ],
                    [
                        'cname' => 'classname5',
                        'ctype' => true,
                        'uid' => 1
                    ]
                ],
                'list' => [
                    [
                        'cid' => 4,
                        'lname' => 'louislist',
                        'lamount' => 85000,
                        'uid' => 1
                    ],
                    [
                        'cid' => 5,
                        'lname' => 'peterlist',
                        'lamount' => 9000000,
                        'uid' => 1
                    ],
                    [
                        'cid' => 3,
                        'lname' => 'list',
                        'lamount' => 900,
                        'uid' => 1
                    ]
                ]
            ]
        );
    }
    public function testDataSet()
    {
        $this->assertEquals(1, $this->getConnection()->getRowCount('user'));
        $this->assertEquals(5, $this->getConnection()->getRowCount('category'));
        $this->assertEquals(3, $this->getConnection()->getRowCount('list'));
    }

    public function testhasList()
    {
        $date = new DateTime();
        $logindata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $reportdata = json_encode(
            array(
                'ListID' => 1
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($logindata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $reportdata = json_decode($reportdata, true);
        $reportdata['Authorization'] = $body['Authorization'];
        $reportdata = json_encode($reportdata);
        $tc = ToolCurl::init(SITE_URL . '/list/has');
        $res = $tc->postJSON($reportdata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals('有這個明細序號', $body['detail']);
        $this->assertEquals('List_H_Suc', $body['status']);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->PostJSON(json_encode(array('Authorization' => $token)), true, true);
    }

    public function testhasCategory()
    {
        $date = new DateTime();
        $logindata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $reportdata = json_encode(
            array(
                'Category' => 3
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($logindata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $reportdata = json_decode($reportdata, true);
        $reportdata['Authorization'] = $body['Authorization'];
        $reportdata = json_encode($reportdata);
        $tc = ToolCurl::init(SITE_URL . '/category/has');
        $res = $tc->postJSON($reportdata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals('有這個分類序號', $body['detail']);
        $this->assertEquals('Category_H_Suc', $body['status']);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->PostJSON(json_encode(array('Authorization' => $token)), true, true);
    }

    public function testshowDayReport()
    {
        $date = new DateTime();
        $logindata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $reportdata = json_encode(
            array(
                'Date_S' => $date->format('Y-m-d')
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($logindata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $reportdata = json_decode($reportdata, true);
        $reportdata['Authorization'] = $body['Authorization'];
        $reportdata = json_encode($reportdata);
        $tc = ToolCurl::init(SITE_URL . '/report/dayreport');
        $res = $tc->postJSON($reportdata, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals('Report_Show_Suc', $body['status']);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->PostJSON(json_encode(array('Authorization' => $token)), true, true);
    }

    public function testshowWeekReport()
    {
        $date = new DateTime();
        $logindata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $reportdata = json_encode(
            array(
                'Date_S' => $date->format('Y-m-d')
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($logindata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $reportdata = json_decode($reportdata, true);
        $reportdata['Authorization'] = $body['Authorization'];
        $reportdata = json_encode($reportdata);
        $tc = ToolCurl::init(SITE_URL . '/report/weekreport');
        $res = $tc->postJSON($reportdata, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals('Report_Show_Suc', $body['status']);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->PostJSON(json_encode(array('Authorization' => $token)), true, true);
    }

    public function testshowMonthReport()
    {
        $date = new DateTime();
        $logindata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $reportdata = json_encode(
            array(
                'Date_S' => $date->format('Y-m-d')
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($logindata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $reportdata = json_decode($reportdata, true);
        $reportdata['Authorization'] = $body['Authorization'];
        $reportdata = json_encode($reportdata);
        $tc = ToolCurl::init(SITE_URL . '/report/monthreport');
        $res = $tc->postJSON($reportdata, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals('Report_Show_Suc', $body['status']);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->PostJSON(json_encode(array('Authorization' => $token)), true, true);
    }

    public function testshowYearReport()
    {
        $date = new DateTime();
        $logindata = json_encode(
            array(
                'email' => 'xgamers5g@hotmail.com',
                'usersecret' => '12345'
            )
        );
        $reportdata = json_encode(
            array(
                'Date_S' => $date->format('Y-m-d')
            )
        );
        $tc = ToolCurl::init(SITE_URL . '/auth/login');
        $res = $tc->postJSON($logindata, true, true);
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $reportdata = json_decode($reportdata, true);
        $reportdata['Authorization'] = $body['Authorization'];
        $reportdata = json_encode($reportdata);
        $tc = ToolCurl::init(SITE_URL . '/report/yearreport');
        $res = $tc->postJSON($reportdata, true, true);
        // echo $res;
        list($header, $body) = explode("\r\n\r\n", $res, 2);
        $body = json_decode($body, true);
        $this->assertEquals('Report_Show_Suc', $body['status']);
        $this->assertEquals(true, array_key_exists('Authorization', $body));
        $token = $body['Authorization'];
        $tc = ToolCurl::init(SITE_URL . '/auth/logout');
        $res = $tc->PostJSON(json_encode(array('Authorization' => $token)), true, true);
    }
}
