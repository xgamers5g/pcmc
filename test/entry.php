<?php
require('init.php');
require(implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'vendor', 'autoload.php']));
Fruit\Seed::Fertilize(new Fruit\Config(dirname(__DIR__), 'test'));
Fruit\Seed::getConfig()->getDb()->exec(
    file_get_contents(Fruit\Seed::getConfig()->base() . '/database/pcmc.sql')
);
$redis = new Redis();
$redis->pconnect('/tmp/redis.sock');
$redis->select(1);
$redis->flushDB();
unset($redis);
