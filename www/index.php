<?php
ini_set("display_errors", "On");
error_reporting(E_ALL&~E_NOTICE);
use Pux\Mux;
use Pux\Executor;

//define
require('init.php');
//autoload
require(implode(DIRECTORY_SEPARATOR, [BASE_DIR, 'vendor', 'autoload.php']));
//Start Fruit Framework
Fruit\Seed::Fertilize(new Fruit\Config(dirname(__DIR__)));
//pux
$mux = require "../route/mux.php";
//echo $_SERVER['DOCUMENT_URI'];
$route = $mux->dispatch($_SERVER['DOCUMENT_URI']);
$res = Executor::execute($route);//start Pux Route
//output
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');
if (isset(json_decode($res, true)['statuscode'])) {
    http_response_code(json_decode($res, true)['statuscode']);
}
echo $res;
