<?php
/**
 * 這是c9s/Pux套件的設定檔
 *
 * 用於建造Restful的Web API
 */
require(implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'vendor', 'autoload.php']));

use Pux\Mux;

$mux = new Pux\Mux;
/**
 * 使用者管理系統的後端web api
 *
 * 本專案屬於內部模組，並不牽涉到第三方的應用。
 *
 * 在這個系統中，因為Resource Owner就是Provider Server本身，
 *
 * 故使用Client Credentials Grant模式來進行認證。
 *
 * (A) Client 向 Authorization Server 認證自己，並且發 Request 到 Token Endpoint
 *
 * (B) Authorization Server 認證 Client ，如果正確的話，核發 Access Token。
 *
 * 與前端溝通資料的方式是在header中加入access_token，user_agent，remote_addr,
 *
 * 至於client端要把access_token存放在哪裡，Provider無權也無法過問，Provider只認Header。
 *
 * database會紀錄access_token所對應到的user_agent, remote_addr以及有效時間，三者綁定成唯一鍵。
 *
 * 而token表中所紀錄到的userid也自成唯一鍵，如此便不會發生多開的情形，
 * 
 * 但是假冒ip、假冒user_agent偷取access_token的攻擊還是無法避免的。
 *
 * 只要有一項發生比對不符的情形，access_token的使用權限就會被刪除，
 *
 * 這時使用者要重新進行登錄的動作。
 *
 * 未來的更新將會有for client端的access_token，但那是另一個夢境了。
 *
 * 溝通準則:雙方用HttpHeader來溝通access_token，而用JSON來溝通資料。
 */
//===================================================================
//Auth
$mux->get('/:str', ['Auth\Controller\MainController', 'indexAction']);
$mux->get('/auth/', ['Auth\Controller\MainController', 'indexAction']);
$mux->get('/auth/:str', ['Auth\Controller\MainController', 'indexAction']);
$mux->get('/auth/:str/', ['Auth\Controller\MainController', 'indexAction']);
$mux->get('/auth/:str/:str2', ['Auth\Controller\MainController', 'indexAction']);
///成功返回200+帶access_token的header，失敗返回400+error,exception599
$mux->post('/auth/login', ['Auth\Controller\MainController', 'loginAction']);
///成功返回200，失敗返回400，exception599
$mux->post('/auth/logout', ['Auth\Controller\MainController', 'logoutAction']);
///成功返回200+帶access_token的header，失敗返回400+error,exception599
$mux->post('/auth/create', ['Auth\Controller\MainController', 'createAction']);
///成功返回200+帶access_token的header，失敗返回400+error，Token無效返回401+error，exception599
$mux->post('/auth/edit/setsecret', ['Auth\Controller\MainController', 'setsecretAction']);
///成功返回200，失敗返回400+error，Token無效返回401+error,exception599
$mux->post('/auth/edit/delete', ['Auth\Controller\MainController', 'deleteAction']);
//===================================================================
$mux->post('/auth/getuserdata', ['Auth\Controller\MainController', 'getuserdataAction']);
$mux->post('/auth/check/username', ['Auth\Controller\MainController', 'checkUserNameAction']);
$mux->post('/auth/check/usersecret', ['Auth\Controller\MainController', 'checkUserSecretAction']);
$mux->post('/auth/check/email', ['Auth\Controller\MainController', 'checkUserEmailAction']);
//===================================================================
//Friend
$mux->post('/friend/get', ['AuthFriend\Controller\AuthFriendController', 'getFriendAction']);
$mux->post('/friend/delete', ['AuthFriend\Controller\AuthFriendController', 'deleteFriendAction']);
$mux->post('/friend/add', ['AuthFriend\Controller\AuthFriendController', 'addFriendAction']);
//===================================================================
//Billing
//Category Start
$mux->post("/category/create", ["Billing\Controller\CategoryController", "create"]);
$mux->post("/category/update", ["Billing\Controller\CategoryController", "update"]);
$mux->post("/category/delete", ["Billing\Controller\CategoryController", "delete"]);
$mux->post("/category/newauth", ["Billing\Controller\CategoryController", "createAtNewAccount"]);
//Category End
//List Start
$mux->post("/list/create", ["Billing\Controller\ListController", "create"]);
$mux->post("/list/update", ["Billing\Controller\ListController", "update"]);
$mux->post("/list/delete", ["Billing\Controller\ListController", "delete"]);
//List End
//Report Start
$mux->post("/list/has", ["Billing\Controller\ReportController", "hasList"]);
$mux->post("/category/has", ["Billing\Controller\ReportController", "hasCategory"]);
$mux->post("/report/dayreport", ["Billing\Controller\ReportController", "showDayReport"]);
$mux->post("/report/monthreport", ["Billing\Controller\ReportController", "showMonthReport"]);
$mux->post("/report/weekreport", ["Billing\Controller\ReportController", "showWeekReport"]);
$mux->post("/report/yearreport", ["Billing\Controller\ReportController", "showYearReport"]);
//Report End

//Search Start
$mux->post("/list/search", ["Billing\Controller\SearchController", "searchList"]);
$mux->post("/category/search", ["Billing\Controller\SearchController", "searchCategory"]);
//Search End
//=================================================================
//Dairy
$mux->post('/diary/create', ['Diary\Controller\DiaryController', 'create']);
$mux->post('/diary/update', ['Diary\Controller\DiaryController', 'update']);
$mux->post('/diary/delete', ['Diary\Controller\DiaryController', 'delete']);
$mux->post('/diary/selectall', ['Diary\Controller\DiaryController', 'selectall']);
$mux->post('/diary/selectpublic', ['Diary\Controller\DiaryController', 'selectpublic']);
$mux->post('/diary/createmessage', ['Diary\Controller\DiaryController', 'createmessage']);
$mux->post('/diary/deletemessage', ['Diary\Controller\DiaryController', 'deletemessage']);
$mux->post('/diary/replymessage', ['Diary\Controller\DiaryController', 'replymessage']);
$mux->post('/diary/selectmessage', ['Diary\Controller\DiaryController', 'selectmessage']);
$mux->post('/diary/updateforbill', ['Diary\Controller\DiaryController', 'updateforbill']);
$mux->post('/diary/updateforbilling', ['Diary\Controller\DiaryController', 'updateforbilling']);

$mux->post('/connect', ['Billing\Controller\TestController', 'connect']);

return $mux;
