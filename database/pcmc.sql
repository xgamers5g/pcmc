-- phpMyAdmin SQL Dump
-- version 4.2.7.1deb1
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 產生時間： 2014 年 10 月 30 日 13:53
-- 伺服器版本: 5.5.37-1
-- PHP 版本： 5.6.0RC4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫： `pcmctest`
--

-- --------------------------------------------------------

--
-- 資料表結構 `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
`cid` int(11) NOT NULL,
  `cname` varchar(30) NOT NULL,
  `ctype` tinyint(1) NOT NULL,
  `uid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 資料表結構 `diary`
--

DROP TABLE IF EXISTS `diary`;
CREATE TABLE IF NOT EXISTS `diary` (
`id` int(11) NOT NULL,
  `cday` date NOT NULL,
  `oday` date NOT NULL,
  `eday` datetime NOT NULL,
  `dtext` text NOT NULL,
  `pstatus` int(1) NOT NULL,
  `mstatus` int(1) NOT NULL,
  `uid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 資料表結構 `diary_message`
--

DROP TABLE IF EXISTS `diary_message`;
CREATE TABLE IF NOT EXISTS `diary_message` (
`id` int(11) NOT NULL,
  `dmessage` text NOT NULL,
  `dmdatetime` datetime NOT NULL,
  `did` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `dmreply` text NOT NULL,
  `dmrdatetime` datetime NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 資料表結構 `list`
--

DROP TABLE IF EXISTS `list`;
CREATE TABLE IF NOT EXISTS `list` (
`lid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `lamount` int(20) NOT NULL,
  `ldate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 資料表結構 `loginlog`
--

DROP TABLE IF EXISTS `loginlog`;
CREATE TABLE IF NOT EXISTS `loginlog` (
`id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(30) NOT NULL,
  `useragent` varchar(255) NOT NULL,
  `action` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 資料表結構 `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `salthash` varchar(200) NOT NULL,
  `username` varchar(255) NOT NULL,
  `usersecret` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 資料表結構 `user_friend`
--

DROP TABLE IF EXISTS `user_friend`;
CREATE TABLE IF NOT EXISTS `user_friend` (
  `userid` int(11) NOT NULL,
  `friendid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `utoi`
--

DROP TABLE IF EXISTS `utoi`;
CREATE TABLE IF NOT EXISTS `utoi` (
  `uid` int(11) NOT NULL,
  `nsid` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`cid`);

--
-- 資料表索引 `diary`
--
ALTER TABLE `diary`
 ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `diary_message`
--
ALTER TABLE `diary_message`
 ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `list`
--
ALTER TABLE `list`
 ADD PRIMARY KEY (`lid`);

--
-- 資料表索引 `loginlog`
--
ALTER TABLE `loginlog`
 ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`), ADD UNIQUE KEY `username` (`username`);

--
-- 資料表索引 `user_friend`
--
ALTER TABLE `user_friend`
 ADD UNIQUE KEY `userid` (`userid`,`friendid`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `category`
--
ALTER TABLE `category`
MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用資料表 AUTO_INCREMENT `diary`
--
ALTER TABLE `diary`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用資料表 AUTO_INCREMENT `diary_message`
--
ALTER TABLE `diary_message`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用資料表 AUTO_INCREMENT `list`
--
ALTER TABLE `list`
MODIFY `lid` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用資料表 AUTO_INCREMENT `loginlog`
--
ALTER TABLE `loginlog`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用資料表 AUTO_INCREMENT `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
