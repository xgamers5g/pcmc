pcmc的由來

Personal Cloud Money Center 個人雲端理財中心 WebAPI

-----------------------------------------------------------------------------------
2014.11.21 17:00 改動

新增在日記中紀錄明細資料的功能。

-----------------------------------------------------------------------------------

-
修改Auth的計畫

    ☆.重構測試，並補上註解。

-----------------------------------------------------------------------------------

-
☆ pcmc個人雲端理財中心WebAPI系統簡介

    1.新的使用者必需進行帳號註冊或者登入才能使用這個系統。

    2.登入成功後系統會產生一個代表用戶身份的Authorization Token，有效時間10分鐘，並寫入Http Header傳送。

    3.進行需要驗證身份的操作時，後端會讀出Header中的Authorization Token並檢測其身份。

    意指，client端必需檢查Http Header中是否存在Authorization Token，有的話必需將之傳送給後端。

    檢測成功後，該Authorization Token即失效，系統會再自動產生一個新的Authorization並寫入json data傳送，

    以維續系統運作。

------------------------------------------------------------------

-
WebApi詳述

    Authorization token會放在Json Data底下。

    當Client作登入動作，Server端所Response的Json Data底下會多出一個

    Authorization欄位: (這個就是要傳送回後端的Authorization Token)。

    之後每進行一次需要身份認證的動作時都必需回傳這個Authroization Token，並接收新的。

    直至用戶主動登出或是Authorization Token過期為止。

-
登入模組

1.登入：[POST]http://SERVERDOMAIN/auth/login

    傳入array

            ('email' => 用戶電郵, 'usersecret' => 用戶密碼)

    回傳array
            
            ('statuscode' => http狀態碼, 'detail' => 狀態細節, 'Authorization' => 維持登入狀態的Autorization Token)

2.登出：[POST]http://SERVERDOMAIN/auth/logout

    傳入array
            
            ('Authorization' => 維持登入狀態的Authorization Token)

    回傳array
            
            ('statuscode' => http狀態碼, 'detail' => 狀態細節)

3.註冊：[POST]http://SERVERDOMAIN/auth/create

    傳入array

            ('email' => 用戶電郵, 'username' => 用戶帳號, 'usersecret' => 用戶密碼)

    回傳array

            ('statuscode' => http狀態碼, 'detail' => 狀態細節)

4.改密碼：[POST]http://SERVERDOMAIN/auth/edit/setsecret

    傳入array
            
            ('usersecret' => 用戶新密碼, 'Authorization' => 維持登入狀態的Authorization Token)

    回傳array

            ('statuscode' => http狀態碼, 'detail' => 狀態細節, 'Authorization' => 維持登入狀態的Autorization Token)

5.砍帳號：[POST]http://SERVERDOMAIN/auth/edit/delete

    傳入array
            
            ('Authorization' => 維持登入狀態的Authorization Token)

    回傳array

            ('statuscode' => http狀態碼, 'detail' => 狀態細節)

6.取得用戶資料：[POST]http://SERVERDOMAIN/auth/getuserdata

    傳入array

            ('Authorization' => 維持登入狀態的Authorization Token)

    回傳array

            ('id' => 使用者的id, 'email' => 使用者的電郵, 'username' => 使用者的帳號, 'statuscode' => http狀態碼, 'detail' => 狀態細節, 'Authorization' => 維持登入狀態的Autorization Token)

7.驗證用戶名：[POST]http://SERVERDOMAIN/auth/check/username

規則：A-Z,a-z,0-9，至少要有5個字元，用戶名不能重覆。

    傳入array

            ('username' => 需要檢測的使用者名稱)

    回傳array

            ('statuscode' => http狀態碼, 'detail' => 狀態細節)

8.驗證用戶密碼：[POST]http://SERVERDOMAIN/auth/check/usersecret

規則：A-Z,a-z,0-9，至少要有5個字元。

    傳入array

            ('usersecret' => 需要檢測的使用者密碼)

    回傳array

            ('statuscode' => http狀態碼, 'detail' => 狀態細節)

9.驗證電郵：[POST]http://SERVERDOMAIN/auth/check/email

規則：它要是一個email，不能重覆。

    傳入array

            ('email' => 需要檢測的電郵地址)

    回傳array

            ('statuscode' => http狀態碼, 'detail' => 狀態細節)
---------------------------------------------------------
好友模組

1.加好友：[POST]http://SERVERDOMAIN/friend/add

    傳入array

            ('Authorization' => 維持登入狀態的Autorization Token, 'fid' => 好友id)

    回傳array
            
            ('statuscode' => http狀態碼, 'detail' => 狀態細節, 'Authorization' => 維持登入狀態的Autorization Token)

2.取得好友名單：[POST]http://SERVERDOMAIN/friend/get

    傳入array

            ('Authorization' => 用戶登入令牌)

    回傳array
            
            ('statuscode' => http狀態碼, 'detail' => 狀態細節, 'Authorization' => 維持登入狀態的Autorization Token, 'friendlist' => 好友名單(陣列))

3.刪除好友：[POST]http://SERVERDOMAIN/friend/delete

    傳入array

            ('Authorization' => 維持登入狀態的Autorization Token, 'fid' => 好友id)

    回傳array
            
            ('statuscode' => http狀態碼, 'detail' => 狀態細節, 'Authorization' => 維持登入狀態的Autorization Token)

---------------------------------------------------------
理財模組

1.新增明細(收支表): http://SERVERDOMAIN/list/create

傳入array('Category' => 使用者輸入的分類序號, 'ListName' => 使用者輸入的明細名稱, 'Amount' => 使用者輸入的金額, 
'ListDate' => 時間,格式為'Y-m-d', 'Authorization' => 維持登入狀態的Authorization Token)

回傳array('detail' => 新增明細成功, 'status' => 狀態碼, 'Authorization' => 維持登入狀態的Autorization Token)

2.修改明細(收支表): http://SERVERDOMAIN/list/update

傳入array('ListID' => 明細序號, 'Category' => 使用者輸入的分類序號, 'ListName' => 使用者輸入的明細名稱,  
'Amount' => 使用者輸入的金額, 'ListDate' => 時間,格式為'Y-m-d', 
'Authorization' => 維持登入狀態的Authorization Token)

回傳array('detail' => 修改明細成功, 'status' => 狀態碼, 'Authorization' => 維持登入狀態的Autorization Token)

3.刪除明細(收支表): http://SERVERDOMAIN/list/delete

傳入array('ListID' => 明細序號, 'Authorization' => 維持登入狀態的Authorization Token)

回傳array('detail' => 修改明細成功, 'status' => 狀態碼, 'Authorization' => 維持登入狀態的Autorization Token)

4.新增分類: http://SERVERDOMAIN/category/create

傳入array('CategoryName' => 使用者輸入的分類名稱, 'Payment' => 收入或支出狀態(收入: true, 支出:false), 
'Authorization' => 維持登入狀態的Authorization Token)

回傳array('detail' => 新增分類成功, 'status' => 狀態碼, 'Authorization' => 維持登入狀態的Autorization Token)

5.修改分類: http://SERVERDOMAIN/category/update

傳入array('Category' => 使用者輸入的分類序號, 'CategoryName' => 使用者輸入的分類名稱, 
'Payment' => 收入或支出狀態(收入: true, 支出:false), 'Authorization' => 維持登入狀態的Authorization Token)

回傳array('detail' => 修改分類成功, 'status' => 狀態碼, 'Authorization' => 維持登入狀態的Autorization Token)

6.刪除分類: http://SERVERDOMAIN/category/delete

傳入array('Category' => 使用者輸入的分類序號, 'Authorization' => 維持登入狀態的Authorization Token)

回傳array('detail' => 狀態細節, 'status' => 狀態碼, 'Authorization' => 維持登入狀態的Autorization Token)

7.印出日報表: http://SERVERDOMAIN/report/dayreport

傳入array('Date_S' => 期間開始的日期，格式為'Y-m-d', 'Date_E' => 期間結束的日期，格式為'Y-m-d', 
'Authorization' => 維持登入狀態的Autorization Token)

回傳array('detail' => array(

                                'Category' => 分類的array,

                                'Income' => 使用者輸入期間的明細收入統計,

                                'Pay' => 使用者輸入期間的明細支出統計,

                                'Total' => 使用者輸入期間的明細淨統計,

                                'List' => 日明細的array),
'status' => 狀態碼,
'Authorization' => 維持登入狀態的Autorization Token)

8.印出月報表: http://SERVERDOMAIN/report/monthreport

傳入array('Date_S' => 期間開始的日期，格式為'Y-m-d',
'Authorization' => 維持登入狀態的Autorization Token)

回傳array('detail' => array(

                                'Category' => 分類的array,

                                'Income' => 使用者輸入月份的明細收入統計,

                                'Pay' => 使用者輸入月份的明細支出統計,

                                'Total' => 使用者輸入月份的明細淨統計,

                                'List' => 月明細的array),
'status' => 狀態碼,
'Authorization' => 維持登入狀態的Autorization Token)

9.印出週報表: http://SERVERDOMAIN/report/weekreport

傳入array('Date_S' => 期間開始的日期，格式為'Y-m-d',
'Authorization' => 維持登入狀態的Autorization Token)

回傳array('detail' => array(

                                'Category' => 分類的array,

                                'Income' => 使用者輸入季的明細收入統計,

                                'Pay' => 使用者輸入季的明細支出統計,

                                'Total' => 使用者輸入季的明細淨統計,

                                'List' => 週明細的array ),
'status' => 狀態碼,
'Authorization' => 維持登入狀態的Autorization Token)

10.印出年報表: http://SERVERDOMAIN/report/yearreport

傳入array('Date_S' => 期間開始的日期，格式為'Y-m-d',
'Authorization' => 維持登入狀態的Autorization Token)

回傳array('detail' => array(

                                'Category' => 分類的array,

                                'Income' => 使用者輸入年的明細收入統計,

                                'Pay' => 使用者輸入年的明細支出統計,

                                'Total' => 使用者輸入年的明細淨統計,

                                'List' => 年明細的array ),
'status' => 狀態碼,
'Authorization' => 維持登入狀態的Autorization Token)

11.驗證是否有明細資料: http://SERVERDOMAIN/list/has

傳入array('ListID' => 明細序號, 'Autorization' => 維持登入狀態的Autorization Token);

回傳array('detail' => 驗證資訊, 'status' => 狀態碼, 'Autorization' => 維持登入狀態的Autorization Token);

12.驗證是否有明細資料: http://SERVERDOMAIN/category/has

傳入array('Category' => 明細序號, 'Autorization' => 維持登入狀態的Autorization Token);

回傳array('detail' => 驗證資訊, 'status' => 狀態碼, 'Autorization' => 維持登入狀態的Autorization Token);

13.搜尋明細名稱來取得資料: http://SERVERDOMAIN/list/search

傳入array('ListName' => 明細名稱, 'Autorization' => 維持登入狀態的Autorization Token);

回傳array('detail' => array( 'List' => 明細的array ), 
'status' => 狀態碼, 
'Autorization' => 維持登入狀態的Autorization Token);

13.搜尋明細名稱來取得資料: http://SERVERDOMAIN/category/search

傳入array('CategoryName' => 分類名稱, 'Autorization' => 維持登入狀態的Autorization Token);

回傳array('detail' => array( 'Category' => 分類的array ), 
'status' => 狀態碼, 
'Autorization' => 維持登入狀態的Autorization Token);

14.在新增帳號時，新增預設類別： http://SERVERDOMAIN/category/newauth

傳入array('Autorization' => 維持登入狀態的Autorization Token);

回傳array('detail' => '新帳號新增類別成功', 
'status' => 狀態碼, 
'Autorization' => 維持登入狀態的Autorization Token);

預設支出類別：
    早餐 晚餐 零食 日常用品 社交 午餐 飲料 交通 娛樂 衣物 購物 禮物 醫療 投資 轉帳 房租 禮金 電話費 信用卡 其他

預設收入分類：
    薪水 獎金 補助費 投資 其他
-------------------------------------------------------------------

-
系統設計圖

    /mindmap/

------------------------------------------------------------

-
Fruit framework設定檔

    /config/config.php

    /config/test.php  //測試用

-------------------------------------------------------------

-
系統底層類別

    /src/Auth/System/Model.php   

    (Model的父類主目的是開啟資料連線)

-------------------------------------------------------------

-
Model層

    /src/Auth/Model/AuthModel.php
    /src/Auth/Model/AuthValidator.php
    /src/Auth/Model/Response.php
    /src/Auth/Model/TokenMaker.php
    /src/Auth/Model/DataBox.php
    /src/Auth/Model/ElementValidator.php  

--------------------------------------------------------------

-
Controller層

    /src/Auth/Controller/MainController.php

---------------------------------------------------------------

-
View層

    /src/Auth/View/ViewRender.php

----------------------------------------------------------------

-
Tools自訂工具組

    /src/Auth/Tools/ToolSession.php
    /src/Auth/Tools/ToolCurl.php
    /src/Auth/Tools/ToolPostHandler.php
    /src/Auth/Tools/ToolGetUserstatuscode.php

-----------------------------------------------------------------

-
Router

    /route/route.php

------------------------------------------------------------------

-
資料庫架構檔

    /database/pcmc.sql

-------------------------------------------------------------------

-
測試程式

    /test/

--------------------------------------------------------------------

-
nginx環境設定：

    1.composer update，make pux，建立名為pcmc的Database，匯入pcmc.sql。

    2.建立web server，把root指向專案的/www底下，nginx的設定範例如下：

    server {

        server_name YOUR_SERVER_DOMAIN;

        listen 80;

        listen 443 ssl;

        ssl on;

        ssl_certificate YOUR_PUBLIC_KEY;

        ssl_certificate_key YOUR_PRIVATE_KEY;

        root YOUR_SITE_ROOT/www;

        location / {

            try_files $uri @php;

        }

        location @php {

            fastcgi_split_path_info ^(.+)(\?/.+)$;

            fastcgi_pass 127.0.0.1:9000;

            include fastcgi_params;

            fastcgi_param SCRIPT_FILENAME $document_root/index.php;

        }

    }

----------------------------------------------------------

                                                                                              AuthModule 2014.10.28 Garth_Wang


--------------------------------
建立簡單的Model(只有單筆資料的增刪改查功能)，在使用這些Model來建構Controller與Lib

====================================================
Billing:

1.src/Billing/System

    a. Model.php (完成測試與註解)

    b. Controller.php (完成測試與註解)

    c. View.php (完成測試與註解)

2.src/Billing/Model

    a. ListModel.php (完成測試與註解)

    b. CategoryModel.php (完成測試與註解)

    c. LCModel.php (完成測試與註解)

3.src/Billing/Conroller

    a. ListController.php (完成註解)

    b. CategoryController.php (完成註解)

4.src/Billing/View

    a. ErrorView.php (完成測試與註解)

    b. CorrectView.php (完成測試與註解)

5.src/Billing/Lib

    a. CheckInput.php (完成測試與註解)

    b. LCVerify.php (完成測試與註解)

    c. Tools.php (完成測試與註解)

====================================================

BillingTest:

1.test/BillingTest/System

    a. ModelTest.php

    b. ViewTest.php

2.test/BillingTest/Model

    a. ListModelTest.php

    b. CategoryModelTest.php

3.test/BillingTest/Controller

4.test/BillingTest/View

    a. ErrVTest.php

    b. CVTest.php

5.test/BillingTest/Lib

    a. CheckITest.php

    b. LCVerifyTest.php

    c. ToolsTest.php

===================================================

Config:

1.route/

    route.php (完成List和Category的路徑)

===================================================

Q1:如何驗證使用者?

    1.偽裝使用者登入資訊

    2.偽裝登入驗證

    3.直接繞過驗證使用此系統

Q2:如何確認輸入資料的正確性?

    各種injection
        ex:在輸入欄位中輸入js或其他語言的語法

==================================================
此架構的架構問題

缺點：

1.程式撰寫上變得麻煩:

    增、刪、改：這三個功能都比較沒有問題，除了分類的刪除功能，因為分類刪除會有兩種情況；情況一，使用者想保留原本明細並且刪除分類。情況二，使用者刪除分類的同時也刪除明細。
    查：使用許多的迴圈與判斷式，導致程式上比較複雜。
    Model功能太單一，造成許多查詢都要讓Lib去判斷應該要取出什資料，再由Model去取出來；這樣搜尋資料庫的次數沒有減少，反而還增加了。應該要把Model的功能在重新設計一次，讓Model功能比較不會太過單一，導致要一直去資料庫取資料

2.在資料表關聯部份複雜，需要從各個Model將資料取出來，再來做比對。

3.驗證機制跟計算綁在一起，導致日後在修改時程式可讀性降低。

4.修改傳輸模式(使用物件)

優點：

1.不用下複雜的sql語法，只需要簡單的INSERT、DELETE、UPDATE、SELECT。

2.比較像OOP(?)，皆為使用物件控制

====================================================

Diary

日記功能說明

    這個日記能讓您建立當天及七天前的日記,
    
    您可以修改日記,但為了避免您用腦過度,
    系統僅提供您修改當天或七天前的日記內容,

	您可以刪除日記,當您選擇刪除日記時,
	系統會詢問您是否確定刪除所選日記(原則上我們不建議您使用這個功能),

	最後您當然可以看到您所寫的日記.

	**2014.11 新增日記公開設定及留言功能**

	您可以選擇是否公開單篇日記,且是否允許留言,

	留言者可選擇該留言為公開留言或私密留言,

	若您接受留言,則您可以回覆該留言,

	當然您也可以刪除留言,並查看留言.


日記模組程式架構圖

	2014.11 架構更改為 Diary_V2
	mindmap/Diary/Diary_V2.xmind
	舊架構:
	mindmap/Diary/Diary.xmind
	

程式架構說明

1.src/Diary/Controller

	DiaryController.php

2.src/Diary/Lib

	CheckInput.php
	JsonCode.php
	StatusCode.php

3.src/Diary/Model

	DataCheck.php
	DiaryAssembly.php
	DiaryModel.php
	MessageModel.php

4.src/Diary/System

	Controller.php
	Model.php


測試

1.test/Diary/Controller

	DiaryControllerTest.php

2.test/Diary/Lib

	CheckInputTest.php

3.test/Diary/Model

	DataCheckTest.php
	DiaryAssemblyTest.php
	DiaryModelTest.php
	MessageModelTest.php

